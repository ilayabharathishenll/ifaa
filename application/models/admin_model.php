<?php 
Class admin_model extends CI_Model {
	
      Public function __construct() { 
         parent::__construct(); 
         $this->load->library('session');
      } 
	
       public function login($username,$password){
           
        //echo $username;
        //echo $password;
        //$role = $this->input->post('usertype');
        $this->db->where('user_email', $username);
        $this->db->where('user_password', $password);
         $query = $this->db->get('dms_user');
         //print_r($query); die;
       
        
        // $query = $this->db->get_where('tbladmin',array('admin_username'=>$username,'admin_password'=>$password))->result_array();
         //$query = $this->db->get("select admin_username,admin_password,admin_email_id from tbladmin where admin_username='$username' and admin_password='$password'")->result_array();
        if($query->num_rows == 1)
        { 
            $row = $query->row();
            //print_r($row); die;
            $roleId = $row->user_role;
  
            $res =    $this->db->get_where('dms_role',array('role_id'=>$roleId))->row_array();
          //echo '<pre>';   print_r($res); die;
            //$rolename = $res['role_name'];
            
                $permission = explode(",", $res['role_permission']);
            
           
            $userSessiondata = array(
                                    'user_id' => $row->user_id,
                                    'user_name' => $row->user_name,
                                    'user_email' => $row->user_email,
                                    'user_role' => $row->user_role,
                                    'user_permission' => $permission,
                                    'login'=>TRUE
                                    );
            
            $this->session->set_userdata('login',$userSessiondata);
            return true;
        }
        return false;
    }
    
    
    function fetchCategoryTree($parent = 0, $spacing = '', $user_tree_array = '') {

        if (!is_array($user_tree_array))
          $user_tree_array = array();

        $sql = "SELECT `document_temp_id`, `document_temp_name`, `document_temp_parent` FROM `dms_document_template` WHERE 1 AND `document_temp_parent` = $parent ORDER BY document_order_id ASC";
        //die;
        $query = mysql_query($sql);
        //print_r(mysql_fetch_object($query)); die;
        if (mysql_num_rows($query) > 0) {
          while ($row = mysql_fetch_object($query)) {
            $user_tree_array[] = array("document_temp_id" => $row->document_temp_id, "document_temp_name" => $spacing . $row->document_temp_name);
            $user_tree_array = $this->fetchCategoryTree($row->document_temp_id, $spacing . '&nbsp; &nbsp; &nbsp; &nbsp;', $user_tree_array);
          }
        }
        //print_r($user_tree_array); die;
        return $user_tree_array;
    }
    
    function skip_temp($grant = 0, $doc = '')
    {
        $docu = $this->db->get_where("dms_skip_temp", array('grant_temp_id' => $grant, 'document_temp_id' => $doc))->result_array();
        
        if($docu)
        {
            return "false";    
        }
        else
        {
            return "true";
        } 
        
    }
    
    function parent_Status($grant = 0, $doc = '',$temp = '')
    {//$temp="edrfe";
      // return $temp;die;
//         return "true";
//         die;
        $docu = $this->db->get_where("dms_document_template", array('document_temp_parent' => $doc))->result_array();
      // print_r($docu);die;
        $parent = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $grant, 'document_temp_id' => $doc))->row_array();
       // print_r($parent);die;
        if($parent)
        {
            
            if($temp=='5'){
                return "true";  
            }else{
            $par = $parent['parent_temp_id'];

                $sqle = "SELECT * FROM `dms_metadata` WHERE `template_id` = $par AND `document_temp_id` = '$doc' AND  `grant_temp_id` = $grant";
                $querye = $this->db->query($sqle)->result_array() ;
                if($querye)
                {

                  
             return "true";    
                }
                else
                { //echo "retr";die; 
                    return "false";
                }
            }
        }
        else
        {
            return "false";
            
        }
        
          
    }
    
    
    function statusApproval_Status($workflow = 0, $parent = '',$userId = '')
    { //echo 'hhhh'; die;
        $approval = $this->db->get_where("dms_assin_template", array('document_temp_id' => $parent['document_temp_id'], 'work_flow' => 'disable'))->row_array();
        //print_r($workflow); die;
        if($approval)
        {
            foreach($workflow as $value)
            {
               $value['user_id'];
               $value['role_id'];
               $value['step'];

               $data1 = array('parent_temp_id' => $parent['parent_temp_id'],
                   'grant_temp_id' => $parent['grant_temp_id'],
                   'document_temp_id' => $parent['document_temp_id'],
                   'role_id' => $value['role_id'],
                   'user_id' => $value['user_id'],
                   'temp_approve_status' => "Approved",
                   'approval_sender' => $userId,
                   'temp_approve_step' => $value['step'],
                   );
                   //print_r($data1);

                $result = $this->db->insert('dms_temp_approve', $data1);

            }
            
            $this->db->where('parent_temp_id', $parent['parent_temp_id']);
            $this->db->update('dms_parent_temp', array('parent_approval' => 'Approved', 'parent_status' => 'Approved'));
        }
        else
        {
            
            foreach($workflow as $value)
            {
               $value['user_id'];
               $value['role_id'];
               $value['step'];

               $data1 = array('parent_temp_id' => $parent['parent_temp_id'],
                   'grant_temp_id' => $parent['grant_temp_id'],
                   'document_temp_id' => $parent['document_temp_id'],
                   'role_id' => $value['role_id'],
                   'user_id' => $value['user_id'],
                   'temp_approve_status' => "Pending",
                   'approval_sender' => $userId,
                   'temp_approve_step' => $value['step'],
                   );
                   //print_r($data1);

                $result = $this->db->insert('dms_temp_approve', $data1);

            }
            $parent = $parent['parent_temp_id'];
            $grant = $parent['grant_temp_id'];
            $doc = $parent['document_temp_id'];
            $steps = $this->db->get_where("dms_temp_approve", array('parent_temp_id' => $parent['parent_temp_id'], 'grant_temp_id' => $parent['grant_temp_id'], 'document_temp_id' => $parent['document_temp_id'], 'temp_approve_step' => '1'))->row_array();
             $user = $this->db->get_where("dms_user", array('user_id' => $steps['user_id']))->row_array();
            $user_email = $user['user_email'];
            $this->load->library('email', $config);  // mail to lss
            $this->email->from('dms@erp1.in', 'DMS');
            // $this->email->to($email);
            $this->email->to($user_email);
            $body = 'You have a notification for Approval.
                    parent_temp_id:' . $parent . '
                    grant_temp_id:' . $grant . '
                        document_temp_id:' . $doc . '
                Please approve given Document';
                   // <a href="http://www.crm.mauzoo.com/client/confirm/' . $insert . '">Confirm</a>';
            $this->email->subject('Approval');
            $this->email->message($body);
            //$path = './user/images/';
            // $this->email->attach($path.$img);
            $email_success = $this->email->send();
        }
    }
    
    function Approval_mail($user = 0, $parent)
    {
        $parent = $parent['parent_temp_id'];
        $grant = $parent['grant_temp_id'];
        $doc = $parent['document_temp_id'];
        $user = $this->db->get_where("dms_user", array('user_id' => $steps['user_id']))->row_array();
            $user_email = $user['user_email'];
            $this->load->library('email', $config);  // mail to lss
            $this->email->from('dms@erp1.in', 'DMS');
            // $this->email->to($email);
            $this->email->to($user_email);
            $body = 'You have a notification for Approval.
                    parent_temp_id:' . $parent . '
                    grant_temp_id:' . $grant . '
                        document_temp_id:' . $doc . '
                Please approve given Document';
                   // <a href="http://www.crm.mauzoo.com/client/confirm/' . $insert . '">Confirm</a>';
            $this->email->subject('Approval');
            $this->email->message($body);
            //$path = './user/images/';
            // $this->email->attach($path.$img);
            $email_success = $this->email->send();
    }
    
    
    function terminate_Status($workflow = 0, $parent = '',$userId = '')
    {
        $approval = $this->db->get_where("dms_assin_template", array('document_temp_id' => $parent['document_temp_id'], 'work_flow' => 'disable'))->row_array();
        
        if($approval)
        {
            foreach($workflow as $value)
            {
               $value['user_id'];
               $value['role_id'];
               $value['step'];

               $data1 = array('parent_temp_id' => $parent['parent_temp_id'],
                   'grant_temp_id' => $parent['grant_temp_id'],
                   'document_temp_id' => $parent['document_temp_id'],
                   'role_id' => $value['role_id'],
                   'user_id' => $value['user_id'],
                   'temp_approve_status' => "Approved",
                   'approval_sender' => $userId,
                   'temp_approve_step' => $value['step'],
                   );
                   //print_r($data1);

                $result = $this->db->insert('dms_temp_approve', $data1);

            }
            
            $this->db->where('parent_temp_id', $parent['parent_temp_id']);
            $this->db->update('dms_parent_temp', array('parent_approval' => 'Approved', 'parent_status' => 'Approved'));
        }
        else
        {
            foreach($workflow as $value)
            {
               $value['user_id'];
               $value['role_id'];
               $value['step'];

               $data1 = array('parent_temp_id' => $parent['parent_temp_id'],
                   'grant_temp_id' => $parent['grant_temp_id'],
                   'document_temp_id' => $parent['document_temp_id'],
                   'role_id' => $value['role_id'],
                   'user_id' => $value['user_id'],
                   'temp_approve_status' => "Pending",
                   'approval_sender' => $userId,
                   'temp_approve_step' => $value['step'],
                   );
                   //print_r($data1);

                $result = $this->db->insert('dms_temp_approve', $data1);

            }
        }
    }
    
    public function grant_serch($cat = '', $year = '',$key = '',$status = '', $language = '',$region = '', $art = '',$grantee_name = '')
    {
       // echo $status;
        if($language)
        { 
            
             //$this->db->where("FIND_IN_SET('$language', grantee_language) OR grantee_language='$language'");
             $this->db->where("FIND_IN_SET('$language', grantee_language)");
//             $data = $this->db->get('dms_grantee')->result_array();
//             print_r($data); die;
        }
        if($region)
        {
             $this->db->where("FIND_IN_SET('$region', grantee_geogrtaphical_area)");
               // $this->db->where('grantee_geogrtaphical_area', $region);
        }
        if($status)
        {
             $this->db->where('categorytype_id', $status);
        }
        if($art)
        {
            $where = "FIND_IN_SET('".$art."', grantee_disciplinary_field)";
                $this->db->where($where);
        }
        if($grantee_name) 
        {          // $this->db->like('grantee_name',$grantee_name);
                $this->db->where('grantee_name', $grantee_name);
        }
//        if($language OR $region OR $art OR $grantee_name)
//        {
//            $data = $this->db->get('dms_grantee')->result_array();
//            if($data)
//            {
//                foreach($data as $value)
//                {
//                    $gid[] = $value['grantee_id'];
//                }
//                //print_r($gid);die;
//                $this->db->where_in('grantee_name', $gid);
//            }
//            else
//            {
//               return false; 
//            }
//            
//            
//        }
    
        if($cat)
        {
            $this->db->where('category_id', $cat);
        }
        if($year)
        {
            $this->db->where('category_year', $year);
        }
        if($key)
        {
            $this->db->where('keyword', $key);
        }
//        if($grantee_name)
//        {
//            echo $grantee_name; die;
//            $this->db->where('grantee_name', $grantee_name);
//        }
       $aa= $this->db->get('dms_grant')->result_array(); 
     // echo $this->db->last_query();
      // echo "<pre>";print_r($aa);die;
      return $aa;
    }
    
    public function dropdown_state()
    {
        $data = $this->db->get("dms_state")->result_array();
        if($data){
        ?>
            <select class="form-control lstFruits" multiple="multiple" name="state[]">
                    <?php foreach ($data as $value) { ?>
                    <option <?php if($result1['grantee_state'] == $value['state_id']){ echo "selected" ; } ?> value="<?php echo $value['state_id']; ?>"><?php echo $value['state_name'] ?></option>
                    <?php } ?>
            </select>
<?php
        }
    }
    
    public function dropdown_deliverables()
    {
        $data = $this->db->get("dms_deliverables")->result_array();
        if($data){
        ?>
            <select class="form-control lstFruits" id="deliverables" multiple="multiple" name="deliverables[]">
                <?php foreach ($data as $value) { ?>
                <option <?php if($result1['deliverables'] == $value['deliverables_id']){ echo "selected" ; } ?> value="<?php echo $value['deliverables_id']; ?>"><?php echo $value['deliverables_name'] ?></option>
                <?php } ?>
            </select>
<?php
        }
    }
    
    public function dropdown_language()
    {
        $data = $this->db->get("dms_language")->result_array();
        if($data){
        ?>
        <select id="lstFruits" class="lstFruits" multiple="multiple" name="language[]">
            <?php foreach ($data as $value) { ?>
            <option value="<?php echo $value['language_id']; ?>"><?php echo $value['language_name']; ?></option>
            <?php } ?>
        </select>
<?php
        }
    }
    
    public function dropdown_outcome()
    {
        $data = $this->db->get("dms_outcome")->result_array();
        if($data){
        ?>
        <select class="form-control lstFruits" multiple="multiple" id="outcome" name="grantee_possible_outcome[]">
            <?php foreach($data as $valu) { ?>
            <option <?php if($result1['grantee_possible_outcome'] == $valu['outcome_id']){ echo "selected" ; } ?> value="<?php echo $valu['outcome_id']; ?>"><?php echo $valu['outcome_name']; ?></option>
            <?php } ?>

        </select>
<?php
        }
    }
    
    
    public function dropdown_disiplinary()
    {
        $data = $this->db->get("dms_disciplinary_field")->result_array();
//        
?>
        <select class="form-control lstFruits" id="selectdisplinary" multiple="multiple" name="disciplinary_field[]">
            <?php foreach($data as $valu) { ?>
            <option <?php if($result1['grantee_disciplinary_field'] == $valu['field_id']){ echo "selected" ; } ?> value="<?php echo $valu['field_id']; ?>"><?php echo $valu['field_name']; ?></option>
            <?php } ?>

        </select>
<?php
    }
    
    public function add_parent_img($id, $scan, $detail,  $indNum = '')
    {
       // print_r($detail); die; 
        //echo $scan['name']['0'];
        
        if ($scan['name']) {
            foreach ($scan['name'] as $key => $value) {
                //echo $scan['name'][$key];
                $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                $file_name = $random . $scan['name'][$key];
                $file_size = $scan['size'][$key];
                $file_tmp = $scan['tmp_name'][$key];
                $file_type = $scan['type'][$key];
                $file_ext = strtolower(end(explode('.', $scan['name'][$key])));
                $expensions = array("gif", "jpg", "jpeg", "png", "mp4", "mp3", "pdf", "pdf", "tiff", "swf", "psd", "bmp", "jpc", "aiff", "wbmp", "xbm");
                if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                }
                move_uploaded_file($file_tmp, "local_path/" . $file_name);
                //$copy_img = $file_name;
                if($file_ext){
                    move_uploaded_file($file_tmp, "local_path/" . $file_name);
                $data = array('parent_temp_id' =>$id, 'image_name' => $file_name, 'image_type' => 'scan', 'metadata_individual_number' => $indNum);
                
                $this->db->insert('dms_parent_image', $data);
                }
                
            }
        }
        if($detail['name'])
        {
//            echo "hh";
//            print_r($detail); die;
            foreach ($detail['name'] as $key => $value) {
                $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                $file_name = $random . $detail['name'][$key];
                $file_size = $detail['size'][$key];
                $file_tmp = $detail['tmp_name'][$key];
                $file_type = $detail['type'][$key];
                $file_ext = strtolower(end(explode('.', $detail['name'][$key])));
                $expensions = array("gif", "jpg", "jpeg", "png", "mp4", "mp3", "pdf", "pdf", "tiff", "swf", "psd", "bmp", "jpc", "aiff", "wbmp", "xbm");
                if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                }
//                move_uploaded_file($file_tmp, "uploads/" . $file_name);
//                $detai_img = $file_name;
                if($file_ext){
                    move_uploaded_file($file_tmp, "local_path/" . $file_name);
                    $detai_img = $file_name;
                    $data1 = array('parent_temp_id' =>$id, 'image_name' => $detai_img, 'image_type' => 'detail', 'metadata_individual_number' => $indNum);
    
                    $this->db->insert('dms_parent_image', $data1);
                }
            }
        }

    }
    
    public function add_child_img($id, $scan, $detail, $indNum = '')
    {
       
        //echo $scan['name']['0'];
        
        if ($scan['name']) {
            
            foreach ($scan['name'] as $key => $value) {
                //echo $scan['name'][$key];
                $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                $file_name = $random . $scan['name'][$key];
                $file_size = $scan['size'][$key];
                $file_tmp = $scan['tmp_name'][$key];
                $file_type = $scan['type'][$key];
                $file_ext = strtolower(end(explode('.', $scan['name'][$key])));
                $expensions = array("gif", "jpg", "jpeg", "png", "mp4", "mp3", "pdf", "pdf", "tiff", "swf", "psd", "bmp", "jpc", "aiff", "wbmp", "xbm");
                if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                }
       //   move_uploaded_file($file_tmp, "uploads/" . $file_name);       
//       move_uploaded_file($file_tmp, "../local_path/" . $file_name); // external directory.
              move_uploaded_file($file_tmp, "local_path/" . $file_name);
                //$copy_img = $file_name;
                
                if($file_ext){
                
//                    move_uploaded_file($file_tmp, "uploads/" . $file_name);
                                //            move_uploaded_file($file_tmp, "uploads/" . $file_name);       
//       move_uploaded_file($file_tmp, "../local_path/" . $file_name); // external directory.
              move_uploaded_file($file_tmp, "local_path/" . $file_name);
                $data = array('child_temp_id' =>$id, 'image_name' => $file_name, 'image_type' => 'scan', 'metadata_individual_number' => $indNum);
                
                $this->db->insert('dms_child_image', $data);
                 // echo "<pre>"; print_r($data); die; 
                }
                
            }
        }
        if($detail['name'])
        {
//            echo "hh";
           // print_r($detail); die;
            foreach ($detail['name'] as $key => $value) {
                 $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                $file_name = $random . $detail['name'][$key];
                $file_size = $detail['size'][$key];
                $file_tmp = $detail['tmp_name'][$key];
                $file_type = $detail['type'][$key];
                $file_ext = strtolower(end(explode('.', $detail['name'][$key])));
                $expensions = array("gif", "jpg", "jpeg", "png", "mp4", "mp3", "pdf", "pdf", "tiff", "swf", "psd", "bmp", "jpc", "aiff", "wbmp", "xbm");
                if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                }
//                move_uploaded_file($file_tmp, "uploads/" . $file_name);
//                $detai_img = $file_name;
                if($file_ext){
                    move_uploaded_file($file_tmp, "local_path/" . $file_name);
                    $detai_img = $file_name;
                    $data1 = array('child_temp_id' =>$id, 'image_name' => $detai_img, 'image_type' => 'detail', 'metadata_individual_number' => $indNum);
    
                    $this->db->insert('dms_child_image', $data1);
                }
            }
        }

    }
    
    function numberTowords($num)
{ 
$ones = array( 
0 => "zero",
1 => "one", 
2 => "two", 
3 => "three", 
4 => "four", 
5 => "five", 
6 => "six", 
7 => "seven", 
8 => "eight", 
9 => "nine", 
10 => "ten", 
11 => "eleven", 
12 => "twelve", 
13 => "thirteen", 
14 => "fourteen", 
15 => "fifteen", 
16 => "sixteen", 
17 => "seventeen", 
18 => "eighteen", 
19 => "nineteen" 
); 
$tens = array( 
1 => "ten",
2 => "twenty", 
3 => "thirty", 
4 => "forty", 
5 => "fifty", 
6 => "sixty", 
7 => "seventy", 
8 => "eighty", 
9 => "ninety" 
); 
$hundreds = array( 
"hundred", 
"thousand", 
"million", 
"billion", 
"trillion", 
"quadrillion" 
); //limit t quadrillion 
$num = number_format($num,2,".",","); 
$num_arr = explode(".",$num); 
$wholenum = $num_arr[0]; 
$decnum = $num_arr[1]; 
$whole_arr = array_reverse(explode(",",$wholenum)); 
krsort($whole_arr); 
$rettxt = ""; 
foreach($whole_arr as $key => $i){ 
if($i < 20){ 
$rettxt .= $ones[$i]; 
}elseif($i < 100){ 
$rettxt .= $tens[substr($i,0,1)]; 
$rettxt .= " ".$ones[substr($i,1,1)]; 
}else{ 
$rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
$rettxt .= " ".$tens[substr($i,1,1)]; 
$rettxt .= " ".$ones[substr($i,2,1)]; 
} 
if($key > 0){ 
$rettxt .= " ".$hundreds[$key]." "; 
} 
} 
if($decnum > 0){ 
$rettxt .= " and "; 
if($decnum < 20){ 
$rettxt .= $ones[$decnum]; 
}elseif($decnum < 100){ 
$rettxt .= $tens[substr($decnum,0,1)]; 
$rettxt .= " ".$ones[substr($decnum,1,1)]; 
} 
} 
return $rettxt; 
} 
 

   } 
 