<?php

error_reporting(~E_NOTICE);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AdminLogin extends CI_Controller {

    public $data1;

    Public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('csv_model');
        if ($this->session->userdata['login']['admin_role_id'] != '3') {
            $this->session->set_flashdata('flash_message', 'login_again');
            redirect('admin', 'refresh');
        }
    }

// Show admin dashboard page
    public function index() {
        
    }

    public function dashboard($param1 = '') {
        if ($param1 == 'edit') {
            $pagedata['page'] = 'Employee Management';
            $pagedata['pagetitle'] = 'Edit Employee';
        } else {
            $pagedata['data'] = $this->db->get('tblteacher')->result_array();
            $pagedata['pagetitle'] = 'Dashboard';
        }
        $this->load->view('admin/header');
        $this->load->view('admin/dashboard', $pagedata);
        $this->load->view('admin/footer');
    }

//for show addteacher page
    public function show_addteacher($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Employee';
        } else {

            $pagedata['pagetitle'] = 'Add Employee';
        }
        $pagedata['page'] = 'Employee Management';

        $id = $this->input->post('id');
        $pagedata['param1'] = $param1;
        $pagedata['data1'] = $this->db->get_where("tblcountry", array('country_status' => 'active'))->result_array();
        $pagedata['data2'] = $this->db->get_where('tblstate', array('state_country_id' => $id, 'state_status' => 'active'))->result_array();
        $pagedata['data3'] = $this->db->get_where('tblcity', array('city_state_id' => $id, 'city_status' => 'active'))->result_array();
        $pagedata['data4'] = $this->db->get('tblrole')->result_array();
        $pagedata['data5'] = $this->db->get_where("tbldepartment", array('department_status' => 'active'))->result_array();
        $pagedata['data6'] = $this->db->get('tblclassmaster')->result_array();
        $pagedata['data7'] = $this->db->get('tblsection')->result_array();
        $pagedata['data8'] = $this->db->get_where("tbldesignation", array('designation_status' => 'active'))->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addteacher', $pagedata);
        $this->load->view('admin/footer');
    }

//for add teacher record
    public function addteacher($param1 = '', $param2 = '', $param3 = '') {
//for reume upload
        if ($_FILES['image']['name']) {

            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $file_tmp = $_FILES['image']['tmp_name'];
            $file_type = $_FILES['image']['type'];
            $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
            $expensions = array("doc", "docx", "pdf");
            if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
            }
            move_uploaded_file($file_tmp, "local_path/" . $file_name);
            $data['image'] = $file_name;
//echo $file_name;die;
        }

        $data = array(
            'teacher_name' => $this->input->post('username'),
            'teacher_dob' => $this->input->post('dob'),
            'teacher_gender' => $this->input->post('gender'),
            'teacher_email_id' => $this->input->post('email_id'),
            'teacher_password' => $this->input->post('password'),
            'teacher_address' => $this->input->post('address'),
            'teacher_country' => $this->input->post('country'),
            'teacher_state' => $this->input->post('state'),
            'teacher_city' => $this->input->post('city'),
            'teacher_final_degree' => $this->input->post('final_degree'),
            'teacher_final_degree_passout_year' => $this->input->post('passout_year'),
            'teacher_final_degree_division' => $this->input->post('division'),
            'teacher_contact_no' => $this->input->post('contact_no'),
            'teacher_role_id' => '0',
            'teacher_department_id' => $this->input->post('department'),
            'teacher_class_id' => '0',
            'teacher_section_id' => '0',
            'teacher_designation_id' => $this->input->post('designation'),
            'teacher_status' => $this->input->post('status'),
            'teacher_earning' => $this->input->post('earning'),
            'teacher_deduction' => $this->input->post('deduction'),
            'teacher_basic_salary' => $this->input->post('basic_salary'),
            'teacher_jobresume' => $file_name,
            'teacher_joinning_date' => $this->input->post('joinning_date'),
            'teacher_termination_date' => $this->input->post('termination_date')
        );
        if ($param1 == 'create') {

            if ($this->input->post('confirm_password') == $this->input->post('password')) {
                $result = $this->db->insert('tblteacher', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addteacher', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addteacher', 'refresh');
                }
            } else {
                $this->session->set_flashdata('flash_message', 'password and confirm password not matched');
                redirect('Adminlogin/show_addteacher', 'refresh');
            }
        } else if ($param1 == 'edit') {
            $pagedata['pagetitle'] = 'Edit Employee';
            $id = $this->input->post('hidden_id');
            $this->db->where('teacher_id', $id);
            $this->db->update('tblteacher', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/dashboard', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('teacher_id', $param2);
            $this->db->delete('tblteacher');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/dashboard', 'refresh');
        }
    }

//for show addclass page
    public function show_addclass($param1 = '', $param2 = '') {
        $this->session->unset_userdata('user1');
        if ($param1) {

            $pagedata['edit_rel'] = $param1;
        }

        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Class Details';
        } else {
            $pagedata['pagetitle'] = 'Add Class Details';
        }
        $pagedata['page'] = 'Basic Record';

        $pagedata['param1'] = $param1;
        $pagedata['data2'] = $this->db->get_where('tblsubject', array('subject_status' => 'active'))->result_array();
        $pagedata['data3'] = $this->db->get_where('tblclassmaster', array('classmaster_status' => 'active'))->result_array();
        $pagedata['data1'] = $this->db->get_where('tblsection', array('section_status' => 'active'))->result_array();

        $pagedata['data'] = $this->db->get('tblclass')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addclass', $pagedata);
        $this->load->view('admin/footer');
    }

//for add class record
    public function addclass($param1 = '', $param2 = '', $param3 = '') {
        $var = $this->input->post('no_of_subject');
        $val11 = '';
        $val1 = '';
        if ($var) {
            foreach ($var as $val1) {
                $val11 = $val11 . ',' . $val1;
            }
//print_r($var);die;
            $var1 = $this->input->post('fee');

            $val12 = '';
            $sum = 0;
            foreach ($var1 as $val12) {
                $sum = $sum + $val12;
            }

            $ab1 = '';
            foreach ($var as $key => $val1) {
                $res = $this->db->get_where('tblsubject', array('subject_id' => $val1))->result_array();
                $ab = $res[0]['subject_name'] . "=" . $var1[$key] . ', ';
// echo $ab;
                $ab1 .= $ab;
            }


            $string1 = preg_replace('/\s+/', '', $val11);

            $string = trim($string1, ', ');
            $string11 = preg_replace('/\s+/', '', $ab1);

            $string2 = trim($string11, ', ');
        }
// echo  $string;
//die;
        $data = array(
            'class_name' => $this->input->post('name'),
            'class_section_id' => $this->input->post('section'),
            'class_start_date' => $this->input->post('start_date'),
            'class_end_date' => $this->input->post('end_date'),
            'class_no_of_subject' => $string,
            'class_fee' => $string2,
            'class_total_fee' => $sum,
            'class_status' => $this->input->post('status')
        );
// echo '<pre>'; print_r($data); die;
        if ($param1 == 'create') {
            $result = $this->db->insert('tblclass', $data);

//  echo $this->db->last_query(); die;
            if (!$result) {
                $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                redirect('Adminlogin/show_addclass', 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                redirect('Adminlogin/show_addclass', 'refresh');
//$this->db->last_query();
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('class_id', $id);
            $this->db->update('tblclass', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addclass', 'refresh');
        } elseif ($param1 == 'delete') {

            $this->db->where('class_id', $param2);
            $this->db->delete('tblclass');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addclass', 'refresh');
        }
    }

//for show  addsection page
    public function show_addsection($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Section';
        } else {

            $pagedata['pagetitle'] = 'Add Section';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblsection')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addsection', $pagedata);
        $this->load->view('admin/footer');
    }

//for add class record
    public function addsection($param1 = '', $param2 = '', $param3 = '') {

        $id = $this->input->post('name');
        $q = $this->db->get_where("tblsection", array('section_name' => $id))->result_array();

        $data = array(
            'section_name' => $this->input->post('name'),
            'section_description' => $this->input->post('description'),
            'section_status' => $this->input->post('status'),
            'section_no_of_period' => $this->input->post('no_of_period')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'section name already exist.');
                redirect('Adminlogin/show_addsection', 'refresh');
            } else {
                $result = $this->db->insert('tblsection', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addsection', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addsection', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('section_id', $id);
            $this->db->update('tblsection', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addsection', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('section_id', $param2);
            $this->db->delete('tblsection');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addsection', 'refresh');
        }
    }

//for show  addclassperiod page
    public function show_addclassperiod($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Class Period';
        } else {

            $pagedata['pagetitle'] = 'Add Class Period';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblclassperiod')->result_array();
        $SQL = "SELECT * FROM tblteacher WHERE teacher_status ='active' AND  teacher_termination_date = '0000-00-00' ";
        $query = $this->db->query($SQL);
        $pagedata['data1'] = $query->result_array();

        //$pagedata['data1'] = $this->db->get_where('tblteacher', array('teacher_status' => 'active'))->result_array();
        $pagedata['data2'] = $this->db->get_where('tblclassmaster', array('classmaster_status' => 'active'))->result_array();
        $pagedata['data3'] = $this->db->get_where('tblsection', array('section_status' => 'active'))->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addclassperiod', $pagedata);
        $this->load->view('admin/footer');
    }

//for add classperiod record
    public function addclassperiod($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblclassperiod", array('classperiod_name' => $id))->result_array();
        $data = array(
            'classperiod_name' => $this->input->post('name'),
            'classperiod_teacher_id' => $this->input->post('teacher'),
            'classperiod_class_id' => $this->input->post('class'),
            'classperiod_section_id' => $this->input->post('section'),
            'classperiod_start_time' => $this->input->post('stime'),
            'classperiod_end_time' => $this->input->post('etime'),
            'classperiod_status' => $this->input->post('status')
        );

        if ($param1 == 'create') {

            if ($q) {
                $this->session->set_flashdata('flash_message', 'classperiod  name already exist.');
                redirect('Adminlogin/show_addclassperiod', 'refresh');
            } else {
                $result = $this->db->insert('tblclassperiod', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addclassperiod', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addclassperiod', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('classperiod_id', $id);
            $this->db->update('tblclassperiod', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addclassperiod', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('classperiod_id', $param2);
            $this->db->delete('tblclassperiod');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addclassperiod', 'refresh');
        }
    }

//for show  adddepartment page
    public function show_adddepartment($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Department';
        } else {

            $pagedata['pagetitle'] = 'Add Department';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tbldepartment')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/adddepartment', $pagedata);
        $this->load->view('admin/footer');
    }

//for add department record
    public function adddepartment($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tbldepartment", array('department_name' => $id))->result_array();

        $data = array(
            'department_name' => $this->input->post('name'),
            'department_status' => $this->input->post('status'),
            'department_description' => $this->input->post('description')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'department name already exist.');
                redirect('Adminlogin/show_adddepartment', 'refresh');
            } else {
                $result = $this->db->insert('tbldepartment', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_adddepartment', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_adddepartment', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('department_id', $id);
            $this->db->update('tbldepartment', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_adddepartment', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('department_id', $param2);
            $this->db->delete('tbldepartment');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_adddepartment', 'refresh');
        }
    }

//for show  adddesignation page
    public function show_adddesignation($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Designation';
        } else {

            $pagedata['pagetitle'] = 'Add Designation';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tbldesignation')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/adddesignation', $pagedata);
        $this->load->view('admin/footer');
    }

//for add designation record
    public function adddesignation($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tbldesignation", array('designation_name' => $id))->result_array();

        $data = array(
            'designation_name' => $this->input->post('name'),
            'designation_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'designation  name already exist.');
                redirect('Adminlogin/show_adddesignation', 'refresh');
            } else {
                $result = $this->db->insert('tbldesignation', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_adddesignation', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_adddesignation', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('designation_id', $id);
            $this->db->update('tbldesignation', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_adddesignation', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('designation_id', $param2);
            $this->db->delete('tbldesignation');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_adddesignation', 'refresh');
        }
    }

//for show  addrole page
    public function show_addrole($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit User Type';
        } else {

            $pagedata['pagetitle'] = 'Add User Type';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblrole')->result_array();
        $pagedata['data1'] = $this->db->get_where('tblpermission', array('permission_status' => 'active'))->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addrole', $pagedata);
        $this->load->view('admin/footer');
    }

//for add role record
    public function addrole($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblrole", array('role_name' => $id))->result_array();

        $data = array(
            'role_name' => $this->input->post('name'),
            'role_permission_id' => $this->input->post('permission'),
            'role_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'role  name already exist.');
                redirect('Adminlogin/show_addrole', 'refresh');
            } else {
                $result = $this->db->insert('tblrole', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addrole', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addrole', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('role_id', $id);
            $this->db->update('tblrole', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addrole', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('role_id', $param2);
            $this->db->delete('tblrole');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addrole', 'refresh');
        }
    }

//for show  addpermission page
    public function show_addpermission($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Permission';
        } else {

            $pagedata['pagetitle'] = 'Add Permission';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblpermission')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addpermission', $pagedata);
        $this->load->view('admin/footer');
    }

//for add permission record
    public function addpermission($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblpermission", array('permission_name' => $id))->result_array();

        $data = array(
            'permission_name' => $this->input->post('name'),
            'permission_description' => $this->input->post('description'),
            'permission_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'permission  name already exist.');
                redirect('Adminlogin/show_addpermission', 'refresh');
            } else {
                $result = $this->db->insert('tblpermission', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addpermission', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addpermission', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('permission_id', $id);
            $this->db->update('tblpermission', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addpermission', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('permission_id', $param2);
            $this->db->delete('tblpermission');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addpermission', 'refresh');
        }
    }

//for show  addcountry page
    public function show_addcountry($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Country';
        } else {

            $pagedata['pagetitle'] = 'Add Country';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblcountry')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addcountry', $pagedata);
        $this->load->view('admin/footer');
    }

//for add country record
    public function addcountry($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblcountry", array('country_name' => $id))->result_array();

        $data = array(
            'country_name' => $this->input->post('name'),
            'country_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'country name already exist.');
                redirect('Adminlogin/show_addcountry', 'refresh');
            } else {
                $result = $this->db->insert('tblcountry', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addcountry', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addcountry', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('country_id', $id);
            $this->db->update('tblcountry', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addcountry', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('country_id', $param2);
            $this->db->delete('tblcountry');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addcountry', 'refresh');
        }
    }

//for show  addstate page
    public function show_addstate($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit State';
        } else {

            $pagedata['pagetitle'] = 'Add State';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblstate')->result_array();
        $pagedata['data1'] = $this->db->get_where('tblcountry', array('country_status' => 'active'))->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addstate', $pagedata);
        $this->load->view('admin/footer');
    }

//for add state record
    public function addstate($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblstate", array('state_name' => $id))->result_array();

        $data = array(
            'state_name' => $this->input->post('name'),
            'state_country_id' => $this->input->post('country_id'),
            'state_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'state  name already exist.');
                redirect('Adminlogin/show_addstate', 'refresh');
            } else {
                $result = $this->db->insert('tblstate', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addstate', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addstate', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('state_id', $id);
            $this->db->update('tblstate', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addstate', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('state_id', $param2);
            $this->db->delete('tblstate');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addstate', 'refresh');
        }
    }

//for show  addcity page
    public function show_addcity($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit City';
        } else {

            $pagedata['pagetitle'] = 'Add City';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblcity')->result_array();
        $pagedata['data1'] = $this->db->get_where('tblstate', array('state_status' => 'active'))->result_array();

        $this->load->view('admin/header');
        $this->load->view('admin/addcity', $pagedata);
        $this->load->view('admin/footer');
    }

//for add city record
    public function addcity($param1 = '', $param2 = '', $param3 = '') {
        $data = array(
            'city_name' => $this->input->post('name'),
            'city_state_id' => $this->input->post('state_id'),
            'city_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            $result = $this->db->insert('tblcity', $data);
            if (!$result) {
                $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                redirect('Adminlogin/show_addcity', 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                redirect('Adminlogin/show_addcity', 'refresh');
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('city_id', $id);
            $this->db->update('tblcity', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addcity', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('city_id', $param2);
            $this->db->delete('tblcity');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addcity', 'refresh');
        }
    }

//for show  addsubject page
    public function show_addsubject($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Subject';
        } else {

            $pagedata['pagetitle'] = 'Add Subject';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblsubject')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addsubject', $pagedata);
        $this->load->view('admin/footer');
    }

//for add subject record
    public function addsubject($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblsubject", array('subject_name' => $id))->result_array();

        $data = array(
            'subject_name' => $this->input->post('name'),
            'subject_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'subject  name already exist.');
                redirect('Adminlogin/show_addsubject', 'refresh');
            } else {
                $result = $this->db->insert('tblsubject', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addsubject', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addsubject', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('subject_id', $id);
            $this->db->update('tblsubject', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addsubject', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('subject_id', $param2);
            $this->db->delete('tblsubject');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addsubject', 'refresh');
        }
    }

//for show  changepassword page
    public function show_changepassword($param1 = '') {
        $pagedata['pagetitle'] = 'Change Password';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tbladmin')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/changepassword', $pagedata);
        $this->load->view('admin/footer');
    }

//for changepassword
    public function changepassword() {
        $oldpassword = $this->input->post('old_password');
        $newpassword = $this->input->post('new_password');
        $confirmpassword = $this->input->post('confirm_password');
        $data = array(
            'admin_password' => $this->input->post('new_password')
        );


        $id = $this->session->userdata['login']['admin_id'];
        $query = $this->db->get_where('tbladmin', array('admin_id' => $id))->result_array();
//        if (($query[0]['admin_password'] != $oldpassword)) {
//            $this->session->set_flashdata('flash_message', 'wrong old password');
//            redirect('Adminlogin/show_changepassword', 'refresh');
//        }
//        if (($oldpassword == '')) {
//            $this->session->set_flashdata('flash_message', 'enter old password.');
//            redirect('Adminlogin/show_changepassword', 'refresh');
//        }
        if (($query[0]['admin_password'] == $oldpassword) && $newpassword) {
            $this->db->where('admin_id', $id);
            $this->db->update('tbladmin', $data);
            $this->session->set_flashdata('flash_message', 'Update  password sucessfully');
            redirect('Adminlogin/show_changepassword', 'refresh');
        }
//         if ($oldpassword==""&& $newpassword=="") {
//                $this->session->set_flashdata('flash_message', ' password required');
//                 redirect('Adminlogin/show_changepassword', 'refresh');
//            }
        else {
            $this->session->set_flashdata('flash_message', 'old password and new password notmatched.');
            redirect('Adminlogin/show_changepassword', 'refresh');
        }
    }

//for show  profile page
    public function show_profile($param1 = '') {
//$this->session->userdata($userSessiondata);       
        $id = $this->session->userdata['login']['admin_id'];
        $pagedata['pagetitle'] = 'Profile';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get_where('tbladmin', array('admin_id' => $id))->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/profile', $pagedata);
        $this->load->view('admin/footer');
    }

//for update profile
    public function profileupdate() {
        $id = $this->session->userdata['login']['admin_id'];

        if ($_FILES['image']['name']) {

            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $file_tmp = $_FILES['image']['tmp_name'];
            $file_type = $_FILES['image']['type'];
            $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
            $expensions = array("gif", "jpg", "jpeg", "png");
            if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
            }
            move_uploaded_file($file_tmp, "local_path/" . $file_name);
            $data['image'] = $file_name;
        } else {

            $abc = $this->db->get_where("tbladmin", array('admin_id' => $id))->result_array();

            $file_name = $abc[0]['admin_image'];
        }

        $emailid = $this->input->post('emailid');
        $username = $this->input->post('username');

        $data = array(
            'admin_username' => $this->input->post('username'),
            'admin_email_id' => $this->input->post('emailid'),
            'admin_image' => $file_name
        );
        $this->db->where('admin_id', $id);
        $this->db->update('tbladmin', $data);
        $this->session->set_flashdata('flash_message', 'profile Update  sucessfully');
        redirect('Adminlogin/show_profile', 'refresh');
    }

//for show addstudent page
    public function show_addstudent($param1 = '', $param2 = '') {
        $pagedata['page'] = 'Student Management';
        if ($param2 == 'edit') {
            $pagedata['pagetitle'] = 'Edit Student';
        } else {
            $pagedata['pagetitle'] = 'Add Student';
        }
        $id = $this->input->post('id');
        $pagedata['param1'] = $param1;
        $pagedata['data1'] = $this->db->get_where("tblcountry", array('country_status' => 'active'))->result_array();
        $pagedata['data2'] = $this->db->get_where('tblstate', array('state_country_id' => $id, 'state_status' => 'active'))->result_array();
        $pagedata['data3'] = $this->db->get_where('tblcity', array('city_state_id' => $id, 'city_status' => 'active'))->result_array();
        $pagedata['data6'] = $this->db->get_where('tblclassmaster', array('classmaster_status' => 'active'))->result_array();
        $pagedata['data7'] = $this->db->get_where('tblsection', array('section_status' => 'active'))->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addstudent', $pagedata);
        $this->load->view('admin/footer');
    }

//for add student record
    public function addstudent($param1 = '', $param2 = '', $param3 = '') {
        $data = array(
            'student_name' => $this->input->post('username'),
            'student_dob' => $this->input->post('dob'),
            'student_gender' => $this->input->post('gender'),
            'student_address' => $this->input->post('address'),
            'student_country' => $this->input->post('country'),
            'student_state' => $this->input->post('state'),
            'student_city' => $this->input->post('city'),
            'student_contact_no' => $this->input->post('contact_no'),
            'student_email_id' => $this->input->post('email_id'),
            'student_password' => $this->input->post('password'),
            'student_role_id' => 'student',
            'student_class_id' => $this->input->post('class'),
            'student_section_id' => $this->input->post('section'),
            'student_addmission_date' => $this->input->post('admission_date'),
            'student_comments' => $this->input->post('comments'),
            'student_status' => $this->input->post('status'),
            'student_guardian_name' => $this->input->post('guardian_name'),
            'student_guardian_contact_no' => $this->input->post('guardian_contact_no')
        );
        if ($param1 == 'create') {
            if ($this->input->post('confirm_password') == $this->input->post('password')) {
                $result = $this->db->insert('tblstudent', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addstudent', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addstudent', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('student_id', $id);
            $this->db->update('tblstudent', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_allstudent', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('student_id', $param2);
            $this->db->delete('tblstudent');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_allstudent', 'refresh');
        }
    }

// Show all student page
    public function show_allstudent($par1 = '') {

        $pagedata['page'] = 'Student Management';
        $pagedata['pagetitle'] = 'All Student';
        $pagedata['data'] = $this->db->get('tblstudent')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/allstudent', $pagedata);
        $this->load->view('admin/footer');
    }

// Show all student page
    public function show_allteacher($par1 = '') {

        $pagedata['page'] = 'Employee Management';
        $pagedata['pagetitle'] = 'All Employee';
        $pagedata['data'] = $this->db->get('tblteacher')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/allteacher', $pagedata);
        $this->load->view('admin/footer');
    }

// state display on country change
    public function regionselect() {

        $id = $this->input->post('id');
        if ($id != '0') {
            $data = $this->db->get_where("tblstate", array('state_country_id' => $id))->result_array();

            echo '<option value="">Select State</option>';
            foreach ($data as $row1) {
                echo '<option value="' . $row1['state_id'] . '">' . $row1['state_name'] . '</option>';
            }
        } else {

            echo '<option value="">Select State</option>';
        }
    }

// city display on state change
    public function regionselect1() {

        $id = $this->input->post('id');
        if ($id != '0') {
            $data = $this->db->get_where("tblcity", array('city_state_id' => $id))->result_array();

            echo '<option value="">Select City</option>';
            foreach ($data as $row1) {
                echo '<option value="' . $row1['city_id'] . '">' . $row1['city_name'] . '</option>';
            }
        } else {

            echo '<option value="">Select City</option>';
        }
    }

//for show  addclassmaster page
    public function show_addclassmaster($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Class';
        } else {

            $pagedata['pagetitle'] = 'Add Class';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblclassmaster')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addclassmaster', $pagedata);
        $this->load->view('admin/footer');
    }

//for add classmaster record
    public function addclassmaster($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblclassmaster", array('classmaster_name' => $id))->result_array();

        $data = array(
            'classmaster_name' => $this->input->post('name'),
            'classmaster_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'classmaster  name already exist.');
                redirect('Adminlogin/show_addclassmaster', 'refresh');
            } else {
                $result = $this->db->insert('tblclassmaster', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addclassmaster', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addclassmaster', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('classmaster_id', $id);
            $this->db->update('tblclassmaster', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addclassmaster', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('classmaster_id', $param2);
            $this->db->delete('tblclassmaster');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addclassmaster', 'refresh');
        }
    }

//for show  book issue  page
    public function bookissue($param1 = '', $param2 = '') {
        $this->session->unset_userdata('user2');
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Book Issue Details';
        } else {

            $pagedata['pagetitle'] = 'Book Issue Details';
        }
        $pagedata['page'] = 'Library Management';


        $pagedata['param1'] = $param1;
        $pagedata['data4'] = $this->db->get('tbllibrary')->result_array();
        $pagedata['data'] = $this->db->get_where('tblstudent', array('student_status' => 'active'))->result_array();

        $pagedata['data2'] = $this->db->get_where('tblbookcategory', array('category_status' => 'active'))->result_array();

        $pagedata['data1'] = $this->db->get_where('tblbook', array('book_status' => 'StockIn'))->result_array();

        $this->load->view('admin/header');
        $this->load->view('admin/bookissue', $pagedata);
        $this->load->view('admin/footer');
    }

//for show  addholiday page
    public function show_addholiday($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Event';
        } else {

            $pagedata['pagetitle'] = 'Add Event';
        }
        $pagedata['page'] = 'Basic Record';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblholidaycalender')->result_array();

        $this->load->view('admin/header');
        $this->load->view('admin/addholiday', $pagedata);
        $this->load->view('admin/footer');
    }

//for add holiday  record
    public function addholiday($param1 = '', $param2 = '', $param3 = '') {

        $data = array(
            'holiday_name' => $this->input->post('name'),
            'holiday_no_of_days' => $this->input->post('event_days'),
            'holiday_from_date' => $this->input->post('from_date'),
            'holiday_to_date' => $this->input->post('to_date'),
            'holiday_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            $result = $this->db->insert('tblholidaycalender', $data);
            if (!$result) {
                $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                redirect('Adminlogin/show_addholiday', 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                redirect('Adminlogin/show_addholiday', 'refresh');
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('holiday_id', $id);
            $this->db->update('tblholidaycalender', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addholiday', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('holiday_id', $param2);
            $this->db->delete('tblholidaycalender');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addholiday', 'refresh');
        }
    }

// section display on class change
    public function classselect() {

        $id = $this->input->post('id');
        if ($id != '0') {

            $data1 = $this->db->get_where("tblclass", array('class_name' => $id))->result_array();
            foreach ($data1 as $row1) {
//echo $row1["class_section_id"];


                $data = $this->db->get_where("tblsection", array('section_id' => ($row1["class_section_id"])))->result_array();

                foreach ($data as $row1) {

                    echo '<option value="' . $row1['section_id'] . '">' . $row1['section_name'] . '</option>';
                }
            }
        } else {

            echo '<option value="">Select Section </option>';
        }
    }

//for show  addbook page
    public function show_addbook($param1 = '', $param2 = '') {


        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Book';
        } else {

            $pagedata['pagetitle'] = 'Add Book';
        }
        $pagedata['page'] = 'Library Management';
        $pagedata['param1'] = $param1;
        $pagedata['data1'] = $this->db->get_where('tblbookcategory', array('category_status' => 'active'))->result_array();

        $pagedata['data'] = $this->db->get('tblbook')->result_array();
        $this->load->view('admin/header');
        $this->load->view('modal');
        $this->load->view('admin/addbook', $pagedata);
        $this->load->view('admin/footer');
    }

    function showcsv() {
        $this->load->view('admin/uploadCsvView', $data1);
    }

    function uploadData() {
        $this->csv_model->uploadData();
// $this->db->last_query();
        redirect('Adminlogin/show_addbook');
    }

//for add book record
    public function addbook($param1 = '', $param2 = '', $param3 = '') {

        $data = array(
            'book_name' => $this->input->post('name'),
            'book_category' => $this->input->post('category'),
            'book_writer' => $this->input->post('writer'),
            'book_publication' => $this->input->post('publication'),
            'book_price' => $this->input->post('price'),
            'book_qty' => $this->input->post('qty'),
            'book_actual_qty' => $this->input->post('qty'),
            'book_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {

            $result = $this->db->insert('tblbook', $data);
// echo $this->db->last_query();die;
            if (!$result) {
                $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                redirect('Adminlogin/show_addbook', 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                redirect('Adminlogin/show_addbook', 'refresh');
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('book_id', $id);
            $this->db->update('tblbook', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addbook', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('book_id', $param2);
            $this->db->delete('tblbook');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addbook', 'refresh');
        }
    }

// not  selected section display on class change
    public function classselect1() {

        $id = $this->input->post('id');
///echo $id;  
        if ($id != '0') {
            $array1 = $this->db->get_where("tblsection", array('section_status' => 'active'))->result_array();
//echo $this->db->last_query(); die;
            foreach ($array1 as $res) {
                $res1[] = $res['section_id'];
            }

//print_r($res1);
            $data1 = $this->db->get_where("tblclass", array('class_name' => $id))->result_array();
            if (count($data1) > 0) {

                foreach ($data1 as $row1) {
                    $a1[] = $row1['class_section_id'];
                }
//print_r($data1);

                $result = array_diff($res1, $a1);
                $row12 = array_values($result);
//print_r($result);
                foreach ($row12 as $match) {
//echo $match;
                    $data12 = $this->db->get_where("tblsection", array('section_id' => $match))->result_array();
                    $abc[] = $data12[0]['section_id'];
                    echo '<option value="' . $match . '">' . $data12[0]['section_name'] . '</option>';
                }
            } else {
                $array12 = $this->db->get_where("tblsection", array('section_status' => 'active'))->result_array();
//print_r($array12);
                foreach ($array12 as $res) {
//echo $res['section_id'];
                    echo '<option value="' . $res['section_id'] . '">' . $res['section_name'] . '</option>';
                }
            }
        }
    }

// not  selected subject display on subject change
    public function subjectselect() {

        $id = $this->input->post('id');
        $selectId = $this->input->post('selectId');
//echo $id;
//$id = preg_replace('/\s+/', '', $id1);
        if ($id != '0') {
            $array1 = $this->db->get("tblsubject", array('section_status' => 'active'))->result_array();
            foreach ($array1 as $res) {
                $res1[] = $res['subject_id'];
            }


            $a1 = $this->session->userdata['user1'];
            // print_r($a1);
            if (count($a1) >= 1) {
                foreach ($a1 as $key => $sub1) {

                    foreach ($sub1 as $sub2) {
                        if ($sub2 == $id) {
//unset($sub1);
                            echo $selectId;
//  echo "This subject is already selected , so you can't select ths subject.";
                        }
//                    else{
//                        echo "selected.";
//                    }
                    }
                }
            }

//print_r($a1);
            $a1['id'][$id] = $id;

            $this->session->set_userdata('user1', $a1);
            $array2 = $this->session->userdata['user1']['id'];
            $row11 = array_values($array2);
//print_r($row11);

            $result = array_diff($array2, $res1);
            $row12 = array_values($result);
//print_r($array2);

            foreach ($row12 as $match) {

                $data12 = $this->db->get_where("tblsubject", array('subject_id' => $match))->result_array();
                $abc[] = $data12[0]['subject_id'];
//echo '<option value="' . $match . '">' . $data12[0]['subject_name'] . '</option>';
            }
        }
    }

//for show  addhostel page
    public function show_addhostel($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Hostel Details';
        } else {

            $pagedata['pagetitle'] = 'Add Hostel Details';
        }
        $pagedata['page'] = 'Hostel Management';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblhostel')->result_array();
//$this->load->view('admin/header');
        $this->load->view('admin/table', $pagedata);
//$this->load->view('admin/footer');
    }

//for add hostel record
    public function addhostel($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblhostel", array('country_name' => $id))->result_array();

        $data = array(
            'country_name' => $this->input->post('name'),
            'country_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'country name already exist.');
                redirect('Adminlogin/show_addhostel', 'refresh');
            } else {
                $result = $this->db->insert('tblhostel', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addhostel', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addhostel', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('hostel_id', $id);
            $this->db->update('tblhostel', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addhostel', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('hostel_id', $param2);
            $this->db->delete('tblhostel');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addhostel', 'refresh');
        }
    }

// delete row of subject
    public function subjectdelete() {

        $id = $this->input->post('id');
        $rValue = $this->input->post('rValue');
        //echo  $rValue;
        $array2 = $this->session->userdata['user1'];
        // print_r($array2);
        //$array2
        unset($array2['id'][$rValue]);
        // print_r($array2);
        $this->session->set_userdata('user1', $array2);



        // echo $id;
        $array1 = explode(',', $id);
        // print_r($array1);
        $query = $this->db->get_where('tblclass', array('class_id' => $array1[1]))->result_array();

        $sub = rtrim($query[0]['class_fee'], ',');
        $array12 = explode(',', $sub);
//print_r($array12);

        $subject1 = rtrim($query[0]['class_no_of_subject'], ',');
        $array123 = explode(',', $subject1);
// echo  $sub= rtrim($query[0]['class_no_of_subject'],',');
//echo  $array=explode(',',$sub1);
        // print_r($array123);
//array for subject id 
//foreach($array1 as $key=>$value1){
//if($key==2){
        if (count($array123) >= 1) {
            foreach ($array123 as $key => $array131) {
//echo $key;
                if ($array1[2] == $array131) {
                    unset($array123[$key]);
                }
                $value1 = $value1 . ',' . $aray131;

//                  }}
                if ($array1[3]) {
                    $amount = $query[0]['class_total_fee'] - $array1[3];
                }
            }
        }


        // print_r($array123);
//array for fee with subject id
// if(count($array12)>=1){
        foreach ($array1 as $key => $value1) {
// echo $key;
            if ($key == 0) {
                foreach ($array12 as $key => $array13) {
//echo $key;
                    if ($value1 == $key) {
                        unset($array12[$key]);
                    }
                    $val11 = $val11 . ',' . $aray13;
                }
            }
        }
//}
// print_r($array12);
        $subject = implode(',', $array123);
        $fee = implode(',', $array12);
        $this->db->set('class_total_fee', $amount);
        $this->db->set('class_no_of_subject', $subject);
        $this->db->set('class_fee', $fee);
        $this->db->where('class_id', $array1[1]);
        $this->db->update('tblclass');
    }

//for show  addbookcategory page
    public function show_addbookcategory($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit  Book Category';
        } else {

            $pagedata['pagetitle'] = 'Add Book Category';
        }
        $pagedata['page'] = 'Library Management';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblbookcategory')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/addbookcategory', $pagedata);
        $this->load->view('admin/footer');
    }

//for add bookcategoryadd country record
    public function addbookcategory($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("tblbookcategory", array('category_name' => $id))->result_array();

        $data = array(
            'category_name' => $this->input->post('name'),
            'category_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('flash_message', 'category name already exist.');
                redirect('Adminlogin/show_addbookcategory', 'refresh');
            } else {
                $result = $this->db->insert('tblbookcategory', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                    redirect('Adminlogin/show_addbookcategory', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                    redirect('Adminlogin/show_addbookcategory', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('category_id', $id);
            $this->db->update('tblbookcategory', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_addbookcategory', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('category_id', $param2);
            $this->db->delete('tblbookcategory');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_addbookcategory', 'refresh');
        }
    }

//for show general setting page
    public function show_generalsetting($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'General Setting';
        } else {

            $pagedata['pagetitle'] = 'General Setting';
        }
        $pagedata['page'] = 'General Setting';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('tblgeneralsetting')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/generalsetting', $pagedata);
        $this->load->view('admin/footer');
    }

//for update general setting record
    public function update_generalsetting($param1 = '', $param2 = '', $param3 = '') {
        $data1 = array(
            '1' => $this->input->post('title'),
            '2' => $this->input->post('fine_days'),
            '3' => $this->input->post('allow_books'),
            '4' => $this->input->post('security_money'),
            '5' => $this->input->post('perday_fine_rupees')
        );
// echo "<pre>";
// print_r($data1);


        $data2 = array(
            '1' => $this->input->post('name1'),
            '2' => $this->input->post('name2'),
            '3' => $this->input->post('name3'),
            '4' => $this->input->post('name4'),
            '5' => $this->input->post('name5')
        );
//echo "<pre>";
//print_r($data2);

        foreach ($data2 as $key => $val) {
// echo $val;
            $array1[$val][] = $data1[$key];
        }
//print_r($array1) ;
        $data = $this->db->get('tblgeneralsetting')->result_array();
// print_r($data);
        $count = count($data1);
        foreach ($array1 as $key => $row) {
//echo $row[0];
//echo $key;
// echo $data[0]['general_id'];
// if($key==$data['general_id']){

            $this->db->set('general_value', $row[0]);
            $this->db->where('general_id', $key);
            $this->db->update('tblgeneralsetting');
// echo $this->db->last_query();
// }
        }
        $this->session->set_flashdata('flash_message', 'Update  sucessfully');
        redirect('Adminlogin/show_generalsetting', 'refresh');
    }

    public function exportCSV() {


        $this->load->model('csv_model');
        $this->load->dbutil();
        $this->load->helper('file');
//get the object
        $report = $this->csv_model->getCSV();
		
		//print_r($report); die;

        $delimiter = ",";
        $newline = "\r\n";
        $new_report = $this->dbutil->csv_from_result($report, $delimiter, $newline);
		//print_r($new_report); die;
		$sql = "select * from tblbookcategory";
		$query = $this->db->query($sql);
        $new_report = $query->result_array();
		
		
// write file
        write_file($this->file_path .'/csv_file.csv', $new_report);
		
		//print_r($this->file_path); die;
		
//force download from server
        $this->load->helper('download');
        $data = file_get_contents($this->file_path . '/csv_file.csv');
        $name = 'BookCategory-' . date('d-m-Y') . '.csv';
        force_download($name, $data);
		

    }

// book display on category change
    public function bookcategoryselect() {

        $id = $this->input->post('id');
        if ($id != '0') {
            $data = $this->db->get_where("tblbook", array('book_category' => $id, 'book_status' => 'StockIn'))->result_array();

            echo '<option value="">Select Book</option>';
            foreach ($data as $row1) {
                echo '<option value="' . $row1['book_id'] . '">' . $row1['book_name'] . '</option>';
            }
        } else {

            echo '<option value="">Select book</option>';
        }
    }

//for show  library fee page
    public function show_libraryfee($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Security Deposit';
        } else {

            $pagedata['pagetitle'] = 'Security Deposit';
        }
        $pagedata['page'] = 'Library Management';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get_where('tblstudent', array('student_status' => 'active'))->result_array();
        $pagedata['data1'] = $this->db->get_where('tbllibraryregister')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/librarysecurity', $pagedata);
        $this->load->view('admin/footer');
    }

//for add librarysecurity record
    public function addlibrarysecurity($param1 = '', $param2 = '', $param3 = '') {

        $data = array(
            'register_student' => $this->input->post('student'),
            'register_amount' => $this->input->post('security_amount'),
            'register_status' => $this->input->post('status')
        );
//echo $this->input->post('student');die;
        if ($param1 == 'create') {
            $result = $this->db->insert('tbllibraryregister', $data);
            if (!$result) {
                $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                redirect('Adminlogin/show_libraryfee', 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                redirect('Adminlogin/show_libraryfee', 'refresh');
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('register_id', $id);
            $this->db->update('tbllibraryregister', $data);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/show_libraryfee', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('register_id', $param2);
            $this->db->delete('tbllibraryregister');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/show_libraryfee', 'refresh');
        }
    }

//for show  book return page
    public function show_bookreturn($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Book Return Details';
        } else {

            $pagedata['pagetitle'] = 'Book Return Details';
        }
        $pagedata['page'] = 'Library Management';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get_where('tblstudent', array('student_status' => 'active'))->result_array();
        $id = $this->input->post('student');
        //echo $id;die;
        $query = $this->db->get_where('tblbookissue', array('bookissue_status' => 'issued', 'bookissue_student' => $id))->result_array();
        //echo "<pre>";
        //print_r($query);die;
        $pagedata['data1'] = $query;
        $this->load->view('admin/header');
        $this->load->view('admin/bookreturn', $pagedata);
        $this->load->view('admin/footer');
    }

// not  selected book display on book change
    public function selectbook() {

        $id = $this->input->post('id');
        $selectId = $this->input->post('selectId');
//echo $id;

        if ($id != '0') {
            $array1 = $this->db->get("tblbook")->result_array();
            foreach ($array1 as $res) {
                $res1[] = $res['book_id'];
            }
//print_r($res1);

            $a1 = $this->session->userdata['user2'];

            if (count($a1) >= 1) {
                foreach ($a1 as $key => $sub1) {

                    foreach ($sub1 as $sub2) {

                        if ($sub2 == $id) {

                            echo $selectId;
                        }
                    }
                }
            }

//print_r($a1);
            $a1['id'][$id] = $id;

            $this->session->set_userdata('user2', $a1);
            $array2 = $this->session->userdata['user2']['id'];
            $row11 = array_values($array2);
//print_r($row11);

            $result = array_diff($array2, $res1);
            $row12 = array_values($result);
//print_r($array2);

            foreach ($row12 as $match) {

                $data12 = $this->db->get_where("tblbook", array('book_id' => $match))->result_array();
                $abc[] = $data12[0]['book_id'];
            }
            $query = $this->db->get_where('tblbook', array('book_status' => 'StockIn', 'book_qty' => '0', 'book_id' => $id))->result_array();
            //echo $id;
        }
    }

//for add book issue record
    public function addbookissue($param1 = '', $param2 = '', $param3 = '') {
        $var = $this->input->post('no_of_book');
        $count1 = count($var);
        $val11 = '';
        $val1 = '';

        $var1 = $this->input->post('no_of_category');
        $count1 = count($var1);
        $val12 = '';
        $val2 = '';
        if ($var1) {
            foreach ($var1 as $val2) {
                $val12 = $val12 . ',' . $val2;
            }

            $string2 = preg_replace('/\s+/', '', $val12);

            $string22 = trim($string2, ', ');
        }

        $data1 = array(
            'bookissue_student' => $this->input->post('student'),
            'bookissue_no_of_category' => $string22,
            'bookissue_no_of_book' => $string,
            'bookissue_date' => date('Y-m-d'),
            'bookissue_date' => $this->input->post('issue_date'),
            'bookissue_status' => $this->input->post('status')
        );
        $sql = "SELECT * FROM tblbook WHERE (book_qty>0) AND book_status='StockIn' ";
        if ($param1 == 'create') {

            if ($var || $var1) {

                foreach ($var as $val1) {

                    $data1 = array(
                            'bookissue_student' => $this->input->post('student'),
                            'bookissue_no_of_category' => $string22,
                            'bookissue_no_of_book' => $val1,
                            'bookissue_date' => $this->input->post('issue_date'),
                            'bookissue_status' => $this->input->post('status')
                        );
                   // echo "<pre>";
                    //print_r($data1);
                   // echo $val1;
                    if ($this->input->post('Reserve') == "Reserve") { 
                        $result = $this->db->insert('tblbookissue', $data1);
                    } else if ($this->input->post('Issue') == "Issue") {
                        //print_r($var);
                        //if ($var) {
                            //foreach ($var as $val1) {

                               
                            //}
                        //}
                        if (count($sql) >= 1) {
                             $book = $this->db->get_where("tblbook", array('book_id' => $val1))->result_array();
                                $this->db->set('book_qty', $book[0]['book_qty'] - 1);
                                $this->db->where('book_id', $val1);
                                $this->db->update('tblbook');
                            
                            $result = $this->db->insert('tblbookissue', $data1);
                        } else {
                            $this->session->set_flashdata('flash_message', 'book is out of stock.');
                        }
                    }
                }
            }


//echo $this->db->last_query(); die;
            if (!$result) {
                $this->session->set_flashdata('permission_message', 'record inserted not Sucessfull');
                redirect('Adminlogin/bookissue', 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', 'record inserted  Sucessfull');
                redirect('Adminlogin/bookissue', 'refresh');
//$this->db->last_query();
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('bookissue_id', $id);
            $this->db->update('tblbookissue', $data1);
            $this->session->set_flashdata('flash_message', 'Update  sucessfully');
            redirect('Adminlogin/bookissue', 'refresh');
        } elseif ($param1 == 'delete') {

            $this->db->where('bookissue_id', $param2);
            $this->db->delete('tblbookissue');
            $this->session->set_flashdata('flash_message', 'Delete  sucessfully');
            redirect('Adminlogin/bookissue', 'refresh');
        }
    }

//for diplay bookissue previous record
    public function library_deposit() {
        $id = $this->input->post('id');

        $register = $this->db->get_where("tbllibraryregister", array('register_student' => $id))->result_array();

        $query = $this->db->get('tblgeneralsetting')->result_array();
        $security_money = $query[3]['general_value'];
        $fine_rupees = $query[4]['general_value'];
        $fine_days = $query[1]['general_value'];
        $allow_book = $query[2]['general_value'];
        $student_security_money = $register[0]['register_amount'];

        if (count($register) >= 1) {
            if ($student_security_money >= $security_money) {

                $bookk = $this->db->get_where("tblbookissue", array('bookissue_student' => $id, 'bookissue_status' => 'reserved'))->result_array();
                if (count($bookk) == 1) {

                    echo "<table style='width:430px;padding:10px;margin-left:100px'>";
                    foreach ($bookk as $bookno1) {
                        $bookname11 = $this->db->get_where("tblbook", array('book_id' => $bookno1['bookissue_no_of_book']))->result_array();
                        $dd1 = $bookno1['bookissue_date'];
                        $ab1 = $bookname11[0]['book_name'];
                        echo "<tr><th>Reserve Book</th><th>Date</th></tr><tr><td  id='tr1'>" . $ab1 . "</td><td>" . $dd1 . "</td></tr>";
                    }
                    echo "<table>";
                }

                if (count($bookk) == 1) {
                    echo "<p id='p3' style='margin-left:200px;margin-top:10px;'><b>You can't reserve more than one book.</b></p>";
                }

                $book = $this->db->get_where("tblbookissue", array('bookissue_student' => $id, 'bookissue_status' => 'issued'))->result_array();

                $sum = 0;
                if (count($book) >= 1) {
                    echo"<table style='width:430px;padding:10px;margin-left:100px'><tr><th style='width:400px'>Book Name</th><th style='width:300px'>Fine</th><th style='width:300px'>Issue Date</th></tr>";
                    foreach ($book as $bookno) {
                        $bookname1 = $this->db->get_where("tblbook", array('book_id' => $bookno['bookissue_no_of_book']))->result_array();

                        echo "<tr><td>" . $bookname1[0]['book_name'] . "</td><td>";

                        $dd = $bookno['bookissue_date'];
                        $date = str_split($dd, 8);
                        $date1 = $date[1];
                        $now = date("d");

                        $final_day = $now - $date1 . "<br>";
                        if ($final_day > $fine_days) {

                            $a = $final_day - $fine_days;
                            $b = $a * $fine_rupees;

                            echo $b . "</td><td>" . $dd . "</td></tr>";
                            $sum = $sum + $b;
                        } else {
                            echo "</td><td>" . $dd . "</td></tr>";
                        }
                    }
                    echo "</table>";
                }

                if (count($book) >= $allow_book) {
                    //".$allow_book."
                    echo "<p id='p2' style='margin-left:200px;margin-top:10px;'><b>You can't issue more than " . $allow_book . " books.</b></p>";
                }

                $issuebookno = count($book) + count($bookk);

                if ($issuebookno == $allow_book) {
                    echo "<p id='p4' style='margin-left:200px;margin-top:10px;'><b>You can't issue and reserve more than " . $allow_book . " books.</b></p>";
                }

                echo "<table style='width:300px;padding:10px;margin-left:100px;margin-top:20px'><tr><th>Others</th><th>Total</th></tr>";
                echo "<tr><td>Library Fee</td><td id='lbfee'> " . $register[0]['register_amount'] . "</td></tr>";
                echo "<tr><td>Total Fine</td><td> " . $sum . "</td></tr>";
                echo "<tr><td>Total Book</td><td id='issuebookno'> " . $issuebookno . "</td></tr>";

                $fine = $register[0]['register_amount'] - $sum;
                echo "<tr><td>Remaining Security Amount</td><td>" . $fine . "</td></tr></table>";
            } else {
                $amt = $security_money - $student_security_money;
                echo "Security amount is  not completed. Pay amount is '" . $amt . "' Rs.";
            }
        } else {
            echo "<p id='p1' style='margin-left:200px;margin-top:10px;'><b>Library fee not deposit.<b></p>";
        }
    }

    //for diplay bookissue previous record
    public function reserve_book() {
        $id = $this->input->post('id');
        $selectId = $this->input->post('selectId');
        $reserve1 = $this->db->get_where('tblbookissue', array('bookissue_status' => 'reserved', 'bookissue_no_of_book' => $id))->result_array();
        $reserve2 = $this->db->get_where('tblbookissue', array('bookissue_status' => 'issued', 'bookissue_no_of_book' => $id))->result_array();


        $no1 = count($reserve1);
        //print_r($reserve1);
        //die;

        $query = $this->db->get_where('tblbook', array('book_status' => 'StockIn', 'book_qty' => '0', 'book_id' => $id))->result_array();
        $no = $query[0]['book_qty'];
        
        if (count($query) > 0) {

            //echo "book is not available for issue.";
            $selected = "selected";
        }

        if (count($reserve1)>0) {

            // echo " book is already reserved";
            $reserve = "reserve";
        }

        echo $selected . "/" . $reserve;
    }

//for return book
    public function bookreturn_record($param1 = '') {

        if ($_POST) {
            $id = $this->input->post('student');
            //echo $id;die;
            $query = $this->db->get_where('tblbookissue', array('bookissue_status' => 'issued', 'bookissue_student' => $id))->result_array();
            //echo "<pre>";
            //print_r($query);die;
            $pagedata['page'] = 'Library Management';
            $pagedata['pagetitle'] = 'Book Return Details';
            $pagedata['data1'] = $query;
            $this->load->view('admin/header');
            $this->load->view('admin/bookreturn', $pagedata);
            $this->load->view('admin/footer');
        }
    }

//for book return
    public function bookreturn_student() {
        $id = $this->input->post('id');
        //echo $id;die;
        $data1 = $this->db->get_where('tblbookissue', array('bookissue_status' => 'issued', 'bookissue_student' => $id))->result_array();
        //print_r($data1);
        echo "<table id='datatable1' cellpadding='0' cellspacing='0' border='0' class='datatable table table-striped table-bordered table-hover'>";
        // echo "<table style='width:560px;margin-left:55px;background-color:white;margin-top: 50px;border: 3px grey solid;'>";
        echo "<thead><tr style='height:45px;'><th style='width:150px;text-align: center'>S.No.</th><th style='width:300px;'>Book Name</th><th>Date</th><th>Fine </th><th>Action</th></tr></thead><tbody>";

        $i = 1;
        if (count($data1) > 0) {
            // $i = 0;
            foreach ($data1 as $row) {
                $query = $this->db->get_where('tblbook', array('book_id' => $row['bookissue_no_of_book']))->result_array();
                //echo "<pre>"; print_r($query);
                $formaction = 'edit';
                echo "<form action='' method='post'><tr style='height:45px;'><td style='width:100px;text-align: center'>";
                echo $i;
                echo "</td><td style='width:300px;'>";
                echo $query[0]['book_name'];
                echo "</td><td><input type='text'  readonly='' id='datepicker1' name='return_date' value='";
                echo date('Y-m-d');
                echo "'></td><td><input type ='text' data-validation='number'  name='fine' id='fine" . $i . "'>";
                echo "</td><td>";
                echo "<input type='submit' class='btn btn-primary' onclick='abc($i,$row[bookissue_id],$row[bookissue_no_of_book]);' value='Return'  name='return'></td></tr></form>";
                $i++;
            }
        }
        echo "</tbody></table>";
    }

    // update record when book return
    public function bookreturn_update() {

        $fine = $this->input->post('fine');
        $bookid = $this->input->post('bookid');
        $bookno = $this->input->post('bookno');


        // die;
        $book = $this->db->get_where("tblbook", array('book_id' => $bookno))->result_array();

        $this->db->set('book_qty', $book[0]['book_qty'] + 1);
        $this->db->where('book_id', $bookno);
        $this->db->update('tblbook');


        $this->db->set('bookissue_fine', $fine);
        $this->db->set('bookissue_status', 'opened');
        $this->db->where('bookissue_id', $bookid);
        $this->db->update('tblbookissue');
        //echo $this->db->last_query();die;
        $book1 = $this->db->get_where("tblbookissue", array('bookissue_id' => $bookid))->result_array();
        //echo $book1[0]['bookissue_student'];die;
        $data1 = array(
            'bookreturn_student' => $book1[0]['bookissue_student'],
            'bookreturn_book' => $bookno,
            'bookreturn_date' => date('Y-m-d'),
            'bookreturn_status' => 'return'
        );

        $result = $this->db->insert('tblbookreturn', $data1);


        $this->session->set_flashdata('flash_message', 'Book return  sucessfully');
        redirect('Adminlogin/show_bookreturn', 'refresh');
    }

//for show  book list page
    public function show_booklist($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Book Details';
        } else {

            $pagedata['pagetitle'] = 'Book Details';
        }
        $pagedata['page'] = 'Library Management';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get_where('tblstudent', array('student_status' => 'active'))->result_array();
        //$id = $this->input->post('student');
        //echo $id;die;
        $query = $this->db->get_where('tblbookissue', array('bookissue_status' => 'issued', 'bookissue_student' => $id))->result_array();
        //echo "<pre>";
        //print_r($query);die;
        // $pagedata['data1'] = $query;
        $this->load->view('admin/header');
        $this->load->view('admin/booklist', $pagedata);
        $this->load->view('admin/footer');
    }

    //for book list
    public function booklist_student() {
        $id = $this->input->post('id');
        //echo $id;die;
        $data1 = $this->db->get_where('tblbookissue', array('bookissue_student' => $id))->result_array();
        echo "<pre>";
        //print_r($data1);die;
        echo "<table id='datatable1' cellpadding='0' cellspacing='0' border='0' class='datatable table table-striped table-bordered table-hover'>";
        // echo "<table style='width:560px;margin-left:55px;background-color:white;margin-top: 50px;border: 3px grey solid;'>";
        echo "<thead><tr style='height:45px;'><th style='width:100px;text-align: center'>S.No.</th><th style=''>Book Name</th><th style='200px'>Status</th><th>Date</th><th>Action</th></tr></thead><tbody>";

        $i = 1;
        if (count($data1) > 0) {
            // $i = 0;$disabled = "disabled='disabled'"; 
            if ($condition) {
                $disabled = "";
            }
            foreach ($data1 as $row) {
                $query = $this->db->get_where('tblbook', array('book_id' => $row['bookissue_no_of_book']))->result_array();
                //echo "<pre>"; print_r($query);
                //die;

                if ($row['bookissue_status'] == 'opened') {
                    $status = 'return';
                } else {
                    $status = $row['bookissue_status'];
                }
                echo "<form action='' method='post'><tr style='height:45px;'><td style='width:100px;text-align: center'>";
                echo $i;
                echo "</td><td style='width:300px;'>";
                echo $query[0]['book_name'];
                echo "</td><td id='status" . $i . "'>";
                echo $status;
                echo "</td><td>";
                echo $row['bookissue_date'];

                echo "</td><td>";
                if ($status == 'return' || $status == 'issued') {
                    echo "<input type='submit' class='btn btn-primary' onclick='abc($row[bookissue_id],$row[bookissue_no_of_book]);' value='Issued'  id='reserved' disabled='disabled' " . $disabled . "></td></tr></form>";
                } else {
                    echo "<input type='submit' class='btn btn-primary' onclick='abc($row[bookissue_student],$row[bookissue_id],$row[bookissue_no_of_book]);' value='Issued'></td></tr></form>";
                }
                $i++;
            }
        }
        echo "</tbody></table>";
    }

    // update record when book return
    public function reservebook_update() {


        echo $bookid = $this->input->post('bookid');
        echo $bookno = $this->input->post('bookno');
        echo $student = $this->input->post('student');


        //die;

        $bookissue = $this->db->get_where("tblbookissue", array('bookissue_student' => $student, 'bookissue_status' => 'issued'))->result_array();
        // print_r($bookissue); die;
        if (count($bookissue) == 3) {
            $this->session->set_flashdata('flash_message', 'You cannot issue this book beacuse you have already issued 3 books.');
        } else {
            $book = $this->db->get_where("tblbook", array('book_id' => $bookno))->result_array();
            if ($book[0]['book_qty'] > 0) {
                $this->db->set('book_qty', $book[0]['book_qty'] - 1);
                $this->db->where('book_id', $bookno);
                $this->db->update('tblbook');
            }

            $this->db->set('bookissue_date', date('Y-m-d'));
            $this->db->set('bookissue_status', 'issued');
            $this->db->where('bookissue_id', $bookid);
            $this->db->update('tblbookissue');
            $this->session->set_flashdata('flash_message', 'Book issue  sucessfully');
        }
        //echo $this->db->last_query();die;
        //$this->session->set_flashdata('flash_message', 'Book issue  sucessfully');
        redirect('Adminlogin/show_booklist', 'refresh');
    }

    //for show  all student book page
    public function show_allstudentbook($param1 = '', $param2 = '') {

        $pagedata['pagetitle'] = 'All Book List';

        $pagedata['page'] = 'Library Management';
        $pagedata['param1'] = $param1;
        $pagedata['data1'] = $this->db->get('tblbookissue')->result_array();
        $this->load->view('admin/header');
        $this->load->view('admin/studentallbook', $pagedata);
        $this->load->view('admin/footer');
    }

}
