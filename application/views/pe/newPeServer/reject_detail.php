<?php $userId = $this->session->userdata['login']['user_id'];
$RoleId = $this->session->userdata['login']['user_role'];
?> 
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>-->
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                        <div class="col-md-12">
                                <!-- BOX -->
                                <div class="box border green">
                                        <div class="box-title">
                                                <h4><i class="fa fa-table"></i><?php echo $pagetitle; ?></h4>
                                                <div class="tools hidden-xs">
                                                        <a href="#box-config" data-toggle="modal" class="config">
                                                                <i class="fa fa-cog"></i>
                                                        </a>
                                                        <a href="javascript:;" class="reload">
                                                                <i class="fa fa-refresh"></i>
                                                        </a>
                                                        <a href="javascript:;" class="collapse">
                                                                <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                        <a href="javascript:;" class="remove">
                                                                <i class="fa fa-times"></i>
                                                        </a>
                                                </div>
                                        </div>
                                        <div class="box-body">
                                                <table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                                        <thead>
                                                               <tr>
                                                                        <th class="center hidden-xs">S.NO</th>
                                                                        <th class="center hidden-xs">Grantee Name</th>
                                                                        <th class="center hidden-xs">Programme Name</th>
                                                                        <th class="center hidden-xs">Programme Status</th>
                                                                        <th class="center hidden-xs">Year</th>
                                                                        <th class="center hidden-xs">Document Name</th>
                                                                        <th class="center hidden-xs">PE Name</th>
                                                                        <th class="center hidden-xs">Document Status</th>
                                                                        <th class="center hidden-xs">Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                                    //print_r($data);
                                                                $i = 1;
                                                                foreach ($data as $row) {
                                                                     //$step = $row['temp_approve_id'] - '1';
                                                                    if($RoleId == '5'){
                                                                    $qry = $this->db->get_where("dms_temp_approve", array('temp_approve_id' => $row['temp_approve_id'], 'approval_sender' => $userId))->row_array();
                                                                    }
                                                                    else
                                                                    {
                                                                        $qry = $this->db->get_where("dms_temp_approve", array('temp_approve_id' => $row['temp_approve_id'], 'user_id' => $userId))->row_array();
                                                                    }
                                                                    if($qry)
                                                                    {
                                                                        $grant = $this->db->get_where('dms_grant', array('id' => $qry['grant_temp_id']))->row_array();
                                                                        $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $grant['grantee_name']))->row_array();
                                                                        $doc = $this->db->get_where('dms_document_template', array('document_temp_id' => $qry['document_temp_id']))->row_array();
                                                                        $created_by = $this->db->get_where('dms_user', array('user_id' => $grant['created_by']))->row_array();
                                                                        $cat = $this->db->get_where('dms_category', array('category_id' => $grant['category_id']))->row_array();
                                                                        $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $grant['categorytype_id']))->row_array();
                                                                
                                                            ?>
                                                                        <tr class="gradeX">
                                                                                <td class="center hidden-xs"><?php echo $i; ?></td>
                                                                                <td class="center hidden-xs"><?php echo $grantee['grantee_name']; ?></td>
                                                                                <td class="center hidden-xs"><?php echo $cat['category_name']; ?></td>
                                                                                <td class="center hidden-xs"><?php echo $status['categorytype_name']; ?></td>
                                                                                <td class="center hidden-xs"><?php echo $grant['category_year']; ?></td>
                                                                                <td class="center hidden-xs"><?php echo $doc['document_temp_name']; ?></td>
                                                                                <td class="center hidden-xs"><?php echo $created_by['user_name']; ?></td>
                                                                                 <td class="center hidden-xs"><?php echo $docstatus; ?></td>
                                                                                <td class="center hidden-xs">
<!--                                                                                    <a onclick="sks('<?php echo $row['comments']; ?>', '<?php echo $row['temp_approve_id']; ?>', '<?php echo $row['approvel_comment_id']; ?>')" style="color:green" data-toggle="modal" data-target="#approve"  href="javascript();" title="Edit Record">View Detail</a> -->
                                                                                    <a style="color:green" href="<?php echo base_url(); ?>pelogin/approval_template/<?php echo $row['temp_approve_id']; ?>/comment/<?php echo $row['approvel_comment_id']; ?>" title="detail">View Detail</a>
                                                                                </td>
        <!--                                                                            /
                                                                                <a style="color:red" href="javascript:;" title="Delete Record" onclick="confirm_modal('<?php echo base_url(); ?>adminlogin/add_user/delete/<?php echo $row['user_id']; ?>');"><i class="fa fa-trash-o fa-1x" aria-hidden="true"></i></a></td>-->
                                                                        </tr>
                                                                <?php $i++; } } ?>
                                                                
                                                               
                                                        </tbody>
                                                        <tfoot>
                                                                <tr>
                                                                       <th class="center hidden-xs">S.NO</th>
                                                                        <th class="center hidden-xs">Grantee Name</th>
                                                                        <th class="center hidden-xs">Category Name</th>
                                                                        <th class="center hidden-xs">Category Status</th>
                                                                        <th class="center hidden-xs">Year</th>
                                                                        <th class="center hidden-xs">Document Name</th>
                                                                        <th class="center hidden-xs">PE Name</th>
                                                                        <th class="center hidden-xs">Document Status</th>
                                                                        <th class="center hidden-xs">Action</th>
                                                                </tr>
                                                        </tfoot>
                                                </table>
                                        </div>
                                </div>
                                <!-- /BOX -->
                        </div>
                </div>
                <!-- /DASHBOARD CONTENT -->
                
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>

<!-- Modal -->
  <div class="modal fade" id="approve" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $reasion; ?></h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/approval_statusChange/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" id="comm" style="margin-bottom:10px">

                        
                    
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a class="btn btn-default" data-dismiss="modal" style="color:green" data-toggle="modal" data-target="#reply"  href="javascript();" title="Edit Record">Reply</a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="modal fade" id="reply" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $reasion; ?></h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/approval_reaply/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Comment : </label>
                        <div class="col-md-5">
                            <input type="hidden" class="form-control" 
                                   name="userid" value="<?php echo $userId; ?>">
                           
                            <input type="hidden" class="form-control" 
                                   name="comment_id" id="comment_id" value=""> 
                            <input type="hidden" class="form-control" 
                                   name="approval_id" id="approval_id" value=""> 
                            <input type="hidden" class="form-control" 
                                   name="status" value="Rejected"> 
<!--                            <input type="hidden" class="form-control" 
                                   name="doc_id" value="<?php echo $doc; ?>"> -->

                            <textarea rows="3" cols="6" class="form-control" 
                                        data-validation="length" 
                                        data-validation-length="min1" 
                                        data-validation-error-msg="Description is required"
                                        placeholder="Enter Description"
                                        name="comment"></textarea>                                                                                          
                        </div>
                        </br>
                        <div class="control-label col-md-4" style="text-align: right;">
                            <div style="text-align: right;">

                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                    value="Validate"   name="submit" onclick="return chkpass();">Send</button>
                            </div>
                        </div>
                    
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<script>
    function sks(e, apId, commentId)
    {
       // alert(apId);
        
        $("#approval_id").val(apId);
        $("#comment_id").val(commentId);
        $.ajax({
            url: "<?php echo base_url(); ?>pelogin/allCommentbyapprove",
            type: "POST",
            data: {'id': apId},
            success: function (response)
            {
                //alert(response);
                $("#comm").html(response);
//                $("#state").html(response);
//                $("#addstatemore").modal('hide');

            }

        });
    }
    

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );


//$(function () {   
//    $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd'}).val();
// $("#datepicker1").datepicker({
//     dateFormat: 'yy-mm-dd',
//     minDate: new Date(),
//    
//});
//});


</script>
<script type="text/javascript">
$(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
</script>