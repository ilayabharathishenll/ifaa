<?php 
    error_reporting(~E_NOTICE);
    //print_r($parent_img); die;
    $userPermission = $this->session->userdata['login']['user_permission'];
    $grantee_name = $this->db->get_where("dms_grantee", array('grantee_id' => $result['grantee_name']))->row_array();
    $language = $this->db->get("dms_language")->result_array();
    $category = $this->db->get_where("dms_category", array('category_id' => $result['category_id']))->row_array();
    $status = $this->db->get_where("dms_categorytype", array('categorytype_id' => $result['categorytype_id']))->row_array();
    $key = explode(",", $parent['parent_keyword']);
    $roleId = $this->session->userdata['login']['user_role'];
    $document = $this->db->get_where("dms_document_template", array('document_temp_id' => $doc))->row_array();
    
    $Assi_doc = $this->db->get_where("dms_assin_template", array('document_temp_id' => $doc, 'termination' => 'enable'))->row_array();
    
    if($parent) 
    {
        //$i =0;
        $meta = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc))->result_array();
        $meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'General Information'))->result_array();
        $meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'Author, Publisher, Access'))->result_array();
        $meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'Physical Characteristics'))->result_array();
        $meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'Digitisation Details'))->result_array();
        $meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'Others'))->result_array();
    }
    
    
    ?> 

<div class="headone">
    <h4><?php echo $document['document_temp_name']; ?></h4>
</div>
<div class="boxtemplateagain" style="background: #fff;">
    <div  class="col-md-12" style="text-align: right; margin: 10px 0;">
        <?php if($text_editor == 'enable') { ?>
<!--            <button  style="margin-right:5px;"class="btn btn-primary start" onclick="myFunction()">Print Description</button>-->
        
        <?php }
            if($Assi_doc)
            { ?>
                
                    <button type="button" class="btn btn-primary start" 
                            value="Validate" id="parent_sub"   name="submit" onclick="terminate_flow('<?php echo $grant; ?>','<?php echo $doc; ?>','<?php echo $parent['parent_temp_id']; ?>');"> Termination Button</button>
               
        <?php
        
            }
        ?> 
    </div>
      
      
                                   
    <table class="datatable table table-striped table-bordered table-hover templatetable">
        <thead>
        <tr>
            <th>Program and Initiatives : </th>
            <th>Grant Status : </th>
            <th>Grant ID : </th>
            <th>Grantee name : </th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php echo $category['category_name']; ?></td>
                <td><?php echo $status['categorytype_name']; ?></td>
                <td><?php echo $result['grant_number'] ?> </td>
                <td> <?php echo $grantee_name['grantee_name'] ?> </td>
            </tr>

        </tbody>
    </table>
                                            
                                      
      
    <div class="padingagain">
        <?php if($parent['parent_status'] == 'Approved'){ ?>
        <form role="form" action="<?php echo base_url(); ?>pelogin/update_parent_template/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
        <?php }else { ?> 
         <form role="form" action="<?php echo base_url(); ?>pelogin/add_parent_template/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">   
        <?php }  ?>
                                           
                                           
            <div class="row" style="margin-bottom:10px">
                <div class="col-md-12">
                    
                    <input type="hidden" class="form-control" 
                               name="hidden_id" value="<?php echo $parent['parent_temp_id']; ?>">  
                        <input type="hidden" class="form-control" 
                               name="grant_id" value="<?php echo $grant; ?>"> 
                        <input type="hidden" class="form-control" 
                               name="doc_id" value="<?php echo $doc; ?>">
                        
                        
                        <input type="hidden" class="form-control" 
                               name="aid" value="<?php echo $aid; ?>"> 
                        <input type="hidden" class="form-control" 
                               name="pid" value="<?php echo $pid; ?>">
                        <input type="hidden" class="form-control" 
                               name="liid" value="<?php echo $liid; ?>"> 
                    
                    </div>
                </div>
             <?php $copytype = explode(".",$parent['parent_scann_copy']);
             $addtionaltype = explode(".",$parent['parent_additional_detail']);
             ?>
           
<!--                <div class="row" style="margin-bottom:10px">
                    <div class="col-md-12">
                        <div class="box-title">
                            <h4>Upload Scanned Copy</h4>
                            <div class="tools hidden-xs">

                            </div>
                        </div>

                            <label class="control-label col-md-3">Add Files Here : </label>
                            <div class="col-md-3">

                                <input name="scanned_image[]" type="file"
                                    class="file-input" style="font-size:11px;" value="<?php echo $result['user_image']; ?>" >                                                                                  
                            </div>
                            <div class="col-md-3">
                                <?php if($copytype['1'] == 'pdf' OR $copytype['1'] == 'docx' OR $copytype['1'] == 'doc'){ ?>
                                <a href="<?php echo base_url()."uploads/".$parent['parent_scann_copy']; ?>" target="_blank"><img style=" width: 30%; height: 30%" src="<?php echo base_url(); ?>front/images/pdf.png"></a>
                                <?php } else{ ?>
                                <img  class="img-responsive" src="<?php echo base_url()."uploads/".$parent['parent_scann_copy']; ?>" style="height:90px;width:120px;" alt=""/>
                                <?php } ?>
                            </div>
                            
                    </div>
                </div>-->
             
                

                 
                                             
<!--                <div class="col-md-12">
                    <div class="box-title">
                        <h4>Upload Additional Detail</h4>
                        <div class="tools hidden-xs">

                        </div>
                    </div>
                </div>
           
            
                <div class="row" style="margin-bottom:10px">
                    <label class="control-label col-md-3">Add Files Here : </label>
                    <div class="col-md-3">

                        <input name="detail_image" type="file"
                            class="file-input" style="font-size:11px;" value="<?php echo $parent['user_image']; ?>" >                                                                                  
                    </div>
                    <div class="col-md-3">
                        
                </div>-->
                

                <div class="row" style="margin-bottom:10px">
                    <?php  if (in_array("edit_grant", $userPermission)){ ?>
                    <div class="control-label col-md-4" id="stat_valid" style="text-align: right; <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
<!--                        <div style="text-align: right;  <?php if($parent['parent_status'] == 'Completed' OR $parent['parent_status'] == 'Approved' OR $parent['parent_status'] == 'Terminate'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">

                            <button type="submit" class="btn btn-primary start" style="width:120px" 
                                    value="Validate"  name="submit">Update File</button>
                        </div>-->
                        <div style="text-align: right;  <?php //if($parent['parent_status'] == 'Completed' OR $parent['parent_status'] == 'Approved' OR $parent['parent_status'] == 'Terminate'){ ?> <?php //} ?>">
                           <?php  if (in_array("add_grant", $userPermission)){ ?>
                            <button type="submit" class="btn btn-primary start" style="width:120px" 
                                    value="Validate" id="parent_sub"   name="submit" onclick="return chkpass();">Save Detail</button>
                              <?php  } ?>
                        </div>
                            <?php  if($doc!='1'){ //echo $skip."/".$stat."/".$parent['parent_status']; ?>
                        <div style="text-align: right; margin-top: 10px;">

                            <button type="button" class="btn btn-primary start"  <?php if($skip == "false" OR $stat == "false" OR $parent['parent_status'] == 'Completed' OR $parent['parent_status'] == 'Approved'OR $parent['parent_status'] == 'Terminate'){ ?> disabled style="width:120px; background-color: gray; border-color: gray;" <?php }else{ ?> style="width:120px;" <?php } ?>
                                  name="proceed" onclick="procid('<?php echo $parent['parent_temp_id']; ?>');"> Proceed </button>
                        </div>
                            <?php  } ?>
                        <?php if($doc=='44'){ ?>
                        <div style="text-align: right; margin-top: 10px;">

                            <button type="button" class="btn btn-primary start" style="width:136px" 
                                     <?php if($skip == "false" OR $stat == 'false' OR $parent['parent_status'] == 'Completed' OR $parent['parent_status'] == 'Pending' OR $parent['parent_status'] == NULL OR $parent['parent_status'] == 'Terminate'){ ?> disabled <?php } ?>
                                  name="completestatus" onclick="cmpstatus('<?php echo $grant; ?>');"> Completed Status </button>
                        </div>
                            <?php } ?>
                        <div style="text-align: right; margin-top: 10px;">
                <?php  if (in_array("add_grant", $userPermission)){ ?>
                            <button type="button" class="btn btn-primary start" style="width:120px" 
                                    onclick="skiptemp('<?php echo $grant; ?>','<?php echo $doc; ?>');" name="skip">Skip Work flow</button>
                        <?php  } ?>
                        </div>
                        
                    </div>
                    <?php  } ?>
                </div>
                                            
                                            
            </form>
            
      </div>
    </div>
</div>

<script>
function myFunction() {
    //window.print('adadad');
    //htmlspecialchars_decode(stripslashes($data['page_description']));
    var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById('chk').value;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
        window.location='<?php echo $_SERVER[HTTP_REFERER]; ?>';
         return false;
}
//function completestatus(e)
//        {
//            alert(e);
//            $.ajax({
//                    url: "<?php echo base_url(); ?>pelogin/completestatus",
//                    type: "POST",
//                    data: {'id': e,},
//                    success: function (response)
//                    {
//                        alert(response);
//                            //$("#demo").html(response);
//                            //$('#stat_valid').hide();
//                        
//                    }
//
//                });
//        }
</script>
                               