<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
$grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $grant['grantee_name']))->row_array();

?>
<style>
   .multiselect-native-select .btn-group
   {
       width:100%;
       display:block;
   }
   .multiselect-native-select .btn-group .multiselect
   {
       width:100%;
   }
</style>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                <div style="text-align: right;">
<?php if($param2 == 'comment'){ ?>
                    <a class="btn btn-primary start" onclick="sks('<?php echo $row['comments']; ?>', '<?php echo $param1; ?>', '<?php echo $param3; ?>')" style="width:120px;float:right;margin:10px 5px;" data-toggle="modal" data-target="#approve_com"  href="javascript();" title="Edit Record">View Comments</a>
<?php } else{ ?>
                    <button type="button" class="btn btn-primary start"  data-toggle="modal" data-target="#approve" style="width:120px;float:right;margin:10px 5px;">Approve</button>
                    <button type="button" class="btn btn-primary start"  data-toggle="modal" data-target="#reject" style="width:120px;float: right;margin:10px 5px;" >Reject</button>
<?php } ?>
                </div>
                </div>
                    </div>
               

                   
                
                
                
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row ">
                    
<!--                    <div class="col-md-3 paddinglake">
                                <div class="boxtemplate">
                                    <div class="headone">
                                        <h4>Category </h4>
                                    </div>
                                    <ul>
                                        <?php $i = '1';
                                        foreach($cat as $value){ ?>
                                        <li class="dropdown-toggle" id="dropdownMenu<?php echo $i; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <?php echo $value['category_name'];?>
                                            <ul class="advancecontent" id="advancecontent<?php echo $i; ?>">
                                                <?php 
                                                $this->db->group_by('status');
                                                $sta = $this->db->get_where('dms_cat_year', array('category_id' => $value['category_id']))->result_array();
                                                  if($sta) {
                                                      $j = '1';
                                                      foreach($sta as $val)
                                                      {
                                                    $staus = $this->db->get_where('dms_categorytype', array('categorytype_id' => $val['status']))->row_array();
                                                    
                                                ?>
                                                <li id="dropdownsecond<?php echo $i.$j; ?>" > 
                                                <?php echo $staus['categorytype_name']; ?>
                                                <ul>
                                                     <?php
                                                        $year = $this->db->get_where('dms_cat_year', array('status' => $val['status'], 'category_id' => $value['category_id'] ))->result_array();
                                                       if($year) {
                                                      foreach($year as $val)
                                                      {
                                                        ?>
                                                    <li> <?php echo $val['year']; ?>
                                                    
                                                        <ul>
                                                            <?php
                                                            $grant = $this->db->get_where('dms_grant', array('categorytype_id' => $staus['categorytype_id'], 'category_year' => $val['year'], 'category_id' => $value['category_id'] ))->result_array();
                                                            if($grant) {
                                                            foreach($grant as $grnt)
                                                            {
                                                                $staus = $this->db->get_where('dms_grantee', array('grantee_id' => $grnt['grantee_name']))->row_array();
                                                            ?>
                                                            <li>
                                                                <a href="javascript:;" onclick="template('<?php echo $grnt['id']; ?>')"><?php echo $staus['grantee_name']; ?></a>
                                                                </li>
                                                            <?php } } ?>
                                                        </ul>
                                                    
                                                    </li>
                                                       <?php } } ?>
                                                    
                                                </ul>
                                                
                                                </li>
                                        <?php $j++; } }?>
                                            </ul>
                                        
                                        </li>
                                        <?php $i++; } ?>
                                        
                                    </ul>
                                    <input type="hidden" class="form-control" id="ccc" 
                                               name="ccc" value=""> 
                                    <input type="hidden" class="form-control"  id="ddd"
                                               name="ddd" value=""> 
                                    
                                </div>
                    
                    </div>-->
                   
                    <input type="hidden" class="form-control" id="ccc" 
                                               name="ccc" value=""> 
                                    <input type="hidden" class="form-control"  id="ddd"
                                               name="ddd" value=""> 
                    <div class="col-md-3 paddinglake">
                                <div class="boxtemplateagain">
                                    <div class="headone">
                                        <h4>Document </h4>
                                    </div>
                                    <ul>
                                        <?php foreach($doc as $value){ ?>
                                        <li> <i class="fa fa-caret-right" aria-hidden="true"></i>
                                            <a href="javascript:;" onclick="documents('2','<?php echo $value['document_temp_id']; ?>')"><?php  echo $value['document_temp_name'];?></a>
                                            <ul>
                                                <?php 
                                                //$this->db->group_by('status');
                                                $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $value['document_temp_id']))->result_array();
                                                  if($sta) {
                                                      foreach($sta as $val)
                                                      {
                                                    //$staus = $this->db->get_where('dms_categorytype', array('categorytype_id' => $val['status']))->row_array();
                                                    
                                                ?>
                                                <li><i class="fa fa-caret-right" aria-hidden="true"></i> 
                                                    <a href="javascript:;" onclick="documents('3','<?php echo $val['document_temp_id']; ?>')"><?php echo $val['document_temp_name']; ?></a>
                                                <ul>
                                                    <?php
                                                      $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $val['document_temp_id']))->result_array();
                                                      if($sta) {
                                                      foreach($sta as $val)
                                                      {
                                                        ?>
                                                    <li><i class="fa fa-caret-right" aria-hidden="true"></i>
                                                        <a href="javascript:;" onclick="documents('4','<?php echo $val['document_temp_id']; ?>')"><?php echo $val['document_temp_name']; ?></a>
                                                        
<!--                                                        <ul>
                                                            <?php
                                                              $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $val['document_temp_id']))->result_array();
                                                              if($sta) {
                                                              foreach($sta as $val)
                                                              {
                                                                ?>
                                                            <li><i class="fa fa-caret-right" aria-hidden="true"></i>
                                                                <a href="javascript:;" onclick="documents()"><?php echo $val['document_temp_name']; ?></a>

                                                            </li>
                                                               <?php } } ?>

                                                        </ul>-->
                                                    
                                                    </li>
                                                       <?php } } ?>
                                                    
                                                </ul>
                                                
                                                </li>
                                        <?php } }?>
                                            </ul>
                                        
                                        </li>
                                        <?php } ?>
                                        
                                    </ul>
                                    
                                    
                                </div>
                    
                    </div>
                      
                      <div class="col-md-9 paddinglake">
                        
                                <!-- BASIC -->
<!--                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo 'Final Praposal'; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <div id="demo">
                
                                        </div>
                                       
                                    </div>
                                </div>-->
                         <div class="boxtemplateagain">
                                    <div class="headone">
                                        <h4><?php echo $grantee['grantee_name']; ?></h4>
                                    </div>
                             
                             
                             <div id="demo">
                
                                        </div>
                             
                             
                             
                             
                             
                         </div>


                        
                    </div>
                </div>
                
                
<!--                <div id="demo">
                
            </div>-->
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>
    
  <!-- Trigger the modal with a button -->
<!--  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

  <!-- Modal -->
  <div class="modal fade" id="approve" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Approval Form</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/approval_statusChange/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Description </label>
                        <div class="col-md-5">

                            <input type="hidden" class="form-control" 
                                   name="id" value="<?php echo $data['temp_approve_id']; ?>"> 
                            <input type="hidden" class="form-control" 
                                   name="status" value="Approved"> 
<!--                            <input type="hidden" class="form-control" 
                                   name="doc_id" value="<?php echo $doc; ?>"> -->

                            <textarea rows="3" cols="6" class="form-control" 
                                        placeholder="Enter Description"
                                        name="description"></textarea>                                                                                          
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="submit" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="return chkpass();">Approve</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
  <div class="modal fade" id="reject" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Rejection Form</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/approval_statusChange/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Description </label>
                        <div class="col-md-5">

                            <input type="hidden" class="form-control" 
                                   name="id" value="<?php echo $data['temp_approve_id']; ?>">  
                            <input type="hidden" class="form-control" 
                                   name="status" value="Rejected"> 
 <!--                            <input type="hidden" class="form-control" 
                                   name="doc_id" value="<?php echo $doc; ?>"> -->

                            <textarea rows="3" cols="6" class="form-control"
                                      placeholder="Enter Description"
                                      name="description"></textarea>                                                                                          
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="submit" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="return chkpass();">Reject</button>
                        </div>
                    </div>
                </div>
            </form>
              
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
    <div class="modal fade" id="approve_com" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $reasion; ?></h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/approval_statusChange/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" id="comm" style="margin-bottom:10px">

                        
                    
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a class="btn btn-default" data-dismiss="modal" style="color:green" data-toggle="modal" data-target="#reply"  href="javascript();" title="Edit Record">Reply</a>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="modal fade" id="reply" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $reasion; ?></h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/approval_reaply/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Comment : </label>
                        <div class="col-md-5">
                            <input type="hidden" class="form-control" 
                                   name="userid" value="<?php echo $userId; ?>">
                           
                            <input type="hidden" class="form-control" 
                                   name="comment_id" id="comment_id" value=""> 
                            <input type="hidden" class="form-control" 
                                   name="approval_id" id="approval_id" value=""> 
                            <input type="hidden" class="form-control" 
                                   name="status" value="Rejected"> 
<!--                            <input type="hidden" class="form-control" 
                                   name="doc_id" value="<?php echo $doc; ?>"> -->

                            <textarea rows="3" cols="6" class="form-control" 
                                        placeholder="Enter Description"
                                        name="comment"></textarea>                                                                                          
                        </div>
                        </br>
                        <div class="control-label col-md-4" style="text-align: right;">
                            <div style="text-align: right;">

                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                    value="Validate"   name="submit" onclick="return chkpass();">Send</button>
                            </div>
                        </div>
                    
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  

    <script>

        $(window).load(function() {
            var e = "<?php echo $grant['id']; ?>";
            $("#ccc").val(e);
           var temp = '1';
        
                $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/template",
                    type: "POST",
                    data: {'id': e, 'temp': temp},
                    success: function (response)
                    {
                        $("#demo").html(response);
                        abss();
                        
                        //alert(response);

                    }

                });
                
            
        });
        
    function sks(e, apId, commentId)
    {
        //alert(apId);
        
        $("#approval_id").val(apId);
        $("#comment_id").val(commentId);
        $.ajax({
            url: "<?php echo base_url(); ?>pelogin/allCommentbyapprove",
            type: "POST",
            data: {'id': apId},
            success: function (response)
            {
                //alert(response);
                $("#comm").html(response);
//                $("#state").html(response);
//                $("#addstatemore").modal('hide');

            }

        });
    }
    </script>

    <script>   
        function year()
        {
            var cat = document.getElementById('e3').value;
            var id = document.getElementById('status').value;
           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/yearDropdown",
                    type: "POST",
                    data: {'status': id, 'category': cat},
                    success: function (response)
                    {
                        $("#years").html(response);

                    }

                });
            }
        }
        
        function template(e)
        {
            //alert(e);
           //var temp = document.getElementById('temp_type').value;
           $("#ccc").val(e);
           var temp = '1';
           if(temp=='')
           {
               alert("PLZ Select template");
           }
           
           // var id = document.getElementById('status').value;
           if(temp){
                $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/template",
                    type: "POST",
                    data: {'id': e, 'temp': temp},
                    success: function (response)
                    {
                        $("#demo").html(response);
                        
                        //alert(response);

                    }

                });
            }
        }
        
        function documents(e,valu)
        {
            //alert(e);
            //var id = '1';
            $("#ddd").val(valu);
            var id = document.getElementById('ccc').value;
            if(id=='')
            {
                alert("PLZ select Grantee");
                return false;
            }
//            $("#ccc").val(e);
//             $("#demo").html("response");
             $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/all_template",
                    type: "POST",
                    data: {'id': id, 'temp': e, 'doc': valu},
                    success: function (response)
                    {
                        //alert(response);
                        if(response=='no')
                        {
                            alert('Plese compleate previous step');
                        }
                        else
                        {
                        //alert(response);
                            $("#demo").html(response);
                            abss();
                        }
                        
                        //$("#ddd").val(e);
                        

                    }

                });
        }
        function abss(){
        $('.lstFruits').multiselect({
                includeSelectAllOption: true
            });
             $('#btnSelected').click(function () {
                var selected = $("#lstFruits option:selected");
                var message = "";
                selected.each(function () {
                    message += $(this).text() + " " + $(this).val() + "\n";
                });
                alert(message);
            });
        //alert();
        $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        "onSelect": function () {
//            var input = $(this);
//            $("#datepicker2").datepicker("option", "minDate", input.datepicker("getDate"));
//            $("#datepicker2").datepicker("refresh");
            var start = $('#datepicker1').datepicker('getDate');
            
            var day = start.getDate();
            var month = start.getMonth() + 6;
            var year = start.getFullYear();
            if (month < 10) {
                month = "0" + month;
            }
            if (day < 10) {
                day = "0" + day;
            }

            var sss = year + "-" + month + "-" + day;

            $('#end_date').val(sss);
            
        }
        
    });
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker3').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    CKEDITOR.replace( 'chk' );
    
}
        
        function procid(e)
        {
            //alert(e);
            $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/change_temp_status",
                    type: "POST",
                    data: {'id': e,},
                    success: function (response)
                    {
                        alert(response);
                            //$("#demo").html(response);
                            $('#parent_sub').hide();
                        
                    }

                });
        }
        
        
        function sss(id, e)
        {
            //alert(e);
            $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/getuser",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                        $("#user"+e).html(response);
                        //alert(response);

                    }

                });
        }
        
        

</script>
<script>
    $(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
//});
//
//$(function() {
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
    
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });

</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function contect()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>
    
    <script>
window.rowCounts = 1;
    function addMoredetail(frm, allowbook) {
//$('.add').click(function (event) {

         rowCounts++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCounts + '"><td><input type="text" class="form-control" placeholder="Enter Document Id"  name="document_id[]"> </td><td><input type="text" class="form-control" placeholder="Enter Handling Staff"  name="handling_staff[]"></td><td><input type="text" class="form-control" placeholder="Enter Creator/Auther"  name="auther[]"></td><td><input type="text" class="form-control" placeholder="Enter Copyright"  name="copyright[]"></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCounts + ');">Delete</a></div></td>';


            jQuery('#addeddetail').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>
    
<script>
window.rowCount = 1;
    function addMoreuser(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCount + '"><td><select name="group[]" class="form-control" onchange="sss(this.value, ' + rowCount + ')"><option>Select Group</option><?php $grp = $this->db->get('dms_role')->result_array(); foreach ($grp as $row) { ?><option value="<?php echo $row['role_id']; ?>" <?php if ($result['role_id'] == $row['role_id']) { echo "selected";}?> ><?php echo $row['role_name']; ?></option><?php } ?></select></td><td><select name="user[]" id="user' + rowCount + '" class="form-control"><option>Select User</option></select></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></td>';


            jQuery('#addedflow').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>

<script>
window.rowCount = 1;
    function addkeyRows(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<div class="row" id="rowCount' + rowCount + '" style="margin-top:5px;"><label  class="control-label col-md-3"><span style="visibility:hidden;">Keywords</span></label><div class="col-md-5"><input type="text" class="form-control mng" id="i'+ rowCount +'" name="keyword[]"  value="" placeholder="Enter Keywords" ></div><div class="col-md-4"><a href="javascript:void(0);" style="font-size: 11px;padding: 5px;" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div>';


            jQuery('#addedkey').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>
<script>
 //1111111   
    
  $(document).ready(function () {
    $("li[id*=dropdownMenu1] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu1').click(function(){
    if(abc == 'on'){
        $('#advancecontent1').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent1').show(); 
        abc = 'on'; 
   }
     });
     
//22222222222222222

      $(document).ready(function () {
    $("li[id*=dropdownMenu2] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu2').click(function(){
    if(abc == 'on'){
        $('#advancecontent2').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent2').show(); 
        abc = 'on'; 
   }
     }); 
     
//33333333333333333333333333333

      $(document).ready(function () {
    $("li[id*=dropdownMenu3] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu3').click(function(){
    if(abc == 'on'){
        $('#advancecontent3').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent3').show(); 
        abc = 'on'; 
   }
     }); 
     
     //444444444444444444444444444444
     
     
      $(document).ready(function () {
    $("li[id*=dropdownMenu4] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu4').click(function(){
    if(abc == 'on'){
        $('#advancecontent4').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent4').show(); 
        abc = 'on'; 
   }
     }); 
     
     //5555555555555555555
     
      $(document).ready(function () {
    $("li[id*=dropdownMenu5] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu5').click(function(){
    if(abc == 'on'){
        $('#advancecontent5').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent5').show(); 
        abc = 'on'; 
   }
     }); 
     
     //6666666666666666666666
     
      $(document).ready(function () {
    $("li[id*=dropdownMenu6] ul").click(function() {
     
          abc = 'off'; 

    });
});
 var abc = 'on'; 
   $('#dropdownMenu6').click(function(){
    if(abc == 'on'){
        $('#advancecontent6').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent6').show(); 
        abc = 'on'; 
   }
     }); 
     
     //7777777777777777777777777
     
      $(document).ready(function () {
    $("li[id*=dropdownMenu7] ul").click(function() {
     
          abc = 'off'; 

    });
});
 var abc = 'on'; 
   $('#dropdownMenu7').click(function(){
    if(abc == 'on'){
        $('#advancecontent7').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent7').show(); 
        abc = 'on'; 
   }
     }); 
     
     
     
     
     
    
//    $('#dropdownMenu2').click(function(){
//  
//    $('#advancecontent2').toggle();
//    });
//    
//    $('#dropdownMenu3').click(function(){
//  
//    $('#advancecontent3').toggle();
//    });
    
//    $('#dropdownMenu4').click(function(){
//  
//    $('#advancecontent4').toggle();
//    });
//    
//    $('#dropdownMenu5').click(function(){
//  
//    $('#advancecontent5').toggle();
//    });
//    
//    $('#dropdownMenu6').click(function(){
//  
//    $('#advancecontent6').toggle();
//    });
//    
//    $('#dropdownMenu7').click(function(){
//  
//    $('#advancecontent7').toggle();
//    });
    
    
    </script>





