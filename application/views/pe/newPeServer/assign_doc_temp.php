<?php
error_reporting(~E_NOTICE);
$userId = $this->session->userdata['login']['user_id'];
$userPermission = $this->session->userdata['login']['user_permission'];
//if ($param1) {
//    
//    $result = $this->db->get_where('tblteacher', array('teacher_id' => $param1))->row_array();
//    
//    $earning = explode(",",$result['teacher_earning']);
//    $earning_amount = explode(",",$result['teacher_earningAmount']);
//    $deduction = explode(",",$result['teacher_deduction']);
//    $deduction_amount = explode(",",$result['teacher_deductionAmount']);
//    $tax = explode(",",$result['teacher_tax']);
//    $tax_amount = explode(",",$result['teacher_taxAmount']);
//
//    $formaction = 'edit';
//} else {
//    $formaction = 'create';
//}
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('pelogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php  if (in_array("add_comment", $userPermission)){ ?>
                <div class="row" id="somment_div" style="display: none;">
                    <div class="col-md-12">
                        <div style="text-align: right;">

                            <button type="button" class="btn btn-primary start"  data-toggle="modal" data-target="#approve" style="width:120px;float:right;margin:10px 5px;">Comment</button>
<!--                            <button type="button" class="btn btn-primary start"  data-toggle="modal" data-target="#reject" style="width:120px;float: right;margin:10px 5px;" >Reject</button>-->
                        </div>
                    </div>
                </div>
                <?php } ?>
                
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                
                
                
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row ">
                    
                    <div class="col-md-3 paddinglake">
                                <div class="boxtemplate">
                                    <div class="headone">
                                        <h4>Category </h4>
                                    </div>
                                    <ul>
                                        
                                        <?php
                                        //$grn_ID[] = '';
                                       foreach ($data_cat as $catego)
                                       {
                                           $grant_ID[] .= $catego['grant_temp_id'];
                                          $gnt = $this->db->get_where('dms_grant', array('id' => $catego['grant_temp_id']))->row_array();
                                           $cat_ID[] .= $gnt['category_id'];
//                                          $this->db->group_by('category_id');
//                                          $cat = $this->db->get_where('dms_category', array('category_id' => $gnt['category_id']))->result_array();
                                         // print_r($cat);
                                       }
                                        $this->db->group_by('category_id');
                                        $this->db->where_in('category_id', $cat_ID);
                                        $cat = $this->db->get('dms_category')->result_array();
                                       // echo $this->db->last_query();
                                       //print_r($grn_ID);
                                        $i = '1';
                                        foreach($cat as $value){ ?>
                                        <li class="dropdown-toggle" id="dropdownMenu<?php echo $i; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <?php echo $value['category_name'];?>
                                            <ul class="advancecontent" id="advancecontent<?php echo $i; ?>">
                                                <?php 
                                                $this->db->group_by('status');
                                                
                                                $sta = $this->db->get_where('dms_cat_year', array('category_id' => $value['category_id']))->result_array();
                                                  if($sta) {
                                                      $j = '1';
                                                      foreach($sta as $val)
                                                      {
                                                    $staus = $this->db->get_where('dms_categorytype', array('categorytype_id' => $val['status']))->row_array();
                                                    
                                                ?>
                                                <li id="dropdownsecond<?php echo $i.$j; ?>" > 
                                                <?php echo $staus['categorytype_name']; ?>
                                                <ul>
                                                     <?php
                                                        $this->db->group_by('year');
                                                        $year = $this->db->get_where('dms_cat_year', array('status' => $val['status'], 'category_id' => $value['category_id'] ))->result_array();
                                                       if($year) {
                                                      foreach($year as $val)
                                                      {
                                                        ?>
                                                    <li> <?php echo $val['year']; ?>
                                                    
                                                        <ul>
                                                            <?php
                                                            $grant = $this->db->get_where('dms_grant', array('categorytype_id' => $staus['categorytype_id'], 'category_year' => $val['year'], 'category_id' => $value['category_id']))->result_array();
                                                            if($grant) {
                                                            foreach($grant as $grnt)
                                                            {
                                                               if($grant_ID) {
                                                                if(in_array($grnt['id'], $grant_ID)) 
                                                                {
                                                                    $staus = $this->db->get_where('dms_grantee', array('grantee_id' => $grnt['grantee_name']))->row_array();
                                                                    ?>
                                                                    <li>
                                                                        <a href="javascript:;" onclick="template('<?php echo $grnt['id']; ?>')"><?php echo $staus['grantee_name']; ?></a>
                                                                    </li>
                                                            <?php } } } } ?>
                                                        </ul>
                                                    
                                                    </li>
                                                       <?php } } ?>
                                                    
                                                </ul>
                                                
                                                </li>
                                        <?php $j++; } }?>
                                            </ul>
                                        
                                        </li>
                                        <?php $i++; }  ?>
                                        
                                    </ul>
                                    <input type="hidden" class="form-control" id="ccc" 
                                               name="ccc" value=""> 
                                    <input type="hidden" class="form-control"  id="ddd"
                                               name="ddd" value=""> 
                                    
                                </div>
                    
                    </div>
                   
                    
                    <div class="col-md-3 paddinglake">
                                <div class="boxtemplateagain">
                                    <div class="headone">
                                        <h4>Document </h4>
                                    </div>
                                    <div id="docDiv">
                                        
                                    </div>
                                    
                                    
                                </div>
                    
                    </div>
                      
                      <div class="col-md-6 paddinglake">
                        
                                <!-- BASIC -->
<!--                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo 'Final Praposal'; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <div id="demo">
                
                                        </div>
                                       
                                    </div>
                                </div>-->
                         <div class="boxtemplateagain">
                                    <div class="headone">
                                        <h4>Process Document </h4>
                                    </div>
                             
                             
                             <div id="demo">
                
                                        </div>
                             
                             
                             
                             
                             
                         </div>


                        
                    </div>
                </div>
          <!-- Modal -->
  <div class="modal fade" id="approve" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Comment</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/user_comment/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Comment <span style="color: red;margin-left: 1px">*</span></label>
                        <div class="col-md-5">

                            <input type="hidden" class="form-control" id="ccc" 
                                               name="grant" value=""> 
                                    <input type="hidden" class="form-control"  id="ddd"
                                               name="document" value=""> 
<!--                            <input type="hidden" class="form-control" 
                                   name="doc_id" value="<?php echo $doc; ?>"> -->

                            <textarea rows="3" cols="6" class="form-control" 
                                        data-validation="length" 
                                        data-validation-length="min1" 
                                        data-validation-error-msg="Description is required"
                                        placeholder="Enter Description"
                                        name="comment"></textarea>                                                                                          
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="submit" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="return chkpass();">Comment</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>

    <script>   
        function year()
        {
            var cat = document.getElementById('e3').value;
            var id = document.getElementById('status').value;
           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/yearDropdown",
                    type: "POST",
                    data: {'status': id, 'category': cat},
                    success: function (response)
                    {
                        $("#years").html(response);

                    }

                });
            }
        }
        
        function template(e)
        {
           $("#ccc").val(e);
           selectCategory();
           $("#demo").hide();
        
        }
        
        function selectCategory()
        {
            //alert("aaa");
            var grant = document.getElementById('ccc').value;
            $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/selectCategory",
                    type: "POST",
                    data: {'grant': grant,},
                    success: function (response)
                    {
                        $("#docDiv").html(response);
                        
                    }

                });
        }
        
        function documents(e,valu)
        {
            //alert(valu);
            //var id = '1';
            $("#ddd").val(valu);
            //$("#vvv").val(valu);
            //var temp = document.getElementById('temp_type').value;
            var id = document.getElementById('ccc').value;
            var docu = document.getElementById('ddd').value;
            //alert(docu);
            if(id)
            {
               $('#somment_div').show(); 
            }
            if(id=='')
            {
                alert("PLZ select Grantee");
                return false;
            }
//            $("#ccc").val(e);
//             $("#demo").html("response");
             $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/all_template",
                    type: "POST",
                    data: {'id': id, 'temp': e, 'doc': valu},
                    success: function (response)
                    {
                        //alert(response);
                        if(response=='no')
                        {
                            alert('Plese compleate previous step');
                        }
                        else
                        {
                        //alert(response);
                             $("#demo").show();
                            $("#demo").html(response);
                            abss();
                        }
                        
                        //$("#ddd").val(e);
                        

                    }

                });
        }
        
        function procid(e)
        {
            //alert(e);
            $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/change_temp_status",
                    type: "POST",
                    data: {'id': e,},
                    success: function (response)
                    {
                        //alert(response);
                            //$("#demo").html(response);
                            $('#stat_valid').hide();
                        
                    }

                });
        }
        
        
        function sss(id, e)
        {
            //alert(e);
            $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/getuser",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                        $("#user"+e).html(response);
                        //alert(response);

                    }

                });
        }
        
        

</script>
<script>
    function abss(){
        $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        "onSelect": function () {
//            var input = $(this);
//            $("#datepicker2").datepicker("option", "minDate", input.datepicker("getDate"));
//            $("#datepicker2").datepicker("refresh");
            var start = $('#datepicker1').datepicker('getDate');
            
            var day = start.getDate();
            var month = start.getMonth() + 6;
            var year = start.getFullYear();
            if (month < 10) {
                month = "0" + month;
            }
            if (day < 10) {
                day = "0" + day;
            }

            var sss = year + "-" + month + "-" + day;

            $('#end_date').val(sss);
            
        }
        
    });
    $('.datepicker').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker3').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    //CKEDITOR.replace( 'chk' );
    }
    $(function() {
//    $('#datepicker1').datepicker( {
//        changeMonth: true,
//        changeYear: true,
//        changeDay: true,
//        showButtonPanel: true,
//        dateFormat: 'yy-mm-dd',
//        
//    });
    
//});
//
//$(function() {

    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
    
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });

</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function contect()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>
    
    <script>
window.rowCount = 1;
    function addMoredetail(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
           // alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCount + '"><td><input type="text" class="form-control" required="required" name="title[]" value="" placeholder="Enter Title"></td><td><input type="text" class="form-control" required="required" name="value[]" value=""  placeholder="Enter Value"></td><td><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></td></tr>';
            jQuery('#addeddetail').append(recRow);
        
        


    }

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);

        jQuery('#rowCount' + removeNum).remove();
    }
</script>
    
<script>
window.rowCount = 1;
    function addMoreuser(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCount + '"><td><select name="group[]" class="form-control" onchange="sss(this.value, ' + rowCount + ')"><option>Select Group</option><?php $grp = $this->db->get('dms_role')->result_array(); foreach ($grp as $row) { ?><option value="<?php echo $row['role_id']; ?>" <?php if ($result['role_id'] == $row['role_id']) { echo "selected";}?> ><?php echo $row['role_name']; ?></option><?php } ?></select></td><td><select name="user[]" id="user' + rowCount + '" class="form-control"><option>Select User</option></select></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></td>';


            jQuery('#addedflow').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>

<script>
window.rowCount = 1;
    function addkeyRows(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<div class="row" id="rowCount' + rowCount + '" style="margin-top:5px;"><label  class="control-label col-md-3"><span style="visibility:hidden;">Keywords</span></label><div class="col-md-5"><input type="text" class="form-control mng" id="i'+ rowCount +'" name="keyword[]"  value="" placeholder="Enter Keywords" ></div><div class="col-md-4"><a href="javascript:void(0);" style="font-size: 11px;padding: 5px;" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div>';


            jQuery('#addedkey').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>
<script>
window.rowCount = 1;
    function addMoreweblink(frm, allowbook,e) {
//$('.add').click(function (event) {

         rowCount++;
        
//if($(".trcounter").length<3)
//{
    var recRow ='<tr id="rowCounts' + rowCount + '" class="trcounter"><td> Weblink <span style="color: red;margin-left: 1px">*</span> :<input type="hidden" class="form-control" required="required" name="title[]" value="Weblink" placeholder="Enter Title"></td><td><input type="text" class="form-control" required="required" name="value[]" value=""  placeholder="Enter Value"></td><td><input type="hidden" name="type[]" required="required" value="not define"><input type="hidden" class="form-control" name="detail[]" required="required" value="General Information"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRoww(' + rowCount + ');">Delete</a></td></tr>';
            //alert($(e).closest('tr'));
            $(e).closest('tr').after(recRow);
        
        

//}
    }

    var rel = '<?php echo $edit_rel; ?>';

    function removeRoww(removeNum, y) {

         //alert(removeNum);

        jQuery('#rowCounts' + removeNum).remove();
    }
</script>
<script>
 //1111111   
    
  $(document).ready(function () {
      
     // $('#advancecontent1').hide(); 
     
    $("li[id*=dropdownMenu1] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu1').click(function(){
    if(abc == 'on'){
        $('#advancecontent1').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent1').show(); 
        abc = 'on'; 
   }
     });
     
//22222222222222222

      $(document).ready(function () {
    $("li[id*=dropdownMenu2] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu2').click(function(){
    if(abc == 'on'){
        $('#advancecontent2').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent2').show(); 
        abc = 'on'; 
   }
     }); 
     
//33333333333333333333333333333

      $(document).ready(function () {
    $("li[id*=dropdownMenu3] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu3').click(function(){
    if(abc == 'on'){
        $('#advancecontent3').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent3').show(); 
        abc = 'on'; 
   }
     }); 
     
     //444444444444444444444444444444
     
     
      $(document).ready(function () {
    $("li[id*=dropdownMenu4] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu4').click(function(){
    if(abc == 'on'){
        $('#advancecontent4').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent4').show(); 
        abc = 'on'; 
   }
     }); 
     
     //5555555555555555555
     
      $(document).ready(function () {
    $("li[id*=dropdownMenu5] ul").click(function() {
     
          abc = 'off'; 

    });
});
var abc = 'on'; 
   $('#dropdownMenu5').click(function(){
    if(abc == 'on'){
        $('#advancecontent5').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent5').show(); 
        abc = 'on'; 
   }
     }); 
     
     //6666666666666666666666
     
      $(document).ready(function () {
    $("li[id*=dropdownMenu6] ul").click(function() {
     
          abc = 'off'; 

    });
});
 var abc = 'on'; 
   $('#dropdownMenu6').click(function(){
    if(abc == 'on'){
        $('#advancecontent6').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent6').show(); 
        abc = 'on'; 
   }
     }); 
     
     //7777777777777777777777777
     
      $(document).ready(function () {
    $("li[id*=dropdownMenu7] ul").click(function() {
     
          abc = 'off'; 

    });
});
 var abc = 'on'; 
   $('#dropdownMenu7').click(function(){
    if(abc == 'on'){
        $('#advancecontent7').hide(); 
 abc = 'off'; 
       }
   else{
         $('#advancecontent7').show(); 
        abc = 'on'; 
   }
     }); 
     
     
     
     
     
    
//    $('#dropdownMenu2').click(function(){
//  
//    $('#advancecontent2').toggle();
//    });
//    
//    $('#dropdownMenu3').click(function(){
//  
//    $('#advancecontent3').toggle();
//    });
    
//    $('#dropdownMenu4').click(function(){
//  
//    $('#advancecontent4').toggle();
//    });
//    
//    $('#dropdownMenu5').click(function(){
//  
//    $('#advancecontent5').toggle();
//    });
//    
//    $('#dropdownMenu6').click(function(){
//  
//    $('#advancecontent6').toggle();
//    });
//    
//    $('#dropdownMenu7').click(function(){
//  
//    $('#advancecontent7').toggle();
//    });
    
    
    </script>





