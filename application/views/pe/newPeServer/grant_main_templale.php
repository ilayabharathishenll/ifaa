<?php 
//echo phpinfo(); die;
error_reporting(~E_NOTICE);

$userPermission = $this->session->userdata['login']['user_permission'];
if ($param1) {
     $meta = $this->db->get_where('dms_grant_metadeta', array('grant_temp_id' => $param1))->result_array();
     $result1 = $this->db->get_where("dms_grantee", array('grantee_id' => $result['grantee_name']))->row_array();
//    $result = $this->db->get_where('tblteacher', array('teacher_id' => $param1))->row_array();
//    
//    $earning = explode(",",$result['teacher_earning']);
//    $earning_amount = explode(",",$result['teacher_earningAmount']);
//    $deduction = explode(",",$result['teacher_deduction']);
//    $deduction_amount = explode(",",$result['teacher_deductionAmount']);
//    $tax = explode(",",$result['teacher_tax']);
//    $tax_amount = explode(",",$result['teacher_taxAmount']);
    
    $key = explode(",",$result['keyword']);
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
$displi = $this->db->get("dms_disciplinary_field")->result_array();
$peUser = $this->db->get_where("dms_user", array('user_role' => '5'))->result_array();
$language = $this->db->get("dms_language")->result_array();
$state = $this->db->get("dms_state")->result_array();
$deliverables = $this->db->get("dms_deliverables")->result_array();
$outcome = $this->db->get("dms_outcome")->result_array();
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>pelogin/add_grant_main/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                           <div class="row" style="margin-bottom:10px">    
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grant No<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" id="grnt_no" onchange="grnt();"
                                                               data-validation="length"
                                                               data-validation-length="min1"
                                                               placeholder="Enter grant number"
                                                               name="grant_no" value="<?php echo $result['grant_number']; ?>"><br>
                                                        <p id="grnt_p" style=" color: red;"></p>
                                                        
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Archive Tag<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" data-validation="length" 
                                                               data-validation-length="min1"
                                                               data-validation-error-msg="grand ID is Required."
                                                               placeholder="Enter Archive Tag"
                                                               name="archiv_tag" value="<?php echo $result['archiv_tag']; ?>">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grantee Name<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php if($result['grantee_name']) { ?>
                                                        <input type="hidden" class="form-control" 
                                                               name="grantee_id" value="<?php echo $result['grantee_name']; ?>"> 
                                                        <input type="text" class="form-control" 
                                                               name="grantee_name" value="<?php echo $result1['grantee_name']; ?>">
<!--                                                         <select style="width: 100%;" name="grantee_id" id="grand_name" class="e1" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Category Status is required.">
                                                            <option value="">Select Grantee Name</option>
                                                            <?php
                                                            foreach ($data2 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['grantee_id']; ?>" <?php
                                                                if ($result['grantee_name'] == $row['grantee_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['grantee_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>-->
                                                        <?php } else { ?>
                                                        <select style="width: 100%;" name="grantee_id" id="grand_name" class="e1" onchange="manual(this.value)">
                                                            <option value="">Select Grantee Name</option>
                                                            <?php
                                                            foreach ($data2 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['grantee_id']; ?>" <?php
                                                                if ($result['grantee_name'] == $row['grantee_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['grantee_name']; ?></option>
                                                                    <?php } ?>
                                                        </select></br>
                                                        <div id="catg">
                                                            <input type="text" class="form-control"  id="grntee_name" 
                                                                     name="grantee_name" 
                                                                    data-validation="length" 
                                                                    data-validation-length=min1" 
                                                                    data-validation-error-msg="Grantee Name is required"
                                                                    placeholder="Enter Grantee Name"
                                                                    value="" onchange="granteename();">
                                                        </div>
                                                        <?php } ?>
                                                   <div id="error" style="color:red;"></div>
                                                    </div> 
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Select Programme/Initiative<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                         
                                                        <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $result['id']; ?>"> 
                                                          
                                                        <select style="width: 100%;" name="category" id="e3" class="e1" onChange="year();" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Programs is required.">
<!--                                                            <option>Select Programs</option>-->
                                                            <?php
                                                            foreach ($data1 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['category_id']; ?>" <?php
                                                                if ($result['category_id'] == $row['category_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['category_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>                                                                                          
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Select Grant Status<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       
                                                        <select name="cat_status" id="status" class="form-control" onChange="year();" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Programs Status is required.">
<!--                                                            <option>Select Grant Status</option>-->
                                                            <?php
                                                            foreach ($data3 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['categorytype_id']; ?>" <?php
                                                                if ($result['categorytype_id'] == $row['categorytype_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['categorytype_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
    
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Start Date<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly="" class="form-control" id="datepicker1"  
                                                                            class="form-control"  name="start_date" 
                                                                            data-validation="length" 
                                                                            data-validation-length=min1" 
                                                                            data-validation-error-msg="Start  date is required"
                                                                            placeholder="Enter date"
                                                                               value="<?php echo $result['start_date']; ?>"> 
                                                            
                                                    </div>

                                                </div>
                                            </div>  
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> End Date<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly="" class="form-control " id="datepicker2"  
                                                                            class="form-control"  name="end_date" 
                                                                            data-validation="length" 
                                                                            data-validation-length=min1" 
                                                                            data-validation-error-msg="End date is required"
                                                                            placeholder="Enter End date"
                                                                               value="<?php echo $result['end_date']; ?>">
                                                            
                                                    </div>

                                                </div>
                                                
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Extension Till</label>
                                                    <div class="col-md-8">
                                                        <input type="text" readonly="" class="form-control" id="datepicker3"  
                                                                            class="form-control"  name="extension" 
                                                                            placeholder="Enter Extension Till"
                                                                               value="<?php echo $result['extension']; ?>">
                                                            
                                                    </div>

                                                </div>
                                            </div>
                                            </br>
                                            
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Grant Duration</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"  name="grant_duration" 
                                                                            placeholder="Enter Grant Guration"
                                                                               value="<?php echo $result['grant_duration']; ?>">
                                                            
                                                    </div>

                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Grant Year<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <select name="grant_year" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Grant Year is required.">
                                                            <option>Select Grant year</option>
                                                            <?php
                                                            //$years = date("Y");
                                                            for ($i = 0; $i < 23; $i++) {
                                                                $years = date("Y") - $i;
                                                                ?>
                                                                <option value="<?php echo $years ?>" <?php
                                                                if ($result['category_year'] == $years) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $years; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                    </div>

                                                </div>
                                               
                                                
                                                
                                            </div>
                                            
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Programme Executive </label>
                                                    <div class="col-md-8">
                                                        <select name="program_executive" class="form-control" >
                                                            <option>Select Programme Executive</option>
                                                            <?php
                                                            //$years = date("Y");
                                                            foreach ($peUser as $pe) {
                                                                ?>
                                                                <option value="<?php echo $pe['user_id']; ?>" <?php
                                                                if ($result['program_executive'] == $pe['user_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $pe['user_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                        
                                                            
                                                    </div>

                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grant Amount</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"  
                                                                            class="form-control"  name="grant_amount" 
                                                                            placeholder="Enter Grant Amount"
                                                                               value="<?php echo $result['grant_amount']; ?>">
                                                    </div>

                                                </div>
                                               
                                                
                                                
                                            </div>
                                            </br>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grant Type </label>
                                                    <div class="col-md-8">
                                                        <select name="program_types" class="form-control">
                                                            <option value="1" <?php
                                                                if ($result['description_type'] == "1") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Individual</option>
                                                            <option value="2" <?php
                                                                if ($result['description_type'] == "2") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Organization</option>
                                                             <option value="3" <?php
                                                                if ($result['description_type'] == "3") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Fellowship</option>
                                                        </select>
                                                        
                                                            
                                                    </div>

                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Short/Long Term </label>
                                                    <div class="col-md-8">
                                                        <select name="sub_types" class="form-control">
                                                            <option value="short" <?php
                                                                if ($result['description_sub_type'] == "short") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Short Term </option>
                                                            <option value="long" <?php
                                                                if ($result['description_sub_type'] == "short") {
                                                                    echo "selected";
                                                                }
                                                                ?>>Long Term </option>
                                                        </select>
                                                    </div>

                                                </div>
                                               
                                                
                                                
                                            </div>
                                            </br>
                                            
<!--                                            <div class="row" style="margin-bottom:10px">
                                               
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Grant Year<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <select name="grant_year" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Grant Year is required.">
                                                            <option>Select Grant year</option>
                                                            <?php
                                                            //$years = date("Y");
                                                            for ($i = 0; $i < 25; $i++) {
                                                                $years = date("Y") - $i;
                                                                ?>
                                                                <option value="<?php echo $years ?>" <?php
                                                                if ($result['category_year'] == $years) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $years; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="control-label col-md-6" style="text-align: right">
                                                
                                                    <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                                </div>
                                            </div>-->
                                            
                                                <br>
                                                     
                            
                                                <div class="box-title">
                                                    <h4>Grant Metadata </h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                           
<!--                                            <div style="text-align: left">
                                                
                                                <a href="javascript:;" class="btn btn-primary col-md-12" style="text-align: left" data-toggle="collapse">Grantee Meta Data</a>
                                                
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                            </div>-->
                                            </br>
                                            
<!--                                            <div id="demo" class="collapse">-->
                                                    
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Possible Deliverables</label>
                                                        <div class="col-md-6" id="deliveryediv">
                                                            <select class="form-control lstFruits" id="deliverables" multiple="multiple" name="deliverables[]">
                                                                <?php foreach ($deliverables as $value) { ?>
                                                                <option <?php if($result['deliverables'] == $value['deliverables_id']){ echo "selected" ; } ?> value="<?php echo $value['deliverables_id']; ?>"><?php echo $value['deliverables_name'] ?></option>
                                                                <?php } ?>
                                                            </select>

<!--                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                    data-validation-length="min1" 
                                                                                  data-validation-error-msg="Geographical area of work is required." 
                                                                                  class="form-control" 
                                                                                  placeholder="Enter Geographical area of work"
                                                                                  name="geogrtaphical_area" value="<?php echo $result1['grantee_geogrtaphical_area']; ?>"> -->
                                                        </div>
                                                        <div  class="col-md-2">
                                                            <button type="button" class="btn btn-primary "  data-toggle="modal" data-target="#adddeliverablesmore">+</button>
                                                        </div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Disciplinary field of work </label>
                                                        <div class="col-md-6" id="dspdiv">
                                                            <select class="form-control lstFruits" id="selectdisplinary" multiple="multiple" name="disciplinary_field[]">
                                                                <?php foreach($displi as $valu) { ?>
                                                                <option <?php if($result['grantee_disciplinary_field'] == $valu['field_id']){ echo "selected" ; } ?> value="<?php echo $valu['field_id']; ?>"><?php echo $valu['field_name']; ?></option>
                                                                <?php } ?>
                                                                
                                                            </select>
                                                        </div>
                                                        <div  class="col-md-2">
                                                            <button type="button" class="btn btn-primary "  data-toggle="modal" data-target="#adddisiplinary">+</button>
                                                        </div>
                                                    </div> 
                                                   
                                                </div>
                                                
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Language</label>
                                                        <div class="col-md-6" id="languagediv">
                                                            <select id="lstFruits" class="lstFruits" multiple="multiple" name="language[]">
                                                                <?php foreach ($language as $value) { ?>
                                                                <option value="<?php echo $value['language_id'] ?>"><?php echo $value['language_name'] ?></option>
                                                                <?php } ?>
                                                            </select>

<!--                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                  data-validation-error-msg="Language is required." 
                                                                                  data-validation-length="min1" 
                                                                                  class="form-control"  
                                                                                   placeholder="Enter Language"
                                                                                  name="language" value="<?php echo $result1['grantee_language']; ?>"> -->
                                                        </div>
                                                        <div id="mobile" style="color:red;margin-left: 150px"></div>
                                                        <div  class="col-md-1">
                                                            <button type="button" class="btn btn-primary "  data-toggle="modal" data-target="#addlanguagemore">+</button>
                                                        </div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Possible outcome</label>
                                                        <div class="col-md-6" id="outcomediv">
                                                           <select class="form-control lstFruits" multiple="multiple" id="outcome" name="grantee_possible_outcome[]">
                                                                <?php foreach($outcome as $valu) { ?>
                                                                <option <?php if($result['grantee_possible_outcome'] == $valu['outcome_id']){ echo "selected" ; } ?> value="<?php echo $valu['outcome_id']; ?>"><?php echo $valu['outcome_name']; ?></option>
                                                                <?php } ?>
                                                                
                                                            </select>
                                                        </div>
                                                        <div  class="col-md-1">
                                                            <button type="button" class="btn btn-primary "  data-toggle="modal" data-target="#addoutcomemore">+</button>
                                                        </div>
                                                    </div>                                              
                                                </div>
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6" id="gworkdiv"> 
                                                        <label  class="control-label col-md-4">Geographical area of work </label>
                                                        <div class="col-md-8" id="giodiv">
                                                            <select class="form-control lstFruits" multiple="multiple" name="geogrtaphical_area[]">
                                                                <?php foreach ($state as $value) { ?>
                                                                <option <?php if($result['grantee_geogrtaphical_area'] == $value['state_id']){ echo "selected" ; } ?> value="<?php echo $value['state_id']; ?>"><?php echo $value['state_name'] ?></option>
                                                                <?php } ?>
                                                            </select>

<!--                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                    data-validation-length="min1" 
                                                                                  data-validation-error-msg="Geographical area of work is required." 
                                                                                  class="form-control" 
                                                                                  placeholder="Enter Geographical area of work"
                                                                                  name="geogrtaphical_area" value="<?php echo $result1['grantee_geogrtaphical_area']; ?>"> -->
                                                        </div>
                                                        </br>
                                                    </div>
                                                     
                                                   
                                                </div>
                                                <div id="grntee_detail">
                                                <div class="box-title">
                                                    <h4>Grantee Detail</h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Address 1 </label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" 
                                                                   placeholder="Enter address1 "
                                                               name="address1" value="<?php echo $result1['grantee_address1']; ?>">
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4"> Address 2</label>
                                                    <div class="col-md-8"><input type="text" class="form-control"
                                                                   placeholder="Enter Address"
                                                                 name="address2" value="<?php echo $result1['grantee_address2']; ?>"  >

                                                    </div>
                                                </div>
                                            </div>
<!--                                                <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Grantee Phone<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)"  data-validation="number" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="grantee Phone is required."
                                                                   placeholder="Enter Phone Number"
                                                                   name="phone" value="<?php echo $result1['grantee_phone']; ?>"  onchange="mno();" >
                                                        <p id="error1" style="color:red"></p>
    
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Alternative Number<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" onkeypress="javascript:return isNumber(event)" 
                                                               placeholder="Enter Phone Number"
                                                               name="alternative" value="<?php echo $result1['grantee_alternative_no']; ?>" data-validation="number">
                                                    </div>
                                                </div>
                                            </div>  
                                             -->
                                              
                                            
                                            <div class="row" style="margin-bottom:10px">
                                               <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">City</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"
                                                                   placeholder="Enter City"
                                                               name="city" value="<?php echo $result1['grantee_city']; ?>">
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">State</label>
                                                    <div class="col-md-6" id="statediv">
                                                        <select class="form-control lstFruits" multiple="multiple" name="state[]">
                                                                <?php foreach ($state as $value) { ?>
                                                                <option <?php if($result1['grantee_state'] == $value['state_id']){ echo "selected" ; } ?> value="<?php echo $value['state_id']; ?>"><?php echo $value['state_name'] ?></option>
                                                                <?php } ?>
                                                        </select>
                                                       
<!--                                                      <input type="text" class="form-control"  data-validation="length"
                                                                            data-validation-length="min1" 
                                                                              data-validation-error-msg="State is required." 
                                                                              class="form-control" 
                                                                              placeholder="Enter State" 
                                                                              name="state" value="<?php echo $result1['grantee_state']; ?>"> -->
                                                    </div>
                                                    <div  class="col-md-2">
                                                            <button type="button" class="btn btn-primary "  data-toggle="modal" data-target="#addstatemore">+</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Pin</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"  
                                                                   placeholder="Enter Pin Code"
                                                               id="" name="pin" value="<?php echo $result1['grantee_pin']; ?>">
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Mobile No</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)" name="phone" value="<?php echo $result1['grantee_phone']; ?>"  onchange="mno();" >
                                                        <p id="error1" style="color:red"></p>
    
                                                    </div>
                                                </div>
<!--                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Country<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="country is required."
                                                                   placeholder="Enter Country"
                                                               id="" name="country" value="<?php echo $result1['grantee_country']; ?>"> 
                                                    </div>
                                                </div>                                              -->
                                            </div>
                                             
                                             <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Email</label>
                                                        <div class="col-md-8">

                                                          <input type="text" class="form-control" 
                                                                                   placeholder="Enter Email"
                                                                                  name="email" value="<?php echo $result1['grantee_email']; ?>"> 
                                                        </div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4"> Website  </label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control"  
                                                                        placeholder="Enter Website"
                                                                        name="website" value="<?php echo $result1['grantee_website']; ?>">
                                                        </div>
                                                    </div>
                                                    </br>
                                                </div>
<!--                                             <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Geographical area of work </label>
                                                        <div class="col-md-8">
                                                            <select class="form-control lstFruits" multiple="multiple" name="geogrtaphical_area[]">
                                                                <?php foreach ($state as $value) { ?>
                                                                <option <?php if($result1['grantee_geogrtaphical_area'] == $value['state_id']){ echo "selected" ; } ?> value="<?php echo $value['state_id']; ?>"><?php echo $value['state_name'] ?></option>
                                                                <?php } ?>
                                                            </select>

                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                    data-validation-length="min1" 
                                                                                  data-validation-error-msg="Geographical area of work is required." 
                                                                                  class="form-control" 
                                                                                  placeholder="Enter Geographical area of work"
                                                                                  name="geogrtaphical_area" value="<?php echo $result1['grantee_geogrtaphical_area']; ?>"> 
                                                        </div>
                                                        </br>
                                                    </div>
                                                     
                                                   
                                                </div>-->
<!--                                            <div class="row" style="margin-bottom:10px"> 
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Country<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="country is required."
                                                                   placeholder="Enter Country"
                                                               id="" name="country" value="<?php echo $result1['grantee_country']; ?>"> 
                                                    </div>
                                                </div>

                                            </div>-->
<!--                                                </div>-->
                                                
                                                
<!--                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Email<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">

                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                  data-validation-error-msg="Email is required." 
                                                                                  data-validation-length="min1" 
                                                                                  class="form-control" 
                                                                                   placeholder="Enter Email"
                                                                                  name="email" value="<?php echo $result1['grantee_email']; ?>"> 
                                                        </div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4"> Website  <span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control"  data-validation="length" 
                                                                       data-validation-length="min1" 
                                                                       data-validation-error-msg="Website is required."
                                                                        placeholder="Enter Website"
                                                                        name="website" value="<?php echo $result1['grantee_website']; ?>">
                                                        </div>
                                                    </div>
                                                    </br>
                                                </div>-->
                                            
                                                
<!--                                            <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Language1</label>
                                                        <div class="col-md-6" id="langtest" >
                                                            <select id="lstFruits1" class="lstFruits1" multiple="multiple" name="language[]">
                                                                <?php foreach ($language as $value) { ?>
                                                                <option value="<?php echo $value['language_id'] ?>"><?php echo $value['language_name'] ?></option>
                                                                <?php } ?>
                                                            </select>

                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                  data-validation-error-msg="Language is required." 
                                                                                  data-validation-length="min1" 
                                                                                  class="form-control"  
                                                                                   placeholder="Enter Language"
                                                                                  name="language" value="<?php echo $result1['grantee_language']; ?>"> 
                                                        </div>
                                                        <div id="mobile" style="color:red;margin-left: 150px"></div>
                                                        <div  class="col-md-1">
                                                            <button type="button" class="btn btn-primary "  data-toggle="modal" data-target="#addlanguagemore">+</button>
                                                        </div>
                                                    </div>    -->
                                                    
                                            </div>    
                                            <div  style="margin-bottom:10px; text-align: right">
                                                <?php  if (in_array("add_grant", $userPermission)){ ?>
                                                    <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">CREATE</button>
                                                <?php } ?>
                                            </div>

                                            
                                          </form>                     

                                        </div>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->
                               
                                    <!-- /BASIC -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>


<!--     modal popup for state-->
    
<div class="modal fade" id="addstatemore" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add State</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/add_state/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Name <span style="color: red;margin-left: 1px">*</span></label>
                        <div class="col-md-5"> 
                            <input type="text" class="form-control"  id="states_name"
                                        data-validation="length" 
                                        data-validation-length="min1" 
                                        data-validation-error-msg="Description is required"
                                        placeholder="Enter Description"
                                        name="states_name" value="">                                                                                           
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="button" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="addstate();">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>                    

    <!--     modal popup for adddeliverables-->
    
<div class="modal fade" id="adddeliverablesmore" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Deliverables</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/add_deliverables/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Name <span style="color: red;margin-left: 1px">*</span></label>
                        <div class="col-md-5"> 
                            <input type="text" class="form-control"  id="deliverables_name"
                                        data-validation="length" 
                                        data-validation-length="min1" 
                                        data-validation-error-msg="deliverables name is required"
                                        placeholder="Enter Description"
                                        name="deliverables_name" value="">                                                                                           
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="button" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="adddeliverables();">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>                    

<!--     modal popup for language -->
    
<div class="modal fade" id="addlanguagemore" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Language</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/add_state/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Name <span style="color: red;margin-left: 1px">*</span></label>
                        <div class="col-md-5"> 
                            <input type="text" class="form-control"  id="languagess_name"
                                        data-validation="length" 
                                        data-validation-length="min1" 
                                        data-validation-error-msg="Description is required"
                                        placeholder="Enter Description"
                                        name="states_name" value="">                                                                                           
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="button" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="addlanguagemore();">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>                    
 
<!--     modal popup for language -->
    
<div class="modal fade" id="addoutcomemore" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Outcome</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/add_state/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Name <span style="color: red;margin-left: 1px">*</span></label>
                        <div class="col-md-5"> 
                            <input type="text" class="form-control"  id="outcome_name"
                                        data-validation="length" 
                                        data-validation-length="min1" 
                                        data-validation-error-msg="Description is required"
                                        placeholder="Enter outcome"
                                        name="outcome_name" value="">                                                                                           
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="button" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="addoutcome();">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>                    
     
    
<!--     modal popup for disiplinary fielded-->
    
    <div class="modal fade" id="adddisiplinary" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Disciplinary field of work</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/user_comment/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Name <span style="color: red;margin-left: 1px">*</span></label>
                        <div class="col-md-5"> 
                            <input type="text" class="form-control"  id="displi_name"
                                        data-validation="length" 
                                        data-validation-length="min1" 
                                        data-validation-error-msg="Description is required"
                                        placeholder="Enter Description"
                                        name="displi_name" value="">                                                                                           
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="button" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="adddisiplinary();">save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

    <script type="text/javascript">
        $(function () { //alert();
            $('.lstFruits').multiselect({ 
                includeSelectAllOption: true
            });
            $('#btnSelected').click(function () { 
                var selected = $("#lstFruits option:selected");
                var message = "";
                selected.each(function () {
                    message += $(this).text() + " " + $(this).val() + "\n";
                });
                alert(message);
            });
        });
        
        $(function () { //alert();
            $('.lstFruits1').multiselect({ 
                includeSelectAllOption: true
            });
           
        });
        
        function granteename()
        {
            var name = document.getElementById('grntee_name').value;
            //alert(name);
            if(name){
                $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/get_Grantee_name",
                    type: "POST",
                    data: {'name': name},
                    success: function (response)
                    {
                       // alert(response);
                        if(response!="good"){
                            $("#error").html(response);
                        }
                        else
                        {
                            $("#error").hide();
                        }
//                        $("#addstatemore").modal('hide');
//                        $('.lstFruits').multiselect({ 
//                        includeSelectAllOption: true
//                    });

                    }

                });
            }
        }
        
function addstate()
{
    //alert('aaa');
    var name = document.getElementById('states_name').value;
    //alert(name);
    if(name){
        $.ajax({
            url: "<?php echo base_url(); ?>pelogin/add_state",
            type: "POST",
            data: {'name': name},
            success: function (response)
            {
                //alert(response);
                //$("#state").html(response);
               // $(".multiselect-container").append(response);
//                var res = response.split('|');
//                $("#statediv #lstFruits").append(res['0']);
//                $("#statediv .multiselect-native-select .btn-group .multiselect-container").append(res['1']);
//                $("#statediv").html(response);
                $("#statediv").html(response);
                $("#addstatemore").modal('hide');
                $('.lstFruits').multiselect({ 
                includeSelectAllOption: true
            });

            }

        });
    }
}

function adddeliverables()
{
    //alert('aaa');
    var name = document.getElementById('deliverables_name').value;
    //alert(name);
    if(name){
        $.ajax({
            url: "<?php echo base_url(); ?>pelogin/add_deliverables",
            type: "POST",
            data: {'name': name},
            success: function (response)
            {
                //alert(response);
                //$("#deliverables").html(response);
//                var res = response.split('|');
//                $("#deliveryediv #lstFruits").append(res['0']);
//                $("#deliveryediv .multiselect-native-select .btn-group .multiselect-container").append(res['1']);
                
                //$(".multiselect-container").append(response);
                $("#deliveryediv").html(response);
                $("#adddeliverablesmore").modal('hide');
                $('.lstFruits').multiselect({ 
                includeSelectAllOption: true
            });

            }

        });
    }
}

function addlanguagemore()
{
    //alert('aaa');
    var name = document.getElementById('languagess_name').value;
    //alert(name);
    if(name){
        $.ajax({
            url: "<?php echo base_url(); ?>pelogin/add_language",
            type: "POST",
            data: {'name': name},
            success: function (response)
            {
               var res = response.split('|');
                //alert(res);
                //console.log(response);
                //<li><a tabindex="0"><label class="checkbox"><input type="checkbox" value="29"> chacha</label></a></li>
//                $("#languagediv #lstFruits").append(res['0']);
//                $("#languagediv .multiselect-native-select .btn-group .multiselect-container").append(res['1']);
                
                $("#languagediv").html(response);
                $("#addlanguagemore").modal('hide');
                $('.lstFruits').multiselect({ 
                    includeSelectAllOption: true
                });
                
                
            }

        });
    }
}

function addoutcome()
{
    //alert('aaa');
    var name = document.getElementById('outcome_name').value;
    //alert(name);
    if(name){
        $.ajax({
            url: "<?php echo base_url(); ?>pelogin/add_outcome",
            type: "POST",
            data: {'name': name},
            success: function (response)
            {
                //alert(response);
                //$("#outcome").html(response);
//                var res = response.split('|');
//                $("#outcomediv #lstFruits").append(res['0']);
//                $("#outcomediv .multiselect-native-select .btn-group .multiselect-container").append(res['1']);
                //$(".multiselect-container").append(response);
                $("#outcomediv").html(response);
                $("#addoutcomemore").modal('hide');
                $('.lstFruits').multiselect({ 
                    includeSelectAllOption: true
                });
            }

        });
    }
}
        
        
function adddisiplinary()
{
    var name = document.getElementById('displi_name').value;
    //alert(name);
    if(name){
        $.ajax({
            url: "<?php echo base_url(); ?>pelogin/adddisiplinary_field",
            type: "POST",
            data: {'name': name},
            success: function (response)
            {
                //alert(response);
                //$("#selectdisplinary").html(response);
//                var res = response.split('|');
//                $("#dspdiv #lstFruits1").append(res['0']);
//                $("#dspdiv .multiselect-native-select .btn-group .multiselect-container").append(res['1']);
                //$(".multiselect-container").append(response);
                $("#dspdiv").html(response);
                $("#adddisiplinary").modal('hide');
                $('.lstFruits').multiselect({ 
                    includeSelectAllOption: true
                });

            }

        });
    }
}
    </script>
    <script>   
        function year()
        {
            var cat = document.getElementById('e3').value;
            var id = document.getElementById('status').value;
           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/yearDropdown",
                    type: "POST",
                    data: {'status': id, 'category': cat},
                    success: function (response)
                    {
                        $("#years").html(response);

                    }

                });
            }
        }
        
        function grnt()
        {
            //alert('aaaa');
            var grnt = document.getElementById('grnt_no').value;
           // alert(grnt);
//            var id = document.getElementById('status').value;
//           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/grantNoValidation",
                    type: "POST",
                    data: {'grnt': grnt},
                    success: function (response)
                    {
                        //alert(response);
                        if(response=="yes")
                        {
                            $('#grnt_no').focus();
                            document.getElementById('grnt_no').value = '';
                            $('#grnt_p').text('Grant Number is Allredy exist');
                            $("#grnt_p").show();
                            //document.getElementById('grnt_p').value = 'Grant Number is Allredy exist';
                        }
                        else
                        {
                            $("#grnt_p").hide();
                        }
                        
                        //$("#years").html(response);

                    }

                });
//            }
        }
        
        function manual(e)
        {
            if(e!='')
            {
                $("#grntee_name").val('');
                $("#catg").hide();
                $("#grntee_detail").hide();
                $("#error").hide();
            }
            else
            {
                $("#catg").show();
                $("#grntee_detail").show();
            }
        }
        

</script>
<script>
    $(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        "onSelect": function () {
//            var input = $(this);
//            $("#datepicker2").datepicker("option", "minDate", input.datepicker("getDate"));
//            $("#datepicker2").datepicker("refresh");
            var start = $('#datepicker1').datepicker('getDate');
            
            var day = start.getDate();
            var month = start.getMonth() + 6;
            var year = start.getFullYear();
            if (month < 10) {
                month = "0" + month;
            }
            if (day < 10) {
                day = "0" + day;
            }

            var sss = year + "-" + month + "-" + day;

            $('#end_date').val(sss);
            
        }
    });
//});
//
//$(function() {
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker3').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
    
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });

</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function contect()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>
    <script>
window.rowCount = 1;
    function addMoredetail(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCount + '"><td><input type="text" class="form-control" required="required" name="title[]" value="" placeholder="Enter Title"></td><td><input type="text" class="form-control" required="required" name="value[]" value=""  placeholder="Enter Value"></td><td><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></td></tr>';
            jQuery('#addeddetail').append(recRow);
        
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>






