<?php 
        $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $data['grantee_name']))->row_array();
        $category = $this->db->get_where('dms_category', array('category_id' => $data['category_id']))->row_array();
        $parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '8'))->row_array();
        $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $data['categorytype_id']))->row_array();
        
         $individualData = $this->db->get_where('dms_individual_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '18'))->result_array();

        $meta = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36'))->result_array();
        $meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'General Information'))->result_array();
        $meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Author, Publisher, Access'))->result_array();
        $meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Physical Characteristics'))->result_array();
        $meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Digitisation Details'))->result_array();
        $meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Audio-Visual Material Details'))->result_array();
        $meta6 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Others'))->result_array();
?>
<div class="container-fluid bread">
	<div class="col-md-4">
	<p class="bread_content1">Video Files</p>
	</div>
	<div class="col-md-8">
	<p class="bread_content2">Home/Searched Result/<?php echo $data['grant_number']; ?>/Video files</p>
	</div>
	</div>
	
	<div class="image_content">
	<div class="col-md-8 image_content_left">
	<h5><?php echo $grantee['grantee_name']; ?></h5>
	<h6><?php echo $category['category_name'].' | '.$data['start_date'].' - '.$data['start_date'].' | '.$status['categorytype_name']; ?></h6>
	
	<div class="col-md-12 image_content_images">
            <?php $i ='1';
            foreach($individualData as $valas ){ ?>
            <div class="col-md-3">
                <audio controls>
                    <source src="<?php echo base_url()."uploads/".$valas['individual_file']; ?>" type="audio/mpeg">
                </audio>
                <div class="video_captions">
                    <h5>Video <?php echo $i; ?></h5>	
                </div>
            </div>
            <?php $i++; } ?>
	
<!--	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 2</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 3</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 4</h5>	
	</div>
	</div>-->
	
	
	
	</div>
	
<!--	<div class="col-md-12 image_content_images">
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 5</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 6</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 7</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 8</h5>	
	</div>
	</div>
	
	
	
	</div>-->
	
<!--	<div class="col-md-12 image_content_images">
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 9</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 10</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 11</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 12</h5>	
	</div>
	</div>
	<div class="pagin">
	<ul class="pagination">
	<li class="active"><a href="javascript:;">1</a></li>
  <li><a href="javascript:;">2</a></li>
  <li><a href="javascript:;">3</a></li>
  <li><a href="javascript:;">4</a></li>
  <li><a href="javascript:;">5</a></li>
</ul>
	
	</div>
	
	</div>-->

	</div>
	<div class="col-md-4 image_content_right">
	<h5>Metadata</h5>
	<?php if($meta1) { ?>
            <h4 style="margin: 20px 0;"><strong>General Information</strong></h4>

            <?php foreach($meta1 as $value) {?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
            
        <?php } } if($meta2) { ?>

        <h4  style="margin: 20px 0;"><strong>Author, Publisher, Access</strong></h4>
	<?php foreach($meta2 as $value) {?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php } } if($meta3) { ?>
        <h4  style="margin: 20px 0;"><strong>Physical Characteristics</strong></h4>
	<?php foreach($meta3 as $value) {?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php } } if($meta4) { ?>
        <h4  style="margin: 20px 0;"><strong>Digitization Details</strong></h4>
	<?php foreach($meta4 as $value) {?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        <?php } } ?>
	
	</div>
	</div>