<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="javascript:;" 
                                           aria-hidden="true">X</a>
                                        <h4><i class="fa fa-smile-o"></i> <?php
                                            echo
                                            $this->session->flashdata('flash_message');
                                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                                    </div>
                                    <?php
                                }
                                if ($this->session->flashdata('permission_message')) {
                                    ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <a class="close" data-dismiss="alert" href="javascript:;" 
                                           aria-hidden="true">X</a>
                                        <h5><i class="fa fa-frown-o"></i> <?php
                                            echo
                                            $this->session->flashdata('permission_message');
                                            ?><i class="fa fa-thumbs-down"></i></h5>
                                    </div>
                                    <?php  }?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action ="<?php echo base_url(); ?>adminlogin/changepassword" method="post">
                                            <div class="form-group"> 
                                                <label> Old Password</label>
                                                <input type="password" class="form-control" data-validation="length" 
		 data-validation-length="min4" 
		 data-validation-error-msg="Enetr old password"
                                                        name="old_password"  placeholder="Enter old password">
                                            </div>
                                            <div class="form-group"> 
                                                <label> New Password</label>

                                                <input type="password" class="form-control"  id="pass"
		 data-validation-strength="2"
                                                        name="new_password" id="pass" placeholder="Enter new password" >                                                                                                   </div>
                                            <div class="form-group"> 
                                                <label>Repeat Password</label>
                                                <input type="password" class="form-control"  id="conpass"
                                                       name="confirm_password" id="conpass" placeholder="Enter confirm password" >
                                             <div id="error" style="color:red;"></div>
                                            </div>
                                            <div>
                                                <button type="submit" class="btn btn-primary start" value="Validate"
                                                        name="change_password" onclick="return chkpass();">Change Password</button>                                                          
                                            </div>
                                            
                                           
                                                
                                            
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->
                                
                                    <!-- /BASIC -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>
<script>
    function chkpass(){
    var pass=document.getElementById("pass").value;
    var conpass=document.getElementById("conpass").value;
    if(pass!=conpass)
    {
        document.getElementById("error").innerHTML="password and confirm password not matched.";
    //alert('wrong');
    return false;
    }
    else
    {
       
        return true;
    }
    
    
}
</script>
<script>

   $.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#presentation').restrictLength( $('#pres-max-length') );
  $.validate({
 modules : 'security',
  borderColorOnError : '#FFF',
  addValidClassOnAll : true
});


</script>