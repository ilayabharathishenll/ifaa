<?php
error_reporting(~E_NOTICE);
$userId = $this->session->userdata['login']['user_id'];
$userPermission = $this->session->userdata['login']['user_permission'];

?>

<style>
  .bold
  {
      font-size:13px;
      color:#fff;
      border-radius:4px;
  }     
</style>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('pelogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                        
                        </div>
                    </div>
                </div>
                <?php  if (in_array("add_comment", $userPermission)){ ?>
                <div class="row" id="somment_div" style="display: none;">
                    <div class="col-md-12">
                        <div style="text-align: right;">

                            <button type="button" class="btn btn-primary start"  data-toggle="modal" data-target="#approve" style="width:120px;float:right;margin:10px 5px;">Comment</button>
<!--                            <button type="button" class="btn btn-primary start"  data-toggle="modal" data-target="#reject" style="width:120px;float: right;margin:10px 5px;" >Reject</button>-->
                        </div>
                    </div>
                </div>
                <?php } ?>
                
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                
                <div class="row ">
                    <div class=" col-md-12" style="text-align: left; margin: 10px 0px;">

                        <button class="btn btn-primary start" style="width:50px" onclick="adremovecat()">Cat</button>
                    
                            <button class="btn btn-primary start" style="width:50px" onclick="adremovedoc()">Doc</button>
                    </div>
                    
                    <div class="col-md-3 paddinglake" id="catdiv">
                                <div class="boxtemplate">
                                    <div class="headone">
                                        <h4>Category</h4>
                                    </div>
                                    <ul>
                                        <?php $i = '1';
                                       // echo "<pre>";
                                       
                                        foreach($cat as $value){ ?>               
                                        <li class="dropdown-toggle" id="dropdownMenu<?php echo $i; ?>" onclick="testcat34('<?php echo $i; ?>')" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <?php echo $value['category_name'];?>
                                            <ul class="advancecontent" id="advancecontent<?php echo $i; ?>">
                                                <?php 
                                                $this->db->group_by('status');
                                                
                                  $sta = $this->db->get_where('dms_cat_year', array('category_id' => $value['category_id']))->result_array();
                                                  // print_r($sta);
                                  if($sta) {
                                                      $j = '1';
                                                      foreach($sta as $val)
                                                      {
                                                    $staus = $this->db->get_where('dms_categorytype', array('categorytype_id' => $val['status']))->row_array();
                                                    
                                                ?>
                                                <li id="dropdownsecond<?php echo $i.$j; ?>" > 
                                                <?php echo $staus['categorytype_name']; ?>   <!-- Status Name-->
                                                <ul>
                                                     <?php
                                                        $this->db->group_by('year');
                                                        $year = $this->db->get_where('dms_cat_year', array('status' => $val['status'], 'category_id' => $value['category_id'] ))->result_array();
                                                       if($year) {
                                                           $l = 1;
                                                      foreach($year as $val)
                                                      {
                                                        ?>
                                                    <li> <?php echo $val['year']; ?>  <!-- Year Name-->
                                                    
                                                        <ul>
                                                            <?php
                                                            $grant = $this->db->get_where('dms_grant', array('categorytype_id' => $staus['categorytype_id'], 'category_year' => $val['year'], 'category_id' => $value['category_id']))->result_array();
                                                            //print_r($grant);
                                                            if($grant) {
                                                                $k = 1;
                                                            foreach($grant as $grnt)
                                                            {
                                                                $sts = $this->db->get_where('dms_grantee', array('grantee_id' => $grnt['grantee_name']))->row_array();
                                                            ?>
                                                            <li>
                                                                <!--<a href="javascript:void(0)" id="cata<?php echo $i.$l.$j.$k; ?>" class="catsaa catse<?php echo $grnt['id']; ?>" gg="advancecontent<?php echo $i; ?>" onclick="template('<?php echo $grnt['id']; ?>', 'cata<?php echo $i.$l.$j.$k; ?>')"><?php echo $sts['grantee_name']; ?></a>--> 
                                                             <a href="javascript:void(0)" id="cata<?php echo $i.$l.$j.$k; ?>" class="catsaa catse<?php echo $grnt['id']; ?>" gg="advancecontent<?php echo $i; ?>" onclick="template('<?php echo $grnt['id']; ?>','cata<?php echo $i.$l.$j.$k; ?>');template456('<?php echo $grnt['id']; ?>')" ><?php echo $sts['grantee_name']; ?></a>        
                                                            </li>
                                                            <?php $k++; } } ?>
                                                        </ul>
                                                    
                                                    </li>
                                                       <?php $l++; } } ?>
                                                    
                                                </ul>
                                                
                                                </li>
                                        <?php $j++; } }?>
                                            </ul>
                                        
                                        </li>
                                        <?php $i++; } ?>
                                        
                                    </ul>
                                    <input type="hidden" class="form-control" id="ccc" 
                                               name="ccc" value="">
                                    <input type="hidden" class="form-control"  id="ddd"
                                               name="ddd" value=""> 
                                    
                                </div>
                    
                    </div>
                    <div class="col-md-3 paddinglake" id="docdiv">
                                <div class="boxtemplateagain">
                                    <div class="headone">
                                        <h4>Document</h4>
                                    </div>
           <!--------------------------- new  --------------------------------->
                               <div id ="demo1">
                                  <ul>
                                        <?php 
                                        $i = '1';
                                        foreach($doc as $value){
                                         
                                             ?>
                                        <li class="dropdown-toggle" id="dropdownMenus<?php echo $i; ?>" onclick="test34('<?php echo $i; ?>')" data-toggle="dropdowns" aria-haspopup="true" aria-expanded="true"> 
                                    
                                            <a href="javascript:void(0);" id="doca<?php echo $i; ?>" onclick="documents('<?php echo $value['document_temp_id']; ?>', 'doca<?php echo $i; ?>', 'advancecontents<?php echo $i; ?>')"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $value['document_temp_name'];?> </a> 
                                            
                                    <ul class="advancecontents" id="advancecontents<?php echo $i; ?>">
                                                <?php 
//                                                $this->db->group_by('status');
                                                $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $value['document_temp_id']))->result_array();
                                                  if($sta) {
                                                      $j = '1';
                                                      foreach($sta as $val)
                                                      { 
                                                     
                                                ?>
                                                <li id="dropdownseconds<?php echo $i.$j; ?>">
                                                  
                                                    <a href="javascript:void(0);" class="seca11" id="seca<?php echo $i.$j; ?>" onclick="documents('<?php echo $val['document_temp_id']; ?>', 'seca<?php echo $i.$j; ?>', 'advancecontents<?php echo $i; ?>')"> <i class="fa fa-caret-right" aria-hidden="true"></i>  <?php echo $val['document_temp_name']; ?> </a>  
                                                <ul>
                                                    <?php
                                                      $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $val['document_temp_id']))->result_array();
                                                      if($sta) {
                                                          $k =1;
                                                      foreach($sta as $val)
                                                      {
                                                        ?>
                                                    <li><i class="fa fa-caret-right" aria-hidden="true"></i>
                                                        <a href="javascript:void(0);" class="thia11" id="thia<?php echo $i.$j.$k; ?>" onclick="documents('<?php echo $val['document_temp_id']; ?>', 'thia<?php echo $i.$j.$k; ?>', 'advancecontents<?php echo $i; ?>')"><?php echo $val['document_temp_name']; ?></a> 
                                                    </li>
                                                       <?php $k++; } } ?>
                                                    
                                                </ul>
                                                
                                                </li>
                                        <?php $j++; } }?>
                                            </ul>
                                        
                                        </li>
                                        <?php $i++; } ?>
                                        
                                    </ul>   
                                        
                                    </div>
     <!--------------------------- end new  --------------------------------->
<!--                                <ul>
                                        <?php 
                                        $i = '1';
                                        foreach($doc as $value){
                                         
                                             ?>
                                        <li class="dropdown-toggle" id="dropdownMenus<?php echo $i; ?>" onclick="test34('<?php echo $i; ?>')" data-toggle="dropdowns" aria-haspopup="true" aria-expanded="true"> 
                                           
                                    <a href="javascript:void(0);" id="doca<?php echo $i; ?>" onclick="documents('<?php echo $value['document_temp_id']; ?>', 'doca<?php echo $i; ?>', 'advancecontents<?php echo $i; ?>')"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $value['document_temp_name'];?> </a>  for parent 
                                            <ul class="advancecontents" id="advancecontents<?php echo $i; ?>">
                                                <?php 
//                                                $this->db->group_by('status');
                                                $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $value['document_temp_id']))->result_array();
                                                  if($sta) {
                                                      $j = '1';
                                                      foreach($sta as $val)
                                                      { 
                                                     
                                                ?>
                                                <li id="dropdownseconds<?php echo $i.$j; ?>">
                                                  
                                                    <a href="javascript:void(0);" class="seca11" id="seca<?php echo $i.$j; ?>" onclick="documents('<?php echo $val['document_temp_id']; ?>', 'seca<?php echo $i.$j; ?>', 'advancecontents<?php echo $i; ?>')"> <i class="fa fa-caret-right" aria-hidden="true"></i>  <?php echo $val['document_temp_name']; ?> </a>  for child 
                                                <ul>
                                                    <?php
                                                      $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $val['document_temp_id']))->result_array();
                                                      if($sta) {
                                                          $k =1;
                                                      foreach($sta as $val)
                                                      {
                                                        ?>
                                                    <li><i class="fa fa-caret-right" aria-hidden="true"></i>
                                                        <a href="javascript:void(0);" class="thia11" id="thia<?php echo $i.$j.$k; ?>" onclick="documents('<?php echo $val['document_temp_id']; ?>', 'thia<?php echo $i.$j.$k; ?>', 'advancecontents<?php echo $i; ?>')"><?php echo $val['document_temp_name']; ?></a> sub child.-
                                                    </li>
                                                       <?php $k++; } } ?>
                                                    
                                                </ul>
                                                
                                                </li>
                                        <?php $j++; } }?>
                                            </ul>
                                        
                                        </li>
                                        <?php $i++; } ?>
                                        
                                    </ul>  -->
                                </div>
                    </div>
                      
                    <div class="col-md-6 paddinglake" id="finalDiv">
                        <div class="boxtemplateagain">
                            <div id="demo">
                <input type="hidden" value="<?php echo $param6?>" name="task_on" id="task_on">
                            
                            </div>
                        </div>
                    </div>
                </div>
          <!-- Modal -->
  <div class="modal fade" id="approve" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Comment</h4>
        </div>
        <div class="modal-body">
            <form role="form" action="<?php echo base_url(); ?>pelogin/user_comment/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                <div class="row" style="margin-bottom:10px">

                        <label class="control-label col-md-3">Comment <span style="color: red;margin-left: 1px">*</span></label>
                        <div class="col-md-5">

                            <input type="hidden" class="form-control" id="ccc" value="<?php echo  $param1; ?>"
                                               name="grant" value=""> 
                                    <input type="hidden" class="form-control"  id="ddd" value="<?php echo  $param5; ?>"
                                               name="document" value=""> 

                            <textarea rows="3" cols="6" class="form-control" 
                                        data-validation="length" 
                                        data-validation-length="min1" 
                                        data-validation-error-msg="Description is required"
                                        placeholder="Enter Description"
                                        name="comment"></textarea>                                                                                          
                        </div>

                    <div class="control-label col-md-4" style="text-align: right <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                        <div style="text-align: right;">

                            <button type="submit" class="btn btn-primary start" style="width:120px" 
                                value="Validate"   name="submit" onclick="return chkpass();">Comment</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>
     <input type="hidden" class="form-control"  id="catshowid"
                                               name="catshowid" value=""> 

    <script> 
        $(document).ready(function () {
           //var catsaa = document.getElementByClassName('catsaa<?php echo $param1; ?>').value;
           //alert('catsaa<?php echo $param1; ?>');
             var i = '1';
            for(i=1; i<30; i++)
            {
                $("#advancecontents"+i).hide(); 
            }
            var extcls = 'catse<?php echo $param1; ?>';
            var cat = '<?php echo $param1; ?>';
            var liid = '<?php echo $param2; ?>';
            var pid = '<?php echo $param3; ?>';
            var aid = '<?php echo $param4; ?>';
            var doc_id = '<?php echo $param5; ?>';
            //alert(doc_id);
           $("#ccc").val(cat);  $("#ddd").val(doc_id);
         //alert("<?php echo $param1; ?>");
        <?php if($param1){ ?>
         
         $("#ccc").val(cat);
        //$(".actives").removeClass('actives');
        $('.'+extcls).addClass('actives');
        $('#'+liid).addClass('actives');
         template(cat, liid);
         template456(cat, liid); 

         <?php } 
         if($param3){
         ?>
                 //alert(pid);
            $("#ddd").val(doc_id);
            $('#'+pid).show();
            $('#'+aid).addClass('active');
            documents(doc_id, aid, pid);
         <?php } ?>
});
        
        
        function year()
        {
            var cat = document.getElementById('e3').value;
            var id = document.getElementById('status').value;
           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/yearDropdown",
                    type: "POST",
                    data: {'status': id, 'category': cat},
                    success: function (response)
                    {
                        $("#years").html(response);

                    }

                });
            }
        }
        
           function template(e,liid)
        {
           var task_on = $("#task_on").val();
           //alert(task_on);
        var currentli = $(this).attr('gg');

            $("#catshowid").val(liid);
           $("#ccc").val(e);
           var temp = '1';
            
           if(temp=='')
           {
               alert("Please Select template");
           } 
           // var id = document.getElementById('status').value;
            
           if(temp){
               
                $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/template",
                    type: "POST",
                    data: {'id': e, 'temp': temp, 'liid': liid},
                    success: function (response)
                    {
                        if(task_on  == ""){
                        $("#demo").html(response);
                        }
                        abss();
                        
                        $('#lstFruits').multiselect({
                            includeSelectAllOption: true
                        });
                        //alert(response);
                    }
                });
            }
        }    
           
        
   function template456(e,liid)
        { 
           var temp = '1';
           if(temp=='')
           {
               alert("Please Select template");
           } 
       if(temp){
               
                $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/boldHeader",
//                     url: "<?php echo base_url(); ?>pelogin/show_template_manage",
                    type: "POST",
                    data: {'id': e, 'temp': temp, 'liid': liid},
                    success: function (response)
                    {
//                        alert(response); 
                        $("#demo1").html(response);
                        
//                      abss();
//                        
//                          $('#lstFruits').multiselect({
//                           includeSelectAllOption: true
//                        });
                        //alert(response);
                    }
                });
            }
            
        } 
        
      function documents(valu,aid, pid)
        {
            //var id = '1';
            $("#ddd").val(valu);
            //$("#vvv").val(valu);
            var liid = document.getElementById('catshowid').value;
            //alert(liid);
            var id = document.getElementById('ccc').value;
            var docu = document.getElementById('ddd').value;
            if(id)
            {
               $('#somment_div').show(); 
            }
            if(id=='')
            {
                alert("Please Select Grantee");
                return false;
            }
//            alert(pid);
//            alert(valu);
//            alert(aid);
//            alert(id);
            
//            $("#ccc").val(e);
//             $("#demo").html("response");
             $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/all_template",
                    type: "POST",
                    data: {'id': id, 'doc': valu, 'liid': liid, 'aid': aid, 'pid': pid},
                 
                    success: function (response)
                    { 
                       //alert(response);
                        if(response=='no')
                        {
                            //alert('Please complete previous step');
                        }
                        else
                        {
                        //alert(response);
                            $("#demo").html(response);
                            abss();
                        }  
                    }

                });
        }
        
        function delete_image(removeNum, id) {

            var r=confirm("Are you sure you want to Delete Image?");
            if(r==true)
            {
                $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/delete_parent_image",
                    type: "POST",
                    data: {'id': id,},
                    success: function (response)
                    {
                            jQuery('#rowCounta' + removeNum).remove();
                        
                    }

                });
            }
            else
            {
                return false;
            }

            
        }
        
        function delete_child_image(removeNum, id) {

            var r=confirm("Are you sure you want to Delete Image?");
            if(r==true)
            {
                $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/delete_child_image",
                    type: "POST",
                    data: {'id': id,},
                    success: function (response)
                    {
                            jQuery('#rowCounta' + removeNum).remove();
                        
                    }

                });
            }
            else
            {
                return false;
            }

            
        }
        
        function procid(e)
        {
            //alert(e);
            var r=confirm("Are you sure you want to proceed?");
            if(r==true)
            {
                $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/change_temp_status",
                    type: "POST",
                    data: {'id': e,},
                    success: function (response)
                    {
                        //alert(response);
                            //$("#demo").html(response);
                            $('#stat_valid').hide();
                        
                    }

                });
            }
            else
            {
                return false;
            }

        }
        function cmpstatus(e)
        {
            //alert(e);
            
            $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/completestatus",
                    type: "POST",
                    data: {'id': e,},
                    success: function (response)
                    {
                        //alert(response);
                            //$("#demo").html(response);
                            //$('#stat_valid').hide();
                        
                    }

                });
        }
        
//        function completestatus(e)
//        {
//            
//        }
        
        function skiptemp(g , d)
        {
//             alert(g);
//            alert(d);
            $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/skip_temp",
                    type: "POST",
                    data: {'grnt': g, 'doc': d},
                    success: function (response)
                    {
                       /// alert(response);
                            //$("#demo").html(response);
                            $('#stat_valid').hide();
                        
                    }

                });
        }
        function terminate_flow(e,d,p)
        {
           //alert(p);
            $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/terminate_temp_status",
                    type: "POST",
                    data: {'cat_status': e,'doc_status': d,'parent_id': p},
                    success: function (response)
                    {
                        //alert(response);
                            //$("#demo").html(response);
                            $('#stat_valid').hide();
                        
                    }

                });
        }
        
        function sss(id, e)
        {
            //alert(e);
            $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/getuser",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                        $("#user"+e).html(response);
                        //alert(response);

                    }

                });
        }
        
        function assignForArc()
        {
            
            var grant = document.getElementById('ccc').value;
            var doc = document.getElementById('ddd').value;
            //alert(grant+doc);
            $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/assignForArc",
                    type: "POST",
                    data: {'doc': doc, 'grant': grant},
                    success: function (response)
                    {
                        //alert(response);
                        if(response=='yes')
                        {
                           $("#ajaxmsgyes").show();
                           $("#ajaxmsgno").hide();
                        }
                        else
                        {
                            $("#ajaxmsgno").show();
                            $("#ajaxmsgyes").hide();
                        }
                        
                    }

                });
        }
        
function adremovecat()       
{
    //alert("aa");
   
    if($("#catdiv").hasClass('catcls'))
    {
        if($("#docdiv").hasClass('doccls'))
        { 
            $("#catdiv").show(); 
            $("#catdiv").removeClass('catcls');
            $("#finalDiv").removeClass('col-md-12');
            $("#finalDiv").addClass('col-md-9');
        }
        else
        { 
            $("#catdiv").show(); 
            $("#catdiv").removeClass('catcls');
            $("#finalDiv").removeClass('col-md-9');
            $("#finalDiv").addClass('col-md-6');
        }
    }
    else
    {
        if($("#docdiv").hasClass('doccls'))
        {  
            $("#catdiv").addClass('catcls');
            $("#catdiv").hide();
            $("#finalDiv").removeClass('col-md-9');
            $("#finalDiv").addClass('col-md-12');
        }
        else
        { 
            $("#catdiv").addClass('catcls');
            $("#catdiv").hide();
            $("#finalDiv").removeClass('col-md-6');
            $("#finalDiv").addClass('col-md-9');
        }
    }
    
    
    
//    $("#pagin li a").click(function() {
//        $("#pagin li").removeClass("active");
//        $(this).parent().addClass("active");
     
//        $("#pagin li a").removeAttr( "style" )
//        $("#pagin li a").removeClass("current1");
//        $(this).addClass("current1");
}

function adremovedoc()       
{
    if($("#docdiv").hasClass('doccls'))
    {
        if($("#catdiv").hasClass('catcls'))
        { 
            $("#docdiv").show(); 
            $("#docdiv").removeClass('doccls');
            $("#finalDiv").removeClass('col-md-12');
            $("#finalDiv").addClass('col-md-9');
        }
        else
        { 
            $("#docdiv").show(); 
            $("#docdiv").removeClass('doccls');
            $("#finalDiv").removeClass('col-md-9');
            $("#finalDiv").addClass('col-md-6');
        }
    }
    else
    {
        if($("#catdiv").hasClass('catcls'))
        { 
            $("#docdiv").addClass('doccls');
            $("#docdiv").hide();
            $("#finalDiv").removeClass('col-md-9');
            $("#finalDiv").addClass('col-md-12');
        }
        else
        { 
           $("#docdiv").addClass('doccls');
            $("#docdiv").hide();
            $("#finalDiv").removeClass('col-md-6');
            $("#finalDiv").addClass('col-md-9'); 
        }
    }
}
</script>
<script>
    function abss(){
        $('.lstFruits').multiselect({
                includeSelectAllOption: true
            });
             $('#btnSelected').click(function () {
                var selected = $("#lstFruits option:selected");
                var message = "";
                selected.each(function () {
                    message += $(this).text() + " " + $(this).val() + "\n";
                });
                alert(message);
            });
        //alert();
        $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        "onSelect": function () {
//            var input = $(this);
//            $("#datepicker2").datepicker("option", "minDate", input.datepicker("getDate"));
//            $("#datepicker2").datepicker("refresh");
            var start = $('#datepicker1').datepicker('getDate');
            
            var day = start.getDate();
            var month = start.getMonth() + 6;
            var year = start.getFullYear();
            if (month < 10) {
                month = "0" + month;
            }
            if (day < 10) {
                day = "0" + day;
            }

            var sss = year + "-" + month + "-" + day;

            $('#end_date').val(sss);
            
        }
        
    });
    $('.datepicker').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker3').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
     $('#datepicker4').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker5').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
     $('#datepicker6').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker7').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
     $('#datepicker8').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker9').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker31').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
     $('#datepicker21').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker11').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
     $('#datepicker16').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker17').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
     $('#datepicker18').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker19').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    CKEDITOR.replace( 'chk' );
    
}
    $(function() {

    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
    
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });

</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function contect()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>
    

    
<script>
window.rowCount = 1;
    function addMoredetail(frm, allowbook, a="") {
//$('.add').click(function (event) {
//alert(a);
         rowCount++;

    var recRow ='<tr id="rowCount' + rowCount + '"><td><input type="text" class="form-control" required="required" name="title[]" value="" placeholder="Enter Title"></td><td><input type="text" class="form-control" required="required" name="value[]" value=""  placeholder="Enter Value"></td><td><input type="hidden" name="type[]" required="required" value="not define"><input type="hidden" class="form-control" name="detail[]" required="required" value="Others"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></td></tr>';
            jQuery('#addeddetail' + a).append(recRow);
    
    }

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);

        jQuery('#rowCount' + removeNum).remove();
    }
</script>

<script>
window.rowCount = 1;
    function addMorehandlingstaf(frm, allowbook,e) {
//$('.add').click(function (event) {

         rowCount++;
        
if($(".trcounter").length<3)
{
    var recRow ='<tr id="rowCounts' + rowCount + '" class="trcounter"><td>Handling Staff <span style="color: red;margin-left: 1px">*</span> :<input type="hidden" class="form-control" required="required" name="title[]" value="Handling Staff" placeholder="Enter Title"></td><td><input type="text" class="form-control" required="required" name="value[]" value=""  placeholder="Enter Value"></td><td><input type="hidden" name="type[]" required="required" value="not define"><input type="hidden" class="form-control" name="detail[]" required="required" value="General Information"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRoww(' + rowCount + ');">Delete</a></td></tr>';
            //alert($(e).closest('tr'));
            $(e).closest('tr').after(recRow);
        
        

}
    }

    var rel = '<?php echo $edit_rel; ?>';

    function removeRoww(removeNum, y) {

         //alert(removeNum);

        jQuery('#rowCounts' + removeNum).remove();
    }
</script>

<script>
window.rowCount = 1;
    function addMoreweblink(frm, allowbook,e) {
//$('.add').click(function (event) {

         rowCount++;
        
//if($(".trcounter").length<3)
//{
    var recRow ='<tr id="rowCounts' + rowCount + '" class="trcounter"><td> Weblink <span style="color: red;margin-left: 1px">*</span> :<input type="hidden" class="form-control" required="required" name="title[]" value="Weblink" placeholder="Enter Title"></td><td><input type="text" class="form-control" required="required" name="value[]" value=""  placeholder="Enter Value"></td><td><input type="hidden" name="type[]" required="required" value="not define"><input type="hidden" class="form-control" name="detail[]" required="required" value="General Information"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRoww(' + rowCount + ');">Delete</a></td></tr>';
            //alert($(e).closest('tr'));
            $(e).closest('tr').after(recRow);
        
        

//}
    }

    var rel = '<?php echo $edit_rel; ?>';

    function removeRoww(removeNum, y) {

         //alert(removeNum);

        jQuery('#rowCounts' + removeNum).remove();
    }
</script>
    
<script>
window.rowCount = 1;
    function addMoreuser(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

    var recRow ='<tr id="rowCount' + rowCount + '"><td><select name="group[]" class="form-control" onchange="sss(this.value, ' + rowCount + ')"><option>Select Group</option><?php $grp = $this->db->get('dms_role')->result_array(); foreach ($grp as $row) { ?><option value="<?php echo $row['role_id']; ?>" <?php if ($result['role_id'] == $row['role_id']) { echo "selected";}?> ><?php echo $row['role_name']; ?></option><?php } ?></select></td><td><select name="user[]" id="user' + rowCount + '" class="form-control"><option>Select User</option></select></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></td>';


            jQuery('#addedflow').append(recRow);

    }
  
    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

        jQuery('#rowCount' + removeNum).remove();
    }
</script>

<script>
window.rowCount = 1;
    function addMoreuser(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

    var recRow ='<tr id="rowCount' + rowCount + '"><td><select name="group[]" class="form-control" onchange="sss(this.value, ' + rowCount + ')"><option>Select Group</option><?php $grp = $this->db->get('dms_role')->result_array(); foreach ($grp as $row) { ?><option value="<?php echo $row['role_id']; ?>" <?php if ($result['role_id'] == $row['role_id']) { echo "selected";}?> ><?php echo $row['role_name']; ?></option><?php } ?></select></td><td><select name="user[]" id="user' + rowCount + '" class="form-control"><option>Select User</option></select></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></td>';


            jQuery('#addedflow').append(recRow);

    }
  
    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

        jQuery('#rowCount' + removeNum).remove();
    }
</script>


<script>
window.rowCount = 1;
    function addMorescanfile(frm, allowbook, a="") {
//$('.add').click(function (event) {
//alert(a);
         rowCount++;

    var recRow ='<tr id="rowCount' + rowCount + '"><td><label class="control-label">Add Files : </label></td><td><input type="file" class="file-input" id="i'+ rowCount +'" name="scanned_image[]"  value="" ></td><td><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></td></tr>';
            jQuery('#uplodescanndiv' + a).append(recRow);
    
    }

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);

        jQuery('#rowCount' + removeNum).remove();
    }
    
</script>

<script>
window.rowCount = 1;
    function addMoreAdditionalfile(frm, allowbook, a="") {
//$('.add').click(function (event) {
//alert(a);
         rowCount++;

    var recRow ='<tr id="rowCount' + rowCount + '"><td><label class="control-label">Add Files : </label></td><td><input type="file" class="file-input" id="i'+ rowCount +'" name="detail_image[]"  value="" ></td><td><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></td></tr>';
            jQuery('#uplodeAdditionaldiv' + a).append(recRow);
    
    }

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);

        jQuery('#rowCount' + removeNum).remove();
    }
</script>

<script>
//for category

$(".catsaa").click(function() {
        abc = 'off'; 
       // alert(this.id);
        
        
        $(".actives").removeClass('actives');
        $('#'+this.id).addClass('actives');
        var currentli = $(this).attr('gg');
        $(".advancecontent").hide();
        $('#'+currentli).show(); 
        //testcat34(i)
        //return false
          //alert('asd');
});

var abc = 'on'; 
function testcat34(i)
{
    //alert(i);
    $(".advancecontent").hide();
        if(abc == 'on'){
            $('#advancecontent'+i).hide(); 
            abc = 'off'; 
        }
        else{
            $('#advancecontent'+i).show(); 
            abc = 'on'; 
        }
}
  
    </script>
    
    <script>
        //for Document
 //1111111   
    
//  $(document).ready(function () {
//        var i = '1';
//        for(i=1; i<30; i++)
//        {
//            $("#advancecontents"+i).hide(); 
//        }
//
//});
$(".seca11").click(function() {
        abcs = 'on'; 
        //alert(this.id);
        //$(this).addClass("active");
        $(".active").removeClass('active');
        $('#'+this.id).addClass('active');
        return false
          //alert('asd');
});
$(".thia11").click(function() {
        abcs = 'on'; 
        //alert(this.id);
        //$(this).addClass("active");
        $(".active").removeClass('active');
        $('#'+this.id).addClass('active');
        return false
          //alert('asd');
});
 var abcs = 'off';
function test34(i)
{
    $(".advancecontents").hide();
     $(".active").removeClass('active');
        $("#doca"+i).addClass('active');
        if(abcs == 'off'){
            $('#advancecontents'+i).hide(); 
            abcs = 'on'; 
        }
        else{
            //$(".doca"+i).hide(); 
            $('#advancecontents'+i).show(); 
            abcs = 'off'; 
        }
    
}
 
    </script>


 <script>
      function delete_record(id){
          var result = confirm("Are you sure to delete?");
                                                if (result) {
                                                   // return true;
                                                
       $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/delete_individual_record",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                       // $("#user"+e).html(response);
                     // alert("Delete Successfully");
location.reload();
                    }

                });   
                } else {
                                                    return false;
                                                }
      }
      function delete_child_record(id){
          var result = confirm("Are you sure to delete?");
                                                if (result) {
                                                   // return true;
                                                
       $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/delete_child_record",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                       // $("#user"+e).html(response);
                     // alert("Delete Successfully");
location.reload();
                    }

                });   
                } else {
                                                    return false;
                                                }
      }
       function delete_child_record_multi(id,id1){
          var result = confirm("Are you sure to delete?");
                                                if (result) {
                                                   // return true;
                                                
       $.ajax({
                    url: "<?php echo base_url(); ?>pelogin/delete_child_record_multi",
                    type: "POST",
                    data: {'id': id,'id1': id1},
                    success: function (response)
                    {//alert(response);
                        console.log(response);
                       // $("#user"+e).html(response);
                     // alert("Delete Successfully");
location.reload();
                    }

                });   
                } else {
                                                    return false;
                                                }
      }
  </script>





