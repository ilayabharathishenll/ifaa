<?php
error_reporting(~E_NOTICE);
$RoleId = $this->session->userdata['login']['user_role'];
$id_ = $this->session->userdata['login']['user_id'];
//echo phpinfo();die;
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <ul class="breadcrumb">
                                    <li>
                                            <i class="fa fa-home"></i>
                                            <a href="<?php echo base_url('pelogin'); ?>/dashboard">Home</a>
                                    </li>
<!--                                                                <li><?php echo $pagetitle; ?></li>-->
                                    <li>
                                            <a href="javascript:;"><?php echo $pagetitle; ?></a>
                                    </li>
<!--                                                                <li>Dynamic Tables</li>-->
                            </ul>
                                    <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                    <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>
                            </div>
                            <div class="description"></div>
                            <?php if ($this->session->flashdata('flash_message')) { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <a class="close" data-dismiss="alert" href="javascript:;" 
                                       aria-hidden="true">X</a>
                                    <h4><i class="fa fa-smile-o"></i> <?php
                                        echo
                                        $this->session->flashdata('flash_message');
                                        ?>  <i class="fa fa-thumbs-up"></i></h4>
                                </div>
                            <? }
                            if($this->session->flashdata('permission_message')){ ?>
                                <div class="alert alert-block alert-danger fade in">
                                    <a class="close" data-dismiss="alert" href="javascript:;" 
                                       aria-hidden="true">X</a>
                                    <h4><i class="fa fa-frown-o"></i> <?php
                                        echo
                                        $this->session->flashdata('permission_message');
                                        ?></h4>
                                </div>
                                    <?php }
                                    if($RoleId == '5'){
                                    ?>

                                <div class="row">
                                    <div class="col-md-12">
                                            <!-- BOX -->
                                            <div class="box border primary">
                                                    <div class="box-title">
                                                            <h4><i class="fa fa-table"></i><?php echo 'Ongoing/Completed Grants Created by Others';?></h4>
                                                            <div class="tools hidden-xs">
                                                                    <a href="#box-config" data-toggle="modal" class="config">
                                                                            <i class="fa fa-cog"></i>
                                                                    </a>
                                                                    <a href="javascript:;" class="reload">
                                                                            <i class="fa fa-refresh"></i>
                                                                    </a>
                                                                    <a href="javascript:;" class="collapse">
                                                                            <i class="fa fa-chevron-up"></i>
                                                                    </a>
                                                                    <a href="javascript:;" class="remove">
                                                                            <i class="fa fa-times"></i>
                                                                    </a>
                                                            </div>
                                                    </div>
                                                    <div class="box-body">
                                                            <table id="datatable15" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                            <tr>
                                                                                    <th class="center hidden-xs">Grant Number</th>
<!--                                                                                    <th class="center hidden-xs">Programme/Initiative</th>-->
                                                                                    <th class="center hidden-xs">Grant Status</th>
<!--                                                                                    <th class="center hidden-xs">Start Date</th>
                                                                                    <th class="center hidden-xs">End Date</th>
                                                                                    <th class="center hidden-xs">Grantee Name</th>-->
            <!--                                                                        <th class="center hidden-xs">Created By</th>-->
                                                                                    <th class="center hidden-xs">Workflow Status</th>
                                                                                    <th class="center hidden-xs"> Approval User Name</th>
                                                                                    <th class="center hidden-xs"> Document Type</th>
                                                                                    <th class="center hidden-xs"> Created By</th>
                                                                                    
<!--                                                                                    <th class="center hidden-xs"> Comments</th>-->
                                                                                    
                                                                                    <th class="center hidden-xs"> Detail</th>
                                                                            </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php         
                                                                        $balance = "SELECT * FROM `dms_grant`where (categorytype_id='1' or categorytype_id='2') and created_by!=$id_  ORDER BY id DESC LIMIT 10";
                                                                        $query = $this->db->query($balance);
                                                                        $data = $query->result_array();
                                                                        //$data = $this->db->get_where('dms_grant',array('categorytype_id' => '2'))->result_array();
                                                                            $i = 1;
                                                                            foreach ($data as $row) {
                                                                                $creator = $this->db->get_where('dms_user', array('user_id' => $row['created_by']))->row_array();
                                                                            $cat = $this->db->get_where('dms_category', array('category_id' => $row['category_id']))->row_array();
                                                                            $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $row['categorytype_id']))->row_array();
                                                                            $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $row['grantee_name']))->row_array();
                                                                            $cat = $this->db->get_where('dms_category', array('category_id' => $row['category_id']))->row_array();
                                                                            
                                                                           // echo $row['id'];
                                                                            $this->db->where('grant_temp_id', $row['id']);
                                                                            $this->db->limit(1);
                                                                            $this->db->order_by('temp_approve_id',"DESC");
                                                                            $workflow = $this->db->get('dms_temp_approve')->row_array();
                                                                            
                                                                            $this->db->where('comments', $workflow['comments']);
                                                                            $comm = $this->db->get('dms_approval_comment')->row_array();
                                                                            
                                                                            $this->db->where('user_id', $workflow['user_id']);
                                                                            $user = $this->db->get('dms_user')->row_array();
                                                                            
                                                                            $this->db->where('document_temp_id', $workflow['document_temp_id']);
                                                                            $document = $this->db->get('dms_document_template')->row_array();
                                                                            //print_r($workflow);
                                                                        ?>
                                                                            <tr class="gradeX">
                                                                                    <td class="center hidden-xs"><?php echo $row['grant_number']; ?></td>
<!--                                                                                    <td class="center hidden-xs"><?php echo $cat['category_name']; ?></td>-->
                                                                                    <td class="center hidden-xs"><?php echo $status['categorytype_name']; ?></td>
<!--                                                                                    <td class="center hidden-xs"><?php echo $row['start_date']; ?></td>
                                                                                    <td class="center hidden-xs"><?php echo $row['end_date']; ?></td>
                                                                                    <td class="center hidden-xs"><?php echo $grantee['grantee_name']; ?></td>-->
            <!--                                                                        <td class="center hidden-xs"><?php echo $creator['temp_approve_status']; ?></td>-->
                                                                                    <td class="center hidden-xs"><?php echo $workflow['temp_approve_status']; ?></td>
                                                                                    <td class="center hidden-xs"><?php echo $user['user_name']; ?></td>
                                                                                    <td class="center hidden-xs"><?php echo $document['document_temp_name']; ?></td>
                                                                                      <td class="center hidden-xs"><?php echo $creator['user_name']; ?></td>
<!--                                                                                    <td class="center hidden-xs">
                                                                                        <a href="<?php echo base_url(); ?>pelogin/show_template_manage/<?php echo $row['id']; ?>"><?php echo $workflow['temp_approve_comment']; ?></a>
                                                                                    </td>-->
                                                                                    <td class="center hidden-xs"><?php if($workflow['temp_approve_id']){ ?>
                                                                                        <a style="color:green" href="<?php echo base_url(); ?>pelogin/approval_templates/<?php echo $workflow['temp_approve_id']; ?>/comment/<?php echo $workflow['temp_approve_id']; ?>" title="detail">View Detail</a>
<!--                                                                                        <a style="color:green" href="<?php echo base_url(); ?>pelogin/approval_template/<?php echo $row['id']; ?>" title="detail">View Detail</a>-->
                                                                            <?php } ?>
                                                                                    </td>
                                                                            </tr>
                                                                            <?php $i++; } ?>


                                                                    </tbody>
                                                                    <tfoot>
<!--                                                                            <tr>
                                                                                    <th class="center hidden-xs">Grant Number</th>
                                                                                    <th class="center hidden-xs">Programme/Initiative</th>
                                                                                    <th class="center hidden-xs">Grant Status</th>
                                                                                    <th class="center hidden-xs">Start Date</th>
                                                                                    <th class="center hidden-xs">End Date</th>
                                                                                    <th class="center hidden-xs">Grantee Name</th>
                                                                                    <th class="center hidden-xs">workflow status</th>
                                                                                    <th class="center hidden-xs"> Approval User name</th>
                                                                                    <th class="center hidden-xs"> Document Type</th>
                                                                                    <th class="center hidden-xs"> Comments</th>
                                                                            </tr>-->
                                                                    </tfoot>
                                                            </table>
                                                    </div>
                                                
                                            </div>
                                            <!-- /BOX -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                            <!-- BOX -->
                                            <div class="box border primary">
                                                    <div class="box-title">
                                                            <h4><i class="fa fa-table"></i><?php echo 'Ongoing/Completed Grants Created by Self';?></h4>
                                                            <div class="tools hidden-xs">
                                                                    <a href="#box-config" data-toggle="modal" class="config">
                                                                            <i class="fa fa-cog"></i>
                                                                    </a>
                                                                    <a href="javascript:;" class="reload">
                                                                            <i class="fa fa-refresh"></i>
                                                                    </a>
                                                                    <a href="javascript:;" class="collapse">
                                                                            <i class="fa fa-chevron-up"></i>
                                                                    </a>
                                                                    <a href="javascript:;" class="remove">
                                                                            <i class="fa fa-times"></i>
                                                                    </a>
                                                            </div>
                                                    </div>
                                                    <div class="box-body">
                                                            <table id="datatable25" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                            <tr>
                                                                                    <th class="center hidden-xs">Grant Number</th>
<!--                                                                                    <th class="center hidden-xs">Programme/Initiative</th>-->
                                                                                    <th class="center hidden-xs">Grant Status</th>
<!--                                                                                    <th class="center hidden-xs">Start Date</th>
                                                                                    <th class="center hidden-xs">End Date</th>
                                                                                    <th class="center hidden-xs">Grantee Name</th>-->
            <!--                                                                        <th class="center hidden-xs">Created By</th>-->
                                                                                    <th class="center hidden-xs">Workflow Status</th>
                                                                                    <th class="center hidden-xs"> Approval User Name</th>
                                                                                    <th class="center hidden-xs"> Document Type</th>
                                                                                     <th class="center hidden-xs"> Created By</th>
<!--                                                                                    <th class="center hidden-xs"> Comments</th>-->
                                                                                    <th class="center hidden-xs"> Detail</th>
                                                                            </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php         
                                                                 $balance1 = "SELECT * FROM `dms_grant`where (categorytype_id='1' or categorytype_id='2') and created_by=$id_  ORDER BY id DESC LIMIT 10";
                                                                        $query1 = $this->db->query($balance1);
                                                                        $data1 = $query1->result_array();
                                                                        //$data = $this->db->get_where('dms_grant',array('categorytype_id' => '2'))->result_array();
                                                                            $i = 1;
                                                                            foreach ($data1 as $row) {
                                                                                $creator = $this->db->get_where('dms_user', array('user_id' => $row['created_by']))->row_array();
                                                                            $cat = $this->db->get_where('dms_category', array('category_id' => $row['category_id']))->row_array();
                                                                            $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $row['categorytype_id']))->row_array();
                                                                            $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $row['grantee_name']))->row_array();
                                                                            $cat = $this->db->get_where('dms_category', array('category_id' => $row['category_id']))->row_array();
                                                                            
                                                                           // echo $row['id'];
                                                                            $this->db->where('grant_temp_id', $row['id']);
                                                                            $this->db->limit(1);
                                                                            $this->db->order_by('temp_approve_id',"DESC");
                                                                            $workflow = $this->db->get('dms_temp_approve')->row_array();
                                                                           // echo "Dfdfg";
                                                                            //print_r($workflow);
                                                                            $this->db->where('comments', $workflow['temp_approve_comment']);
                                                                            $comm = $this->db->get('dms_approval_comment')->row_array();
                                                                            
                                                                            $this->db->where('user_id', $workflow['user_id']);
                                                                            $user = $this->db->get('dms_user')->row_array();
                                                                            
                                                                            $this->db->where('document_temp_id', $workflow['document_temp_id']);
                                                                            $document = $this->db->get('dms_document_template')->row_array();
                                                                            //print_r($workflow);
                                                                        ?>
                                                                            <tr class="gradeX">
                                                                                    <td class="center hidden-xs"><?php echo $row['grant_number']; ?></td>
<!--                                                                                    <td class="center hidden-xs"><?php echo $cat['category_name']; ?></td>-->
                                                                                    <td class="center hidden-xs"><?php echo $status['categorytype_name']; ?></td>
<!--                                                                                    <td class="center hidden-xs"><?php echo $row['start_date']; ?></td>
                                                                                    <td class="center hidden-xs"><?php echo $row['end_date']; ?></td>
                                                                                    <td class="center hidden-xs"><?php echo $grantee['grantee_name']; ?></td>-->
            <!--                                                                        <td class="center hidden-xs"><?php echo $creator['temp_approve_status']; ?></td>-->
                                                                                    <td class="center hidden-xs"><?php echo $workflow['temp_approve_status']; ?></td>
                                                                                    <td class="center hidden-xs"><?php echo $user['user_name']; ?></td>
                                                                                    <td class="center hidden-xs"><?php echo $document['document_temp_name']; ?></td>
                                                                                     <td class="center hidden-xs"><?php echo $creator['user_name']; ?></td>
<!--                                                                                    <td class="center hidden-xs">
                                                                                        <a href="<?php echo base_url(); ?>pelogin/show_template_manage/<?php echo $row['id']; ?>"><?php echo $workflow['temp_approve_comment']; ?></a>
                                                                                    </td>-->
                                                                                     <td class="center hidden-xs"><?php //print_r($workflow);?><?php if($workflow['temp_approve_id']){ ?>
                                                                                         <a style="color:green" href="<?php echo base_url(); ?>pelogin/approval_templates/<?php echo $workflow['temp_approve_id']; ?>/comment/<?php echo $workflow['temp_approve_id']; ?>" title="detail">View Detail</a>
<!--                                                                                        <a style="color:green" href="<?php echo base_url(); ?>pelogin/approval_template/<?php echo $row['id']; ?>" title="detail">View Detail</a>-->
                                                                                     <?php }?>
                                                                                     </td>
                                                                            </tr>
                                                                            <?php $i++; } ?>


                                                                    </tbody>
                                                                    <tfoot>
<!--                                                                            <tr>
                                                                                    <th class="center hidden-xs">Grant Number</th>
                                                                                    <th class="center hidden-xs">Programme/Initiative</th>
                                                                                    <th class="center hidden-xs">Grant Status</th>
                                                                                    <th class="center hidden-xs">Start Date</th>
                                                                                    <th class="center hidden-xs">End Date</th>
                                                                                    <th class="center hidden-xs">Grantee Name</th>
                                                                                    <th class="center hidden-xs">workflow status</th>
                                                                                    <th class="center hidden-xs"> Approval User name</th>
                                                                                    <th class="center hidden-xs"> Document Type</th>
                                                                                    <th class="center hidden-xs"> Comments</th>
                                                                            </tr>-->
                                                                    </tfoot>
                                                            </table>
                                                    </div>
                                                
                                            </div>
                                            <!-- /BOX -->
                                    </div>
                                </div>
                                    <?php } ?>
                            </div>
                    </div>
                </div>
                               
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>

  




