<?php 
    error_reporting(~E_NOTICE);
    
    $grantee_name = $this->db->get_where("dms_grantee", array('grantee_id' => $result['grantee_name']))->row_array();

    $category = $this->db->get_where("dms_category", array('category_id' => $result['category_id']))->row_array();
    $status = $this->db->get_where("dms_categorytype", array('categorytype_id' => $result['categorytype_id']))->row_array();
    $key = explode(",", $parent['parent_keyword']);
    $roleId = $this->session->userdata['login']['user_role'];
    $document = $this->db->get_where("dms_document_template", array('document_temp_id' => $doc))->row_array();
    
    $Assi_doc = $this->db->get_where("dms_assin_template", array('document_temp_id' => $doc, 'termination' => 'enable'))->row_array();
?> 

<div class="headone">
    <h4><?php echo $document['document_temp_name']; ?></h4>
</div>
<div class="boxtemplateagain" style="background: #fff;">
    <div  class="col-md-12" style="text-align: right; margin: 10px 0;">
        <?php if($text_editor == 'enable') { ?>
            <button  style="margin-right:5px;"class="btn btn-primary start" onclick="myFunction()">Print Description</button>
        
        <?php }
            if($Assi_doc)
            { ?>
                
                    <button type="button" class="btn btn-primary start" 
                            value="Validate" id="parent_sub"   name="submit" onclick="terminate_flow('<?php echo $parent['parent_temp_id']; ?>');"> Termination Button</button>
               
        <?php
        
            }
        ?> 
    </div>
<!--      <div class="headunderbox">
          <h4><?php echo $document['document_temp_name']; ?></h4>
      </div>-->
      
      
                                   
                                            <table class="datatable table table-striped table-bordered table-hover templatetable">
                                                <thead>
                                                <tr>
                                                    <th>Program and Initiatives : </th>
                                                    <th>Grant Status : </th>
                                                    <th>Grant ID : </th>
                                                    <th>Grantee name : </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $category['category_name']; ?></td>
                                                        <td><?php echo $status['categorytype_name']; ?></td>
                                                        <td><?php echo $result['grant_number'] ?> </td>
                                                        <td> <?php echo $grantee_name['grantee_name'] ?> </td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                            
                                      
      
      <div class="padingagain">
                                        <form role="form" action="<?php echo base_url(); ?>pelogin/add_parent_template/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                           <?php if($text_editor == 'enable') { ?>
                                            <div class="row" style="margin-bottom:10px">
                                                 
                                                    <label class="control-label col-md-12">Description </label>
                                                    <div class="col-md-12"   id="chkediter">
                                                         
                                                        
                                                         
                                                        <textarea rows="3" cols="6"  class="ckeditor" id="chk" 
                                                                  placeholder="Enter Description"
                                                                  name="description"><?php if($parent['parent_description']){ echo $parent['parent_description']; } else { echo $description; } ?>
                                                        </textarea>  
                                                        
<!--                                                        <textarea rows="3" cols="6" 
                                                                  placeholder="Enter Description"
                                                                  name="description"><?php if($parent['parent_description']){ echo $parent['parent_description']; } else { echo $description; } ?>
                                                        </textarea>-->
                                                        
                                                    </div>
                                            </div>
                                            <?php } ?>
                                           
                                           
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="col-md-12">
                                                    <div class="box-title">
                                                        <h4>Meta Data</h4>
                                                        <div class="tools hidden-xs">

                                                        </div>
                                                    </div>
                                                    <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $parent['parent_temp_id']; ?>">  
                                                        <input type="hidden" class="form-control" 
                                                               name="grant_id" value="<?php echo $grant; ?>"> 
                                                        <input type="hidden" class="form-control" 
                                                               name="doc_id" value="<?php echo $doc; ?>"> 
                                                    <div class="control-label col-md-11"> 
                                                    <table class="table  granttable" style="border: 1px;">
                                                        <thead>
                                                            <tr>
                                                                <th>Title</th>
                                                                <th>Value</th>
                                                                 <th>
<!--                                                                     <a  class="button btn btn-primary grantbut" style="display:inline-block;" onclick="addMoredetail(this.form,<?php echo $bookallow; ?>);" >Add More</a>-->
                                                                 </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="addeddetail">
                                                            
                                                            
                                                            
                                                           
                                                            <?php if ($parent) {
                                                                $i =0;
                                                                $meta = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc))->result_array();
                                                                $meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'General Information'))->result_array();
                                                                $meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'Author, Publisher, Access'))->result_array();
                                                                $meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'Physical Characteristics'))->result_array();
                                                                $meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'Digitisation Details'))->result_array();
                                                                $meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $grant, 'template_id' =>$parent['parent_temp_id'], 'document_temp_id' => $doc, 'detail' => 'Others'))->result_array();
                                                                ?> 
                                                                    
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <h2>General Information </h2>
                                                                    </td>
                                                                
                                                                </tr>    
                                                                <?php
                                                                foreach($meta1 as $vals) { ?>
                                                            <tr id="rowCount<?php echo $i.'a'; ?>">
                                                                <td><?php //if($vals['metadata_type'] == 'define'){ 
                                                                   
                                                                    echo $vals['metadata_title']; ?>  <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                    <?php// } else { ?>
<!--                                                                    <input type="text" class="form-control" required="required" name="title[]" value="<?php echo $vals['metadata_title']; ?>">-->
                                                                     <?php// }?>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" required="required" name="value[]" value="<?php echo $vals['metadata_value']; ?>" <?php if($vals['metadata_title']=='Date'){ ?> readonly="" id="datepicker1" <?php } ?>>
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" value="<?php echo $vals['metadata_type']; ?>">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                    <?php if($vals['metadata_type']=='define' AND $vals['metadata_title'] == 'Handling Staff') { ?>
                                                                    <a  class="button btn btn-primary grantbut" style="display:inline-block;" onclick="addMorehandlingstaf(this.form,'<?php echo $bookallow; ?>',this);" >Add More</a>
                                                                    
                                                                    <?php } if($vals['metadata_type']!='define'){ ?>
                                                                    <a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow('<?php echo $i.'a'; ?>');">Delete</a>
                                                                <?php } ?>
                                                                </td>
                                                            </tr>
                                                                <?php $i++; } ?>
                                                            <tr>
                                                                    <td colspan="3">
                                                                        <h2>Author, Publisher, Access </h2>
                                                                    </td>
                                                                
                                                            </tr>    
                                                                <?php
                                                                foreach($meta2 as $vals) { ?>
                                                            <tr id="rowCount<?php echo $i.'a'; ?>">
                                                                <td><?php if($vals['metadata_type'] == 'define'){ 
                                                                   
                                                                    echo $vals['metadata_title']; ?>  <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                    <?php } else { ?>
                                                                    <input type="text" class="form-control" required="required" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                     <?php }?>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" required="required" name="value[]" value="<?php echo $vals['metadata_value']; ?>">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" value="<?php echo $vals['metadata_type']; ?>">
                                                                    
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Author, Publisher, Access">
                                                                    <?php if($vals['metadata_type']!='define'){ ?>
                                                                    <a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow('<?php echo $i.'a'; ?>');">Delete</a>
                                                                <?php } ?>
                                                                </td>
                                                            </tr>
                                                                <?php $i++; } ?>
                                                            
                                                            <tr>
                                                                    <td colspan="3">
                                                                        <h2>Physical Characteristics </h2>
                                                                    </td>
                                                                
                                                                </tr>    
                                                                <?php
                                                                foreach($meta3 as $vals) { ?>
                                                            <tr id="rowCount<?php echo $i.'a'; ?>">
                                                                <td><?php if($vals['metadata_type'] == 'define'){ 
                                                                   
                                                                    echo $vals['metadata_title']; ?>  <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                    <?php } else { ?>
                                                                    <input type="text" class="form-control" required="required" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                     <?php }?>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" required="required" name="value[]" value="<?php echo $vals['metadata_value']; ?>">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" value="<?php echo $vals['metadata_type']; ?>">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Physical Characteristics">
                                                                    <?php if($vals['metadata_type']!='define'){ ?>
                                                                    <a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow('<?php echo $i.'a'; ?>');">Delete</a>
                                                                <?php } ?>
                                                                </td>
                                                            </tr>
                                                                <?php $i++; } ?>
                                                            <tr>
                                                                    <td colspan="3">
                                                                        <h2>Digitisation Details </h2>
                                                                    </td>
                                                                
                                                            </tr>    
                                                                <?php
                                                                foreach($meta4 as $vals) { ?>
                                                            <tr id="rowCount<?php echo $i.'a'; ?>">
                                                                <td><?php if($vals['metadata_type'] == 'define'){ 
                                                                   
                                                                    echo $vals['metadata_title']; ?>  <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                    <?php } else { ?>
                                                                    <input type="text" class="form-control" required="required" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                     <?php }?>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" required="required"<?php if($vals['metadata_title']=='Date of digitisation'){ ?> readonly="" id="datepicker2"<?php } ?> name="value[]" value="<?php echo $vals['metadata_value']; ?>">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" value="<?php echo $vals['metadata_type']; ?>">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Digitisation Details">
                                                                    <?php if($vals['metadata_type']!='define'){ ?>
                                                                    <a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow('<?php echo $i.'a'; ?>');">Delete</a>
                                                                <?php } ?>
                                                                </td>
                                                            </tr>
                                                                <?php $i++; } ?>
<!--                                                            <tr>
                                                                    <td colspan="3">
                                                                        <h2>General Information </h2>
                                                                    </td>
                                                                
                                                                </tr>    -->
                                                                <?php
                                                                foreach($meta5 as $vals) { ?>
                                                            <tr id="rowCount<?php echo $i.'a'; ?>">
                                                                <td><?php if($vals['metadata_type'] == 'define'){ 
                                                                   
                                                                    echo $vals['metadata_title']; ?>  <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                    <?php } else { ?>
                                                                    <input type="text" class="form-control" required="required" name="title[]" value="<?php echo $vals['metadata_title']; ?>">
                                                                     <?php }?>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" required="required" name="value[]" value="<?php echo $vals['metadata_value']; ?>">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" value="<?php echo $vals['metadata_type']; ?>">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Others">
                                                                    <?php if($vals['metadata_type']!='define'){ ?>
                                                                    <a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow('<?php echo $i.'a'; ?>');">Delete</a>
                                                                <?php } ?>
                                                                </td>
                                                            </tr>
                                                                <?php $i++; } ?>
                                                                    
                                                            
                                                            
                                                            <?php } else {?>
                                                            
                                                            <tr>
                                                                <td colspan="3">
                                                                    <h2>General Information </h2>
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>Document ID <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Document ID"  placeholder="Enter Title">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value="<?php echo $document['short_code']; ?>"  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Type of Document <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Type of Document">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value="<?php echo $document['document_temp_name']; ?>"  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Digital file name <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Digital file name">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Date" >
                                                                </td>
                                                                <td>
                                                                    <input type="text" readonly="" class="form-control" name="value[]" required="required" value="" id="datepicker1"  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                </td>
                                                            </tr>
                                                            <tr id="trhand">
                                                                <td>Handling Staff <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Handling Staff">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                    <a  class="button btn btn-primary grantbut" style="display:inline-block;" onclick="addMorehandlingstaf(this.form,'<?php echo $bookallow; ?>',this);" >Add More</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Language of The Original Material <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Language of the Original Material">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Comments <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Comments">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Comments">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Search Keywords <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="keywords">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter keyword">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="General Information">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <h2>Author, Publisher, Access </h2>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>Creator/ Author <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Creator/ Author" >
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Author, Publisher, Access">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Creator/Editor <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Creator/Editor">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Author, Publisher, Access">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Publisher <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Publisher" >
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Author, Publisher, Access">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Place of Publication <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Place of Publication">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Author, Publisher, Access">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Copyright <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Copyright">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Author, Publisher, Access">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Access <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Access">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Author, Publisher, Access">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Reference to other documents – URL   <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Reference to other documents – URL">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Reference to other documents – URL">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Author, Publisher, Access">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3"><h2>Physical Characteristics </h2>
                                                                    
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>Dimensions (in cm) <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Dimensions (in cm)">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Physical Characteristics">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Physical Characteristics <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Physical Characteristics">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Physical Characteristics">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Medium of Original Material <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Medium of Original Material">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Physical Characteristics">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Extent/No. of Pages <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Extent/No. of Pages" >
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Physical Characteristics">
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td>Place where the original Material is kept <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Place where the original Material is kept" >
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define" >
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Physical Characteristics">
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td colspan="3"><h2>Digitisation Details</h2>
                                                                    
                                                                </td>
                                                                
                                                            </tr>
                                                            <tr>
                                                                <td>Creator of the Digital Copy <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Creator of the Digital Copy">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Digitisation Details">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Date of digitisation <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Date of digitisation">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" readonly="" name="value[]" id="datepicker2"  required="required" value=""  placeholder="Enter Value" >
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Digitisation Details">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Comments on digitisation <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Comments on digitisation">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Digitisation Details">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Hardware used <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Hardware used">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Digitisation Details">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Software used <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Software used">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Digitisation Details">
                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td>Digital container <span style="color: red;margin-left: 1px">*</span> :
                                                                    <input type="hidden" class="form-control" name="title[]" required="required" value="Digital container">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    <input type="hidden" class="form-control" name="type[]" required="required" value="define">
                                                                    <input type="hidden" class="form-control" name="detail[]" required="required" value="Digitisation Details">
                                                                </td>
                                                            </tr>
                                                            
                                                            
                                                            
<!--                                                            <tr>
                                                                <td>
                                                                     <input type="text" class="form-control" name="title[]" required="required" value=""  placeholder="Enter Title">
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" name="value[]" required="required" value=""  placeholder="Enter Value">
                                                                </td>
                                                                <td>
                                                                    a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow('<?php echo $i.'a'; ?>');">Delete</a>
                                                                </td>
                                                            </tr>-->
                                                            <?php } ?>
                                                            
                                                        </tbody>
                                                    </table>
                                                        <div class=" col-md-4">
                                                        <a  class="button btn btn-primary grantbut" style="display:inline-block;" onclick="addMoredetail(this.form,<?php echo $bookallow; ?>);" >Add More</a>
                                                                
                                                    </div>
                                                    
                                                </div>
                                                        
                                            </div>
                                            </div>
<!--                                            <div class="row" style="margin-bottom:10px">
                                                
                                                    <label class="control-label col-md-3" style="    margin: 6px 0;">Keyword </label>
                                                    <div class="col-md-5">
                                                    
                                                       <?php if(!$key) { ?>   
                                                        <input type="text" class="form-control" name="keyword[]" 
                                                                               placeholder="Enter Keyword"
                                                                               value="<?php echo $parent['parent_keyword']; ?>"> 
                                                       <?php } ?>
                                                    
                                                    </div>
                                                   
                                              
                                                
                                                    <div class=" col-md-4">
                                                        <a  class="button btn addtable start add" style="display:inline-block;margin-top: 7px;" onclick="addkeyRows(this.form,<?php echo $bookallow; ?>);" >Add Keyword</a>
                                                    </div>
                                            
                                            </div>
                                            
                                                
                                             <div id="addedkey" >
                                                 
                                                 <?php if($key) { 
                                    $i = 0;
                                           foreach ($key as $value){
                                    ?>
                                <div class="row" id="rowCount<?php echo $i.'a'; ?>" style="margin-top:10px;">
                                    <div class="control-label col-md-6" >
                                        <label  class="control-label col-md-3" style="    margin: 6px 0;">
                                            <span style="visibility:hidden;">Keywords</span>
                                        </label>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control mng" required="" name="keyword[]"  value="<?php echo $key[$i]; ?>" placeholder="Enter Keywords" >
                                        </div>
                                        <div class="col-md-4">
                                            <a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow('<?php echo $i.'a'; ?>');">Delete</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <?php $i++; } }?>
                                             
                                            </div>    -->
                                            
                                           
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="col-md-12">
                                                <div class="box-title">
                                                    <h4>Upload Scanned Copy</h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                             
                                                    <label class="control-label col-md-4">Add Files Here </label>
                                                    <div class="col-md-4">
                                                         
                                                        <input name="scanned_image" type="file"
                                                            class="file-input" style="font-size:11px;" value="<?php echo $result['user_image']; ?>" >                                                                                  
                                                    </div>
                                                    <div class="col-md-4">
                                                        <!--<img  class="img-responsive" src="<?php echo base_url()."uploads/".$parent['parent_scann_copy']; ?>" style="height:90px;width:120px;" alt=""/>-->
                                                 <a href="<?php echo img_path."local_path/".$parent['parent_scann_copy']; ?>" target="_blank"><img  class="img-responsive" src="<?php echo img_path."local_path/".$parent['parent_scann_copy']; ?>" style="height:90px;width:120px;" alt=""/></a>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                             
                                           
<!--                                            <div class="row" style="margin-bottom:10px">
                                                <div class="col-md-12">
                                                <div class="box-title">
                                                    <h4>Work Flow </h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                               
                                               
                                                
                                                    <table style=" width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Select Group : </th>
                                                                <th>Select User : </th>
                                                                <th>   <a  class="button btn addtable start add" style="display:inline-block;" onclick="addMoreuser(this.form,<?php echo $bookallow; ?>);" >Add user</a> </th>

                                                            </tr>
                                                        </thead>
                                                        <tbody id="addedflow">
                                                            <tr>
                                                                <td>
                                                                    <select name="group[]" class="form-control" onchange="sss(this.value, '1')">
                                                                        <option>Select Group</option>
                                                                        <?php
                                                                        $grp = $this->db->get('dms_role')->result_array();
                                                                        foreach ($grp as $row) {
                                                                         ?>
                                                                         <option value="<?php echo $row['role_id']; ?>" <?php
                                                                         if ($result['role_id'] == $row['role_id']) {
                                                                             echo "selected";
                                                                         }
                                                                         ?> ><?php echo $row['role_name']; ?></option>
                                                                             <?php } ?>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="user[]" id="user1" class="form-control">
                                                                            <option>Select User</option>
                                                                            
                                                                 </select>
                                                                </td>
                                                                <td></td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                   </div>-->
                                          
                                            
                                           
                                            
                                                <div class="col-md-12">
                                                <div class="box-title">
                                                    <h4>Upload Additional Detail</h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="row" style="margin-bottom:10px">
                                                    <label class="control-label col-md-4">Add Files Here </label>
                                                    <div class="col-md-4">
                                                         
                                                        <input name="detail_image" type="file"
                                                            class="file-input" style="font-size:11px;" value="<?php echo $parent['user_image']; ?>" >                                                                                  
                                                    </div>
                                                    <div class="col-md-4">
                                  <!--<img  class="img-responsive" src="<?php echo base_url()."uploads/".$parent['parent_additional_detail']; ?>" style="height:90px;width:120px;" alt=""/>-->
                                                        <img  class="img-responsive" src="<?php echo img_path."local_path/".$parent['parent_additional_detail']; ?>" style="height:90px;width:120px;" alt=""/>
                                                    </div>
                                                </div>
<!--                                            <div class="row" style="margin-bottom:10px">
                                              
                                                
                                                    <label class="control-label col-md-12">comments </label>
                                                    <div class="col-md-12">
                                                         
                                                        <textarea id="comment" rows="3" cols="6" class="form-control" 
                                                                  placeholder="Enter Comments"
                                                                  name="comment"><?php echo $parent['parent_comment']; ?></textarea>                                                                              
                                                    </div>
                                              
                                           
                                            </div>-->
                                             <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-4" id="stat_valid" style="text-align: right; <?php if($roleId == '5'){ ?> display: block; <?php } else { ?> display: none; <?php } ?>">
                                                    <div style="text-align: right;  <?php if($parent['parent_status'] == 'Completed' OR $parent['parent_status'] == 'Approved' OR $parent['parent_status'] == 'Terminate'){ ?> display: none; <?php } ?>">

                                                        <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                                value="Validate" id="parent_sub"   name="submit" onclick="return chkpass();">Save Detail</button>
                                                    </div>
                                                        <?php if($doc!='1'){ ?>
                                                    <div style="text-align: right; margin-top: 10px; <?php if($stat == 'false' OR $parent['parent_status'] == 'Completed' OR $parent['parent_status'] == 'Approved'OR $parent['parent_status'] == 'Terminate'){ ?> display: none; <?php } ?>">

                                                        <button type="button" class="btn btn-primary start" style="width:120px" 
                                                              name="proceed" onclick="procid('<?php echo $parent['parent_temp_id']; ?>');"> Proceed </button>
                                                    </div>
                                                        <?php } ?>
                                                </div>
                                            </div>
                                            
                                            
                                        </form>
      </div>
                                    </div>
                                </div>
                                <script>
                                    CKEDITOR.replace( 'chk' );
                                </script>
<script>
function myFunction() {
    //window.print('adadad');
    var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById('chk').innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}
</script>
                               