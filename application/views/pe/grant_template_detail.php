<style>
  .delopt
  {
      font-size:13px;padding:8px 15px;
      color:#fff;
      background: #D14;
      border-radius:4px;
      margin-left:170px;
  }     
</style>
<?php $userRole = $this->session->userdata['login']['user_role']; ?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>-->
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                        <div class="col-md-12">
                                <!-- BOX -->
                                <div class="box border green">
                                        <div class="box-title">
                                                <h4><i class="fa fa-table"></i><?php echo $pagetitle; ?></h4>
                                                <div class="tools hidden-xs">
                                                        <a href="#box-config" data-toggle="modal" class="config">
                                                                <i class="fa fa-cog"></i>
                                                        </a>
                                                        <a href="javascript:;" class="reload">
                                                                <i class="fa fa-refresh"></i>
                                                        </a>
                                                        <a href="javascript:;" class="collapse">
                                                                <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                        <a href="javascript:;" class="remove">
                                                                <i class="fa fa-times"></i>
                                                        </a>
                                                </div>
                                        </div>
                                        <div class="box-body">

           <a href="javascript:;" class="delopt"  onclick="deleteGrantTbl();">Delete</a> <!-- DELETE BUTTON  -->
                                                
              <table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                                        <thead>
                                                             
                                                               <tr>
                                                             
                                                             <th><input class="checkall" id="select_all" type="checkbox" name="select_all" value=""></th>                                     
                                                                        <!--<th class="center hidden-xs">Delete</th>-->
                                                                        <th>Grant Number</th>
                                                                        <th>Programme/Initiative</th>
                                                                        <th>Grant Status</th>
                                                                        <th>Start Date</th>
                                                                        <th>End Date</th>
                                                                        <th>Grantee Name</th>
                                                                        
                                                                       <!--<th class="center hidden-xs">Created By</th>-->
                                                                        <th>Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                $i = 1;
                                                                foreach ($data as $row) {
                                                                    $creator = $this->db->get_where('dms_user', array('user_id' => $row['created_by']))->row_array();
                                                                $cat = $this->db->get_where('dms_category', array('category_id' => $row['category_id']))->row_array();
                                                                $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $row['categorytype_id']))->row_array();
                                                                $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $row['grantee_name']))->row_array();
                                                            ?>
                                                                <tr class="gradeX">
                                                          
                                                            <td><input class="checkbox"  type="checkbox" name="check[]" value="<?php echo $row['id']; ?>"></td>                               
                                                                    <td><?php echo $row['grant_number']; ?></td>
                                                                        <td><?php echo $cat['category_name']; ?></td>
                                                                        <td><?php echo $status['categorytype_name']; ?></td>
                                                                        <td><?php echo $row['start_date']; ?></td>
                                                                        <td><?php echo $row['end_date']; ?></td>
                                                                        <td><?php echo $grantee['grantee_name']; ?></td>
                                                                                                                                                                                                                                                                        
<!--                                                                    <td class="center hidden-xs"><?php echo $creator['user_name']; ?></td>-->
                                                                        <td>
                                                                            <?php if($userRole == '1'){ ?>
                                                                            <a style="color:green" href="<?php echo base_url(); ?>adminlogin/show_grant_main_template/<?php echo $row['id']; ?>/edit" title="Edit Record"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i></a> | 
                                                                            <a style="color:green" href="<?php echo base_url(); ?>adminlogin/view_grant_main_template/<?php echo $row['id']; ?>/edit" title="View Record">View</a> 
                                                                            <?php } else { ?>
                                                                            <a style="color:green" href="<?php echo base_url(); ?>pelogin/show_grant_main_template/<?php echo $row['id']; ?>/edit" title="Edit Record"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i></a> | 
                                                                            <a style="color:green" href="<?php echo base_url(); ?>pelogin/view_grant_main_template/<?php echo $row['id']; ?>/edit" title="View Record">View</a> 
                                                                            <?php } ?>
                                                                            |
                                                                        <a style="color:red" href="javascript:;" title="Delete Record" onclick="confirm_modal('<?php echo base_url(); ?>adminlogin/show_grant_template_delete/<?php echo $row['id']; ?>');"><i class="fa fa-trash-o fa-1x" aria-hidden="true"></i></a>
                                                                        </td>
                                                                </tr>
                                                                <?php $i++; } ?>
                                                              
                                                               
                                                        </tbody>
                                                      
                                                        <tfoot>
<!--                                                                <tr>
                                                                        <th class="center hidden-xs">Grant Number</th>
                                                                        <th class="center hidden-xs">Programme/Initiative</th>
                                                                        <th class="center hidden-xs">Grant Status</th>
                                                                        <th class="center hidden-xs">Start Date</th>
                                                                        <th class="center hidden-xs">End Date</th>
                                                                        <th class="center hidden-xs">Grantee Name</th>
                                                                        <th class="center hidden-xs">Created By</th>
                                                                        <th class="center hidden-xs">Action</th>
                                                                </tr>-->
                                                        </tfoot>
                                                </table>               
                                        </div>
                                </div>
                                <!-- /BOX -->
                        </div>
                </div>
                <!-- /DASHBOARD CONTENT -->
                
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );


//$(function () {   
//    $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd'}).val();
// $("#datepicker1").datepicker({
//     dateFormat: 'yy-mm-dd',
//     minDate: new Date(),
//    
//});
//});


</script>
<script type="text/javascript">
$(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
</script>
<!-----------------------------------FOR CheckBox...-------------------------------->
 <script>
var select_all = document.getElementById("select_all"); //select all checkbox
var checkboxes = document.getElementsByClassName("checkbox"); //checkbox items

//select all checkboxes
select_all.addEventListener("change", function(e){
    for (i = 0; i < checkboxes.length; i++) { 
        checkboxes[i].checked = select_all.checked;
    }
});
for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].addEventListener('change', function(e){ //".checkbox" change 
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){
            select_all.checked = false;
        }
        //check "select all" if all checkbox items are checked
        if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
            select_all.checked = true;
        }
    });
}

</script>
<!-----------------------------------End Js CheckBox...-------------------------------->

//<!-----------------------------------checkbox delete function-------------------------------> 
<script>
function deleteGrantTbl(e)
{ 
      
//var checkedValue = null; 
//var len = $('[name="check[]"]:checked').length;
//alert($('[name="check[]"]:checked').length);
//alert($(".checkbox").val()); die;


//var aba = [];
//$(".checkbox:checked").map(function(_,el)
//{
//       aba += $(el).val();
//       alert(aba);
//  }).get();

//$('#save_value').click(function() {
//    var sel = $('input[type=checkbox]:checked').map(function(_, el) 
    var sel=$(".checkbox:checked").map(function(_,el)
        {
//        var p = $(el).val();
        return $(el).val();
    }).get();
    
//    alert(sel); die;
 if(sel=='')
 { 
       alert("please select grant"); 
                return false;
 }

 $.ajax({

            url: "<?php echo base_url(); ?>adminlogin/deleteGrantCheckbox",
            type: "POST",
            data: {'id': sel},
            success: function (response)
            {
//                $("#prtbl").html(response);
                alert(response);
                location.reload();
            }
        });
        
}
</script>
//<!----------------------------- End Checkbox Delete Function--------------------------------> 