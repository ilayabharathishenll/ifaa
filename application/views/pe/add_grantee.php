<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('dms_grantee', array('grantee_id' => $param1))->row_array();
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('pelogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>pelogin/add_grantee/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grantee Name<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                         
                                                        <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $result['grantee_id']; ?>"> 
                                                            
                                                        <input type="text" class="form-control" data-validation="length" 
                                                               data-validation-length="min1" 
                                                               data-validation-error-msg="Grantee name is required."
                                                               placeholder="Enter Grantee Name"
                                                               name="name" value="<?php echo $result['grantee_name']; ?>">                                                                                                  
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Grantee Phone<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"  onkeypress="javascript:return isNumber(event)"  data-validation="number" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="grantee Phone is required."
                                                                   placeholder="Enter Phone Number"
                                                                   name="phone" value="<?php echo $result['grantee_phone']; ?>"  onchange="mno();" >
                                                        <p id="error1" style="color:red"></p>
    
                                                    </div>
                                                </div>
                                            </div>  
                                             <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Alternative Number<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" onkeypress="javascript:return isNumber(event)" 
                                                               placeholder="Enter Phone Number"
                                                               name="alternative" value="<?php echo $result['grantee_alternative_no']; ?>" data-validation="number">
                                                    </div>
                                                </div>
                                                 <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Area<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" 
                                                                data-validation="length"
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Area is required."
                                                                   placeholder="Enter Area "
                                                               name="area" value="<?php echo $result['grantee_area']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                              
                                            
                                            <div class="row" style="margin-bottom:10px">
                                               <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4"> Address<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8"><input type="text" class="form-control"  data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Address is required."
                                                                   placeholder="Enter Address"
                                                                 name="address" value="<?php echo $result['grantee_address']; ?>"  >

                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">City<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="City is required."
                                                                   placeholder="Enter City"
                                                               name="city" value="<?php echo $result['grantee_city']; ?>">
                                                    </div>
                                                </div>                                              
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">State<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       
                                                      <input type="text" class="form-control"  data-validation="length"
                                                                            data-validation-length="min1" 
                                                                              data-validation-error-msg="State is required." 
                                                                              class="form-control" 
                                                                              placeholder="Enter State" 
                                                                              name="state" value="<?php echo $result['grantee_state']; ?>"> 
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Pin<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control"  data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="pin is required."
                                                                   placeholder="Enter Pin Code"
                                                               id="" name="pin" value="<?php echo $result['grantee_pin']; ?>">
                                                    </div>
                                                </div>                                              
                                            </div>
                                            <div class="row" style="margin-bottom:10px"> 
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Country<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="country is required."
                                                                   placeholder="Enter Country"
                                                               id="" name="country" value="<?php echo $result['grantee_country']; ?>"> 
                                                    </div>
                                                </div>

                                            </div>
                                        
                    
                            
                    
                                                                
                                            <div style="text-align: left">
                                                
                                                <a href="javascript:;" class="btn btn-primary col-md-12" style="text-align: left" data-toggle="collapse">Grantee Meta data</a>
                                                
<!--                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>-->
                                            </div>
                                            </br></br><br>
                                            
<!--                                            <div id="demo" class="collapse">-->
                                                    
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Geographical area of work <span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">

                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                    data-validation-length="min1" 
                                                                                  data-validation-error-msg="Geographical area of work is required." 
                                                                                  class="form-control" 
                                                                                  placeholder="Enter Geographical area of work"
                                                                                  name="geogrtaphical_area" value="<?php echo $result['grantee_geogrtaphical_area']; ?>"> 
                                                        </div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Disciplinary field of work<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control"  data-validation="length" 
                                                                       data-validation-length="min1" 
                                                                       data-validation-error-msg="Disciplinary field of work is required."
                                                                        placeholder="Enter Geographical area of work"
                                                                    name="disciplinary_field" value="<?php echo $result['grantee_disciplinary_field']; ?>">
                                                        </div>
                                                    </div>                                              
                                                </div>
                                                
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Language<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">

                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                  data-validation-error-msg="Language is required." 
                                                                                  data-validation-length="min1" 
                                                                                  class="form-control"  
                                                                                   placeholder="Enter Language"
                                                                                  name="language" value="<?php echo $result['grantee_language']; ?>"> 
                                                        </div>
                                                                                       <div id="mobile" style="color:red;margin-left: 150px"></div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Possible outcome<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control"  data-validation="length" 
                                                                       data-validation-length="min1" 
                                                                       data-validation-error-msg="Possible outcome is required."
                                                                        placeholder="Enter Possible outcome"
                                                                        name="grantee_possible_outcome" value="<?php echo $result['grantee_possible_outcome']; ?>">
                                                        </div>
                                                    </div>                                              
                                                </div>
                                                
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Email<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">

                                                          <input type="text" class="form-control"  data-validation="length"
                                                                                  data-validation-error-msg="Email is required." 
                                                                                  data-validation-length="min1" 
                                                                                  class="form-control" 
                                                                                   placeholder="Enter Email"
                                                                                  name="email" value="<?php echo $result['grantee_email']; ?>"> 
                                                        </div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4"> Website  <span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control"  data-validation="length" 
                                                                       data-validation-length="min1" 
                                                                       data-validation-error-msg="Website is required."
                                                                        placeholder="Enter Website"
                                                                        name="website" value="<?php echo $result['grantee_website']; ?>">
                                                        </div>
                                                    </div>
                                                    </br>
<!--                                                </div>-->
                                            
                                                
                                                
                                                    
                                                
                                            <div style="text-align: right">
                                                <br><br>
                                                
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                            </div>

                                            </div>
                                                               

                                        </form>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->
                               
                                    <!-- /BASIC -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );


//$(function () {   
//    $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd'}).val();
// $("#datepicker1").datepicker({
//     dateFormat: 'yy-mm-dd',
//     minDate: new Date(),
//    
//});
//});


</script>
<script type="text/javascript">
$(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
</script>