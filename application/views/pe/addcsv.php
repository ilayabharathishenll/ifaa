<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('dms_grantee', array('grantee_id' => $param1))->row_array();
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
 $userPermission = $this->session->userdata['login']['user_permission'];
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('pelogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>pelogin/uploadData/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                            
                                                
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Add CSV<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">

                                                           <input name="userfile" type="file"
                                                            class="file-input" style="font-size:11px;" value="<?php echo $result['user_image']; ?>" > 
                                                        </div>
                                                    </div>
                                                    
                                                    </br>
<!--                                                </div>-->
                                            
                                                
                                                
                                                    
                                                
                                            <div style="text-align: right">
                                                <br><br>
                                           <?php  if (in_array("add_csv", $userPermission)){ ?>     
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                           <?php } ?>
                                            </div>

                                            </div>
                                                               

                                        </form>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->
                               
                                    <!-- /BASIC -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );


//$(function () {   
//    $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd'}).val();
// $("#datepicker1").datepicker({
//     dateFormat: 'yy-mm-dd',
//     minDate: new Date(),
//    
//});
//});


</script>
<script type="text/javascript">
$(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
</script>