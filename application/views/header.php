<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<title><?php echo $pagetitle; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- STYLESHEETS --><!--[if lt IE 9]><script src="js/flot/excanvas.min.js"></script><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script><![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/css/themes/default.css" id="skin-switcher" >
	<link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/css/responsive.css" >
	
        <!-- SELECT2 -->
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/print.css" media="print" />
        
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/select2/select2.min.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/style2.css">
        
	<link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- JQUERY UI-->
        <link href="<?php echo base_url(); ?>assets/css/management.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/jquery/jquery-2.0.3.min.js"></script>
        
        <script src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<!-- DATA TABLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/datatables/media/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/datatables/media/assets/css/datatables.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/datatables/extras/TableTools/media/css/TableTools.min.css" />
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        
       
        
        <style type="text/css" media="print">
@page {
    /*size: auto;  */ 
    margin: 0;  
    margin-left: 100px;
}
</style>
        
        
</head>
<body>
    
    <?php 
    
    $userPermission = $this->session->userdata['login']['user_permission'];
//    print_r($userPermission); die;
    //print_r($userPermission);
    $userId = $this->session->userdata['login']['user_id'];
    $RoleId = $this->session->userdata['login']['user_role'];
    if($RoleId == '4')
    {
        //echo "hhh"; die;
        $docNotice = $this->db->get_where("dms_assign_document", array('user_id' => $userId, 'notification' => NULL));
        $docassi_count = $docNotice->num_rows;
    }
    if($RoleId == '5')
    {
        $approveNotice = $this->db->get_where("dms_temp_approve", array('temp_approve_status' => 'Approved', 'approval_sender' => $userId, 'notification' => NULL));
        $vpprove_count = $approveNotice->num_rows;

        $RejectNotice = $this->db->get_where("dms_temp_approve", array('temp_approve_status' => 'Rejected', 'approval_sender' => $userId, 'notification' => NULL));
        $reject_count = $RejectNotice->num_rows;
        

        $pendingNotice = $this->db->get_where("dms_temp_approve", array('temp_approve_status' => 'Pending', 'approval_sender' => $userId, 'notification' => NULL ));
        //$pending_count = $pendingNotice['COUNT(*)'];
        //print_r($pendingNotice);
        $pending_count = $pendingNotice->num_rows;
    }
    if($RoleId == '6' OR $RoleId == '7')
    {
        $RejectNotice = $this->db->get_where("dms_temp_approve", array('temp_approve_status' => 'Rejected', 'user_id' => $userId, 'notification !=' => 'NULL'));
        $reject_count = $RejectNotice->num_rows;
    }
    
       $q = $this->db->get_where("dms_temp_approve", array('user_id' => $userId, 'temp_approve_status' => 'Pending'))->result_array();
    $i =0;
   
    foreach($q as $value)
    {
        if($value['temp_approve_step'] == '1')
        {
            $i = $i + '1';
        }
        else
        {
            $step = $value['temp_approve_step'] - '1';
            $qry = $this->db->get_where("dms_temp_approve", array('parent_temp_id' => $value['parent_temp_id'], 'temp_approve_status' => 'Approved', 'temp_approve_step' => $step))->result_array();
            if($qry)
            {
                $i = $i + '1';
            }
        }

    }
    $e = 0;
    $rej = $this->db->get_where("dms_approval_comment", array('approval_status' => 'Rejected'))->result_array();
    if($rej)
    {
        foreach($rej as $value)
        {
            $reject = $this->db->get_where("dms_temp_approve", array('temp_approve_id' => $value['temp_approve_id'], 'approval_sender' => $userId))->row_array();
            if($reject)
            {
                $e = $e + '1';
            }
        }
    }
    
    $a = 0;
    $rej = $this->db->get_where("dms_approval_comment", array('approval_status' => 'Approved'))->result_array();
    if($rej)
    {
        foreach($rej as $value)
        {
            $reject = $this->db->get_where("dms_temp_approve", array('temp_approve_id' => $value['temp_approve_id'], 'approval_sender' => $userId))->row_array();
            if($reject)
            {
                $a = $a + '1';
            }
        }
    }
    
    
    ?>
    
	<!-- HEADER -->
	<header class="navbar clearfix" id="header" style="margin:0">
		<div class="container">
				<div class="navbar-brand">
					<!-- COMPANY LOGO -->
                                        <a href="<?php echo base_url('pelogin') ?>/dashboard"> <span style="margin-left:50px; color:#FFFFFF; font-size:24px;">DMS</span> </a>
<!--					<a href="index.html">
						<img src="<?php echo base_url(); ?>assets/img/logo/logo.png" alt="Cloud Admin Logo" class="img-responsive" height="30" width="120">
					</a>-->
					<!-- /COMPANY LOGO -->
					<!-- TEAM STATUS FOR MOBILE -->
					<div class="visible-xs">
						<a href="javascript:;" class="team-status-toggle switcher btn dropdown-toggle">
							<i class="fa fa-users"></i>
						</a>
					</div>
					<!-- /TEAM STATUS FOR MOBILE -->
					<!-- SIDEBAR COLLAPSE -->
					<div id="sidebar-collapse" class="sidebar-collapse btn">
						<i class="fa fa-bars" 
							data-icon1="fa fa-bars" 
							data-icon2="fa fa-bars" ></i>
					</div>
					<!-- /SIDEBAR COLLAPSE -->
				</div>					
				<ul class="nav navbar-nav pull-right">
					
                                    <li class="dropdown" id="header-notification">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-bell"></i>
                                                        <span class="badge"><?php if($RoleId == '4'){ echo $docassi_count;} if($RoleId == '5'){ echo $vpprove_count + $reject_count + $pending_count ; }  if($RoleId == '6' OR $RoleId == '7'){ echo $i + $reject_count; } ?></span>
							
						</a>
						<ul class="dropdown-menu notification">
							<li class="dropdown-title">
                                                                <span><i class="fa fa-bell"></i> <?php if($RoleId == '4'){ echo $docassi_count;} else{ echo $vpprove_count + $reject_count + $pending_count ; } ?> Notifications</span>
							</li>
                                                        <?php if($RoleId == '5'){ ?>
							<li>
								<a href="<?php echo base_url(); ?>pelogin/approved_temp_detail">
                                                                    
									<span class="label label-success badge"><i class="fa fa-thumbs-up"></i></span>
                                                                        
									<span class="body">
                                                                            
										<span class="message">Approve </span>
                                                                                <span style=" color: red;"><?php echo $vpprove_count ;?></span>
										
									</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>pelogin/reject_temp_detail">
									<span class="label label-primary"><i class="fa fa-thumbs-down"></i></span>
									<span class="body">
										<span class="message">Send Back </span>
										<span style=" color: red;"><?php echo $reject_count; ?></span>
									</span>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url('pelogin') ?>/pending_temp_detail">
									<span class="label label-warning"><i class="fa fa-comment"></i></span>
									<span class="body">
										<span class="message">Pending </span>
										<span style=" color: red;"><?php echo $pending_count; ?></span>
									</span>
								</a>
							</li>
                                                        <?php }
                                                        if($RoleId == '6' OR $RoleId == '7'){ ?>
							<li>
								<a href="<?php echo base_url('pelogin') ?>/show_template_approval">
									<span class="label label-warning"><i class="fa fa-comment"></i></span>
									<span class="body">
										<span class="message">Approval Pending </span>
										<span style=" color: red;"><?php echo $i; ?></span>
									</span>
								</a>
							</li>
                                                        <li>
								<a href="<?php echo base_url(); ?>pelogin/reject_temp_detail">
									<span class="label label-primary"><i class="fa fa-thumbs-down"></i></span>
									<span class="body">
										<span class="message"> Approval Comment </span>
										<span style=" color: red;"><?php echo $reject_count; ?></span>
									</span>
								</a>
							</li>
                                                        <?php } 
                                                        if($RoleId == '4'){ ?>
							<li>
								<a href="<?php echo base_url('pelogin') ?>/show_assign_document_temp">
									<span class="label label-warning"><i class="fa fa-comment"></i></span>
									<span class="body">
										<span class="message">Assign Document </span>
										<span style=" color: red;"><?php echo $docassi_count; ?></span>
									</span>
								</a>
							</li>
                                                        
                                                        <?php } ?>
<!--							<li>
								<a href="javascript:;">
									<span class="label label-warning"><i class="fa fa-exclamation-triangle"></i></span>
									<span class="body">
										<span class="message">Database overload.</span>
										<span class="time">
											<i class="fa fa-clock-o"></i>
											<span>6 hrs</span>
										</span>
									</span>
								</a>
							</li>-->
<!--							<li class="footer">
								<a href="javascript:;">See all notifications <i class="fa fa-arrow-circle-right"></i></a>
							</li>-->
						</ul>
					</li>
                                    
                                            <?php
                                                $name1 = $this->session->userdata['login']['user_id'];
                                                $result11 = $this->db->get_where('dms_user', array('user_id' => $name1))->row_array();
                                            ?>
					<li class="dropdown user" id="header-user">
						<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"  style="padding-top:18px;">
                                                    <?php if( $result11['user_image']){ ?>
							<img src="<?php echo img_path . "local_path/" . $result11['user_image']; ?>" alt="" />
                                                        <?php }else{?>
                                                            <i class="fa fa-user"></i>
                                                        <?php } ?>
							<span class="username">Hello <?php echo $result11['user_name'] ?></span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url('pelogin'); ?>/show_profile"><i class="fa fa-user"></i> My Profile</a></li>
							<li><a href="<?php echo base_url('pelogin'); ?>/show_changepassword"><i class="fa fa-cog"></i> Change Password</a></li>
<!--							<li><a href="javascript:;"><i class="fa fa-eye"></i> Privacy Settings</a></li>-->
<!--							<li><a href="<?php echo base_url('admin'); ?>/logout"><i class="fa fa-power-off"></i> Log Out</a></li>-->
						</ul>
					</li >
                                        <li><a href="<?php echo base_url('admin'); ?>/logout"><span style="margin-left:15px; color:#FFFFFF; font-size:15px;"> <i class="fa fa-power-off"></i> Log Out</span></a></li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->
		</div>
		
		<!-- TEAM STATUS -->
		
		<!-- /TEAM STATUS -->
	</header>
	<!--/HEADER -->
	
	<!-- PAGE -->
	<section id="page">
				<!-- SIDEBAR -->
				<div id="sidebar" class="sidebar">
					<div class="sidebar-menu nav-collapse">
						<div class="divide-20"></div>
						<!-- SEARCH BAR -->
                                                    <div id="search-bar">
                                                        <input class="search" type="text" placeholder="Search"><i class="fa fa-search search-icon"></i>
                                                    </div>
						<ul>
                                                    
							<li>
								<a href="<?php echo base_url('pelogin') ?>/dashboard">
									<i class="fa fa-tachometer fa-fw"></i> <span class="menu-text">Dashboard</span>
									<span class="selected"></span>
								</a>					
							</li>
                                                    <?php if (in_array("view_master", $userPermission)){ ?>
<!--                                                        <li class="has-sub <?php if($page=='Master'){echo 'open';} ?>">
                                                            <a href="javascript:;" class="">
                                                                <i class="fa fa-sitemap"></i> <span class="menu-text">Master</span>
                                                                <span class="arrow"></span>
                                                            </a>
                                                            <ul class="sub" style="display:<?php if($page=='Master'){echo 'block';} ?>">
                                                                <li class="<?php if($pagetitle=='Category Type'){echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>adminlogin/show_category_type"><span class="menu-text">Category Type</span></a></li>                            
                                                                <li class="<?php if($pagetitle=='Role detail'){echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>adminlogin/show_role"><span class="menu-text">Role</span></a></li>
                                                                <li class="<?php if($pagetitle=='Add Category'){echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>adminlogin/show_category"><span class="menu-text">Category</span></a></li>
                                                                <li class="<?php if($pagetitle=='Add User'){echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>adminlogin/show_add_user"><span class="menu-text">Add User</span></a></li>
                                                                <li class="<?php if($pagetitle=='User Detail'){echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>adminlogin/show_user_detail"><span class="menu-text">User Detail</span></a></li>
                                                                <li class="<?php if($pagetitle=='Add Grantee'){echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/show_add_grantee"><span class="menu-text">Add Grantee</span></a></li>
                                                                <li class="<?php if($pagetitle=='Grantee Master list'){echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/show_grantee_detail"><span class="menu-text">Grantee Master list</span></a></li>
                                                                
                                                            </ul>
                                                        </li>-->
                                                    <?php }  if (in_array("view_grant", $userPermission)){ ?>
                                                        
                                                        <li class="has-sub <?php if($page=='Grant'){echo 'open';} ?>">
                                                            <a href="javascript:;" class="">
                                                                <i class="fa fa-file-image-o" aria-hidden="true"></i> <span class="menu-text">Grant Detail</span>
                                                                <span class="arrow"></span>
                                                            </a>
                                                            <ul class="sub" style="display:<?php if($page=='Grant'){ echo 'block';} ?>">
                                                                 <?php  if (in_array("add_grant", $userPermission)){ ?>
                                                                <li class="<?php if($pagetitle=='Grant Main Template'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/show_grant_main_template"><span class="menu-text">Create Grant</span></a></li>
<!--                                                                <li class="<?php if($pagetitle=='Grant Template Detail'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/show_grant_template_detail"><span class="menu-text">Grant Template Detail</span></a></li>-->
                                                                 <?php }  if (in_array("view_grant", $userPermission)){ ?>
                                                                <li class="<?php if($pagetitle=='Grant Template Detail'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/show_template_manage"><span class="menu-text">View Grant</span></a></li>
                                                                 <?php } ?>
                                                                
                                                            </ul>
                                                        </li>
                                                    <?php } if (in_array("approval_detail", $userPermission)){ ?>
                                                        <li class="has-sub <?php if($page=='Approval'){echo 'open';} ?>">
                                                            <a href="javascript:;" class="">
                                                                <i class="fa fa-file-image-o" aria-hidden="true"></i> <span class="menu-text">Approval Detail</span>
                                                                <span class="arrow"></span>
                                                            </a>
                                                            <ul class="sub" style="display:<?php if($page=='Approval'){ echo 'block';} ?>">
                                                                <li class="<?php if($pagetitle=='Approved Template'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/approved_temp_detail"><span class="menu-text">Approved</span><span style="color: red;margin-left: 1px"><?php //if($a > 0) { echo $a; } ?></span></a></li>
                                                                <li class="<?php if($pagetitle=='Reject Template'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/reject_temp_detail"><span class="menu-text">Send Back</span><span style="color: red;margin-left: 1px"><?php //if($e > 0) { echo $e; } ?></span></a></li>
                                                                <li class="<?php if($pagetitle=='Pending Template'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/pending_temp_detail"><span class="menu-text">Pending</span><span style="color: red;margin-left: 1px"><?php //if($e > 0) { echo $f; } ?></span></a></li>                                                                                      
                                                            </ul>
                                                        </li>
                                                       <?php } if (in_array("add_approval", $userPermission)){ ?>
                                                        <li class="has-sub <?php if($page=='Approval'){echo 'open';} ?>">
								<a href="javascript:;">
									<i class="fa fa-tachometer fa-fw"></i> <span class="menu-text">View Approval </span>
                                                                        <span style="color: red;margin-left: 1px"><?php //echo $i; ?></span>
									<span class="selected"></span>
								</a>
                                                                <ul class="sub" style="display:<?php if($page=='Approval'){ echo 'block';} ?>">
<!--                                                                    <li class="<?php if($pagetitle=='Approved Template'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/show_template_approvalcomplete"><span class="menu-text">Approved</span><span style="color: red;margin-left: 1px"><?php //if($a > 0) { echo $a; } ?></span></a></li>
                                                                    <li class="<?php if($pagetitle=='Pending Approval'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/show_template_approval"><span class="menu-text">Pending</span><span style="color: red;margin-left: 1px"><?php //if($e > 0) { echo $f; } ?></span></a></li>                                                                                                                              -->
                                                                <li class="<?php if($pagetitle=='Approved Template'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/approved_temp_detail"><span class="menu-text">Approved</span><span style="color: red;margin-left: 1px"><?php //if($a > 0) { echo $a; } ?></span></a></li>
                                                                     <li class="<?php if($pagetitle=='Pending Approval'){ echo 'active';} ?>" ><a class="" href="<?php echo base_url(); ?>pelogin/pending_temp_detail"><span class="menu-text">Pending</span><span style="color: red;margin-left: 1px"><?php //if($e > 0) { echo $f; } ?></span></a></li>
                                                                </ul>
							</li>
                                                        <?php } if (in_array("view_comment", $userPermission)){ ?>
							<li>
								<a href="<?php echo base_url('pelogin') ?>/View_user_comment">
									<i class="fa fa-tachometer fa-fw"></i> <span class="menu-text">Comments detail</span>
									<span class="selected"></span>
								</a>					
							</li>
                                                    <?php }
                                                        if($RoleId == '4'){
                                                            $ASIusers = $this->db->get_where("dms_user", array('user_id' => $userId))->row_array();
                                                            if($ASIusers){        
                                                        ?>
                                                            <li>
                                                                    <a href="<?php echo base_url('pelogin') ?>/show_assign_document_temp">
                                                                            <i class="fa fa-tachometer fa-fw"></i> <span class="menu-text">Assign Document</span>
                                                                            <span class="selected"></span>
                                                                    </a>					
                                                            </li>
                                                        <?php } }  if (in_array("view_csv", $userPermission)){ ?> 
							
<!--							<li>
                                                                    <a href="<?php echo base_url('pelogin') ?>/csvfile">
                                                                            <i class="fa fa-tachometer fa-fw"></i> <span class="menu-text">Add CSV</span>
                                                                            <span class="selected"></span>
                                                                    </a>					
                                                         </li>-->
                                                        <?php } ?> 
						</ul>
						<!-- /SIDEBAR MENU -->
					</div>
				</div>
				<!-- /SIDEBAR -->
                                <div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                            <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              <h4 class="modal-title">Are you sure you want to delete it ?</h4>
                                            </div>
                                            <!--<div class="modal-body">


                                            </div>-->
                                            <input type="hidden" id="griil_id"/>
                                            <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                                                <a href="javascript:;" class="btn btn-danger" id="delete_grill" onclick="delet_grill();"> Yes</a>
                                                <button type="button" class="btn btn-info" data-dismiss="modal"> No</button>
                                            </div>
                                      </div>
                                    </div>
                                </div>
                               
