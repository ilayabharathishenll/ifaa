  <?php 
        $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $data['grantee_name']))->row_array();
        $category = $this->db->get_where('dms_category', array('category_id' => $data['category_id']))->row_array();
        $parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '8'))->row_array();
        $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $data['categorytype_id']))->row_array();
        
        $individualData = $this->db->get_where('dms_individual_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '50'))->result_array();
//echo $this->db->last_query();
        //print_r($individualData);
        $meta = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '34'))->result_array();
        $meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '34', 'detail' => 'General Information'))->result_array();
        $meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '34', 'detail' => 'Author, Publisher, Access'))->result_array();
        $meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '34', 'detail' => 'Physical Characteristics'))->result_array();
        $meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '34', 'detail' => 'Digitisation Details'))->result_array();
        $meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '34', 'detail' => 'Audio-Visual Material Details'))->result_array();
        $meta6 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '34', 'detail' => 'Others'))->result_array();

        
        ?>
<style>
    .doc_imgbox img{
        width: 120px;
    }
</style>
    <div class="container-fluid bread">
	<div class="col-md-4">
	<p class="bread_content1">Documents</p>
	</div>
	<div class="col-md-8">
	<p class="bread_content2">Home/Searched Result/<?php echo $data['grant_number']; ?>/Documents</p>
	</div>
    </div>
    <div class="container-fluid bread">
        <div class="col-md-4">
            <p class="bread_content1"><button onclick="goBack()">Go Back</button></p>
        </div>
       
    </div>
	
	<div class="image_content">
	<div class="col-md-8 image_content_left">
	<h5><?php echo $grantee['grantee_name']; ?></h5>
	<h6><?php  $date2=date_create($data['end_date']); $date1=date_create($data['start_date']);echo $category['category_name'].' | '.date_format($date1,"d-m-Y").' - '.date_format($date2,"d-m-Y").' | '.$status['categorytype_name']; ?></h6>
	
	<div class="col-md-12 pdf">
             <?php $i = '1'; //echo "<pre>";print_r($individualData);
             foreach($individualData as $valas ){ $icon=explode(".",$valas['individual_file']); ?>
            
              <?php   $meta2_ = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => 50, 'metadata_individual_number' => $valas['metadata_individual_number'], 'detail' => 'Author, Publisher, Access','metadata_value'=>"Public"))->result_array();
     // print_r($meta2_);
foreach($meta2_ as $value1) { 
        if($valas['individual_temp_id']==$value1['template_id'])
            { 
           //echo $i;
                ?>
                <div class="col-md-4 doc_imgbox">
                    <a href="<?php echo base_url()."local_path/".$valas['individual_file']; ?>" target="_blank">
                        <?php if($icon[1]=="xls" || $icon[1]=="xlsx"){?>
                        <img src="<?php echo base_url(); ?>front/images/xlxs.png">
                        <?php } else if($icon[1]=="doc" || $icon[1]=="docx"){ ?>
                         <img src="<?php echo base_url(); ?>front/images/word.png">
                        <?php } else if($icon[1]=="pdf"){ ?>
                            <img src="<?php echo base_url(); ?>front/images/pdf.png">
           
                            <?php }else if($icon[1]=="jpg" || $icon[1]=="gif" || $icon[1]=="png" || $icon[1]=="jpeg"){ ?>
                            <img src="<?php echo base_url(); ?>front/images/img.png">
                        <?php }else{ ?>
                             <img src="<?php echo base_url(); ?>front/images/text.png">
                        <?php } ?>
                    </a>
                    <h6><a href="javascript:;" onclick="metadata('<?php echo $data['id']; ?>', '50', '<?php echo $valas['metadata_individual_number']; ?>')" >View Details</a></h6>
                </div>
             <?php $i++; }}} ?>
<!--	<div class="col-md-4">
	<a href="<?php echo base_url(); ?>front/images/sample.pdf" target="_blank"><img src="<?php echo base_url(); ?>front/images/pdf.png"></a>
	<h6><a href="<?php echo base_url(); ?>front/images/sample.pdf" target="_blank">Document 2</a></h6>
	</div>
	<div class="col-md-4">
	<a href="<?php echo base_url(); ?>front/images/sample.pdf" target="_blank"><img src="<?php echo base_url(); ?>front/images/pdf.png"></a>
	<h6><a href="<?php echo base_url(); ?>front/images/sample.pdf" target="_blank">Document 3</a></h6>
	</div>-->
	
	</div>
	
<!--	<div class="col-md-12 pdf">
	<div class="col-md-4">
	<a href="images/sample.pdf" target="_blank"><img src="<?php echo base_url(); ?>front/images/pdf.png"></a>
	<h6><a href="images/sample.pdf" target="_blank">Document 4</a></h6>
	</div>
	<div class="col-md-4">
	<a href="images/sample.pdf" target="_blank"><img src="<?php echo base_url(); ?>front/images/pdf.png"></a>
	<h6><a href="images/sample.pdf" target="_blank">Document 5</a></h6>
	</div>
	<div class="col-md-4">
	<a href="images/sample.pdf" target="_blank"><img src="<?php echo base_url(); ?>front/images/pdf.png"></a>
	<h6><a href="images/sample.pdf" target="_blank">Document 6</a></h6>
	</div>
	
	</div>-->
	
<!--	<div class="col-md-12 pdf">
	<div class="col-md-4">
	<a href="images/sample.pdf" target="_blank"><img src="<?php echo base_url(); ?>front/images/pdf.png"></a>
	<h6><a href="images/sample.pdf" target="_blank">Document 7</a></h6>
	</div>
	<div class="col-md-4">
	<a href="images/sample.pdf" target="_blank"><img src="<?php echo base_url(); ?>front/images/pdf.png"></a>
	<h6><a href="images/sample.pdf" target="_blank">Document 8</a></h6>
	</div>
	<div class="col-md-4">
	<a href="images/sample.pdf" target="_blank"><img src="<?php echo base_url(); ?>front/images/pdf.png"></a>
	<h6><a href="images/sample.pdf" target="_blank">Document 9</a></h6>
	</div>
	
	</div>-->
	
	
	
	
	
	
	
	
	
	
	
            <div class="col-md-12 pagin">

                <ul id="pagin" class="pagination">

                </ul>

            </div>
	</div>
            <div class="col-md-4 image_content_right" id="metadatadiv">
	<h5>Metadata</h5>
<!--        <div>
        <?php $j4=1; if($meta1) { ?>
            <h4 style="margin: 20px 0;"><strong>General Information</strong></h4>

            <?php foreach($meta1 as $value) {if($value['metadata_value']!='' && $j4==1){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        <?php $j4++;}}} ?>
            <div class="panel-group" id="accordion">
    <div>
      <div>
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" style="
   // color: #f90;
    font-weight: bold;
    font-size: 16px;background-color:#ff9900; color:#fff;padding:5px;">Read More ></a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse ">
        <div class="panel-body dd">
       
                 
        <?php $j=1;$j1=1;$j2=1;$j3=1;//echo "<pre>"; print_r($meta2);
        if($meta2) { ?>

        <h4  style="margin: 20px 0;"><strong>Author, Publisher, Access</strong></h4>
	<?php foreach($meta2 as $value) { if($value['metadata_value']!='' && $j==1){  ?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php $j++; }
        }} if($meta3) { ?>
        <h4  style="margin: 20px 0;"><strong>Physical Characteristics</strong></h4>
	<?php foreach($meta3 as $value) { if($value['metadata_value']!=''  && $j1==1){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php $j1++;} }} if($meta4) {  ?>
        <h4  style="margin: 20px 0;"><strong>Digitisation Details</strong></h4>
	<?php foreach($meta4 as $value) { if($value['metadata_value']!=''  && $j2==1){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
 <?php $j2++;} }} if($meta5) { ?>
        <h4  style="margin: 20px 0;"><strong>Audio-Visual Material Details</strong></h4>
	<?php foreach($meta5 as $value) { if($value['metadata_value']!=''  && $j3==1){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
           
 <?php $j3++;} }} ?>
        </div>
      </div>
    </div>
   
   
  </div>   
	</div> -->
	</div>
	</div>
<script>
pageSize = 8;
//alert($(".line-content").length)
    var pageCount =  $(".line-content").length / pageSize;
    for(var i =0 ; i<pageCount; i++){
        $("#pagin").append('<li onclick="report();"><a href="javascript:;" >'+(i+1)+'</a></li> ');
    }
    $("#pagin li").first().find("a").addClass("current1");
    $("#pagin li").first().addClass("active");
        
    showPage = function(page) {
    //alert(page);
        $(".line-content").hide();
        $(".line-content").each(function(n) {
        if (n >= pageSize *(page - 1) && n < pageSize* page)
            $(this).show();
               
        });        
    }
//    
    showPage(1);
    $("#pagin li a").click(function() {
        $("#pagin li").removeClass("active");
        $(this).parent().addClass("active");
     
//        $("#pagin li a").removeAttr( "style" )
        $("#pagin li a").removeClass("current1");
        $(this).addClass("current1");
       
        showPage(parseInt($(this).text())) 
 });
 
function metadata(gnt, doc, id)
{
     //alert(doc)
     $.ajax({
        url: "<?php echo base_url(); ?>user/metadata",
        type: "POST",
        data: {'grnt': gnt, 'doc': doc, 'nmbr': id},
        success: function (response)
        {
            //alert(response);
            $("#metadatadiv").html(response);
            //$("#metadatadiv").html(response);
            //alert(response);

        }

    });
 }
function goBack()
{
    window.history.back();
}

</script>