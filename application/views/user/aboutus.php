
<style>
    .popularbut
    {
    text-align: center;
    font-size: 24px;
    padding: 5px 30px 5px 30px;
    background: #e4e4e4;
    border: 5px solid #f1f1f1;
    display: block;
    text-decoration:none;
    color:#695c56;
    }
    .popularbut:hover{
         text-decoration:none;
         color:#695c56;
    }.search_box
    {
        background: none !important;
        border:none !important;
        padding: 0 !important;
    }
.About-content p {
	text-align: justify;
    font-size:17px;
    color: #594c46;
    line-height:25px;
	    padding-bottom: 10px;
}.About-content strong {
    font-size:17px;
    color: #594c46;
    line-height:25px;
    font-weight: bold;
}
</style>
<script>
    $(document).ready(function(){
        $( "#other" ).click(function() {
        //alert("aaa");
        $( "#usersForm" ).submit();
    });
});
function goBack()
{
    window.history.back();
}

</script>
    <div class="container-fluid bread">
    </div>
      <div class="container About-content">
	  <div class="row">
        <div class="col-md-2">
          <div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
        </div>
        
 
	
<div class="col-md-8">
<h4>ABOUT US</h4>
	<p>
<strong>India Foundation for the Arts (IFA)</strong> welcomes you to <strong>The IFA Archive!</strong> We are delighted to share on this website, the arts and culture projects that we, at IFA have facilitated since 1995. Over the last two decades, <strong>IFA</strong> has supported projects across a staggering breadth of disciplines and genres. These projects which have had varied outcomes as <strong>books, films, performances, archival and educational material and more,</strong> continue to reach a <strong>multitude of publics.</strong> Over the years, this material at IFA has grown to become a <strong>rich and valuable repository</strong> of legion artistic and scholarly engagements and reflections of India's <strong>lived experiences.</strong> </p>
<p><strong>IFA</strong> is an independent, not-for-profit, grantmaking organisation that supports practice, research and education in the arts and culture across India. Set up as a Public Charitable Trust in 1993, IFA started making grants two years later. After twenty years of supporting the arts and culture, we felt the need for the creation of an archive to house this material. These project outcomes  &#x2013 as digital and physical traces of the myriad <strong>readings</strong> and <strong>studies</strong> on the arts and culture in India with its <strong>turns, shifts,</strong> and <strong>fractures</strong>  &#x2013 lay scattered across the IFA Office for a long period. </p>
<p>We believe that the opening up of <strong>The IFA Archive</strong> to researchers, scholars, artists, writers, curators and others who have a <strong>keen interest</strong> and <strong>investment</strong> in the <strong>arts</strong>, will capacitate it to become the <strong>fount</strong> for the <strong>study, exploration,</strong> and <strong>experimentation</strong> of this material  &#x2013 with the potential to set in motion, new trajectories and <strong>discourses.</strong> The range of material at the Archive is open to a <strong>plurality of interpretations,</strong> through <strong>scholarly, artistic,</strong> and <strong>curatorial interventions.</strong> It will provide greater access to and understanding of the work of multiple voices  &#x2013  marginal, mainstream, silent, loud, hushed  &#x2013 from the larger linguistic, geographical, social, cultural and demographical contexts of India. This engagement, in turn, will also provide us at IFA, with a narrative framework to understand the role and necessities of an independent grantmaking body. </p>
<p>Please note that The IFA Archive is a work-in-progress. This online space currently holds the materials of projects supported by IFA in the years 2011 and 2012. Additionally, at the physical site of The IFA Archive, materials from projects supported in the years X, Y, Z will also be available for consultation. Both the online and physical spaces will be continually updated, alongside the ongoing process of archiving. 
We are delighted that the <strong>Lohia Charitable Foundation,</strong> a charitable trust spearheaded by Aarti Lohia, working towards social integration, national good and public welfare initiatives has generously committed to support the <strong>building</strong> of <strong>The IFA Archive</strong>. Because of their unwavering patronage, the critical work facilitated by IFA is regularly made accessible to a wider public under a <strong>single platform</strong> at The IFAArchive. 
</p>
	
		</div>
	
	    <div class="col-md-2">
		</div>
		</div>
	</div>


