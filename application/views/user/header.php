<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/styles.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/jquery.fancybox.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/bootstrap-dist/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js"></script>
		<style>
			.navbar-nav {
				float: right;
			}
			@media screen and (max-width: 1200px) {
				.logo .logo-1 {
					margin-top: 13px;
				}
			}
			@media screen and (max-width: 992px) {
				.logo .logo-2 img {
					max-width: 75px;
				}
				.headertag h2 a {
					font-size: 25px;
				}
				.headertag h3 a {
					font-size: 13px;
				}
			}
			@media screen and (max-width: 768px) {
				.navbar-nav {
					float: none;
				}
				ul.nav.navbar-nav li {
					border: none;
				}
				.navbar-toggle {
					border: 1px solid #000;
					z-index: 999;
					margin-top: 22px;
				}
				.navbar-toggle .icon-bar {
					background-color: #000;
				}
				ul.nav.navbar-nav li a:focus {
					background: #8b8b8b;
				}
				.navbar-collapse {
					box-shadow: none;
					border: 0;
				}
				.menus .logo-1 {
					padding: 0px;
				}
				.menus .logo-1 .log1 {
					height: auto;
				}
			}
			@media screen and (max-width: 440px) {
				.menus .logo-1 .log1 {
					width: 175px;
				}
				.navbar-toggle {
					margin-top: 8px;
				}
				ul.nav.navbar-nav li a {
					padding: 6px !important;
				}
			}
		</style>
	</head>
	<title>
		<?php echo $pagetitle; ?>
	</title>
	<body>
		<div class="container-fluid menus">
			<div class="col-md-12">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topMenu">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="col-md-3 col-sm-4 visible-xs logo-1">
					<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>front/images/logo-trans-1.png" class="log1"></a>
				</div>
				<div class="collapse navbar-collapse" id="topMenu">
					<ul class="nav navbar-nav">
						<li class="active"> <a href="<?php echo base_url(); ?>">Home</a></li>
						<li class="active"> <a href="<?php echo base_url(); ?>user/about_us">About</a></li>
						<li><a href="<?php echo base_url(); ?>user/terms_conditions">Terms</a></li>
						<li class="active"> <a href="http://www.indiaifa.org/" target="blank">Ifa Website</a></li>
						<li class="active"> <a href="<?php echo base_url(); ?>user/contact_us">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container-fluid logo">
			<div class="col-md-3 col-sm-4 hidden-xs logo-1">
				<a href="<?php echo base_url(); ?>"><img class="img-responsive" src="<?php echo base_url(); ?>front/images/logo.jpg" class="log1"></a>
			</div>
			<div class="col-md-3 col-sm-3 hidden-xs logo-2">
				<a href="http://www.lohiafoundation.org/" target="blank">  <img class="img-responsive" src="<?php echo base_url(); ?>front/images/lohio.png" class="log2" ></a>
			</div>
			<div class="col-md-6 col-sm-5 col-xs-12 headertag">
				<h2> <a href="<?php echo base_url(); ?>">The ifa archive</a></h2>
				<h3> <a href="http://www.lohiafoundation.org/" target="blank"> Built with support from the lohia foundation</a></h3>
			</div>
		</div>