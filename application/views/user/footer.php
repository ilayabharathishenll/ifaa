<!-- <div class="container-fluid footer  ">
	<div class="container-fluid footer-margin ">
	<div class="col-md-4">
	<img src="<?php echo base_url(); ?>front/images/logo.png">
	<p>'Apurva' Ground Floor, No 259, 4th Cross,<br>
Raj Mahal Vilas IInd Stage, IInd Block,<br>
Bangalore, India - 560 094<br>
Tel: 91-80-2341 4681 / 82<br>
Fax: 91-80-2341 4683</p>
	</div>
	<div class="col-md-2">
	<h6>IFA Archive is built with support from LOHIA Foundation</h6>
	</div>
	<div class="col-md-2">
	<img class="footer_logo" src="<?php echo base_url(); ?>front/images/lohio.png">
	</div>
	<div class="col-md-4">
	<input type="email" name="" placeholder="Subscribe">
	<h4>IFA Newsletter</h4>
	<ul>
	<li><a href="javascript:;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
	<li><a href="javascript:;"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	<li><a href="javascript:;"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
	<li><a href="javascript:;"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
	</ul>
	</div>
	</div>
	</div> -->

<div class="container-fluid footer  ">
	<div class="container-fluid footer-margin ">
	<div class="col-md-3">

	</div>
	<div class="col-md-6 footer-text">
	<a class="sub-text" href="http://www.indiaifa.org/subscribe.html" target="blank">Subscribe - IFA Newsletter</a>
	<ul class="social-link">
	<li><a href="https://www.facebook.com/IndiaIFA" target="blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
	<li><a href="https://twitter.com/IndiaIFA" target="blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	<!-- <li><a href="javascript:;"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li> -->
	<li><a href="https://www.youtube.com/user/IndiaIFA" target="blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
	</ul>
	</div>
	<div class="col-md-3">
	</div>
</div>
	</div>
<div class="container-fluid sub-footer">
	
	<div class="col-md-3 footer-text">
	<p>Designed by <a href="https://www.istudiotech.in/">iStudio Technologies</a></p>
	</div>
	<div class="col-md-6 footer-text">
	<p> <a href="<?php echo base_url(); ?>user/privacy_policy">Privacy Policy</a> | <a href="<?php echo base_url(); ?>user/terms_conditions">Terms & Conditions</a> |<a href="<?php echo base_url(); ?>user/terms_conditions"> Disclaimer</a> </p>
	</div>
	<div class="col-md-3 footer-text">
	<p><script type="text/JavaScript">

document.write(new Date().getFullYear());

</script> &copy; India Foundation For the Arts</p></p>
	</div>
	</div>
	


	
</body>
</html>