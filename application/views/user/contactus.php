<?php //print_r($data); ?>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/styles.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/font-awesome.min.css">
	<!-- <script src="<?php echo base_url(); ?>assets/js/jquery/jquery-2.0.3.min.js"></script>-->
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<style>
	.col-md-6 {
		padding-left: 0px !important;
	}
	.navbar-nav {
		float: right;
	}
	.carousel-inner .item img {
		height:280px;
	}
	.popularbut {
		text-align: center;
		font-size:20px;
		padding: 5px 30px 5px 30px;
		background: #e4e4e4;
		border: 5px solid #f1f1f1;
		display: block;
		text-decoration:none;
		color:#695c56;
	}
	.popularbut:hover {
		text-decoration:none;
		color:#695c56;
	}
	.Contact-us h4 {
		font-size:25px;
		font-weight: bold;
		color: #ad4a0f;
	}
	.video-block p {
		font-size: 17px;
		font-weight: bold;
		text-align: center;
		margin: 0;
	}
	p {
		font-size:17px;
		color: #58453e;
		line-height: 30px;
		margin-bottom: 20px;
		text-align: justify;
	}
	.Title strong {
		color: #af3c23;
		font-size:20px;
	}
	.map {
		margin-top: 10px;
	}
	.video-block {
		text-align: center;
		border: 2px solid #58453e;
		padding-top: 100px;
		padding-bottom: 100px;
	}
	.map-bottom {
		margin-top: 20px;
	}
	.search_box {
		background: none !important;
		border:none !important;
		padding: 0 !important;
	}
	strong.cont-block {
		color: #ad4a0f;
		font-size: 17px;
		line-height: 30px;
	}
	.click-tect p {
		text-align: center;
		font-size:17px;
	}
</style>
<div class="container-fluid bread">
</div>
<div class="container About-content">
	<div class="col-md-2">
		<div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
	</div>
	<div class="col-md-8">
		<div class="row">
			<div class="Contact-us">
				<h4>CONTACT US</h4>
			</div>
			<p> 
				Thank you for visiting The <strong class="cont-block">IFA Archive</strong> website ! We hope you have found what you were looking for. Write to us if you have any other queries or suggestions at  <strong class="cont-block">archive @indiaifa.org</strong> 
			</p>
			<div class="row">
				<div class="col-md-6">
					<p>If you would like to consult physical archival material, our physical archive is located close to our main office. The address is:   </p>
					<div class="Title">
						<strong>The IFA Archive</strong>
						<p>First Floor, 124, 1st Cross<br>
							8o Feet Road, Behind Canara Bank<br>
							Ashwath Nagar, Raj Mahal Villas 2nd Stage<br>
							Bangalore - 56o 094 <br>
							Phone: +91-80-23414681/ 82 / 83<br>
						</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7774.034267163654!2d77.56808312420182!3d13.034580815681933!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae17c28f9f3f19%3A0x8f8ffeb9170357c9!2sRaj+Mahal+Vilas+2nd+Stage%2C+Sanjaynagar%2C+Bengaluru%2C+Karnataka+560094!5e0!3m2!1sen!2sin!4v1532071710875" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe></div>
				</div>
			</div>
		</div>
		<div class="row map-bottom">
			<p>
				Please note that The WA Archive is a <strong>work-in-progress.</strong> This online space currently holds the materials of projects supported by IFA in the years <strong>2011</strong> and <strong>2012</strong>. Additionally, at the <strong>physical site</strong> of <strong>The IFA Archive,</strong> materials from projects supported in the years X, Y, Z will also be available for consultation. Both the online and physical spaces will be continually updated, alongside the ongoing process of archiving.
			</p>
			<p>The Archive is open  <strong class="cont-block">every Friday</strong> (unless it's a Government Holiday) <br/>
				between  <strong class="cont-block">11.00 AM and or 01.00 PM.
			</p>
			</strong><br>
			<div class="visit-text"> <strong class="cont-block">Visits to the Physical Archive by prior appointment only. </strong></div>
			<div class="list">
				<p>To book an appointment, please drop a mail to the Archivist Spandana Bhowmik at  <strong class="cont-block">spandana@indiaifa.org</strong> or Assistant Archivist Bhargavi Raju at  <strong class="cont-block">bhargaviraju@indiaifa.org</strong> stating your purpose or for further assistance. </p>
			</div>
			<div class="click-tect">
				<p>Click below for a tour of the physical archive! We hope to see you soon!
			</div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<div class="video-block">
						<p>Embed video of <br>The Physical Archive</p>
					</div>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
	
<script>
	$(document).ready(function(){
		$( "#searchdiv" ).click(function() {
			var cat = document.getElementById('category').value;
			var year = document.getElementById('year').value;
			$( "#usersForm" ).submit();
		});
	});

	function goBack() {
		window.history.back();
	}

	pageSize = 5;
	var pageCount =  $(".line-content").length / pageSize;
	for(var i =0 ; i<pageCount; i++){
		$("#pagin").append('<li onclick="report();"><a href="javascript:;" >'+(i+1)+'</a></li> ');
	}
	$("#pagin li").first().find("a").addClass("current1");
	$("#pagin li").first().addClass("active");
			
	showPage = function(page) {
		$(".line-content").hide();
		$(".line-content").each(function(n) {
		if (n >= pageSize *(page - 1) && n < pageSize* page)
			$(this).show();
		});
	}
	showPage(1);

	$("#pagin li a").click(function() {
		$("#pagin li").removeClass("active");
		$(this).parent().addClass("active");
		$("#pagin li a").removeClass("current1");
		$(this).addClass("current1");
		showPage(parseInt($(this).text())) 
	});
</script>