<?php //print_r($data); ?>
<head>

        

        
</head>
<style>
.col-md-6 {
    padding-left: 0px !important;
}
            .carousel-inner .item img
            {
                height:280px;
            }

    .popularbut
    {
    text-align: center;
    font-size: 24px;
    padding: 5px 30px 5px 30px;
    background: #e4e4e4;
    border: 5px solid #f1f1f1;
    display: block;
    text-decoration:none;
    color:#695c56;
    }
    .popularbut:hover{
         text-decoration:none;
         color:#695c56;
    }.Contact-us h4 {
    font-size:25px;
    font-weight: bold;
    color: #ad4a0f;
}.video-block p {
    font-size: 20px;
    font-weight: bold;
	text-align: center !important;
}.Title.new1 {
    margin-left: -16px;
}
p {
font-size:17px;
    color: #58453e;
    line-height: 30px;
    margin-bottom: 20px;
    text-align: justify;
    margin-left: .25in;
}
.Title strong {
    color: #af3c23;
    font-size:20px;
}.map {
    margin-top: 10px;
}.video-block {
    text-align: center;
    border: 2px solid #58453e;
    padding-top: 100px;
    padding-bottom: 100px;
} .map-bottom {
    margin-top: 20px;
}ol.privacy-list{  
text-align: justify;
    list-style-type: lower-alpha;
    padding-left: 45px !important;
    font-size: 17px;
    line-height: 25px;
}p.var-text {
    margin-left: 0.45in;
}strong.About-content {
    font-size: 20px;
    color: color: #594c46;
    color: #594c46;
}.search_box
    {
        background: none !important;
        border:none !important;
        padding: 0 !important;
    }strong.cont-block {
    color: #ad4a0f;
    font-size: 17px;
    line-height: 25px;
}.click-tect p {
    text-align: center;
    font-size: 20px;
}ul.privacy-list li {
text-align: justify;
line-height: 30px;
    list-style: none;
    padding-left: 50px !important;
    text-indent: -23px;
}ul.second-list li {
    list-style: none;
    padding-left:20px !important;
    text-indent: -13px;
}.mar-top {
    margin-top: 30px;
}strong.About-content1 {
    font-size: 17px;
    color: color: #594c46;
    color: #594c46;
}
</style>
    <div class="container-fluid bread">
        
    </div>
  
        <div class="container About-content">
		<div class="row">
 <div class="col-md-2">
          <div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
        </div>
	
   <div class="col-md-8">

	<div class="Contact-us"><h4>TERMS AND CONDITIONS/DISCLAIMER</h4> </div>

 <strong class="cont-block">By using our Website and its services, you are accepting the practices described in the Terms and Conditions. If you do not agree to these terms and conditions, please do not use the Website and exit immediately. We reserve the right to modify or amend the terms of our terms and conditions from time to time without notice. Your continued use of our Website and any affiliate websites following the posting of changes to these terms and conditions will mean you accept those changes.</strong>
<br/>
 <div class="row mar-top">


<div class="Title"><strong>1. TERMS AND CONDITIONS</strong>

<ol class="privacy-list">
<li>You may download, display or print information from this Website (the "Information") solely for non-commercial, personal use.</li>
<li>You may not distribute, modify, transmit, reuse, repost, or use the content of the Website for public or commercial purposes, including the text, images, graphics, audio, and video, without the written permission of IFA.</li>
</ol>
	</div>
<div class="Title"><strong>COPYRIGHT</strong>
<p>You should assume that everything you see or read on this Website belongs to IFA, its grantees and beneficiaries unless otherwise noted and may not be used except as provided in these Terms and Conditions or in the text on the Website without the written permission of IFA. With the exception of the foregoing limited authorization, no license to or right in any copyright of IFA or of any other party is granted or conferred to you.
</p>
</div>

<div class="Title"><strong>3. INFORMATION</strong>
<p>While IFA uses reasonable efforts to include accurate and up to date information in the Website, IFA makes no warranties or representations with respect to the content of the Website, which is provided "as is". IFA accepts no responsibility or liability whatsoever arising from or in any way connected with the use of this Website or its content. In particular, IFA will not be liable for the accuracy, completeness, adequacy, timeliness, or comprehensiveness of the information contained on the Website. Some jurisdictions do not allow the exclusion of implied warranties, so the above exclusion may not apply to you.
</p>
<p>The information may contain technical inaccuracies or typographical errors. IFA reserves the right to make changes, corrections and/or improvements to the information, and to the products and programmes described in such information, at any time without notice.</p>
</div>
	
<div class="Title"><strong>4. LIMITATIONS OF LIABILITY</strong>
<p>IFA also assumes no responsibility, and shall not be liable for, any damages to, or viruses that may infect, your computer equipment or other property on account of your access to, use of, or browsing in the Website or your downloading of any materials, data, text, images, video, or audio from the Website. IFA reserves the right to interrupt or discontinue any or all of the functionality of its Website. IFA who is controlling this Website accepts no responsibility or liability whatsoever for any interruption or discontinuance of any or all functionality of its Website, whether the result of actions or omissions of an entity of IFA or a third party.</p>
</div>
	
<div class="Title"><strong>5. USAGE OF DATA</strong>
<p> Any communication or material you transmit to the Website by electronic mail or otherwise, including any data, questions, feedback, ideas, queries, comments, suggestions or the like is, and will be treated as, non-confidential and non-proprietary. Anything you transmit or post becomes the property of IFA or its affiliates and may be used for any purpose, including, but not limited to, reproduction, disclosure, transmission, publication, broadcast and posting. Furthermore, IFA is free to use any ideas, concepts, know-how, or techniques contained in any communication you send to the Website for any purpose whatsoever including, but not limited to, developing, manufacturing and marketing products using such information.

 </p>
</div>
	
<div class="Title"><strong>6. PROMOTIONAL MATERIALS AND SERVICES</strong>
<p> The IFA site contains information on its products and services, not all of which are available in every location. A reference to an IFA product or service on the Website does not imply that such product or service is or will be available in your location.</p>
</div>
<div class="Title"><strong>7. THIRD-PARTY LINKS</strong>
<p>Because IFA has no control over and does not endorse any of the sites to which the Website is linked and because IFA has not reviewed any or all of the sites to which the Website is linked, you acknowledge that IFA is not responsible for the content of any off-site pages or any other sites linked to the Website. Your linking to the Website, off-site pages or other sites is at your own risk and without the permission of IFA.</p>
</div>
<div class="Title"><strong>8. RELEASE</strong>
<p> In the event that you have a dispute with one or more other users of the Website, you release India Foundation for the Arts (and our officers, trustees, directors, employees, successors and assigns) from claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected with such disputes.</p>
</div>
<div class="Title"><strong>9. ADDITIONAL ASSISTANCE</strong>
<p>If you do not understand any of the foregoing Terms and Conditions or if you have any questions or comments, we invite you to contact us at <strong class="About-content1">contactus@indiaifa.org.</strong></p>
</div>
<div class="Title"><strong>10. COPYRIGHT NOTICE</strong>
<p>All Website design, content, graphics, text selections, arrangements and all software are Copyright © 2013, India Foundation for the Arts. ALL RIGHTS RESERVED.
 </p>
</div>
<div class="Title"><strong>11. OPTIMIZATION</strong>
<p>The Website is optimized for Google Chrome, Mozilla Firefox and IE.</p>
	</div>	
	<div class="Title"><strong>12. RESOLUTION</strong>
<p>Best viewed in 900 x 1400 screen resolution.</p>
	</div>	
	</div>	
	</div>
		  
		  <div class="col-md-2">
</div>
		</div>
	</div>

<script>
        $(document).ready(function(){
        $( "#searchdiv" ).click(function() {
        
        var cat = document.getElementById('category').value;
        var year = document.getElementById('year').value;
        //alert(cat);
        $( "#usersForm" ).submit();
       
    });
});

function goBack()
{
    window.history.back();
}

pageSize = 5;

    var pageCount =  $(".line-content").length / pageSize;
    for(var i =0 ; i<pageCount; i++){
        $("#pagin").append('<li onclick="report();"><a href="javascript:;" >'+(i+1)+'</a></li> ');
    }
    $("#pagin li").first().find("a").addClass("current1");
    $("#pagin li").first().addClass("active");
        
    showPage = function(page) {
    //alert(page);
        $(".line-content").hide();
        $(".line-content").each(function(n) {
        if (n >= pageSize *(page - 1) && n < pageSize* page)
            $(this).show();
               
        });        
    }
//    
    showPage(1);
    $("#pagin li a").click(function() {
        $("#pagin li").removeClass("active");
        $(this).parent().addClass("active");
     
//        $("#pagin li a").removeAttr( "style" )
        $("#pagin li a").removeClass("current1");
        $(this).addClass("current1");
       
        showPage(parseInt($(this).text())) 
 });

function goBack()
{
    window.history.back();
}
</script>