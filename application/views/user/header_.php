<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/font-awesome.min.css">
<!-- <script src="<?php echo base_url(); ?>assets/js/jquery/jquery-2.0.3.min.js"></script>-->
<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        
        <style>
            .navbar-nav
            {
                float: right;
            }
            .carousel-inner .item img
            {
                height:280px;
            }
        </style>
        </head>
<title>
	<?php echo $pagetitle; ?>
</title>
<body>
	<div class="container-fluid menus">
	<div class="col-md-6">
	</div>
	<div class="col-md-6">
	<ul class="nav navbar-nav">
      <li class="active"> <a href="javascript:;">About us</a></li>
      <li><a href="javascript:;">Terms</a></li>
      <li><a href="javascript:;">People</a></li>
      <li><a href="javascript:;">Contact Us</a></li>
    </ul>
	</div>
	</div>
	<div class="container-fluid logo">
	<div class="col-md-3">
	<img src="<?php echo base_url(); ?>front/images/logo.png">
		</div>
		<div class="col-md-3">
		<img src="<?php echo base_url(); ?>front/images/lohio.png">
		</div>
	<div class="col-md-3">
	<h4 style="float:right;"><a href="javascript:;">Link to main IFA Site</a></h4>
	</div>
	<div class="col-md-3">
	<h4><a href="javascript:;">Link to Lohia Foundation</a></h4>
	
	</div>
	</div>

