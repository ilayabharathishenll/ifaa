<div class="container-fluid footer  ">
	<div class="container-fluid footer-margin ">
	<div class="col-md-4">
	<img src="<?php echo base_url(); ?>front/images/logo.png">
	<p>'Apurva' Ground Floor, No 259, 4th Cross,<br>
Raj Mahal Vilas IInd Stage, IInd Block,<br>
Bangalore, India - 560 094<br>
Tel: 91-80-2341 4681 / 82<br>
Fax: 91-80-2341 4683</p>
	</div>
	<div class="col-md-2">
	<h6>IFA Archive is built with support from LOHIA Foundation</h6>
	</div>
	<div class="col-md-2">
	<img class="footer_logo" src="<?php echo base_url(); ?>front/images/lohio.png">
	</div>
	<div class="col-md-4">
	<input type="email" name="" placeholder="Subscribe">
	<h4>IFA Newsletter</h4>
	<ul>
	<li><a href="javascript:;"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
	<li><a href="javascript:;"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
	<li><a href="javascript:;"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
	<li><a href="javascript:;"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
	</ul>
	</div>
	</div>
	</div>
	<div class="container-fluid sub-footer">
	<div class="col-md-1">
	
	</div>
	<div class="col-md-3">
	<p><script type="text/JavaScript">

document.write(new Date().getFullYear());

</script> India Foundation For the Arts</p>
	</div>
	<div class="col-md-7">
	<p>Privacy Policy | Terms & Conditions | Disclaimer | Designed by <a href="https://www.istudiotech.in/">iStudio Technologies</a></p>
	</div>
	<div class="col-md-1">
	
	</div>
	</div>
	
	
</body>
</html>