<?php

error_reporting(~E_NOTICE);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user extends CI_Controller {

    Public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin_model');
        
        
        
    }

    // Show login page
    public function index() {
        $pagedata['pagetitle'] = 'India Foundation for the Arts';
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/index', $pagedata);
        $this->load->view('user/footer');
    }
    
    public function advance_search() {
        $pagedata['pagetitle'] = 'India Foundation for the Arts';
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/advance_search', $pagedata);
        $this->load->view('user/footer');
    }
    
    public function multy_search($parameter)
    {
        $pagedata['pagetitle'] = $parameter;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/multysearch',$pagedata);
        $this->load->view('user/footer',$pagedata);
    }
      public function about us($param) {
       $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/about_us', $pagedata);
        $this->load->view('user/footer');
    }
	public function privacy policy($param) {
       $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/privacy policy', $pagedata);
        $this->load->view('user/footer');
    }
	 public function contact us($param) {
       $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/contact_us', $pagedata);
        $this->load->view('user/footer');
    }
	
    public function grant_list($parm = '') {
        // echo "hhhh"; die;
        //print_r($_POST); die;
        if($parm == 'popular')
        {
           //echo "hhhh"; die; 
           $this->db->order_by('popular_search', 'desc');
           $pagedata['data'] = $this->db->get('dms_grant')->result_array();
        }
        elseif($_POST){
//            echo "<pre>";
//            print_r($_POST); die;
            if($_POST['search'])
            {
                //echo "hh"; die;
                $catId = $this->db->get_where('dms_category', array('category_name' => $_POST['search']))->row_array();
                $result = $this->db->get_where('dms_grant', array('category_id' => $catId['category_id']))->result_array();
                if($result)
                {
                    $pagedata['data'] = $result;
                }
                else
                {
                    
                }
            }
            else
            {
                $result = $this->admin_model->grant_serch($_POST['category'], $_POST['year'],$_POST['key'], $_POST['status'], $_POST['language'],$_POST['region'], $_POST['art'],$_POST['grantee_name']);
                if($result)
                {
                    $pagedata['data'] = $result;
                }
                else
                {
                    //$pagedata['data'] = "No Result Gound";
                }
            }
            
        }
        else
        {
//            $pagedata['data'] = $this->db->get('dms_grant')->result_array();
        
        }
        $pagedata['pagetitle'] = 'Grant List';
        $pagedata['page'] = 'Searched Result';
        $pagedata['param1'] = $param1;
       // $this->db->select('*')->from('dms_grant')->where("column LIKE '%$keyword%'")->get();
        $this->db->group_by('category_year');
        $pagedata['year'] = $this->db->get('dms_grant')->result_array(); 
        //$pagedata['data'] = $this->db->get('dms_grant')->result_array(); 
        $pagedata['category'] = $this->db->get('dms_category')->result_array();
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/grant_list', $pagedata);
        $this->load->view('user/footer');
    }
    public function document($param) {
        // echo "hhhh"; die;
        //$this->load->view('login');
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/document', $pagedata);
        $this->load->view('user/footer');
    }
    public function image($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/image_page', $pagedata);
        $this->load->view('user/footer');
    }
    public function video($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/popup_video', $pagedata);
        $this->load->view('user/footer');
    }
    public function audio($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/Audio_page', $pagedata);
        $this->load->view('user/footer');
    }
    public function weblink($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/weblink_page', $pagedata);
        $this->load->view('user/footer');
    }
    public function blogs($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/blogs', $pagedata);
        $this->load->view('user/footer');
    }
    public function programs($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/programs', $pagedata);
        $this->load->view('user/footer');
    }
    public function sourcecode($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/Sourcecode', $pagedata);
        $this->load->view('user/footer');
    }
    public function single_grant($param = '') {
        //echo $param; die;
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['document'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'20'))->row_array();
        $pagedata['img'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'17'))->row_array();
        $pagedata['audio'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'18'))->row_array();
        $pagedata['video'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'19'))->row_array();
        $pagedata['weblink'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'21'))->row_array();
        $pagedata['Sourcecode'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'38'))->row_array();
        $pagedata['Programs'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'39'))->row_array();
        $pagedata['Blogs'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'40'))->row_array();
        
        $popu = $data['popular_search'] + '1';
        $datass = array('popular_search' => $popu);
        $this->db->where('id', $param);
        $this->db->update('dms_grant', $datass);
        
        //$data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/single_grant', $pagedata);
        $this->load->view('user/footer');
    }

    /* for admin login  */

    public function login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->admin_model->login($username, $password);
        //print_r($result); die;

        if($result) {
            if ($this->session->userdata['login']['user_role'] == '1')
            {
                $this->session->set_flashdata('flash_message', 'login Successfull');
                redirect('adminlogin/dashboard', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'login Successfull');
                redirect('pelogin/dashboard', 'refresh');
            }

        } else {
            $this->session->set_flashdata('permission_message', 'username and Password is invalid');
            // redirect($_SERVER[HTTP_REFFER]);
            redirect('admin', 'refresh');
        }
    }
    
    public function metadata()
    {
        //print_r($_POST); die;
        $meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'General Information'))->result_array();
        $meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Author, Publisher, Access'))->result_array();
        $meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Physical Characteristics'))->result_array();
        $meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Digitisation Details'))->result_array();
        $meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Audio-Visual Material Details'))->result_array();
        $meta6 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Others'))->result_array();
        ?>
        <h5>Metadata</h5>
        <?php if($meta1) { ?>
            <h4 style="margin: 20px 0;"><strong>General Information</strong></h4>

            <?php foreach($meta1 as $value) {?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
            
        <?php } } if($meta2) { ?>

        <h4  style="margin: 20px 0;"><strong>Author, Publisher, Access</strong></h4>
	<?php foreach($meta2 as $value) {?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php } } if($meta3) { ?>
        <h4  style="margin: 20px 0;"><strong>Physical Characteristics</strong></h4>
	<?php foreach($meta3 as $value) {?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php } } if($meta4) { ?>
        <h4  style="margin: 20px 0;"><strong>Digitization Details</strong></h4>
	<?php foreach($meta4 as $value) {?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        <?php } } 
    }

    /* for admin logout */

    public function logout() {
        $this->session->unset_userdata(); 
        $this->session->sess_destroy();
        $this->session->set_flashdata('flash_message', 'Successfully Logout');
        redirect('admin', 'refresh');
    }
    
//    public function user() {
//        echo "hhhh"; die;
//        //$this->load->view('login');
//    }

}
