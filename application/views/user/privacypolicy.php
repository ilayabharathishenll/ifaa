﻿<?php //print_r($data); ?>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/styles.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>front/css/font-awesome.min.css">
<!-- <script src="<?php echo base_url(); ?>assets/js/jquery/jquery-2.0.3.min.js"></script>-->
<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        

        
</head>
<style>
.col-md-6 {
    padding-left: 0px !important;
}
            .navbar-nav
            {
                float: right;
            }
            .carousel-inner .item img
            {
                height:280px;
            }

    .popularbut
    {
    text-align: center;
    font-size: 24px;
    padding: 5px 30px 5px 30px;
    background: #e4e4e4;
    border: 5px solid #f1f1f1;
    display: block;
    text-decoration:none;
    color:#695c56;
    }
    .popularbut:hover{
         text-decoration:none;
         color:#695c56;
    }.Contact-us h4 {
    font-size:25px;
    font-weight: bold;
    color: #ad4a0f;
}.video-block p {
    font-size:17px;
    font-weight: bold;
	text-align: center !important;
}.Title.new1 {
    margin-left: -16px;
}
p {
font-size:17px;
    color: #58453e;
    line-height:25px;
    margin-bottom:0px;
    text-align: justify;
    margin-left: .25in;
}
.Title strong {
    color: #af3c23;
    font-size:20px;
}.map {
    margin-top: 10px;
}.video-block {
    text-align: center;
    border: 2px solid #58453e;
    padding-top: 100px;
    padding-bottom: 100px;
} .map-bottom {
    margin-top: 20px;
}ol.privacy-list{  
text-align: justify;  list-style-type: lower-alpha;
    padding-left:40px !important;
       font-size: 17px;
    line-height: 25px;
}p.var-text {
    margin-left: 0.35in;
}strong.About-content {
    font-size: 20px;
    color: color: #594c46;
    color: #594c46;
}.search_box
    {
        background: none !important;
        border:none !important;
        padding: 0 !important;
    }strong.cont-block {
    color: #ad4a0f;
    font-size: 17px;
    line-height: 25px;
}.click-tect p {
    text-align: center;
    font-size: 20px;
}ul.privacy-list li {
text-align: justify;
line-height: 30px;
    list-style: none;
    padding-left: 50px !important;
    text-indent: -23px;
}ul.second-list li {
    list-style: none;
    padding-left:20px !important;
    text-indent: -20px;
}.mar-top {
    margin-top: 30px;
}
</style>
    <div class="container-fluid bread">
        
    </div>
<div class="container About-content">
 <div class="col-md-2">
          <div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
        </div>
<div class="col-md-8">
   
	<div class="row">
	<div class="Contact-us"><h4>PRIVACY POLICY</h4> </div>

 <strong class="cont-block">By visiting our Website, you are accepting the practices described in our Privacy Policy. If you do not agree to the terms of this Privacy Policy, please do not use the Website.</strong>
<br/>
 <div class="row mar-top">


<div class="Title"><strong>1. COPYRIGHT PROTECTION</strong>
<p>All content on this Website including graphics, text, icons, interfaces, audio clips, videos, logos and images is the property of IFA and/or its grantees and beneficiaries. Permission is given to use the resources of this Website only for the purposes of making enquiries, making a donation, applying for a grant, becoming a Friend of IFA or placing an order for the purchase of IFA publications. Any other use, including the reproduction, modification, distribution, transmission, republication, display or performance, of the content on this Website can only be made with the express written permission of IFA. All other trademarks, brands and copyrights other than those belonging to IFA belong to their respective owners and are their property. You may not modify any of the materials. Except for personal, informational or non-commercial purposes, you may not copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer or sell any information or work contained on the Website. Except as authorized under the copyright laws, you are responsible for obtaining permission before reusing any copyrighted material that is available on the Website.
</p>
<div class="Title"><strong>2. DATA COLLECTION</strong>
<p>India Foundation for the Arts (IFA) is committed to the ethical collection, retention and use of information that you provide to us about yourself ("Personal Information") on this Website <strong class="About-content">http://www.indiaifa.org/ </strong>("Website")</p>
</div>
<ol class="privacy-list">
<li>When you sign up to receive e-mail newsletters from us;</li>
<li>When you register for and participate in our programs, activities, initiatives, and events;</li>
<li>When you request information or assistance;</li>
<li>In conjunction with processing your grant applications;</li>
<li>In conjunction with your employment inquiries or applications;</li>
<li>In conjunction with any verification of your account information;</li>
<li>In conjunction with investigations into any activity that may violate the law or the Website’s terms and conditions;</li>
<li>When you communicate with us through the Website’s contact us/ feedback form;</li>
<li>When you provide user generated content on any part of the Website that permits it; and</li>
<li>In conjunction with any other place on the Website where you knowingly volunteer personal information.When you interact with any IFA page or account on a social media platform, such as Facebook, Twitter, YouTube or Vimeo, we may also collect the personal information that you make available to us on that page or through that account including your account ID or “handle.”</li>
</ol>
	</div>
<div class="Title"><strong>PURPOSE OF DATA COLLECTION</strong>
<p>Your Personal Information may comprise the following:</p>
	<ol class="privacy-list">
	


<li>your name</li>
<li>your occupation</li>
<li>your email and mailing address</li>
<li>your telephone number </li>
<li>your payment processing details</li>
<li>limited personal details</li>
<li>any other data as IFA may require
</li>
</ol>
	</div>

<div class="Title"><strong>3. COLLECTION OF INFORMATION</strong>
<ol class="privacy-list">
<li> Website Browsing: You browse the Website anonymously. We do not require you to identify yourself or reveal any Personal Information while browsing through the Website. However, you may not be able to access certain sections of the Website or interact with us without supplying us with Personal Information. For instance, you would not be able to transact at the Website or make any donations at the Website, without providing the Website with Personal Information. If you desire to register yourself at the Website, you would be required to provide your Personal Information.
<li>While you are browsing through the Website, the Website's operating system may automatically record some general information ("General Information") about your visit such as:
the date and time you visit the Website, along with the address of the previous website your were visiting, if you linked to us from another website
the type of browser you are using (such as Internet Explorer version 'X')
which 'hit' it is on the Website
<li>The General Information is not Personal Information. IFA's tracking system does not record personal information about individuals or link this information to any Personal Information collected from you.
<li>The General Information is used by IFA for statistical analysis, for tracking overall traffic patterns on the Website and to gauge the public interest in IFA and the Website. Such General Information may be shared by IFA with any person, at IFA's discretion.
</ol>
</div>
	
<div class="Title"><strong>4. USAGE OF INFORMATION</strong>
<ol class="privacy-list">
<li>Personal information will be used by IFA for internal purposes including the following:
<ul class="second-list">
<li>i. sending you online material such as emailers on fundraisers, events, grantee presentations, IFA Quarterly newsletter, donations, reminders for Friends of IFA renewal, reminders for ArtConnect renewal, regular updates on the utilization of donations by IFA and other updates
</li>
<li>ii. offline material such as promotional material, brochures, catalogues, IFA Annual Report, ArtConnect, calendars and others.</li>
</ul>
<li>processing your donations to IFA and purchases of IFA publications on the Website.</li>
<li> delivering IFA publications you have purchased on the Website /sending you receipt for donations made by you to IFA.</li>
<li> maintaining an internal confidential database of all the Personal Information collected from visitors to the Website.</li>
<li> evaluating and administering the Website and IFA's activities, responding to any problems that may arise and gauging visitor trends on the Website.</li>
</ol>
</div>
	
<div class="Title"><strong>5. DISCLOSURE OF PERSONAL INFORMATION BY IFA</strong>
<ol class="privacy-list">
<li>Within IFA, access to Personal Information collected by IFA will be given only to those persons who are authorized by IFA and third parties hired by IFA to perform administrative services. IFA will provide access to third parties for inter alia— entering and managing Personal Information in IFA's Database, processing your orders or donations, preparing address labels, sending emails, which require such third parties to have access to your Personal Information. IFA cannot guarantee that such parties will keep your Personal Information confidential and IFA will not be liable in any manner for any loss of confidentiality attributable to such third parties.</li>
 <li>IFA may share Personal Information with any of persons who are associated with IFA, including companies and non-governmental organisations affiliated with IFA in any manner. IFA will retain ownership rights over such information and will share only such portions of the Personal Information as it deems fit.</li>
<li>IFA is not liable in any manner whatsoever for the loss, damage(whether direct, indirect, consequential or incidental) or harm caused to you by the misuse of your Personal Information by a third party who is not an employee of IFA.
 </li><li> Notwithstanding anything contained herein or any other contract between you and IFA, IFA reserves the right to disclose any Personal Information about you without notice or consent as needed to satisfy any requirement of law, regulation, legal request or legal investigation, to conduct investigations of breaches of law, to protect the Website, to protect IFA and its property, to fulfill your requests, to protect our visitors and other persons if required, by the policy of IFA.
</li>
</ol>
</div>
	
<div class="Title"><strong>6. SECURITY</strong>
<ol class="privacy-list">
<li>IFA endeavors to use up-to-date security measures to protect your Personal Information.</li>
<li>IFA however does not make any express or implied warranty with respect to the security measures that it may employ from time to time for the protection of the Personal Information.</li>
</ol>
</div>
<div class="Title"><strong>7. THIRD-PARTY LINKS</strong>
<p>From time to time, this Website may contain links to websites that are not owned, operated or controlled by India Foundation for the Arts. All such links are provided solely as a convenience to you. If you use these links, you will leave this Website. We are not responsible for any content, materials or other information located on or accessible from any other website, nor any products or services that you buy from any other websites or any donations made through any other websites. We do not endorse, guarantee or make any representations or warranties regarding any other websites, or any content, materials or other information located or accessible from any other websites, or the products or services that you may obtain from using any other websites or how any donations you make through such websites will be used. If you decide to access any other
 </p>
</div>
<div class="Title"><strong>8. ACCESS AND INTERFERENCE</strong>
<p> You agree that you will not use any robot, spider, scraper or other automated means to access the Website for any purpose without our express written permission. Additionally, you agree that you will not: (a) take any action that, in our sole discretion, imposes, or may impose an unreasonable or disproportionately large load on our infrastructure; (b) interfere or attempt to interfere with the proper working of the Website or any activities conducted on the site; or (c) bypass any measures we may use to prevent or restrict access to the Website.</p>
</div>
<div class="Title"><strong>9. FORCE MAJEURE</strong>
<p>Neither India Foundation for the Arts nor you shall be responsible for damages or for delays or failures in performance resulting from acts or occurrences beyond their reasonable control, including, without limitation: fire, lightning, explosion, power surge or failure, water, acts of God, war, revolution, civil commotion or acts of civil or military authorities or public enemies; any law, order, regulation, ordinance or requirement of any government or legal body or any representative of any such government or legal body; labor unrest, including, without limitation, strikes, slowdowns, picketing or boycotts; or inability to secure raw materials, transportation facilities or fuel, energy shortages, or acts or omissions of other common carriers.</p>
</div>
<div class="Title new1">
<strong>10. VARIATION OF THE PRIVACY POLICY</strong>
<p class="var-text">IFA shall be absolutely entitled at its sole discretion from time to time add to, alter, delete or modify any of the terms and conditions contained herein. Such changes, additions, alterations, deletions or modifications shall be binding on you once you visit the Website after the Privacy Policy has been so amended.</p>
</div>
	</div>	</div>
	  </div>
	  <div class="col-md-4">
</div>
	    </div>
<script>
        $(document).ready(function(){
        $( "#searchdiv" ).click(function() {
        
        var cat = document.getElementById('category').value;
        var year = document.getElementById('year').value;
        //alert(cat);
        $( "#usersForm" ).submit();
       
    });
});

function goBack()
{
    window.history.back();
}

pageSize = 5;

    var pageCount =  $(".line-content").length / pageSize;
    for(var i =0 ; i<pageCount; i++){
        $("#pagin").append('<li onclick="report();"><a href="javascript:;" >'+(i+1)+'</a></li> ');
    }
    $("#pagin li").first().find("a").addClass("current1");
    $("#pagin li").first().addClass("active");
        
    showPage = function(page) {
    //alert(page);
        $(".line-content").hide();
        $(".line-content").each(function(n) {
        if (n >= pageSize *(page - 1) && n < pageSize* page)
            $(this).show();
               
        });        
    }
//    
    showPage(1);
    $("#pagin li a").click(function() {
        $("#pagin li").removeClass("active");
        $(this).parent().addClass("active");
     
//        $("#pagin li a").removeAttr( "style" )
        $("#pagin li a").removeClass("current1");
        $(this).addClass("current1");
       
        showPage(parseInt($(this).text())) 
 });

function goBack()
{
    window.history.back();
}
</script>