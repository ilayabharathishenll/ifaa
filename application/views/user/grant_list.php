<?php //print_r($data); ?>
<style>
	.popularbut {
		text-align: center !important;
		font-size:17px;
		padding:5px 0px 5px 0px;
		display: block;
		text-decoration: none;
		color: #695c56;
		background: #e3dcda;
		border: 3px solid #b4a49f;
		border-radius: 8px;
	}
	.popularbut:hover {
		text-decoration:none;
		color:#695c56;
	}
	.search_box {
		color: #695c56;
		background: #e3dcda;
		border: 3px solid #b4a49f;
		border-radius: 8px;
	}
	select {
		text-align-last: center;
	}
	.searchdiv {
		-webkit-transform: rotate(180deg);
		-moz-transform: rotate(180deg);
		-o-transform: rotate(180deg);
		-ms-transform: rotate(180deg);
		transform: rotate(180deg);
		position: absolute;
		top: 2px;
		right: -70px;
		cursor: pointer;
	}
	@media screen and (max-width: 1200px) {
		.searchdiv {
			right: -130px;
		}
		.About-content,
		.grt-content,
		.grt-desc {
			padding: 0;
		}
	}
	@media screen and (max-width: 992px) {
		.go-back {
			padding: 0 15px;
			margin-bottom: 25px;
		}
		.filter_box select {
			margin-bottom: 25px;
		}
		.filter_box input[type="text"] {
			width: 100%;
		}
		.searchdiv {
			right: 25px;
		}
	}
</style>
<div class="container-fluid bread">
</div>
<div class="container About-content">
	<div class="col-md-2">
		<div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
	</div>
	<div class="col-md-9 grt-content">
		<div class="row grant_content">
			<div class="col-md-12">
				<form role="form" class="clearfix" action="<?php echo base_url(); ?>user/grant_list/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm">
					<div class="col-md-12 filter_box">
						<div class="col-md-4">
							<select id="category" name="category">
								<option value="">Programmes</option>
								<?php foreach($category as $cat){ ?>
								<option value="<?php echo $cat['category_id'];?>"><?=$cat['category_name'];?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-4">
							<select id="year" name="year">
								<option  value="">Year</option>
								<?php foreach($year as $years){ ?>
								<option value="<?php echo $years['category_year'];?>"><?php echo $years['category_year'];?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-4 search-button">
							<input class="search_box" type="text" name="key" placeholder="Keyword..">
							<img id="searchdiv" class="searchdiv" src="<?php echo base_url();?>front/images/Arrow-image.png">
						</div>
					</div>
				</form>
				<?php if($data) {
					$i=1;$img=1;
					foreach(array_unique($data,$id) as $value) {
					   $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $value['grantee_name']))->row_array();
					   $category = $this->db->get_where('dms_category', array('category_id' => $value['category_id']))->row_array();
					   $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $value['categorytype_id']))->row_array();
					   $parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $value['id'], 'document_temp_id' => '46'))->row_array();
					   $enddate = explode('-' , $value['end_date']);
					   ?>
						<div class="col-md-12 grant_result line-content">
							<div class="col-md-12 iocn-image">
								<img src="<?php echo base_url(); ?>front/images/<?php echo $img; ?>.png" style="height:60px;width:60px">
								<h4><?php echo $value['grant_number']; ?> | <?php echo $grantee['grantee_name']; ?></h4>
								<h5><?php echo $category['category_name'].' | '.$value['start_date'].' - '.$value['end_date'].' | '.$status['categorytype_name']; ?></h5>
							</div>
							<div class="col-md-12 grt-desc clearfix">
								<p><?php echo $parent['parent_description']; ?></p>
								<a href="<?php echo base_url(); ?>user/single_grant/<?php echo $value['id']; ?>">Read More ></a>
							</div>
						</div>
						<?php $i++;$img++;
						if ($img==19)
							$img = 1;
					}
				} ?>
				<div class="col-md-12 pagin">
					<ul id="pagin" class="pagination">
					</ul>
				</div>
			</div>
			<div class="col-md-3 hidden">
				<div class="search">
					<form role="form" action="<?php echo base_url(); ?>user/grant_list/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm">
						<div>
							<a href="<?php echo base_url(); ?>user/advance_search/advance" class="popularbut">Advanced Search</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>

<script>
	$(document).ready(function(){
		$( "#searchdiv" ).click(function() {
			var cat = document.getElementById('category').value;
			var year = document.getElementById('year').value;
			$( "#usersForm" ).submit();
		});
	});

	function goBack() {
		window.history.back();
	}

	pageSize = 5;
	var pageCount = $(".line-content").length / pageSize;
	for(var i =0 ; i<pageCount; i++){
		$("#pagin").append('<li onclick="report();"><a href="javascript:;" >'+(i+1)+'</a></li> ');
	}
	$("#pagin li").first().find("a").addClass("current1");
	$("#pagin li").first().addClass("active");
	showPage = function(page) {
		$(".line-content").hide();
		$(".line-content").each(function(n) {
		if (n >= pageSize *(page - 1) && n < pageSize* page)
			$(this).show();
		});
	}
	showPage(1);
	$("#pagin li a").click(function() {
		$("#pagin li").removeClass("active");
		$(this).parent().addClass("active");
		$("#pagin li a").removeClass("current1");
		$(this).addClass("current1");
		showPage(parseInt($(this).text())) 
	});
</script>