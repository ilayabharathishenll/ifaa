<?php 
        $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $data['grantee_name']))->row_array();
        $category = $this->db->get_where('dms_category', array('category_id' => $data['category_id']))->row_array();
        $parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '8'))->row_array();
        $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $data['categorytype_id']))->row_array();
        
         $individualData = $this->db->get_where('dms_individual_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36'))->result_array();

        $meta = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36'))->result_array();
        $meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'General Information'))->result_array();
        $meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Author, Publisher, Access'))->result_array();
        $meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Physical Characteristics'))->result_array();
        $meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Digitisation Details'))->result_array();
        $meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Audio-Visual Material Details'))->result_array();
        $meta6 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '36', 'detail' => 'Others'))->result_array();
?>

<style>.col-md-6.line-content {
    padding-left: 0px;
}
      audio::-internal-media-controls-download-button {
    display:none !important;
}
    .line-content img, .audio_dd
    {
        width:100%;
    }
    .line-content
    {
        border:1px solid #ddd;
        padding: 5px;
        margin: auto;
    }
     #aPlayer > audio { width: 100% }
/* Chrome 29+ */
@media screen and (-webkit-min-device-pixel-ratio:0)
  and (min-resolution:.001dpcm) {
     /* HIDE DOWNLOAD AUDIO BUTTON */
     #aPlayer {
           overflow: hidden;width: 390px; 
    }

    #aPlayer > audio { 
      width: 420px; 
    }
}

/* Chrome 22-28 */
@media screen and(-webkit-min-device-pixel-ratio:0) {
    
      #aPlayer {
           overflow: hidden;width: 390px; 
        }

    #aPlayer > audio { width: 420px; }
}
@media screen and (max-width: 992px) {
    .line-content a img {
        height: 220px;
    }
    .About-content {
        padding: 0;
        margin-top: 0;
    }
}
@media screen and (max-width: 768px) {
    .line-content a img {
        height: 175px;
        margin: auto;
        width: 175px;
    }
}
</style>






<div class="container-fluid bread">
	<div class="col-md-4">
	<p class="bread_content1">Audio Files</p>
	</div>
	<div class="col-md-8">
	<p class="bread_content2">Home/Searched Result/<?php echo $data['grant_number']; ?>/Video files</p>
	</div>
</div>
    <div class="container About-content">
 <div class="col-md-2">
          <div class="go-back"><a href="http://theifaarchive.org/"><img src="http://theifaarchive.org/front/images/Arrow-image.png">Back</a></div>
        </div>
		 
	<?php 
       // $date2=date_create($data['end_date']);//echo date_format($date2,"d-m-Y"); 
        // $date2=date_create($data['end_date']);//echo date_format($date1,"d-m-Y"); 
        ?>
    <div class="col-md-10">
	<div class="image_content">
	<div class="col-md-8 image_content_left">
	<h5><?php echo $grantee['grantee_name']; ?></h5>
	<h6><?php  $date2=date_create($data['end_date']); $date1=date_create($data['start_date']);echo $category['category_name'].' | '.date_format($date1,"d-m-Y").' - '.date_format($date2,"d-m-Y").' | '.$status['categorytype_name']; ?></h6>
	
	<div class="col-md-12 image_content_images" style="padding:0;">
            <?php $i ='1';//echo "<pre>";print_r($individualData);
            foreach($individualData as $valas ){ ?>
            <?php   $meta2_ = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => 36, 'metadata_individual_number' => $valas['metadata_individual_number'], 'detail' => 'Author, Publisher, Access','metadata_value'=>"Public"))->result_array();
     // print_r($meta2_);
foreach($meta2_ as $value1) { 
        if($valas['individual_temp_id']==$value1['template_id'])
            {//id="aPlayer"
           //echo $i;
                ?>
            <div class="col-md-6" style="margin-bottom:25px" >
                <div class="line-content"  >
                    <img src="<?php echo base_url()."front/images/audio.jpg"; ?>">
                  <audio  controls="controls" class="audio_dd" controlsList="nodownload">
                    <source src="<?php echo base_url()."local_path/".$valas['individual_file']; ?>" type="audio/mpeg">
                </audio>
                <div class="video_captions">
                    <h6 class="text-center"><a href="javascript:;" onclick="metadata('<?php echo $data['id']; ?>', '36', '<?php echo $valas['metadata_individual_number']; ?>')">View Details</a></h6>	
                </div>
                </div>
            </div>
            <?php $i++; }}} ?>
	
<!--	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 2</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 3</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 4</h5>	
	</div>
	</div>-->
	
	
	
	</div>
	
<!--	<div class="col-md-12 image_content_images">
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 5</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 6</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 7</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 8</h5>	
	</div>
	</div>
	
	
	
	</div>-->
	
<!--	<div class="col-md-12 image_content_images">
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 9</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 10</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 11</h5>	
	</div>
	</div>
	
	<div class="col-md-3">
	<video width="145" controls>
	<source src="<?php echo base_url(); ?>front/videos/baby.mp4" type="video/mp4">
	</video>
	<div class="video_captions">
	<h5>Video 12</h5>	
	</div>
	</div>
	<div class="pagin">
	<ul class="pagination">
	<li class="active"><a href="javascript:;">1</a></li>
  <li><a href="javascript:;">2</a></li>
  <li><a href="javascript:;">3</a></li>
  <li><a href="javascript:;">4</a></li>
  <li><a href="javascript:;">5</a></li>
</ul>
	
	</div>
	
	</div>-->
        <div class="col-md-12 pagin">
            <ul id="pagin" class="pagination">

            </ul>
	</div>

    </div>
        <div class="col-md-4 image_content_right" id="metadatadiv">
	<h5>Metadata</h5>
<!--        	<div>
        <?php $j4=1; if($meta1) { ?>
            <h4 style="margin: 20px 0;"><strong>General Information</strong></h4>

            <?php foreach($meta1 as $value) {if($value['metadata_value']!='' && $j4==1){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        <?php $j4++;}}} ?>
            <div class="panel-group" id="accordion">
    <div>
      <div>
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" style="
   // color: #f90;
    font-weight: bold;
    font-size: 16px;background-color:#ff9900; color:#fff;padding:5px;">Read More ></a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse ">
        <div class="panel-body dd">
       
                 
        <?php $j=1;$j1=1;$j2=1;$j3=1;//echo "<pre>"; print_r($meta2);
        if($meta2) { ?>

        <h4  style="margin: 20px 0;"><strong>Author, Publisher, Access</strong></h4>
	<?php foreach($meta2 as $value) { if($value['metadata_value']!='' && $j==1){  ?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php $j++; }
        }} if($meta3) { ?>
        <h4  style="margin: 20px 0;"><strong>Physical Characteristics</strong></h4>
	<?php foreach($meta3 as $value) { if($value['metadata_value']!=''  && $j1==1){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php $j1++;} }} if($meta4) {  ?>
        <h4  style="margin: 20px 0;"><strong>Digitisation Details</strong></h4>
	<?php foreach($meta4 as $value) { if($value['metadata_value']!=''  && $j2==1){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
 <?php $j2++;} }} if($meta5) { ?>
        <h4  style="margin: 20px 0;"><strong>Audio-Visual Material Details</strong></h4>
	<?php foreach($meta5 as $value) { if($value['metadata_value']!=''  && $j3==1){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
           
 <?php $j3++;} }} ?>
        </div>
      </div>
    </div>
   
   
  </div>    	</div>-->
	
	</div>
	</div>
		 <div class="col-md-2">
</div>
	</div>
	</div>
<script>
    pageSize = 4;
//alert($(".line-content").length)
    var pageCount =  $(".line-content").length / pageSize;
    for(var i =0 ; i<pageCount; i++){
        $("#pagin").append('<li onclick="report();"><a href="javascript:;" >'+(i+1)+'</a></li> ');
    }
    $("#pagin li").first().find("a").addClass("current1");
    $("#pagin li").first().addClass("active");
        
    showPage = function(page) {
    //alert(page);
        $(".line-content").hide();
        $(".line-content").each(function(n) {
        if (n >= pageSize *(page - 1) && n < pageSize* page)
            $(this).show();
               
        });        
    }
//    
    showPage(1);
    $("#pagin li a").click(function() {
        $("#pagin li").removeClass("active");
        $(this).parent().addClass("active");
     
//        $("#pagin li a").removeAttr( "style" )
        $("#pagin li a").removeClass("current1");
        $(this).addClass("current1");
       
        showPage(parseInt($(this).text())) 
 });
 
    function metadata(gnt, doc, id)
 {
     //alert(doc)
     $.ajax({
        url: "<?php echo base_url(); ?>user/metadata",
        type: "POST",
        data: {'grnt': gnt, 'doc': doc, 'nmbr': id},
        success: function (response)
        {
            //alert(response);
            $("#metadatadiv").html(response);
            //$("#metadatadiv").html(response);
            //alert(response);

        }

    });
 }
 function goBack()
{
    window.history.back();
}
</script>