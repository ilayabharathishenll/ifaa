<?php //echo $data['id'];
		$grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $data['grantee_name']))->row_array();
		$category = $this->db->get_where('dms_category', array('category_id' => $data['category_id']))->row_array();
		$parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '8'))->row_array();
		$individualData = $this->db->get_where('dms_individual_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '17'))->result_array();
		$status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $data['categorytype_id']))->row_array();
		//print_r($individualData['individual_file']); die;
		$meta = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '35'))->result_array();
		$meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '35', 'detail' => 'General Information'))->result_array();
		$meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '35', 'detail' => 'Author, Publisher, Access'))->result_array();
		$meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '35', 'detail' => 'Physical Characteristics'))->result_array();
		$meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '35', 'detail' => 'Digitisation Details'))->result_array();
		$meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '35', 'detail' => 'Audio-Visual Material Details'))->result_array();
		$meta6 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => '35', 'detail' => 'Others'))->result_array();
		$meta_access= $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'],'document_temp_id' => '19'))->result_array();
		//echo "<pre>"; print_r($meta_access);
		?>
<style>
   
    .popularbut
    {
    text-align: center;
    font-size: 24px;
    padding: 5px 30px 5px 30px;
    background: #e4e4e4;
    border: 5px solid #f1f1f1;
    display: block;
    text-decoration:none;
    color:#695c56;
    }
    .popularbut:hover{
         text-decoration:none;
         color:#695c56;
    }
    
    .popup_image{
/*	width:570px !important;
	height:490px;*/
        margin: 0 auto;
        max-width: 100%;
      /* height: 620px;*/
        max-height: 100% !important;
	}
        .modal-dialog
        {
            background:#fff;
            max-height:100%;
            width:80%;
            height:100%;
        }
        
        
        
    .popup_modal
    {
        width: 100%;
        /*height: 100%;*/
        background-color:#fff;
        box-shadow:none;
    }
    .popup_modal .modal-body
    {
        text-align: center;
    }
    .line-content:hover a img
    {
        transform: none !important;
    }
    .line-content
    {
        /*isplay: block !important;*/
    }
    @media screen and (max-width: 992px) {
        .line-content a img {
            height: 220px;
        }
        .About-content {
            padding: 0;
            margin-top: 0;
        }
    }
    @media screen and (max-width: 768px) {
        .line-content a img {
            height: 175px;
            margin: auto;
            width: 175px;
        }
    }
</style>
<div class="container-fluid bread">
	<div class="col-md-4">
		<p class="bread_content1">Image Files</p>
	</div>
	<div class="col-md-8">
		<p class="bread_content2">Home/Searched Result/<?php echo $data['grant_number']; ?>/image files</p>
	</div>
</div>
<div class="container About-content">
	<div class="col-md-2">
		<div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
	</div>
	<div class="col-md-10">
		<div class="image_content">
			<div class="col-md-8 image_content_left">
				<h5><?php echo $grantee['grantee_name']; ?></h5>
				<h6><?php  $date2=date_create($data['end_date']); $date1=date_create($data['start_date']);echo $category['category_name'].' | '.date_format($date1,"d-m-Y").' - '.date_format($date2,"d-m-Y").' | '.$status['categorytype_name']; ?></h6>
				<div class="col-md-12 image_content_images">
					<?php $i = '1';
						// echo "<pre>";print_r($individualData);
						 foreach($individualData as $valas) { 
							$meta2_ = $this->db->get_where('dms_metadata', array('grant_temp_id' => $data['id'], 'document_temp_id' => 17, 'metadata_individual_number' => $valas['metadata_individual_number'], 'detail' => 'Author, Publisher, Access','metadata_value'=>"Public"))->result_array();
						// print_r($meta2_);
						foreach($meta2_ as $value1) { 
						if($valas['individual_temp_id']==$value1['template_id'])
						 {
						//echo $i;
							 ?>
							 <div class="col-md-4 col-sm-6 line-content text-center">
			                    <a class="clearfix"  style="margin-top:20px;" href='<?php echo base_url()."local_path/".$valas['individual_file']; ?>' data-fancybox='images'>
			                      <img src='<?php echo base_url()."local_path/".$valas['individual_file']; ?>' class='img-responsive'>
			                    </a>
			                    <div class="img_captions ">
			                        <h5>Image <?php echo $i; ?></h5>
			                        <h6><a href="javascript:;" onclick="metadata('<?php echo $data['id']; ?>', '35', '<?php echo $valas['metadata_individual_number']; ?>')">View Details</a></h6>
			                    </div>
			                </div>
					<?php $i++; }}} ?>
				</div>
				<div class="col-md-12 pagin">
					<ul id="pagin" class="pagination">
					</ul>
				</div>
				<!--    </div>-->
			</div>
			<div class="col-md-4 image_content_right" id="metadatadiv">
				<h5>Metadata</h5>
			</div>
		</div>
	</div>
	<div class="col-md-2"></div>
</div>
<script>
pageSize = 8;
//alert($(".line-content").length)
	var pageCount =  $(".line-content").length / pageSize;
	for(var i =0 ; i<pageCount; i++){
		$("#pagin").append('<li onclick="report();"><a href="javascript:;" >'+(i+1)+'</a></li> ');
	}
	$("#pagin li").first().find("a").addClass("current1");
	$("#pagin li").first().addClass("active");
		
	showPage = function(page) {
	//alert(page);
		$(".line-content").hide();
		$(".line-content").each(function(n) {
		if (n >= pageSize *(page - 1) && n < pageSize* page)
			$(this).show();
			   
		});        
	}
//    
	showPage(1);
	$("#pagin li a").click(function() {
		$("#pagin li").removeClass("active");
		$(this).parent().addClass("active");
	 
//        $("#pagin li a").removeAttr( "style" )
		$("#pagin li a").removeClass("current1");
		$(this).addClass("current1");
	   
		showPage(parseInt($(this).text())) 
 });
 
 function metadata(gnt, doc, id)
 {
	 //alert(doc)
	 $.ajax({
		url: "<?php echo base_url(); ?>user/metadata",
		type: "POST",
		data: {'grnt': gnt, 'doc': doc, 'nmbr': id},
		success: function (response)
		{
			//alert(response);
			$("#metadatadiv").html(response);
			//$("#metadatadiv").html(response);
			//alert(response);

		}

	});
 }
function goBack()
{
	window.history.back();
}

</script>	