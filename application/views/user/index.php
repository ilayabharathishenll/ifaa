<?php 
	$banner=$this->db->get_where('banner',array('status'=>'active'))->result_array();
?>
<style>
	.carousel-inner .item img {
		height: 403px;
	}
	@media screen and (max-width: 1200px) {
		.carousel-inner .item img {
			height: 330px;
		}
	}
	@media screen and (max-width: 992px) {
		.carousel-inner .item img {
			height: 260px;
		}
	}
	@media screen and (max-width: 768px) {
		.carousel-inner .item img {
			height: 200px;
		}
	}
</style>
<div class="container-fluid banner_section">
	<div id="homeCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php
			$s = 0;
			foreach ($banner as $slide) {
				$active = '';
				if ($s == 0)
					$active = 'active';
			?>
				<li data-target="#homeCarousel" data-slide-to="<?php echo $s; ?>" class="<?php echo $active; ?>"></li>
			<?php 
				$s++;
			} ?>
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<?php
				$i = 1;
				foreach ($banner as $slide) {
					$active = '';
					if ($i == 1)
						$active = 'active';
				?>
				<div class="item <?php echo $active; ?>">
					<img src="<?php echo base_url(); ?>uploads/<?php  echo $slide['banner']; ?>" alt="" style="width:100%;">
				</div>
			<?php  $i++; }
			?>
		</div>
		<!-- Left and right controls -->
		<a class="left carousel-control" href="#homeCarousel" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#homeCarousel" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
<div class="container search-container top-search">
	<div class="row" style="margin-top:0px">
		<form role="form" action="<?php echo base_url(); ?>user/grant_list1/<?php echo $formaction; ?>" method="post" enctype="multipart/form-data" id="usersForm">
			<div class="col-md-3">
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="inner-addon right-addon">
					<input type="text" name="search" id="searchinput" value="" placeholder="Basic Search">
					<span onclick="uu();"><i class="glyphicon glyphicon-search"></i></span>
				</div>
				<script>
					function uu() {
						var val = document.getElementById('searchinput').value;
						if (val) {
							$("#usersForm").submit();
						}
					}
				</script>
			</div>
			<div class="col-md-3 col-sm-6">
				<a href="<?php echo base_url();?>user/advance_search/advance" class="popularbut">Advanced Search</a>
			</div>
			<div class="col-md-3">
			</div>
		</form>
	</div>
	<div class="textunder ifa-text">
		Please note that The IFA Archive is a work-in-progress. This online space currently holds the materials of projects supported by IFA in the years 2011 and 2012. Additionally, at the physical site of The IFA Archive, materials from projects supported in the years X, Y, Z will also be available for consultation. Both the online and physical spaces will be continually updated alongside the ongoing process of archiving. 
	</div>
</div>

<div class="row bg">
	<div class="container home-search-section">
		<div class="col-md-4 col-sm-6 search-listing">
			<div class="listing">
				<a href="<?php echo base_url(); ?>user/multy_search/Years">
					<img src="<?php echo base_url(); ?>front/images/image1.jpg">
					<h4>YEAR</h4>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 search-listing">
			<div class="listing">
				<a href="<?php echo base_url(); ?>user/multy_search/Programme">
					<img src="<?php echo base_url(); ?>front/images/image2.jpg">
					<h4>PROGRAMME</h4>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 search-listing">
			<div class="listing">
				<a href="<?php echo base_url(); ?>user/multy_search/Art_Form">
					<img src="<?php echo base_url(); ?>front/images/image3.jpg">
					<h4>DISCIPLINE</h4>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 search-listing">
			<div class="listing">
				<a href="<?php echo base_url(); ?>user/multy_search/Language">
					<img src="<?php echo base_url(); ?>front/images/image4.jpg">
					<h4>LANGUAGE</h4>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 search-listing">
			<div class="listing">
				<a href="<?php echo base_url(); ?>user/multy_search/Grantees">
					<img src="<?php echo base_url(); ?>front/images/image5.jpg">
					<h4>GRANTEE</h4>
				</a>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 search-listing">
			<div class="listing">
				<a href="<?php echo base_url(); ?>user/multy_search/Region">
					<img src="<?php echo base_url(); ?>front/images/image6.jpg">
					<h4>REGION</h4>
				</a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid ongoing_grants">
	<div id="myGrants" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators" style="display:none;">
			<li data-target="#myGrants" data-slide-to="0" class="active"></li>
			<li data-target="#myGrants" data-slide-to="1"></li>
			<li data-target="#myGrants" data-slide-to="2"></li>
			<li data-target="#myGrants" data-slide-to="3"></li>
		</ol>
		<?php
			$this->db->order_by('id', 'desc');
			$this->db->limit(5);
			$data_grant = $this->db->get('dms_grant')->result_array(); 
		?>
		<div class="carousel-inner grants_text">
			<?php
			$i=1;
			if($data_grant) {
				foreach ($data_grant as $value) {
					$grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $value['grantee_name']))->row_array();
					$category = $this->db->get_where('dms_category', array('category_id' => $value['category_id']))->row_array();
					$status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $value['categorytype_id']))->row_array();
					$parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $value['id'], 'document_temp_id' => '46'))->row_array();
					$enddate = explode('-' , $value['end_date']);
					$start  = date_create($value['start_date']);
					$end  = date_create($value['end_date']);
					$end 	= date_create();
					$diff  	= date_diff( $start, $end );
					$year=$this->admin_model->numberTowords($diff->y);
					$month=$this->admin_model->numberTowords($diff->m);
					$states_grantee = explode(",",$grantee['grantee_state']);
					$val11='';
					if(count($states_grantee)>0) {
						foreach($states_grantee as $row1){
							$g_st = $this->db->get_where('dms_state', array('state_id' => $row1))->row_array();
							$val11.=$g_st['state_name'].", ";
						}
					}
					$sd= explode('-',$value['start_date']);$ed= explode('-',$value['end_date']);
					if ($i==1) { ?>
						<div class="item active">
							<h2>RECENT UPDATES </h2>
							<h4><?php echo ucwords($grantee['grantee_name']); ?> | <?php echo ucwords($category['category_name']).' | '.preg_replace('/,.*/', '', $val11).' | '. $value['category_year']; ?></h4>
							<h3>Grant Period : <?php  echo $value['grant_duration'];?></h3>
							<?php
								if ($parent['parent_description'] == '') { ?>
									<p class="empty-text"><span></span></p>
								<?php } else {
									echo $parent['parent_description'];
								}
							?>
							<a class="link" href="<?php echo base_url(); ?>user/single_grant/<?php echo $value['id']; ?>">Read More ></a>
						</div>
					<?php } else { ?>
						<div class="item">
							<h2>RECENT UPDATES </h2>
							<h4><?php echo ucwords($grantee['grantee_name']); ?> | <?php echo ($category['category_name']).' | '.preg_replace('/,.*/', '', $val11).' | '. $value['category_year']; ?></h4>
							<h3>Grant Period :  <?php echo $value['grant_duration'];?></h3>
							<?php
								if ($parent['parent_description'] == '') { ?>
									<p class="empty-text"><span></span></p>
								<?php } else {
									echo $parent['parent_description'];
								}
							?>
							<a class="link" href="<?php echo base_url(); ?>user/single_grant/<?php echo $value['id']; ?>">Read More ></a>
						</div>
					<?php }
					$i++;
				}
			} ?>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myGrants" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#myGrants" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>
</div>
