                                                                                                                                                       <?php 
        $grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $data['grantee_name']))->row_array();
        $category = $this->db->get_where('dms_category', array('category_id' => $data['category_id']))->row_array();
        $parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '33'))->row_array();
        $status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $data['categorytype_id']))->row_array();
        
        $individualData = $this->db->get_where('dms_child_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '39'))->result_array();
        
?>
<div class="container-fluid bread">
	<div class="col-md-4">
	<p class="bread_content1">Blogs</p>
	</div>
	<div class="col-md-8">
	<p class="bread_content2">Home/Searched Result/<?php echo $data['grant_number']; ?>/Programs</p>
	</div>
	</div>
  <div class="container About-content">
 <div class="col-md-2">
          <div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
        </div>
	 <div class="col-md-8">
	<div class="image_content">
	<div class="col-md-8 image_content_left">
	<h5><?php echo $grantee['grantee_name']; ?></h5>
	<h6><?php echo $category['category_name'].' | '.$data['start_date'].' - '.$data['start_date'].' | '.$status['categorytype_name']; ?></h6>
	
	<div class="col-md-12 pdf">
             <?php $i = '1';
             foreach($individualData as $valas ){ ?>
                <div class="col-md-4 line-content">
                    <a href="" target="_blank"><?php echo $valas['blogs']; ?></a>
                    <h6><a href="javascript:;" onclick="metadata('<?php echo $data['id']; ?>', '39', '<?php echo $valas['metadata_individual_number']; ?>')">View Details</a></h6>
                </div>
             <?php $i++; } ?>
        </div>

	
            <div class="col-md-12 pagin">

                <ul id="pagin" class="pagination">

                </ul>

            </div>
	</div>
            <div class="col-md-4 image_content_right" id="metadatadiv">
	<h5>Metadata</h5>
	
	</div>
	</div></div>
	<div class="col-md-2">
</div>
	
	</div>
	
	
<script>
pageSize = 4;
//alert($(".line-content").length)
    var pageCount =  $(".line-content").length / pageSize;
    for(var i =0 ; i<pageCount; i++){
        $("#pagin").append('<li onclick="report();"><a href="javascript:;" >'+(i+1)+'</a></li> ');
    }
    $("#pagin li").first().find("a").addClass("current1");
    $("#pagin li").first().addClass("active");
        
    showPage = function(page) {
    //alert(page);
        $(".line-content").hide();
        $(".line-content").each(function(n) {
        if (n >= pageSize *(page - 1) && n < pageSize* page)
            $(this).show();
               
        });        
    }
//    
    showPage(1);
    $("#pagin li a").click(function() {
        $("#pagin li").removeClass("active");
        $(this).parent().addClass("active");
     
//        $("#pagin li a").removeAttr( "style" )
        $("#pagin li a").removeClass("current1");
        $(this).addClass("current1");
       
        showPage(parseInt($(this).text())) 
 });
 
function metadata(gnt, doc, id)
{
     //alert(doc)
     $.ajax({
        url: "<?php echo base_url(); ?>user/metadata",
        type: "POST",
        data: {'grnt': gnt, 'doc': doc, 'nmbr': id},
        success: function (response)
        {
            //alert(response);
            $("#metadatadiv").html(response);
            //$("#metadatadiv").html(response);
            //alert(response);

        }

    });
 }
function goBack()
{
    window.history.back();
}

</script>