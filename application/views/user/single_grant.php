<?php
	error_reporting(0);
	$grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $data['grantee_name']))->row_array();
	$category = $this->db->get_where('dms_category', array('category_id' => $data['category_id']))->row_array();
	$parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $data['id'], 'document_temp_id' => '8'))->row_array();
	$status = $this->db->get_where('dms_categorytype', array('categorytype_id' => $data['categorytype_id']))->row_array();
	
	if($grantee['grantee_geogrtaphical_area']) {
		$g = explode(",",$grantee['grantee_geogrtaphical_area']);
	}
	if($grantee['grantee_disciplinary_field']) {
		$d = explode(",",$grantee['grantee_disciplinary_field']);
	}
	if($grantee['grantee_language']) {
		$l = explode(",",$grantee['grantee_language']);
	}
?>
<style>
	@media screen and (max-width: 992px) {
		.go-back {
			margin-bottom: 25px;
		}
		.image_content_left,
		.image_content_right {
			padding: 0px;
		}
	}
</style>
<div class="container-fluid bread">
</div>
<div class="container About-content">
	<div class="col-md-2">
		<div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
	</div>
	<div class="col-md-10">
		<div class="image_content">
			<div class="col-md-8 image_content_left">
				<h5><?php echo $grantee['grantee_name']; ?></h5>
				<h6><?php  $date2=date_create($data['end_date']); $date1=date_create($data['start_date']);echo $category['category_name'].' | '.date_format($date1,"d-m-Y").' - '.date_format($date2,"d-m-Y").' | '.$status['categorytype_name']; ?></h6>
				<div class="col-md-12 grant_content">
					<p> <?php echo $parent['parent_description']; ?></p>
					<div class="row">
						<div class="col-md-12" style="padding:0;">
							<div class="detailmenu">
								<h4>
								Mid-term Deliverables
								<h4>
								<ul class="link-images">
									<?php 
									if($img){ ?>
										<li><img src="<?php echo base_url();?>front/images/Image-iocn.png" class="link-image"><br/><a style="color:#58453e; href="<?php echo base_url(); ?>user/image/<?php echo $data['id']; ?>"> Image </a></li>
									<?php }
									if($audio){ ?>
										<li><img src="<?php echo base_url();?>front/images/Audio-iocn.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/audio/<?php echo $data['id']; ?>"> Audio </a></li>
									<?php }
									if($video){ ?>
										<li><img src="<?php echo base_url();?>front/images/Video.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/video/<?php echo $data['id']; ?>">Video </a></li>
									<?php }
									if($document){ ?>
										<li><img src="<?php echo base_url();?>front/images/PDF-iocn.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/document/<?php echo $data['id']; ?>"> Document </a></li>
									<?php } ?>
									<?php
									if($Blogs){ ?>
									<?php }
									if($Programs){ ?>
									<?php }
									if($Sourcecode){ ?>
										<li><img src="<?php echo base_url();?>front/images/PDF-iocn.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/sourcecode/<?php echo $data['id']; ?>"> Source Code </a></li>
									<?php }
									if($weblink){?>
										<li><img src="<?php echo base_url();?>front/images/Links.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/weblink/<?php echo $data['id']; ?>"> Links </a></li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="padding:0;">
							<div class="detailmenu">
								<h4>
								Final Deliverables
								<h4>
								<ul>
									<?php   if($img1){ ?>
									<li><img src="<?php echo base_url();?>front/images/Image-iocn.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/images/<?php echo $data['id']; ?>"> Image </a></li>
									<?php }
										if($audio1){ ?>
									<li><img src="<?php echo base_url();?>front/images/Audio-iocn.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/audios/<?php echo $data['id']; ?>"> Audio </a></li>
									<?php }
										if($video1){ ?>
									<li><img src="<?php echo base_url();?>front/images/Video.png" class="link-image"><br/><a style="background-color:#ff9900; color:#fff;" href="<?php echo base_url(); ?>user/videos/<?php echo $data['id']; ?>">Video </a></li>
									<?php }
										if($document1){ ?>
									<li><img src="<?php echo base_url();?>front/images/PDF-iocn.png" class="link-image"><br/><a style="background-color:#ff9900; color:#fff;" href="<?php echo base_url(); ?>user/documents/<?php echo $data['id']; ?>"> Document </a></li>
									<?php }
										if($Blogs1){ ?>
									<?php }
										if($Programs1){ ?>
									<?php }
										if($Sourcecode1){ ?>
									<?php }
										if($weblink1){ ?>
									<li><img src="<?php echo base_url();?>front/images/Links.png" class="link-image">
										<br/><a style="background-color:#ff9900; color:#fff;" href="<?php echo base_url(); ?>user/weblinks/<?php echo $data['id']; ?>"> Links </a>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="padding:0;">
							<div class="detailmenu">
								<h4>
								Media Coverage
								<h4>
								<ul>
									<?php   if($img2){ ?>
									<li>
										<img src=""><img src="<?php echo base_url();?>front/images/Image-iocn.png" class="link-image"><br/>
										<a style="background-color:#ff9900; color:#fff;" href="<?php echo base_url(); ?>user/images_/<?php echo $data['id']; ?>"> Image </a>
									</li>
									<?php }
										if($audio2){ ?>
									<li><img src="<?php echo base_url();?>front/images/Audio-iocn.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/audios_/<?php echo $data['id']; ?>"> Audio </a></li>
									<?php }
										if($video2){ ?>
									<li><img src="<?php echo base_url();?>front/images/Video.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/videos_/<?php echo $data['id']; ?>">Video </a></li>
									<?php }
										if($document2){ ?>
									<li><img src="<?php echo base_url();?>front/images/PDF-iocn.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/documents_/<?php echo $data['id']; ?>"> Document </a></li>
									<?php }
										if($Blogs2){ ?>
									<?php }
										if($Programs2){ ?>
									<?php }
										if($Sourcecode2){ ?>
									<?php }
										if($weblink2){ ?>
									<li><img src="<?php echo base_url();?>front/images/Audio-iocn.png" class="link-image"><br/><a style="color:#58453e;" href="<?php echo base_url(); ?>user/weblinks_/<?php echo $data['id']; ?>"> Links </a></li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 image_content_right">
				<h5>Metadata</h5>
				<p><span class="inner_heading">Grant No</span> : <?php echo $data['grant_number']; ?></p>
				<p><span class="inner_heading">Grantee Name</span> : <?php echo $grantee['grantee_name']; ?></p>
				<p><span class="inner_heading">Programme</span> : <?php echo $category['category_name']; ?></p>
				<p><span class="inner_heading">Grant Status</span> : <?php echo $status['categorytype_name']; ?></p>
				<p><span class="inner_heading">Start Date</span> : <?php $date1=date_create($data['start_date']);echo date_format($date1,"d-m-Y"); ?></p>
				<p><span class="inner_heading">End Date</span> : <?php $date2=date_create($data['end_date']);echo date_format($date2,"d-m-Y"); ?></p>
				<p><span class="inner_heading">Duration</span> : <?php echo $data['grant_duration']; ?></p>
				<p><span class="inner_heading">Grant Amount</span> : <?php echo $data['grant_amount']; ?></p>
				<div class="panel-group" id="accordion">
					<div>
						<div>
							<h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" style="
									//color: #f90;
									font-weight: bold;
									font-size: 16px; background-color:#ff9900; color:#fff;padding:5px;">Read More ></a>
							</h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse ">
							<div class="dd">
								<p><span class="inner_heading">Geographical Area of Work</span> : <?php 
									if($g){
									foreach($g as $val)
									{
										$geo = $this->db->get_where('dms_state', array('state_id' => $val))->row_array();
										$dd11.= $geo['state_name'].', ';
									}
									
									
									
									echo   rtrim($dd11,', '); 
									
									}?></p>
								<p><span class="inner_heading">Disciplinary Field of Work</span> : <?php 
									if($d){
										 foreach($d as $val)
										 {
											 $dis = $this->db->get_where('dms_disciplinary_field', array('field_id' => $val))->row_array();
											 $cc11.=$dis['field_name'].', ';
									}
									echo   rtrim($cc11,', ');
										 }
										 //echo $grantee['grantee_disciplinary_field']; ?></p>
								<p><span class="inner_heading">Language</span> : <?php  if($l){
									foreach($l as $val)
									{
										$dis = $this->db->get_where('dms_language', array('language_id' => $val))->row_array();
										$aa11.= $dis['language_name'].', ';
									}
									 echo   rtrim($aa11,', ');
									} ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	function metaDiv(abc)
	{
		if(abc == 'yes'){
			$('#metaDiv').show();
			$('#readDiv').hide();
			$('#hidedDiv').show();
		}
		else {
			$('#metaDiv').hide();
			$('#hidedDiv').hide();
			$('#readDiv').show();
		}
	}
	function goBack() {
		window.history.back();
	}
</script>