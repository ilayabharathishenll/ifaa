<?php 
	$language = $this->db->get("dms_language")->result_array();
	$state = $this->db->get("dms_state")->result_array();
	$Programme = $this->db->get("dms_category")->result_array();
	$status = $this->db->get("dms_categorytype")->result_array();
	$dis = $this->db->get("dms_disciplinary_field")->result_array();
	$grant1 = $this->db->get("dms_grantee")->result_array(); // disiplinari field of the work....
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
<style>
	* {
		box-sizing: border-box;
	}
	.brownbutton {
		color: #fff;
		background: #806b66;
		padding: 8px 14px;
		height: 37px;
		vertical-align: middle;
		display: inline-block;
		margin-top: 57px;
		margin-left: -16px;
	}.bootstrap-select.btn-group .btn .filter-option
	{
		font-size: 17px;
	}
	#myInput {
		background-image: url('/css/searchicon.png');
		background-position: 10px 12px;
		background-repeat: no-repeat;
		width: 100%;
		font-size: 16px;
		padding: 12px 20px 12px 40px;
		border: 1px solid #ddd;
		margin-bottom: 12px;
	}
	#myUL {
		list-style-type: none;
		padding: 0;
		margin: 0;
	}
	#myUL li a {
		border: 1px solid #ddd;
		margin-top: -1px; /* Prevent double borders */
		background-color: #f6f6f6;
		padding: 12px;
		text-decoration: none;
		font-size: 18px;
		color: black;
		display: block
	}
	#myUL li a:hover:not(.header) {
		background-color: #eee;
	}
	.dropdown-menu {
		width:100%;
	}
	.inner selectpicker{
		width:100%;
	}
	.dropdown-toggle.btn-default:hover{
		background: #fff !important;
	} 
	.popularbut {
		text-align: center;
		font-size: 24px;
		padding: 5px 30px 5px 30px;
		background: #e4e4e4;
		border: 5px solid #f1f1f1;
		display: block;
		text-decoration:none;
		color:#695c56;
	}
	.popularbut:hover{
		text-decoration:none;
		color:#695c56;
	}
	@media screen and (max-width: 992px) {
		.go-back {
			padding: 0 15px;
			margin-bottom: 25px;
		}
		select.form-control {
			margin-bottom: 20px;
		}
	}
</style>
<script>
	function myFunction() {
		var input, filter, ul, li, a, i;
		input = document.getElementById("myInput");
		filter = input.value.toUpperCase();
		ul = document.getElementById("myUL");
		li = ul.getElementsByTagName("li");
		for (i = 0; i < li.length; i++) {
			a = li[i].getElementsByTagName("a")[0];
			if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
				li[i].style.display = "";
			} else {
				li[i].style.display = "none";
			}
		}
	}
	$(document).ready(function(){
		$( "#other" ).click(function() {
			$( "#usersForm" ).submit();
		});
	});
	function goBack() {
		window.history.back();
	}
</script>
<div class="container-fluid bread"></div>
<div class="container About-content">
	<div class="col-md-2">
		<div class="go-back"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>front/images/Arrow-image.png">Back</a></div>
	</div>
	<div class="col-md-10">
		<div class="search">
			<form role="form" action="<?php echo base_url(); ?>user/grant_list/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm">
				<div class="col-md-2">
					<select class="form-control" name="year">
						<option value="">Grant Year</option>
						<?php
							for ($i = 0; $i < 60; $i++) {
								$years = date("Y") - $i; ?>
								<option value="<?php echo $years ?>" <?php
									if ($result['category_year'] == $years) {
										echo "selected";
									}
									?> ><?php echo $years; ?>
								</option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<select class="form-control" name="category">
						<option value="">Programme </option>
						<?php foreach ($Programme as $value) { ?>
						<option value="<?php echo $value['category_id']; ?>"><?php echo $value['category_name'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<select class="form-control" name="status">
						<option value="">Grant Status</option>
						<?php foreach ($status as $value) { ?>
						<option value="<?php echo $value['categorytype_id']; ?>"><?php echo $value['categorytype_name'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<select class="form-control" name="language">
						<option value="">Language</option>
						<?php foreach ($language as $value) { ?>
						<option value="<?php echo $value['language_id']; ?>"><?php echo $value['language_name'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<select class="form-control" name="region">
						<option value="">Region</option>
						<?php foreach ($state as $value) { ?>
						<option value="<?php echo $value['state_id']; ?>"><?php echo $value['state_name'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-2">
					<select class="form-control" name="art">
						<option value="">Discipline</option>
						<?php foreach ($dis as $value) { ?>
						<option value="<?php echo $value['field_id']; ?>"><?php echo $value['field_name'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-6 col-sm-8 col-xs-8 search-grantee-name">
					<div class="text-center" style=" margin-right:10px;display: inline-block">
						<select name="grantee_name" class="form-control" data-live-search="true" >
							<option value="">Grantee Name</option>
							<?php foreach ($grant1 as $value) { ?>
							<option value="<?php echo $value['grantee_id']; ?>"><?php echo $value['grantee_name'] ?></option>
							<?php } ?>
						</select>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 search-grantee-name">
					<span id="other" class="brownbutton"><i class="fa fa-chevron-right" aria-hidden="true"></i></span>
				</div>
			</form>
		</div>
	</div>
</div>
	
