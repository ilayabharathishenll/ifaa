<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <!-- BASIC -->
                <div class="box border primary">
                    <div class="box-title">
                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                        <div class="tools hidden-xs">
                            <a href="javascript:;" class="remove">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body big">
                        <form role="form" action="<?php echo base_url(); ?>adminlogin/add_grant_main/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label class="control-label col-md-4">Select Grant Name Category<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">

                                        <input type="hidden" class="form-control" 
                                               name="hidden_id" value="<?php echo $result['id']; ?>"> 

                                        <select name="category" id="e3" class="form-control chosen" onChange="year();" data-validation="length" 
                                                   data-validation-length="min1" 
                                                   data-validation-error-msg="Programs is required.">
                                            <option>Select Programs</option>
                                            <?php
                                            foreach ($data1 as $row) {
                                                ?>
                                                <option value="<?php echo $row['category_id']; ?>" <?php
                                                if ($result['category'] == $row['category_id']) {
                                                    echo "selected";
                                                }
                                                ?> ><?php echo $row['category_name']; ?></option>
                                                    <?php } ?>
                                        </select>                                                                                          
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label class="control-label col-md-4"> Select Grant Status Category<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">

                                        <select name="cat_status" id="status" class="form-control" onChange="year();" data-validation="length" 
                                                   data-validation-length="min1" 
                                                   data-validation-error-msg="Programs Status is required.">
                                            <option>Select Programs Status</option>
                                            <?php
                                            foreach ($data3 as $row) {
                                                ?>
                                                <option value="<?php echo $row['categorytype_id']; ?>" <?php
                                                if ($result['categorytype_id'] == $row['categorytype_id']) {
                                                    echo "selected";
                                                }
                                                ?> ><?php echo $row['categorytype_name']; ?></option>
                                                    <?php } ?>
                                        </select>

                                    </div>
                                </div>
                            </div>  
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label class="control-label col-md-4">Year<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <select name="years" id="years" class="form-control" data-validation="length" 
                                                   data-validation-length="min1" 
                                                   data-validation-error-msg="Programs Year is required.">
                                            <option value="">Select Programs Year</option>
                                        </select>

                                    </div>

                                </div>
                                <div class="control-label col-md-6"> 
                                    <label class="control-label col-md-4">Grant No<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" 
                                               data-validation="length"
                                               data-validation-length="min1"
                                               placeholder="Enter grant number"
                                               name="grant_no" value="<?php echo $result['grant_number']; ?>">

                                    </div>
                                </div>
                            </div>
                            <?php if($formaction=='create'){ ?>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label class="control-label col-md-4">Grant ID<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" data-validation="length" 
                                               data-validation-length="min1"
                                               data-validation-error-msg="grand ID is Required."
                                               placeholder="Enter grant id"
                                               name="grant_id" value="<?php echo $result['gramt_id']; ?>">

                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label class="control-label col-md-4">Select Grantee Name<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <select name="grantee_name" id="grand_name" class="form-control chosen" data-validation="length" 
                                                   data-validation-length="min1" 
                                                   data-validation-error-msg="Category Status is required.">
                                            <option value="">Select Grantee Nam</option>
                                            <?php
                                            foreach ($data2 as $row) {
                                                ?>
                                                <option value="<?php echo $row['grantee_id']; ?>" <?php
                                                if ($result['grantee_id'] == $row['grantee_id']) {
                                                    echo "selected";
                                                }
                                                ?> ><?php echo $row['grantee_name']; ?></option>
                                                    <?php } ?>
                                        </select>
                                   <div id="error" style="color:red;"></div>
                                    </div> 
                                </div>
                            </div>
                            <?php } ?>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Type of Document<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8"> 
                                        <input type="text" class="form-control" data-validation="length" 
                                               data-validation-length="min1" id="document_type"
                                               data-validation-error-msg="document type is Required."
                                               placeholder="Enter Type of Document"
                                               name="document_type" value="<?php echo $result['document_type']; ?>">

                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label class="control-label col-md-4">Digital File Name<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" data-validation="length" 
                                               data-validation-length="min1" id="digitalfile_name"
                                               data-validation-error-msg="Digital file_name is Required."
                                               placeholder="Enter Digital File Name"
                                               name="digitalfile_name" value="<?php echo $result['digitalfile_name']; ?>">
                                    </div>
                                </div>
                            </div>  
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Document ID<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">  
                                               <input type="text" class="form-control" data-validation="length" 
                                               data-validation-length="min1" id="documrnt_id"
                                               data-validation-error-msg="Document_id is Required."
                                               placeholder="Enter Document ID"
                                               name="doc_id" value="<?php echo $result['document_id']; ?>">
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Handling Staff<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                               <input type="text" class="form-control" data-validation="length" 
                                               data-validation-length="min1" id="handling_staff"
                                               data-validation-error-msg="Handling Staff is Required."
                                               placeholder="Enter Handling Staff"
                                               name="handling_staff" value="<?php echo $result['handling_staf']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Language of The Original Material<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" data-validation="length" 
                                                   data-validation-length="min1" 
                                                   data-validation-error-msg="Final Degree is required."
                                                   placeholder="Enter Language of The Original Material"
                                               name="language" value="<?php echo $result['language_original_material']; ?>">
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4"> Medium of The Original Material<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                   data-validation-length="min1" 
                                                   data-validation-error-msg="Mediumis required."
                                                   placeholder="Enter Medium of The Original Material"
                                               id="medium"  name="medium" value="<?php echo $result['medium_original_material']; ?>"  >


                                    </div>
                                </div>                                              
                            </div>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Extend/No of pages<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" data-validation="length" 
                                                   data-validation-length="min1" 
                                                   data-validation-error-msg="Division is required."
                                                   placeholder="Enter Extend/No of pages"
                                               name="page" value="<?php echo $result['page_number']; ?>">
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Physical Characteristics<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">



                                      <input type="text" class="form-control"  data-validation="length" 
                                                            data-validation-length="min1"
                                                              data-validation-error-msg="Physical Characteristics is required."  
                                                               class="form-control"  name="physical"
                                                               placeholder="Enter Physical Characteristics" 
                                                               value="<?php echo $result['physical_characteristics']; ?>">
                                    </div>
                                                                   <div id="mobile" style="color:red;margin-left: 150px"></div>
                                </div>                                              
                            </div>
                            <div class="row" style="margin-bottom:10px"> 
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Comment<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <textarea rows="2" cols="6" class="form-control" 
                                                  placeholder="Enter Comment"
                                                  name="comments"><?php echo $result['comment']; ?></textarea>
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Place where original material is kept<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                            data-validation-length="min1"
                                                              data-validation-error-msg="Physical Characteristics is required."  
                                                               class="form-control"  name="kept"
                                                               placeholder="Enter Place where original material is kept"
                                                               value="<?php echo $result['material_kept']; ?>"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Medium of Digital Copy<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                            data-validation-length="min1"
                                                              data-validation-error-msg="Medium of Digital Copy is required."  
                                                               class="form-control"  name="copy" 
                                                               placeholder="Enter Medium of Digital Copy"
                                                               value="<?php echo $result['digital_copy_medium']; ?>">     
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Creator/Auther<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                            data-validation-length="min1"
                                                              data-validation-error-msg="Creator/Other is required."  
                                                               class="form-control"  name="auther"
                                                               placeholder="Enter Creator/Auther"
                                                               value="<?php echo $result['auther']; ?>">  
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Creator/Editor<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                              data-validation-error-msg="Editor is required."
                                                              data-validation-length="min1"
                                                               class="form-control"  name="editor"
                                                               placeholder="Enter Creator/Editor"
                                                               value="<?php echo $result['editor']; ?>">     
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Publisher<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                              data-validation-error-msg="Publisher is required."
                                                              data-validation-length="min1"
                                                               class="form-control"  name="publisher" 
                                                               placeholder="Enter Publisher"
                                                               value="<?php echo $result['publisher']; ?>">  
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Place of publication<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                              data-validation-error-msg="Publication Place is required."
                                                              data-validation-length="min1"
                                                               class="form-control"  name="publication_place"
                                                               placeholder="Enter Place of publication"
                                                               value="<?php echo $result['publication_place']; ?>">     
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Copyright<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                              data-validation-error-msg="Copyright is required." 
                                                              data-validation-length="min1"
                                                               class="form-control"  name="copyright"
                                                               placeholder="Enter Copyright"
                                                               value="<?php echo $result['copyright']; ?>">  
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Dimensions( in CM)<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                              data-validation-error-msg="Publication Place is required." 
                                                              data-validation-length="min1"
                                                               class="form-control"  name="dimension"
                                                               placeholder="Enter Dimensions"
                                                               value="<?php echo $result['dimensions']; ?>">     
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Creator of digital copy<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                              data-validation-error-msg="Copyright is required."
                                                              data-validation-length="min1"
                                                               class="form-control"  name="creator_digital_copy"
                                                               placeholder="Enter Creator of digital copy"
                                                               value="<?php echo $result['creator_degital_copy']; ?>">  
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Hardware Used<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                              data-validation-error-msg="Publication Place is required."
                                                              data-validation-length="min1"
                                                               class="form-control"  name="hardware"
                                                               placeholder="Enter Hardware Used"
                                                               value="<?php echo $result['hardware']; ?>">     
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Software Used<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control"  data-validation="length" 
                                                              data-validation-error-msg="Copyright is required." 
                                                              data-validation-length="min1"
                                                               class="form-control"  name="software" 
                                                               placeholder="Enter Software Used<"
                                                               value="<?php echo $result['software']; ?>">  
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Description</label>
                                    <div class="col-md-8">
                                        <textarea rows="2" cols="6" class="form-control" placeholder="Enter Description"
                                                  name="description"><?php echo $result['description']; ?></textarea>    
                                    </div>
                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Date<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="datepicker1"  
                                                               class="form-control"  name="date" 
                                                               data-validation="length" 
                                                            data-validation-length=min1" 
                                                            data-validation-error-msg="date is required"
                                                            placeholder="Enter date"
                                                               value="<?php echo $result['date']; ?>">  
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-bottom:10px">
                                <div class="control-label col-md-6">

                                        <label  class="control-label col-md-4">Keywords<span style="color: red;margin-left: 1px">*</span></label>
                                        <div class="col-md-5">
                                        <input type="text" class="form-control mng" data-validation="length" 
                                           data-validation-length=min1"
                                           data-validation-error-msg="Year is required"
                                           name="keyword[]"  value="<?php echo $result['keyword']; ?>"
                                           placeholder="Enter Keyword" >


                                        </div>
                                        <div class="col-md-3">    <a  class="button btn btn-primary start add" style="display:inline-block;" onclick="addMoreRows(this.form,<?php echo $bookallow; ?>);" />Add More</a> </div>

                                </div>
                                <div class="control-label col-md-6"> 
                                    <label  class="control-label col-md-4">Date Of Digital copy<span style="color: red;margin-left: 1px">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="datepicker2" 
                                                data-validation="length" 
                                                data-validation-length=min1"
                                                data-validation-error-msg="Date Of Digital copy is required"
                                                     class="form-control"  name="degital_copy_date" 
                                                     placeholder="Enter Date Of Digital copy"
                                                     value="<?php echo $result['degital_copy_date']; ?>">  
                                    </div>
                                </div>

                            </div>


                            <div id="addedRows" >  

                            </div>



<!--                            <div style="text-align: right">

                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                            </div>-->







                          </form>                     

                        </div>
                    </div>
                </div>
                <!-- /BASIC -->
                <!-- BASIC -->

                    <!-- /BASIC -->
                </div>
            </div>
        </div>
                  







