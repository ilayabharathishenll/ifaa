<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Calendar Display</title>
         <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/logo.png"/>

      
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/cloud-admin.css" >
        <link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/css/themes/default.css" id="skin-switcher" >
        <link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/css/responsive.css" >
        
          
       

        <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- JQUERY UI-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
        <!-- DATE RANGE PICKER -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
        <!-- DATA TABLES -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/datatables/media/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/datatables/media/assets/css/datatables.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/datatables/extras/TableTools/media/css/TableTools.min.css" />
        <!-- FONTS -->
        <!-- TYPEAHEAD -->
        <!-- FILE UPLOAD -->
        <!-- SELECT2 -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/select2/select2.min.css" />
        <!-- UNIFORM -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/uniform/css/uniform.default.min.css" />
        <!-- JQUERY UPLOAD -->
        <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
        <!-- FONTS -->
        <!-- FONTS -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
        <!-- DATE RANGE PICKER -->
        <!--        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
                          <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
                  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->
        <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/theme-default.min.css"
              rel="stylesheet" type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>	
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
<!--        <script src="http://trentrichardson.com/examples/timepicker/jquery-ui-timepicker-addon.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/i18n/jquery-ui-timepicker-addon-i18n.min.js"></script>  -->

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
       
        <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
     
   <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css" />
               <script src="<?php echo base_url(); ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
               <script src="<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js"></script>
               <script src="<?php echo base_url(); ?>assets/js/fullcalendar/gcal.js"></script>
          <script src="<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.js"></script>
          
    </head>
    <body>
  <header class="navbar clearfix" id="header">
            <div class="container">
                <div class="navbar-brand">
                    <!-- COMPANY LOGO -->
                    <a href="<?php echo base_url('Adminlogin') ?>/dashboard"> <span style="margin-left:50px; color:#FFFFFF; font-size:24px;">EAU</span> </a>
                    <!-- /COMPANY LOGO -->
                    <!-- TEAM STATUS FOR MOBILE -->
<!--                    <div class="visible-xs">
                        <a href="javascript:;" class="team-status-toggle switcher btn dropdown-toggle">
                            <i class="fa fa-users"></i>
                        </a>
                    </div>-->
                    <!-- /TEAM STATUS FOR MOBILE -->
                    <!-- SIDEBAR COLLAPSE -->
                    <div id="sidebar-collapse" class="sidebar-collapse btn">
                        <i class="fa fa-bars" 
                           data-icon1="fa fa-bars" 
                           data-icon2="fa fa-bars" ></i>
                    </div>
                    <!-- /SIDEBAR COLLAPSE -->
                </div>
                <!-- NAVBAR LEFT -->

                <!-- /NAVBAR LEFT -->
                <!-- BEGIN TOP NAVIGATION MENU -->					
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->	
                    <li class="dropdown" id="header-notification"  style="visibility: hidden">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i>
                            <span class="badge">7</span>						
                        </a>
                        <ul class="dropdown-menu notification">
                            <li class="dropdown-title">
                                <span><i class="fa fa-bell"></i> 7 Notifications</span>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="label label-success"><i class="fa fa-user"></i></span>
                                    <span class="body">
                                        <span class="message">5 users online. </span>
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>Just now</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="label label-primary"><i class="fa fa-comment"></i></span>
                                    <span class="body">
                                        <span class="message">Martin commented.</span>
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>19 mins</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="label label-warning"><i class="fa fa-lock"></i></span>
                                    <span class="body">
                                        <span class="message">DW1 server locked.</span>
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>32 mins</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="label label-info"><i class="fa fa-twitter"></i></span>
                                    <span class="body">
                                        <span class="message">Twitter connected.</span>
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>55 mins</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="label label-danger"><i class="fa fa-heart"></i></span>
                                    <span class="body">
                                        <span class="message">Jane liked. </span>
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>2 hrs</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="label label-warning"><i class="fa fa-exclamation-triangle"></i></span>
                                    <span class="body">
                                        <span class="message">Database overload.</span>
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>6 hrs</span>
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li class="footer">
                                <a href="javascript:;">See all notifications <i class="fa fa-arrow-circle-right"></i></a>
                            </li>
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN INBOX DROPDOWN -->
                    <li class="dropdown" id="header-message"  style="visibility: hidden">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope"></i>
                            <span class="badge">3</span>
                        </a>
                        <ul class="dropdown-menu inbox"  style="visibility: hidden">
                            <li class="dropdown-title">
                                <span><i class="fa fa-envelope-o"></i> 3 Messages</span>
                                <span class="compose pull-right tip-right" title="Compose message"><i class="fa fa-pencil-square-o"></i></span>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar2.jpg" alt="" />
                                    <span class="body">
                                        <span class="from">Jane Doe</span>
                                        <span class="message">
                                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse mole ...
                                        </span> 
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>Just Now</span>
                                        </span>
                                    </span>

                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar1.jpg" alt="" />
                                    <span class="body">
                                        <span class="from">Vince Pelt</span>
                                        <span class="message">
                                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse mole ...
                                        </span> 
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>15 min ago</span>
                                        </span>
                                    </span>

                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar8.jpg" alt="" />
                                    <span class="body">
                                        <span class="from">Debby Doe</span>
                                        <span class="message">
                                            Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse mole ...
                                        </span> 
                                        <span class="time">
                                            <i class="fa fa-clock-o"></i>
                                            <span>2 hours ago</span>
                                        </span>
                                    </span>

                                </a>
                            </li>
                            <li class="footer">
                                <a href="javascript:;">See all messages <i class="fa fa-arrow-circle-right"></i></a>
                            </li>
                        </ul>
                    </li>
                    <!-- END INBOX DROPDOWN -->
                    <!-- BEGIN TODO DROPDOWN -->
                    <li class="dropdown" id="header-tasks"  style="visibility: hidden">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-tasks"></i>
                            <span class="badge">3</span>
                        </a>
                        <ul class="dropdown-menu tasks">
                            <li class="dropdown-title">
                                <span><i class="fa fa-check"></i> 6 tasks in progress</span>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="header clearfix">
                                        <span class="pull-left">Software Update</span>
                                        <span class="pull-right">60%</span>
                                    </span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                                            <span class="sr-only">60% Complete</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="header clearfix">
                                        <span class="pull-left">Software Update</span>
                                        <span class="pull-right">25%</span>
                                    </span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                            <span class="sr-only">25% Complete</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="header clearfix">
                                        <span class="pull-left">Software Update</span>
                                        <span class="pull-right">40%</span>
                                    </span>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
                                            <span class="sr-only">40% Complete</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="header clearfix">
                                        <span class="pull-left">Software Update</span>
                                        <span class="pull-right">70%</span>
                                    </span>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%;">
                                            <span class="sr-only">70% Complete</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="header clearfix">
                                        <span class="pull-left">Software Update</span>
                                        <span class="pull-right">65%</span>
                                    </span>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" style="width: 35%">
                                            <span class="sr-only">35% Complete (success)</span>
                                        </div>
                                        <div class="progress-bar progress-bar-warning" style="width: 20%">
                                            <span class="sr-only">20% Complete (warning)</span>
                                        </div>
                                        <div class="progress-bar progress-bar-danger" style="width: 10%">
                                            <span class="sr-only">10% Complete (danger)</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="footer">
                                <a href="javascript:;">See all tasks <i class="fa fa-arrow-circle-right"></i></a>
                            </li>
                        </ul>
                    </li>
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <?php
                    $name1 = $this->session->userdata['login']['admin_id'];
                    $result11 = $this->db->get_where('tbladmin', array('admin_id' => $name1))->row_array();
                    ?>

                    <li class="dropdown user" id="header-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                           <?php if( $result11['admin_image']){?>
                            <img src="<?php echo base_url() . "uploads/" . $result11['admin_image']; ?>" alt="" />
                           <?php }else{?>
                            <i class="fa fa-user"></i>
 <?php } ?>
                            <span class="username">Hello <?php echo $result11['admin_username'] ?></span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('Adminlogin'); ?>/show_profile"><i class="fa fa-user"></i> My Profile</a></li>
                            <li><a href="<?php echo base_url('Adminlogin'); ?>/show_changepassword"><i class="fa fa-cog"></i> Change Password</a></li>

                        </ul>
                    </li>
                    <li class="dropdown user" id="header-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <h6 style="color:white;margin-top:3px;margin-bottom: 6px">
                                <i class="fa fa-cog"></i>
                                <b>General Setting</b></span>
                                <i class="fa fa-angle-down"></i></h6>
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url('Adminlogin'); ?>/show_generalsetting">Site Setting</a></li>
<!--                            <li><a href="<?php echo base_url('Adminlogin'); ?>/show_allowbook">Allow Book</a></li>
                        -->
                        </ul>

                    </li>


                    <li><a href="<?php echo base_url(); ?>Admin/logout"> <h6 style="color:white;margin-top:0px;margin-bottom: 6px"><i class="fa fa-power-off"></i><b>&nbsp;Log Out</b></h6></a></li>



                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- TEAM STATUS -->
            <div class="container team-status" id="team-status">
                <div id="scrollbar">
                    <div class="handle">
                    </div>
                </div>
                <div id="teamslider">
                    <ul class="team-list">
                        <li class="current">
                            <a href="javascript:void(0);">
                                <span class="image">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar3.jpg" alt="" />
                                </span>
                                <span class="title">
                                    You
                                </span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 35%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 20%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 10%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                                <span class="status">
                                    <div class="field">
                                        <span class="badge badge-green">6</span> completed
                                        <span class="pull-right fa fa-check"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-orange">3</span> in-progress
                                        <span class="pull-right fa fa-adjust"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-red">1</span> pending
                                        <span class="pull-right fa fa-list-ul"></span>
                                    </div>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="image">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar1.jpg" alt="" />
                                </span>
                                <span class="title">
                                    Max Doe
                                </span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 15%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 40%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 20%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                                <span class="status">
                                    <div class="field">
                                        <span class="badge badge-green">2</span> completed
                                        <span class="pull-right fa fa-check"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-orange">8</span> in-progress
                                        <span class="pull-right fa fa-adjust"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-red">4</span> pending
                                        <span class="pull-right fa fa-list-ul"></span>
                                    </div>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="image">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar2.jpg" alt="" />
                                </span>
                                <span class="title">
                                    Jane Doe
                                </span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 65%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 10%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 15%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                                <span class="status">
                                    <div class="field">
                                        <span class="badge badge-green">10</span> completed
                                        <span class="pull-right fa fa-check"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-orange">3</span> in-progress
                                        <span class="pull-right fa fa-adjust"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-red">4</span> pending
                                        <span class="pull-right fa fa-list-ul"></span>
                                    </div>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="image">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar4.jpg" alt="" />
                                </span>
                                <span class="title">
                                    Ellie Doe
                                </span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 5%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 48%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 27%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                                <span class="status">
                                    <div class="field">
                                        <span class="badge badge-green">1</span> completed
                                        <span class="pull-right fa fa-check"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-orange">6</span> in-progress
                                        <span class="pull-right fa fa-adjust"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-red">2</span> pending
                                        <span class="pull-right fa fa-list-ul"></span>
                                    </div>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="image">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar5.jpg" alt="" />
                                </span>
                                <span class="title">
                                    Lisa Doe
                                </span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 21%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 20%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 40%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                                <span class="status">
                                    <div class="field">
                                        <span class="badge badge-green">4</span> completed
                                        <span class="pull-right fa fa-check"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-orange">5</span> in-progress
                                        <span class="pull-right fa fa-adjust"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-red">9</span> pending
                                        <span class="pull-right fa fa-list-ul"></span>
                                    </div>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="image">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar6.jpg" alt="" />
                                </span>
                                <span class="title">
                                    Kelly Doe
                                </span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 45%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 21%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 10%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                                <span class="status">
                                    <div class="field">
                                        <span class="badge badge-green">6</span> completed
                                        <span class="pull-right fa fa-check"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-orange">3</span> in-progress
                                        <span class="pull-right fa fa-adjust"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-red">1</span> pending
                                        <span class="pull-right fa fa-list-ul"></span>
                                    </div>
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="image">
                                    <img src="<?php echo base_url(); ?>assets/img/avatars/avatar7.jpg" alt="" />
                                </span>
                                <span class="title">
                                    Jessy Doe
                                </span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 7%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 30%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 10%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                                <span class="status">
                                    <div class="field">
                                        <span class="badge badge-green">1</span> completed
                                        <span class="pull-right fa fa-check"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-orange">6</span> in-progress
                                        <span class="pull-right fa fa-adjust"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-red">2</span> pending
                                        <span class="pull-right fa fa-list-ul"></span>
                                    </div>
                                </span>
                            </a>
                        </li>
                        <li>


                            <a href="javascript:void(0);">
                                <span class="image">
                                </span>
                                <span class="title">
                                    Debby Doe
                                </span>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 70%">
                                        <span class="sr-only">35% Complete (success)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-warning" style="width: 20%">
                                        <span class="sr-only">20% Complete (warning)</span>
                                    </div>
                                    <div class="progress-bar progress-bar-danger" style="width: 5%">
                                        <span class="sr-only">10% Complete (danger)</span>
                                    </div>
                                </div>
                                <span class="status">
                                    <div class="field">
                                        <span class="badge badge-green">13</span> completed
                                        <span class="pull-right fa fa-check"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-orange">7</span> in-progress
                                        <span class="pull-right fa fa-adjust"></span>
                                    </div>
                                    <div class="field">
                                        <span class="badge badge-red">1</span> pending
                                        <span class="pull-right fa fa-list-ul"></span>
                                    </div>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /TEAM STATUS -->
        </header>
        <section id="page">
				<!-- SIDEBAR -->
				<div id="sidebar" class="sidebar">
					 <div class="sidebar-menu nav-collapse">
                    <ul>
                        <li class="active">
                            <a href="<?php echo base_url('Adminlogin') ?>/dashboard">
                                <i class="fa fa-tachometer fa-fw"></i> <span class="menu-text">Dashboard</span>
                                <span class="selected"></span>
                            </a>					
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-tasks"></i> <span class="menu-text">Basic Record</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub">
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_adddesignation"><span class="sub-menu-text">Designation</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_adddepartment"><span class="sub-menu-text">Department</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addrole"><span class="sub-menu-text">User Type</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addpermission"><span class="sub-menu-text">Permission</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addclassmaster"><span class="sub-menu-text">Class</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addearning"><span class="sub-menu-text">Earning</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_adddeduction"><span class="sub-menu-text">Deduction</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addtax"><span class="sub-menu-text">Tax</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addleave"><span class="sub-menu-text"> Monthly Leave</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addsection"><span class="sub-menu-text">Section</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addsubject"><span class="sub-menu-text">Subject</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addclass"><span class="sub-menu-text">Class Details</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addclassperiod"><span class="sub-menu-text">Class Period</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addclassTimetable"><span class="sub-menu-text">Class Time-Table</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addcountry"><span class="sub-menu-text">Country</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addstate"><span class="sub-menu-text">State</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addcity"><span class="sub-menu-text">City</span></a></li>
                                <!--                         
                                                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addlibrarymaster"><span class="sub-menu-text">Library</span></a></li>
                                -->

                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addholiday"><span class="sub-menu-text">Event</span></a></li>

                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-users"></i> <span class="menu-text">Employee Management</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub">
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addteacher"><span class="menu-text">Add Employee</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_allteacher"><span class="sub-menu-text">All Employee</span></a></li>
                                 <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_EmployeeAttendance"><span class="sub-menu-text">Employee Attendance</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/view_EmployeeAttendance"><span class="sub-menu-text">View Employee Attendance</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_dailytime"><span class="sub-menu-text">Teacher Daily Timetable</span></a></li>
                                


                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-user"></i> <span class="menu-text">Student Management</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub">
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addstudent"> <span class="menu-text">Add Student</a></li> 
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_allstudent"><span class="menu-text">All Student</a></li>  
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_studentAttendance"><span class="sub-menu-text">Student Attendance</span></a></li>
                               <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/View_StudentAttendance"><span class="sub-menu-text">View Student Attendance</span></a></li>
                                
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-book"></i> <span class="menu-text">Library Management</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub">
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addbookcategory"><span class="menu-text">Book Category</a></li> 
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addbook"><span class="sub-menu-text">Book Stock</span></a></li>
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_libraryfee"><span class="menu-text">Security Deposit</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/bookissue"><span class="menu-text">Book Issue</a></li>  
                           <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_bookreturn"><span class="menu-text">Book Return</a></li>                             
                           
<!--                           <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_booklist"><span class="menu-text">Book List</a></li>                             
                           -->
                           <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_allstudentbook"><span class="menu-text">All Student Books</a></li>                             
                            
                            </ul>
                        </li>
                         <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-sitemap"></i> <span class="menu-text">Hostel Management</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub">
                                <li><a class="" href="<?php echo base_url(); ?>Hostel/addblock"><span class="menu-text">Add Block</a></li>                            
                                <li><a class="" href="<?php echo base_url(); ?>Hostel/floorMaster"><span class="menu-text">Floor Master</a></li>
                                <li><a class="" href="<?php echo base_url(); ?>Hostel/addfloor"><span class="menu-text">Assign Floor</a></li>
                                <li><a class="" href="<?php echo base_url(); ?>Hostel/addroom"><span class="menu-text">Add Room</a></li>
                                <li><a class="" href="<?php echo base_url(); ?>Hostel/allotroom"><span class="menu-text">Allot Room</a></li>
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-pencil-square-o fa-fw"></i> <span class="menu-text">Exam Management</span>
                                <span class="arrow"></span>
                            </a>
                             <ul class="sub">
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_examperiod"><span class="menu-text">Exam Period</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_examtype"><span class="menu-text">Exam Type</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_examtimetable"><span class="menu-text">Exam Timetable</a></li>
                                 <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_exam"><span class="menu-text">Show Exam Timetable</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_grade"><span class="menu-text">Grade</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/publish_examtimetable"><span class="menu-text">Publish Exam Timetable</a></li>                            

                            </ul>
                        </li>
                            <li class="has-sub">
                            <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-clock-o"></i> <span class="menu-text">Result Management</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub">
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_result"><span class="menu-text">Result</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_allresult"><span class="menu-text">Show Result</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/publish_result"><span class="menu-text">Publish Result</a></li>                            

                            </ul>
                        </li>
                         <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-th-large"></i> <span class="menu-text">Finance</span>
                                <span class="arrow"></span>
                            </a>
                             <ul class="sub">
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_donation"><span class="menu-text">Donation</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_duration"><span class="menu-text">Fee Duration</a></li>   
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_studentfee"><span class="menu-text">Student Fee</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_feedisplay"><span class="menu-text">Student Fee Details</a></li>                            
                        
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_hostelfeedisplay"><span class="menu-text">Hostel Fee Details</a></li>     
 <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_labfeedisplay"><span class="menu-text">Library Fee Details</a></li>                            
  
                            </ul>
                        </li>
                        <li class="has-sub">
                            <a href="javascript:;" class="">
                                <i class="fa fa-list-alt"></i> <span class="menu-text">Payroll</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub">
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_addpayroll"><span class="menu-text">Add Payroll</a></li>                            
                                <li><a class="" href="<?php echo base_url('Adminlogin'); ?>/show_viewpayroll"><span class="menu-text">Show Payroll</a></li>                             
                      
                            </ul>
                        </li>
                        <li class="active">
                            <a href="<?php echo base_url('Adminlogin') ?>/show_calendar">
                                <i class="fa fa-calendar"></i> <span class="menu-text">Calender</span>
                                <span class="selected"></span>
                            </a>					
                        </li>
                    </ul>
                    <!-- /SIDEBAR MENU -->
                </div>
				</div>
				<!-- /SIDEBAR -->
		<div id="main-content">
			
			<div class="container">
				<div class="row">
					<div id="content" class="col-lg-12">
						<!-- PAGE HEADER-->
						  <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('Adminlogin'); ?>/dashboard">Home</a>
                                </li>
                              <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo "Holiday Calender"; ?></h3>                          
                            </div>
                        </div>
                    </div>
                </div>
						<!-- /PAGE HEADER -->
						<!-- CALENDAR -->
						<div class="row">
							<div class="col-md-9">
								<!-- BOX -->
								<div class="box border">
									<div class="box-title">
										<h4><i class="fa fa-calendar"></i>Calendar</h4>
										<div class="tools">
											
											<a href="javascript:;" class="remove">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
									<div class="box-body">
										<div class="row">
    <div class="col-md-12">
<div id="calendar">
</div>


    </div>
    </div>
									</div>
								</div>
								<!-- /BOX -->
							</div>
						</div>
						<!-- /CALENDAR -->
						
					</div><!-- /CONTENT-->
				</div>
			</div>
		</div>
	</section>
        
  
<script type="text/javascript">
    

$(document).ready(function() {
    
    
    var abc111 = '';
     function getGrid()
                {
                    //  alert(id);
                    $.ajax({
                        //alert(id);
                        url: "<?php echo base_url(); ?>Adminlogin/getevent",
                        //url : "http://livesoftwaresolution.net/lms/Adminlogin/classselect1",
                        type: "POST",
                        data: '',
                        success: function (response)
                        {
                            //console.log(response);

                         abc111 = jQuery.parseJSON(response);
                         
                            //alert(abc111.length);
                        }
                    });
                }

                getGrid();
                
                
                 
                 setTimeout(function(){   $('#calendar').fullCalendar({
                     eventSources: [
                 {
                color: 'rgba(0, 128, 0, 0.24)',
                fontsize:'16pt',
                textColor: 'black',
                events:  abc111
              }
            
        ]
    }) }, 500);
                
    
            var dooly1 = [];
                var dolly =  {
                        title: 'Event 1',
                        start: '2017-06-13'
                      };
                  dooly1.push(dolly);
                  
                   var dolly_ =  {
                        title: 'Event 1',
                        
                        start: '2017-07-13'
                      };
                  dooly1.push(dolly_);

                //console.log(dooly1); return false;
    
    
    
    // create function for getting calender
    
    
    

    

    
    
    
    
});


//                $('#calendar').fullCalendar({
//     eventSources: [
//         {
//             events: function(start, end, timezone, callback) {
//                 $.ajax({
//                 url: '<?php echo base_url() ?>calendar/get_events',
//                 dataType: 'json',
//                 data: {
//                 // our hypothetical feed requires UNIX timestamps
//                 start: start.unix(),
//                 end: end.unix()
//                 },
//                 success: function(msg) {
//                     var events = msg.events;
//                     callback(events);
//                 }
//                 });
//             }
//         },
//     ]
// });
</script>
<script src="<?php echo base_url(); ?>assets/js/script.js"></script>
     
<!--	<script src="<?php echo base_url(); ?>assets/js/jquery/jquery-2.0.3.min.js"></script>-->
	<!-- JQUERY UI-->
	<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="<?php echo base_url(); ?>assets/bootstrap-dist/js/bootstrap.min.js"></script>
	<!-- DATE RANGE PICKER -->
	<script src="<?php echo base_url(); ?>assets/js/bootstrap-daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
      <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/select2/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/countable/jquery.simplyCountable.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/typeahead/typeahead.min.js"></script>
	<!-- AUTOSIZE -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/autosize/jquery.autosize.min.js"></script>
	<!-- COUNTABLE -->
	
        
        <script src="<?php echo base_url(); ?>assets/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
	<!-- DATA TABLES -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/media/assets/js/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	
	<script>
		jQuery(document).ready(function() {		
			App.setPage("dynamic_table");  //Set current page
			App.init(); //Initialise plugins and elements
		});

                            function delete_confirm(){            
var retVal = confirm("Do you want to continue ?");           
if( retVal == true ){                    
}         
else{                                 
return false;          
}           
}
	</script>
    </body>
</html>