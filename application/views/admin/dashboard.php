<?php
error_reporting(~E_NOTICE);?>
<div id="main-content">
	
    <!-- /SAMPLE BOX CONFIGURATION MODAL FORM-->
        <div class="container">
                <div class="row">
                        <div id="content" class="col-lg-12">
                                <!-- PAGE HEADER-->
                                <div class="row">
                                        <div class="col-sm-12">
                                                <div class="page-header">
                                                        <!-- STYLER -->

                                                        <!-- /STYLER -->
                                                        <!-- BREADCRUMBS -->
                                                        <ul class="breadcrumb">
                                                                <li>
                                                                        <i class="fa fa-home"></i>
                                                                        <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                                                </li>
<!--                                                                <li><?php echo $pagetitle; ?></li>-->
                                                                <li>
                                                                        <a href="javascript:;"><?php echo $pagetitle; ?></a>
                                                                </li>
<!--                                                                <li>Dynamic Tables</li>-->
                                                        </ul>
                                                        <!-- /BREADCRUMBS -->
                                                        <div class="clearfix">
                                                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>
                                                        </div>
                                                        <div class="description"></div>
                                                        <?php if ($this->session->flashdata('flash_message')) { ?>
                                                        <div class="alert alert-block alert-success fade in">
                                                            <a class="close" data-dismiss="alert" href="javascript:;" 
                                                               aria-hidden="true">X</a>
                                                            <h4><i class="fa fa-smile-o"></i> <?php
                                                                echo
                                                                $this->session->flashdata('flash_message');
                                                                ?>  <i class="fa fa-thumbs-up"></i></h4>
                                                        </div>
                                                        <? }
                                                        if($this->session->flashdata('permission_message')){ ?>
                                                        <div class="alert alert-block alert-danger fade in">
                                                            <a class="close" data-dismiss="alert" href="javascript:;" 
                                                               aria-hidden="true">X</a>
                                                            <h4><i class="fa fa-frown-o"></i> <?php
                                                                echo
                                                                $this->session->flashdata('permission_message');
                                                                ?></h4>
                                                        </div>
                                                        <?php }?>
                                                </div>
                                        </div>
                                </div>
                               
                        </div><!-- /CONTENT-->
                </div>
        </div>
</div>

  




