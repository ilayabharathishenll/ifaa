<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $meta = $this->db->get_where('dms_grant_metadeta', array('grant_temp_id' => $param1))->result_array();
    $result1 = $this->db->get_where("dms_grantee", array('grantee_id' => $result['grantee_name']))->row_array();
    $result2 = $this->db->get_where("dms_category", array('category_id' => $result['category_id']))->row_array();
    $result3 = $this->db->get_where("dms_categorytype", array('categorytype_id' => $result['categorytype_id']))->row_array();
    $result4 = $this->db->get_where("dms_user", array('user_id' => $result['program_executive']))->row_array();
    
    
//    
//    $earning = explode(",",$result['teacher_earning']);
//    $earning_amount = explode(",",$result['teacher_earningAmount']);
//    $deduction = explode(",",$result['teacher_deduction']);
//    $deduction_amount = explode(",",$result['teacher_deductionAmount']);
//    $tax = explode(",",$result['teacher_tax']);
//    $tax_amount = explode(",",$result['teacher_taxAmount']);
    
    $key = explode(",",$result['keyword']);
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
$peUser = $this->db->get_where("dms_user", array('user_role' => '5'))->result_array();

$grantee_language = explode(",",$result['grantee_language']);
$grantee_geogrtaphical_area = explode(",",$result['grantee_geogrtaphical_area']);
$grantee_disciplinary_field = explode(",",$result['grantee_disciplinary_field']);
$grantee_state = explode(",",$result['grantee_state']);
$grantee_possible_outcome = explode(",",$result['grantee_possible_outcome']);
$grantee_deliverables = explode(",",$result['deliverables']);



?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                      
                                            <div class="row" style="margin-bottom:10px">    
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grant No<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result['grant_number']; ?>
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Archive Tag<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result['archiv_tag']; ?>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grantee Name<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result1['grantee_name']; ?>
                                                    </div> 
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Programme/Initiative<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                         <?php echo $result2['category_name']; ?>                                                                                          
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grant Status<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       <?php echo $result3['categorytype_name']; ?>
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Start Date<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       <?php echo $result['start_date']; ?>
                                                    </div>

                                                </div>
                                            </div>  
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> End Date<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result['end_date']; ?>
                                                    </div>

                                                </div>
                                                
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Extension Till<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result['extension']; ?>
                                                    </div>

                                                </div>
                                            </div>
                                            </br>
                                            
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Grant Duration<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result['grant_duration']; ?> 
                                                    </div>

                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Grant Year<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result['category_year']; ?>
                                                    </div>

                                                </div>
                                               
                                                
                                                
                                            </div>
                                            
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Programme Executive <span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result4['user_name']; ?>
                                                    </div>

                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Grant Amount<span style="color: red;margin-left: 1px">*</span></label>
                                                    <?php echo $result['grant_amount']; ?>
                                                </div>
                                               
                                                
                                                
                                            </div>
                                            </br>
                                            
                                                
                                            
                                            <div id="grntee_detail">
                                                <div class="box-title">
                                                    <h4>Grantee Detail</h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Address 1<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       <?php echo $result1['grantee_address1']; ?>
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4"> Address 2<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8"> 
                                                        <?php echo $result1['grantee_address2']; ?>

                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row" style="margin-bottom:10px">
                                               <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">City<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result1['grantee_city']; ?>
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">State<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       
                                                      <?php 
                                                     
                                                    $grantee_state = explode(",",$result1['grantee_state']);
                                                    foreach($grantee_state as $value)
                                                    {
                                                        $state = $this->db->get_where("dms_state", array('state_id' => $value))->row_array();
                                                        echo $state['state_name'].', ';
                                                    }
                                                    
                                                    
                                                    
                                                    $displi = $this->db->get("dms_disciplinary_field")->result_array();
                                                    $language = $this->db->get("dms_language")->result_array();
                                                    $deliverables = $this->db->get("dms_deliverables")->result_array();
                                                    $outcome = $this->db->get("dms_outcome")->result_array();
                                                    $grantee_language = explode(",",$result1['grantee_language']);
                                                    $grantee_geogrtaphical_area = explode(",",$result1['grantee_geogrtaphical_area']);
                                                    $grantee_disciplinary_field = explode(",",$result1['grantee_disciplinary_field']);
                                                    
                                                    $grantee_possible_outcome = explode(",",$result1['grantee_possible_outcome']);
                                                    $grantee_deliverables = explode(",",$result1['deliverables']); 
                                                      
                                                       ?> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Pin<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <?php echo $result1['grantee_pin'];
                                                        
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Mobile No<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       <?php echo $result1['grantee_phone']; ?>
                                                    </div>
                                                </div>
                                            </div>
                                             
                                             <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Email<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <?php echo $result1['grantee_email']; ?>
                                                        </div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4"> Website  <span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <?php echo $result1['grantee_website']; ?>
                                                        </div>
                                                    </div>
                                                    </br>
                                                </div>
                                        
                                                <br>
                                                     
                            
                                                <div class="box-title">
                                                    <h4>Grant Meta Data </h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                      
                                            </br>
                                            
                                                    
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Geographical area of work <span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <?php //echo $result1['grantee_geogrtaphical_area'];
                                                            
                                                            $grantee_geogrtaphical_area = explode(",",$result1['grantee_geogrtaphical_area']);
                                                            if($grantee_geogrtaphical_area){
                                                                foreach($grantee_geogrtaphical_area as $value)
                                                                {
                                                                    $state = $this->db->get_where("dms_state", array('state_id' => $value))->row_array();
                                                                    echo $state['state_name'].', ';
                                                                }
                                                            }
                                                            
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Disciplinary field of work<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <?php 
                                                            
                                                            $grantee_disciplinary_field = explode(",", $result1['grantee_disciplinary_field']);
                                                            if($grantee_disciplinary_field){
                                                                foreach($grantee_disciplinary_field as $value)
                                                                {
                                                                    $dd = $this->db->get_where("dms_disciplinary_field", array('field_id' => $value))->row_array();
                                                                    echo $dd['field_name'].', ';
                                                                }
                                                            }
                                                    
                                                            ?>
                                                        </div>
                                                    </div>                                              
                                                </div>
                                                
                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Language<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <?php
                                                            
                                                           $grantee_language = explode(",",$result1['grantee_language']);
                                                            if($grantee_language){
                                                                foreach($grantee_language as $value)
                                                                {
                                                                    $dd = $this->db->get_where("dms_language", array('language_id' => $value))->row_array();
                                                                    echo $dd['language_name'].', ';
                                                                }
                                                            }
                                                    
                                                            ?>
                                                        </div>
                                                <div id="mobile" style="color:red;margin-left: 150px"></div>
                                                    </div>
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Possible outcome<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">
                                                            <?php 
                                                            $grantee_possible_outcome = explode(",",$result1['grantee_possible_outcome']);
                                                            if($grantee_possible_outcome){
                                                                foreach($grantee_possible_outcome as $value)
                                                                {
                                                                    $dd = $this->db->get_where("dms_outcome", array('outcome_id' => $value))->row_array();
                                                                    echo $dd['outcome_name'].', ';
                                                                }
                                                            }
                                                    
                                                            ?>
                                                        </div>
                                                    </div>                                              
                                                </div>
                                                
                                                    
                                                
<!--                                            <div style="text-align: right">
                                                <br><br>
                                                
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">CREATE</button>
                                            </div>-->

                                            </div>
<!--                                            </div>-->
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                          </form>                     

                                        </div>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->
                               
                                    <!-- /BASIC -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>

    <script>   
        function year()
        {
            var cat = document.getElementById('e3').value;
            var id = document.getElementById('status').value;
           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/yearDropdown",
                    type: "POST",
                    data: {'status': id, 'category': cat},
                    success: function (response)
                    {
                        $("#years").html(response);

                    }

                });
            }
        }
        
        
        function grnt()
        {
            //alert('aaaa');
            var grnt = document.getElementById('grnt_no').value;
           // alert(grnt);
//            var id = document.getElementById('status').value;
//           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/grantNoValidation",
                    type: "POST",
                    data: {'grnt': grnt},
                    success: function (response)
                    {
                        //alert(response);
                        if(response=="yes")
                        {
                            $('#grnt_no').focus();
                            document.getElementById('grnt_no').value = '';
                            $('#grnt_p').text('Grant Number is Allredy exist');
                            $("#grnt_p").show();
                            //document.getElementById('grnt_p').value = 'Grant Number is Allredy exist';
                        }
                        else
                        {
                            $("#grnt_p").hide();
                        }
                        
                        //$("#years").html(response);

                    }

                });
//            }
        }
        
        
        function manual(e)
        {
            if(e!='')
            {
                $("#grntee_name").val('');
                $("#catg").hide();
                $("#grntee_detail").hide();
            }
            else
            {
                $("#catg").show();
                $("#grntee_detail").show();
            }
        }
        

</script>
<script>
    $(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
        "onSelect": function () {
//            var input = $(this);
//            $("#datepicker2").datepicker("option", "minDate", input.datepicker("getDate"));
//            $("#datepicker2").datepicker("refresh");
            var start = $('#datepicker1').datepicker('getDate');
            
            var day = start.getDate();
            var month = start.getMonth() + 6;
            var year = start.getFullYear();
            if (month < 10) {
                month = "0" + month;
            }
            if (day < 10) {
                day = "0" + day;
            }

            var sss = year + "-" + month + "-" + day;

            //$('#end_date').val(sss);
            
        }
    });
//});
//
//$(function() {
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
    $('#datepicker3').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
    
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });

</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function contect()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>
    <script>
window.rowCount = 1;
    function addMoredetail(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    //var recRow ='<div class="row" id="rowCount' + rowCount + '" style="margin-top:5px;"><div class="control-label col-md-6" ><label  class="control-label col-md-4"><span style="visibility:hidden;">Keywords</span></label><div class="col-md-5"><input type="text" class="form-control mng" id="i'+ rowCount +'" name="keyword[]"  value="" placeholder="Enter Keywords" ></div><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCount + '"><td><input type="text" class="form-control" required="required" name="title[]" value="" placeholder="Enter Title"></td><td><input type="text" class="form-control" required="required" name="value[]" value=""  placeholder="Enter Value"></td><td><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></td></tr>';
            jQuery('#addeddetail').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>






