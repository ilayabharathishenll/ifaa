<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('dms_assin_template', array('assin_temp_id' => $param1))->row_array();
    $temppar = $this->db->get_where('dms_document_template', array('document_temp_id' => $param1, 'document_temp_parent' => '0'))->row_array();
    $result2 = $this->db->get_where('dms_workflow', array('assin_temp_id' => $param1))->result_array();
    //print_r($result2); die;
    
//    $earning = explode(",",$result['teacher_earning']);
//    $earning_amount = explode(",",$result['teacher_earningAmount']);
//    $deduction = explode(",",$result['teacher_deduction']);
//    $deduction_amount = explode(",",$result['teacher_deductionAmount']);
//    $tax = explode(",",$result['teacher_tax']);
//    $tax_amount = explode(",",$result['teacher_taxAmount']);

    $formaction = 'edit';
} else {
    $formaction = 'create';
}
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>adminlogin/add_create_template/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Select Template Type<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                         
                                                        <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $result['assin_temp_id']; ?>"> 
                                                          
                                                        <select name="temp_type" class="form-control" id="temp_type" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Category is required.">
                                                            <option value="">Select Template Type</option>
                                                            <?php
                                                            //$data4 = $this->admin_model->fetchCategoryTree();
                                                            
                                                            foreach ($data1 as $row) {
                                                                if($row['template_type_id'] != '1'){
                                                                ?>
                                                                <option value="<?php echo $row['template_type_id']; ?>" <?php
                                                                if ($result['template_type_id'] == $row['template_type_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['template_type_name']; ?></option>
                                                            <?php } } ?>
                                                        </select>                                                                                          
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Select Template <span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       
                                                        <select name="temp"class="form-control"  data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Category Status is required.">
                                                            <option>Select Template </option>
                                                            <?php
                                                            $data4 = $this->admin_model->fetchCategoryTree();
                                                            
                                                            foreach ($data4 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['document_temp_id']; ?>" <?php
                                                                if ($result['document_temp_id'] == $row['document_temp_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['document_temp_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
    
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                            <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Step </label>
                                                    <div class="col-md-8">
                                                       
                                                        <input type="text" readonly="" class="form-control" 
                                                               name="step" value="<?php echo $result['assin_temp_step']; ?>"> 
    
                                                    </div>
                                                </div>
                                                
<!--                                                <div class="control-label col-md-4"> 
                                                    
                                                        <label class="control-label col-md-4"> Termination </label>
                                                        <div class="col-md-8">

                                                            <input type="radio" name="termination" value="enable" checked> Enable<br>
                                                            <input type="radio" name="termination" value="disable" <?php if($result['termination'] == 'disable'){ echo 'checked'; } ?> >Disable<br>
                                                        
                                                         </div>
                                                </div>-->
                                            <div class="control-label col-md-6" style="text-align: right">
                                                <div>
                                                
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                            </div>
                                            </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    
                                                        <label class="control-label col-md-4"> Termination Option </label>
                                                        <div class="col-md-8">

                                                            <input type="radio" name="termination" value="enable" checked> Enable<br>
                                                            <input type="radio" name="termination" value="disable" <?php if($result['termination'] == 'disable'){ echo 'checked'; } ?> >Disable<br>
                                                        
                                                         </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <div id="termination">
                                                        <label class="control-label col-md-4"> Text Editor</label>
                                                        <div class="col-md-8">

                                                            <input type="radio" name="text_editor" value="enable" checked> Enable<br>
                                                            <input type="radio" name="text_editor" value="disable" <?php if($result['text_editor'] == 'disable'){ echo 'checked'; } ?> >Disable<br>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            </br></br>
                                            <?php if($result['template_type_id'] == '2' || $result['template_type_id']=='5'  || $result['template_type_id']=='8'){ ?>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="box-title">
                                                    <h4>Work Flow Enable Disable </h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Work Flow </label>
                                                    <div class="col-md-8">
                                                       
                                                        <input type="radio" name="workflow" value="enable" checked> Enable<br>
                                                        <input type="radio" name="workflow" value="disable" <?php if($result['work_flow'] == 'disable'){ echo 'checked'; } ?> >Disable<br>
                                                    </div>
                                                </div>
                                            
                                            </div>
                                               
                                                
                                            </div>
                                            </br></br>
                                            
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="box-title">
                                                    <h4>Work Flow </h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                
                                                <div class="control-label col-md-12">
                                                    <table style=" width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Step : </th>
                                                                <th>Select Group : </th>
                                                                <th>Select User : </th>
                                                                <th style=" float: right"><div class="col-md-3">    <a  class="button btn btn-primary start add" style="display:inline-block;" onclick="addMoreuser(this.form,<?php echo $bookallow; ?>);" />Add user</a> </div></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody id="addedflow">
                                                            <?php if($result2){ //echo "<pre>";
                                                                //print_r($result2);
                                                                $f = 0;
                                                                foreach($result2 as $da)
                                                                {
                                                                ?>
                                                            <tr class="tr1" id="id<?php echo $f+'1';?>">
                                                                <td><input type="hidden" name="steg[]" value="<?php echo $f + '1'; ?>"><?php echo $f + '1'; ?></td>
                                                                <td>
                                                                    <select name="group[]" class="form-control">
                                                                        <option>Select Group</option>
                                                                        <?php
                                                                        $grp = $this->db->get('dms_role')->result_array();
                                                                        foreach ($grp as $row) {
                                                                         ?>
                                                                         <option value="<?php echo $row['role_id']; ?>" <?php
                                                                         if ($da['role_id'] == $row['role_id']) {
                                                                             echo "selected";
                                                                         }
                                                                         ?> ><?php echo $row['role_name']; ?></option>
                                                                             <?php } ?>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="user[]" id="user<?php echo $j; ?>" class="form-control">
                                                                            <option>Select User</option>
                                                                            <?php
                                                                            $urg = $this->db->get('dms_user')->result_array();
                                                                            foreach ($urg as $row) {
                                                                                ?>
                                                                                <option value="<?php echo $row['user_id']; ?>" <?php
                                                                                if ($da['user_id'] == $row['user_id']) {
                                                                                    echo "selected";
                                                                                }
                                                                                ?> ><?php echo $row['user_name']; ?></option>
                                                                             <?php } ?>
                                                                 </select>
                                                                </td>
                                                                <td>
<!--                                                                    <a onclick="return confirm('Are you sure to delete this item?');" href="<?php echo base_url() . "adminlogin/delete_workflow/".$da['workflow_id']; ?>">Delete</a>-->
                                                                    <input type="button" class="btn btn-primary"  value="Delete" onclick="row_del('<?php echo $da['workflow_id']; ?>','<?php  echo $f + '1'; ?>');">
                                                                    
                                                                </td>
                                                                
                                                            </tr>
                                                            <?php $f++;  } }?>
                                                        </tbody>
                                                    </table>


                                                </div>
                                            </div></br>
                                                
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-12"> 
                                                    <label class="control-label col-md-2"> Description</label>
                                                    <div class="col-md-10">
                                                       
                                                        <textarea class="ckeditor" name="description" id="chk" >
                                                                    <?=$result['description']; ?>
                                                        </textarea>
                                                    </div>
                                                </div>
                                            
                                            </div> 
                                            <?php } ?>
                                            
                                            <div id="demo">
                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <div id="demo">
                
            </div>-->
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>

    <script>   
        
        function row_del(id,y){
             //alert(y);return false;
              var result = confirm("Are you sure to delete?");
                                                if (result) {
                                                    //return true;
                                               
           
               $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/delete_workflow",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                        $('#id'+y).hide();

                    }

                });
                 } else {
                                                    return false;
                                                }
        }
        
        
        function year()
        {
            var cat = document.getElementById('e3').value;
            var id = document.getElementById('status').value;
           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/yearDropdown",
                    type: "POST",
                    data: {'status': id, 'category': cat},
                    success: function (response)
                    {
                        $("#years").html(response);

                    }

                });
            }
        }
        
        function template()
        {
            //alert("aa");
           var temp = document.getElementById('temp_type').value;
           if(temp=='')
           {
               alert("PLZ Select template");
           }
           
           // var id = document.getElementById('status').value;
           if(temp){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/template",
                    type: "POST",
                    data: {'id': temp},
                    success: function (response)
                    {
                        $("#demo").html(response);
                        //alert(response);

                    }

                });
            }
        }
        function sss(id, e)
        {
            //alert(e);
            $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/getuser",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                        $("#user"+e).html(response);
                        //alert(response);

                    }

                });
        }
        
        

</script>
<script>
    $(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
//});
//
//$(function() {
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
    
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });

</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function contect()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>
    
<script>
window.rowCount = 1;
var rowf = '<?php echo $f; ?>';
    function addMoreuser(frm, allowbook) {
        rowf++;
        rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr class="tr1" id="rowCount' + rowCount + '"><td><input type="hidden" name="steg[]" value="' + rowf + '">' + rowf + '</td><td><select name="group[]" class="form-control" onchange="sss(this.value, ' + rowCount + ')"><option>Select Group</option><?php $grp = $this->db->get('dms_role')->result_array(); foreach ($grp as $row) { ?><option value="<?php echo $row['role_id']; ?>" <?php if ($result['role_id'] == $row['role_id']) { echo "selected";}?> ><?php echo $row['role_name']; ?></option><?php } ?></select></td><td><select name="user[]" id="user' + rowCount + '" class="form-control"><option>Select User</option></select></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></td>';


            jQuery('#addedflow').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>





