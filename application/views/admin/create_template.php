<!--<script>
$("body").on("mousemove", function(){ 

    $('.ckeditor').each(function(index, element) {
       CKEDITOR.replaceAll();
      $(this).removeClass('ckeditor');
    });

});
</script>-->
<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('tblteacher', array('teacher_id' => $param1))->row_array();
    
    $earning = explode(",",$result['teacher_earning']);
    $earning_amount = explode(",",$result['teacher_earningAmount']);
    $deduction = explode(",",$result['teacher_deduction']);
    $deduction_amount = explode(",",$result['teacher_deductionAmount']);
    $tax = explode(",",$result['teacher_tax']);
    $tax_amount = explode(",",$result['teacher_taxAmount']);

    $formaction = 'edit';
} else {
    $formaction = 'create';
}
//$this->load->assets('ckeditor','ckeditor.js' );
?>

<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                    <form role="form" action="<?php echo base_url(); ?>adminlogin/add_create_template/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Select Template Type<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                         
                                                        <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $result['id']; ?>"> 
                                                          
                                                        <select name="temp_type" class="form-control" id="temp_type" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Category is required.">
                                                            <option value="">Select Template Type</option>
<!--                                                            <option value="1"> Grant Template </option>-->
                                                            <?php
                                                            //$data4 = $this->admin_model->fetchCategoryTree();
                                                            
                                                            foreach ($data6 as $row) {
                                                                if($row['template_type_id'] != '1'){
                                                                ?>
                                                                <option value="<?php echo $row['template_type_id']; ?>" <?php
                                                                if ($result['template_type_id'] == $row['template_type_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['template_type_name']; ?></option>
                                                            <?php } } ?>
                                                        </select>                                                                                          
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Select Template <span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                       
                                                        <select name="temp" id="doc_id" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Category Status is required.">
                                                            <option value="">Select Template </option>
                                                            <?php
                                                            $data4 = $this->admin_model->fetchCategoryTree();
                                                            
                                                            foreach ($data4 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['document_temp_id']; ?>" <?php
                                                                if ($result['document_temp_id'] == $row['document_temp_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['document_temp_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
    
                                                    </div>
                                                </div>
                                            </div>
<!--                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-12"> 
                                                    <div id="termination">
                                                        <label class="control-label col-md-2"> Description </label>
                                                        <div class="col-md-10">

                                                            <textarea class="ckeditor" name="description" cols="90" rows="5" >
                                                                    <?=$result['document_temp_description'];?>
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                            </div>-->
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <div id="termination">
                                                        <label class="control-label col-md-4"> Termination Option </label>
                                                        <div class="col-md-8">

                                                            <input type="radio" name="termination" value="enable" checked> Enable<br>
                                                            <input type="radio" name="termination" value="disable" <?php if($result['termination'] == 'disable'){ echo 'checked'; } ?> >Disable<br>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="control-label col-md-4" style="text-align: right">
                                                
                                                <a href="javascript:;" class="btn btn-primary start" onclick="template()" data-toggle="collapse">Populate </a>
                                                
<!--                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>-->
                                            </div>
                                            <div class="control-label col-md-2" style="text-align: right">
                                                <div style="text-align: left">
                                                
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                            </div>
                                            </div>
                                            </div>

                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <div id="termination">
                                                        <label class="control-label col-md-4"> Text Editor</label>
                                                        <div class="col-md-8">

                                                            <input type="radio" name="text_editor" value="enable" checked> Enable<br>
                                                            <input type="radio" name="text_editor" value="disable" <?php if($result['text_editor'] == 'disable'){ echo 'checked'; } ?> >Disable<br>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="demo">
                
            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<!--                <div id="demo">
                
            </div>-->
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>
    
<!--    <div class="modal fade" id="orderdetail" role="dialog">
    <div class="modal-dialog">
    
       Modal content
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Document Order</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                        <div class="col-md-12">
                                 BOX 
                            <div class="box border primary">
                            <div class="box-title">
                                <h4>Document Order </h4>
                                <div class="tools">
                                    <a href="javascript:;" class="remove">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                <table class="table table-striped mytable">
                                    <thead>
                                    <th style="width:15%;">S.No </th>
                                    <th style="width:15%;">Document Name </th>
                                    <th style="width:15%;">Parent Name </th>

                                    <th style="width:12%;">Action </th>
                                    </thead>
                                    <tbody id="pri" class="tb">
                                        <?php
                                        $quer = $this->db->get_where('dms_document_template', array('document_temp_parent' => '0'))->result_array();

                                        $i = 1;
                                        foreach ($quer as $lib) {

                                            $gr = $this->db->get_where('dms_document_template', array('document_temp_id' => $lib['document_temp_parent']))->row_array();
                                            ?>
                                            <tr id="<?php echo $lib['document_temp_id']; ?>"  class="default">
                                                <td style="width:15%;"><?php echo $i; ?></td>
                                                <td style="width:15%;"><?php echo $lib['document_temp_name']; ?></td>

                                            </tr>

                                            <?php $i++;
                                        } ?>

                                    </tbody>

                                </table>

                            </div>
                            </div>
                            </div>
                                 /BOX 
                        </div>
                </div>
              
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>-->
    

    <script>   
        function year()
        {
            var cat = document.getElementById('e3').value;
            var id = document.getElementById('status').value;
           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/yearDropdown",
                    type: "POST",
                    data: {'status': id, 'category': cat},
                    success: function (response)
                    {
                        $("#years").html(response);

                    }

                });
            }
        }
        
        function template()
        {
            //alert("aa");
           var temp = document.getElementById('temp_type').value;
           var doc = document.getElementById('doc_id').value;
           //alert(doc);
           if(temp=='')
           {
               alert("PLZ Select template");
               return false;
           }
           if(doc=='')
           {
               alert("PLZ Select template");
               return false;
           }
           
           // var id = document.getElementById('status').value;
           if(temp){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/template",
                    type: "POST",
                    data: {'id': temp, 'doc': doc},
                    success: function (response)
                    {
                        $("#demo").html(response);
                        //alert(response);

                    }

                });
            }
        }
        function sss(id, e)
        {
            //alert(e);
            $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/getuser",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                        $("#user"+e).html(response);
                        //alert(response);

                    }

                });
        }
        
         function Documentorder()
        {

            $("#orderdetail").modal('show');
        }
        
//        function terminate(e)
//        {
//            alert(e);
//            if(e=='13')
//            {
//                $("#termination").show();
//            }
//        }
        

</script>
<script>
    $(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
//});
//
//$(function() {
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
    
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });

</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function contect()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>
    
<script>
window.rowCount = 1;
    function addMoreuser(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCount + '"><td><input type="hidden" name="steg[]" value="' + rowCount + '">' + rowCount + '</td><td><select name="group[]" class="form-control" onchange="sss(this.value, ' + rowCount + ')"><option>Select Group</option><?php $grp = $this->db->get('dms_role')->result_array(); foreach ($grp as $row) { ?><option value="<?php echo $row['role_id']; ?>" <?php if ($result['role_id'] == $row['role_id']) { echo "selected";}?> ><?php echo $row['role_name']; ?></option><?php } ?></select></td><td><select name="user[]" id="user' + rowCount + '" class="form-control"><option>Select User</option></select></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></td>';


            jQuery('#addedflow').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>
<!--<script type="text/javascript">
            $('#pri').sortable();
        </script>
<script>
            $(document).ready(function(){
                //alert('aaaa');
            $(document).on("mouseout", function(){
                 console.log('kk');
            //$(document).on('click','#save-reorder',function(){
            //alert("Deff");return false;
           
            var list = new Array();
            $('#pri').find('.default').each(function(){
            var id = $(this).attr('id');
            console.log(id);
            list.push(id);
            });
             //alert("aaa");
            
            var data = JSON.stringify(list);
            $.ajax({
            url: '<?php echo base_url(); ?>adminlogin/get_records', // server url
                    type: 'POST', //POST or GET 
                    data: {token:'reorder', data:data}, // data to send in ajax format or querystring format
                    datatype: 'json',
                    success: function(response) {
                         console.log('kk');
                    //alert(response);
                    }

            });
            });
            });
        </script>-->





