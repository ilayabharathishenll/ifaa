<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('dms_role', array('role_id' => $param1))->row_array();
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>-->
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row" style="<?php if($param1){ ?> display: block; <?php } else { ?> display: none; <?php } ?> ">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>adminlogin/add_role/<?php echo $formaction; ?>" method = "post">
                                            <div class="form-group">
                                                <label>Role Name</label><span style="color: red;margin-left: 3px;">*</span>
                                                <input type="hidden" class="form-control" 
                                                       name="hidden_id"  value="<?php echo $result['role_id']; ?>" >
                                                <input type="text" class="form-control" data-validation="length" 
                                                       data-validation-length="min1" 
                                                       data-validation-error-msg="Role name is required"
                                                       name="name"  value="<?php echo $result['role_name']; ?>" placeholder="Enter role name">
                                            </div>   
                                            <div class="form-group">
                                                <label>Description</label>
                                                <input type="text" class="form-control" 
                                                       name="description"  value="<?php echo $result['role_description']; ?>" placeholder="Enter role Description">
                                            </div>  
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select name="status" class="form-control">

                                                    <option value="active"<?php
                                                    if ($result['role_status'] == 'active') {
                                                        echo 'selected';
                                                    }
                                                    ?>>Active</option>
                                                    <option value="inactive"<?php
                                                            if ($result['role_status'] == 'inactive') {
                                                                echo 'selected';
                                                            }
                                                        ?>>Inactive</option>
                                                </select>
                                            </div>                                            
                                            <div style="text-align: right">
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        name="submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->

                                <!-- /BASIC -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BOX -->
                        <div class="box border primary">
                            <div class="box-title">
                                <h4>Role Details</h4>
                                <div class="tools">
                                    <a href="javascript:;" class="remove">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="example" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Role Name</th>
                                                <th>Description</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$i = 1;
foreach ($data as $row) {
    //$val = $this->db->get_where('tblpermission', array('permission_id' => $row['role_permission_id']))->result_array();
    ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo ucwords($row['role_name']); ?></td>
                                                    <td><?php echo $row['role_description']; ?></td>
                                                    <td><?php echo $row['role_status']; ?></td> 
                                                    <td><a style="color:green" href="<?php echo base_url(); ?>adminlogin/show_role/<?php echo $row['role_id']; ?>/edit" title="Edit Record"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i></a>
<!--                                                        /
                                                        <a style="color:red" href="javascript:;" title="Delete Record" onclick="confirm_modal('<?php echo base_url(); ?>adminlogin/add_role/delete/<?php echo $row['role_id']; ?>');"><i class="fa fa-trash-o fa-1x" aria-hidden="true"></i></a>-->
                                                    </td>
                                                </tr>
    <?php
    $i++;
}
?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <!-- /BOX -->
                    </div>
                </div>
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );

</script>