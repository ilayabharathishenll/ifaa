<?php
$name1=$this->session->userdata['login']['user_id'];
 $result11 = $this->db->get_where('dms_user', array('user_id' => $name1))->row_array();
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('Adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php 
                     echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="javascript:;" 
                                           aria-hidden="true">X</a>
                                        <h4><i class="fa fa-smile-o"></i> <?php
                                            echo
                                            $this->session->flashdata('flash_message');
                                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                                    </div>
                                    <?php
                                }
                                if ($this->session->flashdata('permission_message')) {
                                    ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <a class="close" data-dismiss="alert" href="javascript:;" 
                                           aria-hidden="true">X</a>
                                        <h5><i class="fa fa-frown-o"></i> <?php
                                            echo
                                            $this->session->flashdata('permission_message');
                                            ?><i class="fa fa-thumbs-down"></i></h5>
                                    </div>
                                    <?php }?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-10">
                        <!-- BOX -->
                        <div class="box border">
                            <div class="box-title">
                                <h4><i class="fa fa-user"></i><span class="hidden-inline-mobile">Hello <?php echo $result11['user_name'] ?></span></h4>
                            </div>
                            <div class="box-body">
                                <div class="tabbable header-tabs user-profile">
                                    <ul class="nav nav-tabs">
                                        <li><a href="#pro_edit" data-toggle="tab"><i class="fa fa-edit"></i> <span class="hidden-inline-mobile"> Edit Account</span></a></li>
                                        <li class="active"><a href="#pro_overview" data-toggle="tab"><i class="fa fa-dot-circle-o"></i> <span class="hidden-inline-mobile">Overview</span></a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <!-- OVERVIEW -->
                                        <div class="tab-pane fade in active" id="pro_overview">
                                            <div class="row">
                                                <!-- PROFILE PIC -->
                                                <div class="col-md-4">
                                                    
                                       <!--<img  class="img-responsive" src="<?php echo base_url()."uploads/".$result11['user_image']; ?>" style="height:200px;width:205px;" alt=""/>-->                        
                                            <a href="<?php echo img_path."local_path/".$result11['user_image']; ?>" target="_blank"><img src="<?php echo img_path."local_path/".$result11['user_image']; ?>" style="height:200px;width:205px;"></a> 
                                                    <div class="list-group-item profile-details" style="width:204px;border: 0px solid red">
                                                            <h2><?php echo $result11['admin_username'] ?></h2>
                                                            <p><i class="fa fa-circle text-green"></i> Online</p>
                                                         
                                                        </div>

                                                   														
                                                </div>
                                                <!-- /PROFILE PIC -->
                                                <!-- PROFILE DETAILS -->
                                                <div class="col-md-6">
                                                    <!-- ROW 1 -->
                                                    <div class="row">
                                                        <div class="col-md-12 profile-details">																
                                                            <h3>Basic Information</h3>
                                                            <div class="box-body big">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <h4></h4>
                                                                        <?php foreach ($data as $user) { ?>
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Username</label> 
                                                                                <div class="col-md-8"><label class="col-md-4 control-label"><?php echo $user['user_name']; ?></label> </div>
                                                                            </div><br><br>
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Email Id</label> 
                                                                                <div class="col-md-8"><label class="col-md-4 control-label"><?php echo $user['user_email']; ?></label> </div>
                                                                            </div><br>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="divide-20"></div>
                                                            <!-- BUTTONS -->
                                                            <!-- /BUTTONS -->
                                                        </div>
                                                    </div>
                                                    <!-- /ROW 1 -->
                                                    <div class="divide-40"></div>
                                                </div>
                                                <!-- /PROFILE DETAILS -->
                                            </div>
                                        </div>
                                        <!-- /OVERVIEW -->
                                        <!-- EDIT ACCOUNT -->    
                                        <div class="tab-pane fade" id="pro_edit">
                                            <form class="form-horizontal" role="form" action="<?php echo base_url(); ?>adminlogin/profileupdate" method = "post" enctype="multipart/form-data">
                                                <div class="">
<!--                                                    <div class="col-md-8 col-md-offset-2 centered" >
                                                        <div class="box border primary">
                                                            <div class="box-title">
                                                                <h4><i class="fa fa-bars"></i>General Information</h4>
                                                            </div>-->
                                                             
                                                            <div class="box-body big">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <h3 style="text-align:center;margin:0 0 30px 0;">Basic Information</h3>                                                  
                                                                        <?php foreach ($data as $user) { ?>
                                                                            <div class="form-group">
                                                                                <label class="col-md-4 control-label">Email Id</label> 
                                                                                <div class="col-md-8">


                                                                                    <input type="email" class="form-control" data-validation="email" data-validation-error-msg="Email is required with correct e-mail address."
                                                                                           name="emailid" value="<?php echo $user['user_email']; ?>">
                                                                                </div>
                                                                            </div>
                                                                       
                                                                        <div class="form-group">
                                                                            <label class="col-md-4 control-label">Username</label> 
                                                                            <div class="col-md-8">
                                                                                 

                                                                                <input type="text" class="form-control" data-validation="length" 
                                                                                       data-validation-length="min1" 
                                                                                       data-validation-error-msg="Username is required" name="username" value="<?php echo $user['user_name']; ?>">
                                                                            </div>
                                                                        </div>
                                                                        
                                                                         <div class="form-group">
                                                                            <label class="col-md-4 control-label">Profile Pic</label> 
                                                                            <div class="col-md-8">
                                                                             <input name="image" type="file" data-validation="Mime size required" 
                                                                                     data-validation-allowing="jpg, png" 
                                                                                     data-validation-max-size="300kb" 
                                                                                     data-validation-error-msg-required="No image selected"
                                                                                     class="file-input" value="<?php echo $result['user_image']; ?>" >    
<!--                                                                            <input type="file" class="file-input" 
                                                                               name="image" value="<?php //echo $result['teacher_jobresume']; ?>" >-->
                                                                            </div>
                                                                        </div>
                                                                        
                                                                         <?php } ?>
                                                                        <div class="">
                                                                            <input type="submit" value="Update Account" class="btn btn-primary pull-right"> 
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
<!--                                                        </div>-->
                                                    </div>
                                           
                                        </div>
                                             </form>    
                                        <!-- /EDIT ACCOUNT -->
                                    </div>
                                </div>
                                <!-- /USER PROFILE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /DASHBOARD CONTENT -->
        </div><!-- /CONTENT-->
    </div>
</div>
</div>
<script>


    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>