<div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div class="box-body big">
                                        <div class="control-label col-md-12">
                                            <table class="datatable table table-striped table-bordered table-hover">
                                                <tr>
                                                    <td>Grant Name Category : </td>
                                                    <td>Grant Status Category : </td>
                                                    <td>Grant Year Category : </td>
                                                    
                                                </tr>
                                            </table>
                                             <br>
                                        </div>
                                       
                                        <form role="form" action="<?php echo base_url(); ?>adminlogin/add_grant_main/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Add Files Here </label>
                                                    <div class="col-md-8">
                                                         
                                                        <input name="image" type="file"
                                                            class="file-input" value="<?php echo $result['user_image']; ?>" >                                                                                  
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6" style="text-align: right">
                                                    <div style="text-align: right;">

                                                        <button type="button" class="btn btn-primary start" style="width:120px" 
                                                            value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="box-title">
                                                    <h4>Meta Data</h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-12">
                                                    <table style=" width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Document Id : </th>
                                                                <th>Handling Staff : </th>
                                                                <th>Creator/Author : </th>
                                                                <th>Copyright : </th>
                                                                <th><div class="col-md-3">    <a  class="button btn btn-primary start add" style="display:inline-block;" onclick="addMoreRows(this.form,<?php echo $bookallow; ?>);" />Add More</a> </div></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody id="addedRows">
                                                            <tr>
                                                                <td><input class="form-control" placeholder="Enter Document Id"  type="text" name="document_id[]"> </td>
                                                                <td><input class="form-control" placeholder="Enter Handling Staff"  type="text" name="staff[]"></td>
                                                                <td><input class="form-control" placeholder="Enter Creator/Author"  type="text" name="auther[]"></td>
                                                                <td><input class="form-control" placeholder="Enter Copyright" type="text" name="copyright[]"></td>
                                                                <td></td>

                                                            </tr>
                                                        </tbody>
                                                    </table>


                                                </div>
                                            </div>
                                            </br> <br>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Keyword </label>
                                                    <div class="col-md-8">
                                                         
                                                        <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $result['id']; ?>"> 
                                                          
                                                        <input type="text" class="form-control" 
                                                                               placeholder="Enter Keyword"
                                                                               value="<?php echo $result['copyright']; ?>">                                                                                  
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6" style="text-align: right">
                                                    <div class=" col-md-3" style="text-align: left;">
                                                        <a  class="button btn btn-primary start add" style="display:inline-block;" onclick="addkeyRows(this.form,<?php echo $bookallow; ?>);" />Add Keyword</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="addedkey" >  
                                             
                                            </div>
                                            <br>
                                            
                                            
                                            
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>