<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('dms_category', array('category_id' => $param1))->row_array();
    $yer = $this->db->get_where('dms_cat_year', array('category_id' => $param1))->result_array();
    $result2 = $this->db->get_where('dms_categorytype', array('categorytype_id' => $result1['status']))->row_array();
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
?>
<style>
   .moreadd
   {
       margin: 22px 0;
   }
    .paddinglake .col-md-4
    {
        padding:0 5px;
     
    }
     
    
</style>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>-->
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>adminlogin/add_category/<?php echo $formaction; ?>" method = "post">
                                            <div class="form-group">
                                                <label> Programme Name</label><span style="color: red;margin-left: 3px;">*</span>
                                                <input type="hidden" class="form-control" 
                                                       name="hidden_id"  value="<?php echo $result['category_id']; ?>" >
                                                <input type="text" class="form-control" data-validation="length" 
                                                       data-validation-length="min1" 
                                                       data-validation-error-msg="Programs name is required"
                                                       name="name"  value="<?php echo $result['category_name']; ?>" placeholder="Enter Programme Name">
                                            </div>
                                            <div class="form-group">
                                                <label>Parent Programme </label>
                                                <select name="parent" class="form-control" >
                                                    <option value="">Select Parent Programme</option>

                                                    <?php
                                                    foreach ($datas as $row) {
                                                        ?>
                                                        <option value="<?php echo $row['category_id']; ?>"<?php
                                                        if ($result['parent_id'] == $row['category_id']) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $row['category_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <br>
<!--                                            <div class="form-group">
                                                <label>Category Status</label><span style="color: red;margin-left: 3px;">*</span>
                                                <?php if($param1)
                                                {?>
                                                     <input type="text" class="form-control" 
                                                       name="typename" readonly=""  value="<?php echo $result2['categorytype_name']; ?>">
                                                     <input type="hidden" name="type"  value="<?php echo $result1['status']; ?>">
                                            <?php    } else{
                                                    ?>
                                                <select name="type" class="form-control"  data-validation="length" 
                                                        data-validation-length="min1" 
                                                        data-validation-error-msg="Category type is required" >

                                                    <?php
                                                    foreach ($data1 as $row) {
                                                        ?>
                                                        <option value="<?php echo $row['categorytype_id']; ?>"<?php
                                                        if ($result['categorytype_id'] == $row['categorytype_id']) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $row['categorytype_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                     <?php } ?>
                                            </div> -->
<!--                                            <div class="form-group">
                                                <label>Status</label>
                                                <select name="status" class="form-control">

                                                    <option value="active"<?php
                                                    if ($result['role_status'] == 'active') {
                                                        echo 'selected';
                                                    }
                                                    ?>>Active</option>
                                                    <option value="inactive"<?php
                                                            if ($result['role_status'] == 'inactive') {
                                                                echo 'selected';
                                                            }
                                                    ?>>Inactive</option>
                                                </select>
                                            </div>-->

<!--                                            <div class="form-group">
                                                
                                            <div class="row">
                                            <div class="col-sm-12 paddinglake">
                                                <div class="form-group col-md-4" style="padding-left:0;">
                                                    <label style="display:block;">Year</label>
                                                    <?php if(!$param1) { ?> 
                                                    <input type="text" class="form-control mng" id="io" data-validation="length" 
                                                       data-validation-length=min1" onblur="roomcheck(this.value,'01',this.id);"
                                                       data-validation-error-msg="Year is required"
                                                        name="year[]"  value="<?php echo $result['room_name']; ?>" placeholder="Enter Year" style="display:inline-block;">
                                                    <?php } ?> 
                                                </div>
                                                <div class="form-group col-md-4">
                                                     <label style="display:block;">Status</label>
                                                    <?php if(!$param1) { ?> 
                                                   <select name="type[]" class="form-control"  data-validation="length" 
                                                        data-validation-length="min1" 
                                                        data-validation-error-msg="Category type is required" >

                                                    <?php
                                                    foreach ($data1 as $row) {
                                                        ?>
                                                        <option value="<?php echo $row['categorytype_id']; ?>"<?php
                                                        if ($result['categorytype_id'] == $row['categorytype_id']) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $row['categorytype_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                   <?php } ?>  
                                            </div>
                                                
                                                
                                                
                                                
                                                
                                                
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                  
                                                    
                                                    <a  class="button btn btn-primary start add moreadd" style="display:inline-block;" onclick="addMoreRows(this.form,<?php echo $bookallow; ?>);" />Add More</a> 
                                                </div>
                                                             <p class="sectionerror01">
                                            </div>
                                                    
                                                   
                                    
                                                </div>
                                                  </div>
                                                
                                            <?php if($param1)
                                                        { 
                                                $j = 0;
                                                
                                                foreach($yer as $value){
                                                ?>
                                                <div class="row">
                                                
                                              
                                            <div class="col-sm-12 paddinglake">
                                                <div class="form-group col-md-4" style="padding-left:0;">
                                                    
                                                    
                                                    <input type="text" readonly="" class="form-control mng" id="io" data-validation="length" 
                                                       data-validation-length=min1" onblur="roomcheck(this.value,'01',this.id);"
                                                       data-validation-error-msg="Year is required"
                                                       name=""  value="<?php echo $value['year']; ?>" placeholder="Enter Year" style="display:inline-block;">
                                                    
                                                </div>
                                                <div class="form-group col-md-4">
                                                     
                                                    
                                                    <select name="" readonly class="form-control"  data-validation="length" 
                                                        data-validation-length="min1" 
                                                        data-validation-error-msg="Category type is required" >

                                                    <?php
                                                    foreach ($data1 as $row) {
                                                        ?>
                                                        <option value="<?php echo $row['categorytype_id']; ?>"<?php
                                                        if ($value['status'] == $row['categorytype_id']) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $row['categorytype_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                                    
                                            </div>
                                                
                                               <div class="col-sm-4"> 
                                                                <div class="form-group">
                                                                    <a style="color:red" href="javascript:;" title="Delete Record" onclick="confirm_modal('<?php echo base_url(); ?>adminlogin/delete_year/delete/<?php echo $value['cat_id']; ?>');"><i class="fa fa-trash-o fa-1x" aria-hidden="true"></i></a>
                                                                </div>
                                                            </div> 
                                                   
                                    
                                                </div>
                                                  </div>
                                                <?php $j++; } } ?>
   
                                      
                                                <div class="row">
                                              
                                        
                                                    <div class="col-md-12" id="addedRows" >
                                                        
                                                        
                                                        
                                                      
                                                        
                                                    </div>
                                            
                                                    
                                                </div>-->
                                                
                                                
                                                
                                                
                                                
                                                
                                          
                                            
                                            <div style="text-align: right">
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        name="submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->

                                <!-- /BASIC -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BOX -->
                        <div class="box border primary">
                            <div class="box-title">
                                <h4>Programme Details</h4>
                                <div class="tools">
                                    <a href="javascript:;" class="remove">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="example" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th> Name</th>
<!--                                                <th>Description</th>-->
                                                <th>Parent Category</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$i = 1;
foreach ($data as $row) {
    $val = $this->db->get_where('dms_category', array('category_id' => $row['parent_id']))->result_array();
    ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo ucwords($row['category_name']); ?></td>
                                                    <td><?php echo $val[0]['category_name']; ?></td> 
                                                    <td><a style="color:green" href="<?php echo base_url(); ?>adminlogin/show_category/<?php echo $row['category_id']; ?>/edit" title="Edit Record"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i></a>
                                                        /
                                                        <a style="color:red" href="javascript:;" title="Delete Record" onclick="confirm_modal('<?php echo base_url(); ?>adminlogin/add_category/delete/<?php echo $row['category_id']; ?>');"><i class="fa fa-trash-o fa-1x" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
    <?php
    $i++;
}
?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <!-- /BOX -->
                    </div>
                </div>
                </div>
                <!-- /DASHBOARD CONTENT -->
                
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>
<script>

   
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );

} );


</script>
<script>
window.rowCount = 1;
    function addMoreRows(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
            var recRow = '<div class="row" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-4"><div class="form-group"><input type="text" class="form-control mng" id="i'+ rowCount +'" required="" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class=" col-md-4"><div class="form-group"><select name="type[]" required="" class="form-control" ><?php foreach ($data1 as $row) { ?> <option value="<?php echo $row['categorytype_id']; ?>"<?php if ($result['categorytype_id'] == $row['categorytype_id']) { echo "selected"; }?>><?php echo $row['categorytype_name']; ?></option><?php } ?></select></div></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

            jQuery('#addedRows').append(recRow);
        
        


    }
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>