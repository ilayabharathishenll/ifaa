<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('tblpermission', array('permission_id' => $param1))->row_array();
    $formaction = 'edit'; 
} else {
    $formaction = 'create';
}
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('Adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>-->
                        </div>
                    </div>
                </div> <?php if ($this->session->flashdata('flash_message')) { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="javascript:;" 
                                           aria-hidden="true">X</a>
                                        <h4><i class="fa fa-smile-o"></i> <?php
                                    echo
                                    $this->session->flashdata('flash_message');
                                    ?>  <i class="fa fa-thumbs-up"></i></h4>
                                    </div>
    <?php
}
if ($this->session->flashdata('permission_message')) {
    ?>
                                    <div class="alert alert-block alert-warning fade in">
                                        <a class="close" data-dismiss="alert" href="javascript:;" 
                                           aria-hidden="true">X</a>
                                        <h5><i class="fa fa-frown-o"></i> <?php
                                        echo
                                        $this->session->flashdata('permission_message');
                                        ?><i class="fa fa-thumbs-down"></i></h5>
                                    </div>
                                    <?php }?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>Adminlogin/addpermission/<?php echo $formaction; ?>" method = "post">
                                            <div class="form-group">
                                                <label>Permission Name</label><span style="color: red;margin-left: 3px;">*</span>
                                                <input type="hidden" class="form-control" 
                                                       name="hidden_id"  value="<?php echo $result['permission_id']; ?>" >
                                                <input type="text" class="form-control" data-validation="length" 
		 data-validation-length="min1" 
		 data-validation-error-msg="Permission name is required"
                                                       name="name"  value="<?php echo $result['permission_name']; ?>" placeholder="Enter permission name">
                                            </div>  
                                               <div class="form-group">
                                                <label>Description</label><span style="color: red;margin-left: 3px;">*</span>
                                                <textarea class="form-control" id="description"data-validation="length" 
		 data-validation-length="min1" 
		 data-validation-error-msg="Description is required"
                                                          name="description"><?php echo $result['permission_description']; ?></textarea>
                                            </div>    
                                            <div class="form-group">
                                                <label>Status</label>
                                            
                                               <select name="status" class="form-control">
                                                   <option value="active"<?php if($result['permission_status']=='active'){echo 'selected';} ?>>active</option>
                                                            <option value="inactive"<?php if($result['permission_status']=='inactive'){echo 'selected';} ?>>inactive</option>
                                                </select> 
                                            </div>                                       
                                            <div style="text-align: right">
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        name="submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->
                              
                                    <!-- /BASIC -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BOX -->
                            <div class="box border primary">
                                <div class="box-title">
                                    <h4>Permission Details</h4>
                                    <div class="tools">
                                        <a href="javascript:;" class="remove">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>S.No.</th>
                                                    <th>Permission Name</th>
                                                    <th>Description</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i = 1;                                          
                                                 foreach($data  as $row){
                                                ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo $row['permission_name']; ?></td>
                                                     <td><?php echo $row['permission_description']; ?></td>
                                                    <td><?php echo $row['permission_status']; ?></td> 
                                                    <td><a style="color:green" href="<?php echo base_url(); ?>Adminlogin/show_addpermission/<?php echo $row['permission_id']; ?>/edit" title="Edit Record"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i></a> /
                                                    <a style="color:red" href="javascript:;" title="Delete Record" onclick="confirm_modal('<?php echo base_url(); ?>Adminlogin/addpermission/delete/<?php echo $row['permission_id']; ?>');"><i class="fa fa-trash-o fa-1x" aria-hidden="true"></i></a></td>
                                                </tr>
                                                <?php
                                                $i++;
                                                 } 
                                                ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            <!-- /BOX -->
                        </div>
                    </div>
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>
<script>

   $.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });

  // Restrict presentation length
  $('#description').restrictLength( $('#pres-max-length') );
  $.validate({
 modules : 'security',
  borderColorOnError : '#FFF',
  addValidClassOnAll : true
});


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );

</script>