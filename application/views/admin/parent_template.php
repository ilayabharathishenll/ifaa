<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('tblteacher', array('teacher_id' => $param1))->row_array();
    
    $earning = explode(",",$result['teacher_earning']);
    $earning_amount = explode(",",$result['teacher_earningAmount']);
    $deduction = explode(",",$result['teacher_deduction']);
    $deduction_amount = explode(",",$result['teacher_deductionAmount']);
    $tax = explode(",",$result['teacher_tax']);
    $tax_amount = explode(",",$result['teacher_taxAmount']);

    $formaction = 'edit';
} else {
    $formaction = 'create';
}
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div class="box-body big">
                                        <div class="row" style="margin-bottom:10px">
                                            <div class="box">
                                                <table class="datatable table table-striped table-bordered table-hover">
                                                    <tr>
                                                        <td>Grant Name Category : </td>
                                                        <td>Grant Status Category : </td>
                                                        <td>Grant Year Category : </td>
                                                        <td  style=" width: 20%;"><span style="float: left;">step : &nbsp; </span> <input type="text"  style=" width: 50%;" name="step" class="form-control col-md-2"> </td>
                                                    </tr>
                                                </table>
                                                <br>
                                            </div>
                                        </div>
                                        
                                        <form role="form" action="<?php echo base_url(); ?>adminlogin/add_grant_main/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Description <span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                         
                                                        <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $result['id']; ?>"> 
                                                          
                                                        <textarea rows="3" cols="6" class="form-control" 
                                                                  placeholder="Enter Description"
                                                                  name="description"><?php echo $result['description']; ?></textarea>                                                                                          
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6" style="text-align: right">
                                                    <div style="text-align: right;">

                                                    <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                            value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="box-title">
                                                    <h4>Meta Data</h4>
                                                </div>
                                                <div class="control-label col-md-12">
                                                    <table style=" width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Grant Id : </th>
                                                                <th>Grant Name : </th>
                                                                <th>Creator/Author : </th>
                                                                <th>Copyright : </th>
                                                                <th><div class="col-md-3">    <a  class="button btn btn-primary start add" style="display:inline-block;" onclick="addMoreRows(this.form,<?php echo $bookallow; ?>);" />Add More</a> </div></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody id="addedRows">
                                                            <tr>
                                                                <td><input type="text" class="form-control" placeholder="Enter Grant Id"  name="grant_id[]"> </td>
                                                                <td><input type="text" class="form-control" placeholder="Enter Grant Name"  name="grant_name[]"></td>
                                                                <td><input type="text" class="form-control" placeholder="Enter Creator/Auther"  name="auther[]"></td>
                                                                <td><input type="text" class="form-control" placeholder="Enter Copyright"  name="copyright[]"></td>
                                                                <td></td>

                                                            </tr>
                                                        </tbody>
                                                    </table>


                                                </div>
                                            </div>
                                            </br> <br>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Keyword </label>
                                                    <div class="col-md-8">
                                                         
                                                        <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $result['id']; ?>"> 
                                                          
                                                        <input type="text" class="form-control" 
                                                                               placeholder="Enter Keyword"
                                                                               value="<?php echo $result['copyright']; ?>">                                                                                  
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6" style="text-align: right">
                                                    <div class=" col-md-3" style="text-align: left;">
                                                        <a  class="button btn btn-primary start add" style="display:inline-block;" onclick="addkeyRows(this.form,<?php echo $bookallow; ?>);" />Add Keyword</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="addedkey" >  
                                             
                                            </div>
                                            
                                             <br>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="box-title">
                                                    <h4>Upload Scanned Copy</h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Add Files Here </label>
                                                    <div class="col-md-8">
                                                         
                                                        <input name="image" type="file"
                                                            class="file-input" value="<?php echo $result['user_image']; ?>" >                                                                                  
                                                    </div>
                                                </div>
                                            </div>
                                             
                                             <br>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="box-title">
                                                    <h4>Work Flow </h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-12">
                                                    <table style=" width: 100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Select Group : </th>
                                                                <th>Select User : </th>
                                                                <th><div class="col-md-3">    <a  class="button btn btn-primary start add" style="display:inline-block;" onclick="addMoreuser(this.form,<?php echo $bookallow; ?>);" />Add user</a> </div></th>

                                                            </tr>
                                                        </thead>
                                                        <tbody id="addedflow">
                                                            <tr>
                                                                <td>
                                                                    <select name="group[]" class="form-control" onchange="sss(this.value, '1')">
                                                                        <option>Select Group</option>
                                                                        <?php
                                                                        $grp = $this->db->get('dms_role')->result_array();
                                                                        foreach ($grp as $row) {
                                                                         ?>
                                                                         <option value="<?php echo $row['role_id']; ?>" <?php
                                                                         if ($result['role_id'] == $row['role_id']) {
                                                                             echo "selected";
                                                                         }
                                                                         ?> ><?php echo $row['role_name']; ?></option>
                                                                             <?php } ?>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <select name="user[]" id="user1" class="form-control">
                                                                            <option>Select User</option>
                                                                            
                                                                 </select>
                                                                </td>
                                                                <td></td>

                                                            </tr>
                                                        </tbody>
                                                    </table>


                                                </div>
                                            </div>
                                            
                                             <br>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="box-title">
                                                    <h4>Upload Additional Detail</h4>
                                                    <div class="tools hidden-xs">
                                                        
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Add Files Here </label>
                                                    <div class="col-md-8">
                                                         
                                                        <input name="image" type="file"
                                                            class="file-input" value="<?php echo $result['user_image']; ?>" >                                                                                  
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">comments </label>
                                                    <div class="col-md-8">
                                                         
                                                        <textarea rows="3" cols="6" class="form-control" 
                                                                  placeholder="Enter Comments"
                                                                  name="comment"><?php echo $result['comment']; ?></textarea>                                                                              
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="demo">
                
            </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>

    <script>   
        function year()
        {
            var cat = document.getElementById('e3').value;
            var id = document.getElementById('status').value;
           if(id){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/yearDropdown",
                    type: "POST",
                    data: {'status': id, 'category': cat},
                    success: function (response)
                    {
                        $("#years").html(response);

                    }

                });
            }
        }
        
        function template()
        {
            //alert("aa");
           var temp = document.getElementById('temp_type').value;
           alert(temp);
           // var id = document.getElementById('status').value;
           if(temp){
                $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/template",
                    type: "POST",
                    data: {'temp_id': temp},
                    success: function (response)
                    {
                        $("#demo").html(response);
                        //alert(response);

                    }

                });
            }
        }
        
         function sss(id, e)
        {
            //alert(e);
            $.ajax({
                    url: "<?php echo base_url(); ?>adminlogin/getuser",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                        $("#user"+e).html(response);
                        //alert(response);

                    }

                });
        }
        

</script>
<script>
    $(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
//});
//
//$(function() {
    $('#datepicker2').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
    
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });

</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function contect()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>
    <script>
window.rowCount = 1;
    function addMoreRows(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCount + '"><td><input type="text" class="form-control" placeholder="Enter Grant Id"  name="grant_id[]"> </td><td><input type="text" class="form-control" placeholder="Enter Grant Name"  name="grant_Name[]"></td><td><input type="text" class="form-control" placeholder="Enter Creator/Auther"  name="auther[]"></td><td><input type="text" class="form-control" placeholder="Enter Copyright"  name="copyright[]"></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></td>';


            jQuery('#addedRows').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>
<script>
window.rowCount = 1;
    function addkeyRows(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<div class="row" id="rowCount' + rowCount + '" style="margin-top:5px;"><div class="control-label col-md-6" ><label  class="control-label col-md-4"><span style="visibility:hidden;">Keywords</span></label><div class="col-md-5"><input type="text" class="form-control mng" id="i'+ rowCount +'" name="keyword[]"  value="" placeholder="Enter Keywords" ></div><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';


            jQuery('#addedkey').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>
<script>
window.rowCount = 1;
    function addMoreuser(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
  //var recRow = '<div class="col-sm-12" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control mng" data-validation="length"  id="i'+ rowCount +'" data-validation-length=min1" data-validation-error-msg="room name is required" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

    var recRow ='<tr id="rowCount' + rowCount + '"><td><input type="text" name="grant_id[]"></td><td><input type="text" name="grant_id[]"></td><td><div class="col-md-3"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></td>';


            jQuery('#addedflow').append(recRow);
        
        


    }
    
    
    
    
    
    
    
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>






