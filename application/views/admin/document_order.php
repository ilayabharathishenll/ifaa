<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
           <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">  
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>-->
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                        <div class="col-md-12">
                                <!-- BOX -->
                            <div class="box border primary">
                            <div class="box-title">
                                <h4>Document Order </h4>
                                <div class="tools">
                                    <a href="javascript:;" class="remove">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                <table class="table table-striped mytable">
                                    <thead>
                                    <th style="width:15%;">S.No </th>
                                    <th style="width:15%;">Document Name </th>
<!--                                    <th style="width:15%;">Parent Name </th>-->

<!--                                    <th style="width:12%;">Action </th>-->
                                    </thead>
                                    <tbody id="pri" class="tb">
                                        <?php
//                                        $quer = "SELECT * FROM library where creator=" . $id . " ORDER BY `library`.`order` ASC";
//                                        $query = $this->db->query($quer);
//                                        $data1 = $query->result_array();
                                        $i = 1;
                                        foreach ($data as $lib) {
//     $this->db->set('order', $i);
//        $this->db->where('library_id', $lib['library_id']);
//        $this->db->update('library');
                                            $gr = $this->db->get_where('dms_document_template', array('document_temp_id' => $lib['document_temp_parent']))->row_array();
                                            ?>
                                            <tr id="<?php echo $lib['document_temp_id']; ?>"  class="default">
                                                <td style="width:15%;"><?php echo $i; ?></td>
                                                <td style="width:15%;"><?php echo ucwords($lib['document_temp_name']); ?></td>
<!--                                                <td style="width:15%;"><?php echo $gr['document_temp_name']; ?> </td>-->
<!--                                                <td style="width:12%;"><?php echo $lib['date']; ?></td>
                                                <td style="width:12%;"><input type="text" style="text-align:center;width:80px;color:white;background-color:#<?php echo $lib['color']; ?>; " disabled=""  value="<?php //echo $lib->color;   ?>"></td>
                                                 <td style="width:12%;"><?php echo $lib['access_type']; ?></td>
                                                <td style="width:12%;">-->
                                                    <?php 
//                                               if($lib['user_id']){
//                                                $subscriber = $this->db->get_where("library", array('user_id' => $lib['user_id']))->result_array(); 
//                                                echo count($subscriber);
//                                               }
                                                 //$subscriber = $this->db->get_where('library',array('name'=>$lib['name'],'subscribe_status'=>"subscribe"))->result_array();
                                                 //echo $this->db->last_query();
                                                // echo count($subscriber);
                                                ?>
<!--                                                </td> -->
<!--                                                <td style="width:15%;"><?php echo $lib['status']; ?></td>-->
<!--                                                <td style="width:12%;"> <a href="<?php echo base_url(); ?>index.php/Dashboard/edit_library/<?php echo $lib['library_id']; ?>" class="editbut" >EDIT</a>/
                                                    <a class="editbut" href="javascript:;" onclick="return deletedata(<?php echo $lib['library_id']; ?>);" >DELETE</a>
                                                </td>-->
                                                <!--<th style="width:15%;"><a href="<?php echo base_url(); ?>index.php/Dashboard/email/<?php echo $lib['library_id']; ?>" class="editbut">INVITE USERS</a> </th>-->  
<!--                                                <td><input style="margin: 4px 4px 0;float:right" type="checkbox" id="chk" class="case" name="userchkbox[]" value="<?php echo $lib['library_id']; ?>"></td>-->

        <!--href="<?php echo base_url(); ?>Dashboard/delete_library/<?php echo $lib->library_id; ?>"-->
                                            </tr>

                                            <?php $i++;
                                        } ?>

                                    </tbody>

                                </table>

                            </div>
                            </div>
                            </div>
                                <!-- /BOX -->
                        </div>
                </div>
                <!-- /DASHBOARD CONTENT -->
                
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );


//$(function () {   
//    $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd'}).val();
// $("#datepicker1").datepicker({
//     dateFormat: 'yy-mm-dd',
//     minDate: new Date(),
//    
//});
//});


</script>
<script type="text/javascript">
$(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
</script>
  <script type="text/javascript">
            $('#pri').sortable();
        </script>
<script>
            $(document).ready(function(){
            $(document).on("mouseout", function(){
            //$(document).on('click','#save-reorder',function(){
            //alert("Deff");return false;
            var list = new Array();
            $('#pri').find('.default').each(function(){
            var id = $(this).attr('id');
            console.log(id);
            list.push(id);
            });
            var data = JSON.stringify(list);
            $.ajax({
            url: '<?php echo base_url(); ?>adminlogin/get_records', // server url
                    type: 'POST', //POST or GET 
                    data: {token:'reorder', data:data}, // data to send in ajax format or querystring format
                    datatype: 'json',
                    success: function(response) {
                    //alert(response);
                    }

            });
            });
            });
        </script>