<?php $row=$banner[0];?>

<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
              <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url(); ?>adminlogin/index">Home</a>
                                </li>	
                                <li><?php echo $page_name; ?></li>
                                <li><?php echo $page_title; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>

                            </div>
                            <!--									<div class="description">Blank Page</div>-->
                        </div>
                    </div>
                </div>
          <!-- PAGE HEADER-->
         
            <div class="row">
                    <div class="col-md-6">
                     
                    </div>
            </div>
                <?php if($this->session->flashdata('flash_message'))
                {
                ?>
                        <div class="alert alert-block alert-success fade in">
                                <a class="close" data-dismiss="alert" href="javascript:;" aria-hidden="true">&times;</a>
                                        <p><h4> Successful!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>
										
									
			<?php } ?>	
			
			<?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
						<div class="alert alert-block alert-danger fade in">
											<a class="close" data-dismiss="alert" href="javascript:;" aria-hidden="true">&times;</a>
												<p><h4><i class="fa fa-exclamation-circle"></i> Warning</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
										</div>
										
									
			<?php } ?>				
					
						<!-- /SAMPLE BOX CONFIGURATION MODAL FORM-->
						
						<!-- DATA TABLES -->
						<div class="row">
							<div class="col-md-12">


											  
                                                    <div class="tab-content">
                                                        <div class="box border primary">
                    
                                                            <div class="box-title">
                                                                    <h4><i class="fa fa-table"></i>Edit Banner</h4>
                                                                    <div class="tools hidden-xs">

                                                                            </a>

                                                                    </div>
                                                            </div>
                                                        <div style="padding-top:30px;">
                                                                <?php echo form_open_multipart('adminlogin/editbanner/'.$formAction , array('id' => 'usersForm', 'class' => 'form-horizontal','enctype' => "multipart/form-data")); ?>
								<div class="col-md-8" style="padding-top:30px;"> 
								
                                                                    <input type="hidden" name="id" value="<?php echo $row['id'];?>">			
                                        <div class="form-group">
                                                <label class="control-label col-md-4"><?php echo 'Banner ';?><span class="required">*</span></label>
                                                <div class="col-md-6">
                                                    <?php if($row['banner'] == ''){?>
                                                    <input type="file"  name="banner" data-validation="required" accept="image/*"/>
                                                    <?php } else{?>
                                                    <input type="file"  name="banner" accept="image/*"/>
                                                    <?php }?>
                                                    <br>
                                                    <img src="<?php echo base_url();?>uploads/<?php echo $row['banner'];?>" style ="width:150px; height:100px;" >
                                                <!--<span class="error-span"></span>-->
                                                </div>
                                        </div>  
<!--                                        <div class="form-group">
                                                <label class="control-label col-md-4"><?php echo 'Status ';?><span class="required">*</span></label>
                                                <div class="col-md-6">
                                                      <select name="status" class="form-control" style="width:200px">

                                                    <option value="active"<?php if ($row['status'] == 'active') {
                    echo 'selected';
                } ?>>Active</option>
                                                    <option value="inactive"<?php if ($row['status'] == 'inactive') {
                    echo 'selected';
                } ?>>Inactive</option>
                                                </select>
                                                </div>
                                        </div>  -->
										
                                            </div>
	

                               <div class="form-actions clearfix">

                                  <div class="row">

                                     <div class="col-md-12">

                                        <div class="col-md-offset-3 col-md-9">

                                           

                                         <button type="submit" class="btn btn-primary" id="save"> <?php echo 'Update';?> <i class="fa fa-save"></i></button>
                                         <a onClick="header('header1');" href="<?php echo base_url().'adminlogin/banner_list'; ?>" ><button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button></a>
										 
                                        </div>

                                     </div>

                                  </div>

                               </div>
					
						 <?php echo form_close();?>
                         </div>
												 
                                                        </div>
												 
<!--   ******************persnal_detail ***************************** -->	
							 
</div>
                   
                   
                   
                   
                   <!-- /CONTENT-->
				</div>
			</div>
		</div>
      </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>

  $.validate({
    modules : 'location, date, security, file',
    onModulesLoaded : function() {
      $('#country').suggestCountry();
    }
  });
  
   function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if ( iKeyCode > 31 &&  iKeyCode > 57)
        return false;

    return true;
    }    
</script>
