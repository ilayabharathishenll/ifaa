<?php
error_reporting(~E_NOTICE);

if ($param1) {
    $result = $this->db->get_where('dms_document_template', array('document_temp_id' => $param1))->row_array();
//    $yer = $this->db->get_where('dms_cat_year', array('category_id' => $param1))->result_array();
//    $result2 = $this->db->get_where('dms_categorytype', array('categorytype_id' => $result1['status']))->row_array();
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
?>
<style>
   .moreadd
   {
       margin: 22px 0;
   }
    .paddinglake .col-md-4
    {
        padding:0 5px;
     
    }
     
    
</style>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>-->
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-8">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>adminlogin/add_Documents/<?php echo $formaction; ?>" method = "post">
                                            <div class="form-group">
                                                <label>Document Name</label><span style="color: red;margin-left: 3px;">*</span>
                                                <input type="hidden" class="form-control" 
                                                       name="hidden_id"  value="<?php echo $result['document_temp_id']; ?>" >
                                                <input type="text" class="form-control" data-validation="length" 
                                                       data-validation-length="min1" 
                                                       data-validation-error-msg="Document name is required"
                                                       name="name"  value="<?php echo $result['document_temp_name']; ?>" placeholder="Enter Name">
                                            </div>
                                            <div class="form-group">
                                                <label>Parent Document </label>
                                                <select name="parent" class="form-control"  onchange="orderNum(this.value)">
                                                    <option value="0">Select Parent Document</option>

                                                    <?php
                                                    foreach ($datas as $row) {
                                                        ?>
                                                        <option value="<?php echo $row['document_temp_id']; ?>"<?php
                                                        if ($result['document_temp_parent'] == $row['document_temp_id']) {
                                                            echo "selected";
                                                        }
                                                        ?>><?php echo $row['document_temp_name']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            
                                            <div class="form-group" id="orderNumber">
                                                <label> Order No</label><span style="color: red;margin-left: 3px;">*</span>
                                                <input type="text" class="form-control" data-validation="length" 
                                                       data-validation-length="min1" 
                                                       data-validation-error-msg="Order No is required"
                                                       name="document_order_id"  value="<?php echo $result['document_order_id']; ?>" placeholder="Enter Order Number">
                                            </div>
                                            

                                            
                                            <div style="text-align: right">
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        name="submit">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->

                                <!-- /BASIC -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BOX -->
                        <div class="box border primary">
                            <div class="box-title">
                                <h4>Document Template Details</h4>
                                <div class="tools">
                                    <a href="javascript:;" class="remove">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table id="example" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th> Name</th>
<!--                                                <th>Description</th>-->
                                                <th>Parent Document</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
$i = 1;
foreach ($data as $row) {
    $val = $this->db->get_where('dms_document_template', array('document_temp_id' => $row['document_temp_parent']))->result_array();
    ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><?php echo ucwords($row['document_temp_name']); ?></td>
                                                    <td><?php echo $val[0]['document_temp_name']; ?></td> 
                                                    <td><a style="color:green" href="<?php echo base_url(); ?>adminlogin/show_documents/<?php echo $row['document_temp_id']; ?>/edit" title="Edit Record"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i></a>
                                                        /
                                                        <a style="color:red" href="javascript:;" title="Delete Record" onclick="confirm_modal('<?php echo base_url(); ?>adminlogin/add_documents/delete/<?php echo $row['document_temp_id']; ?>');"><i class="fa fa-trash-o fa-1x" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
    <?php
    $i++;
}
?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <!-- /BOX -->
                    </div>
                </div>
                </div>
        </div>
                <!-- /DASHBOARD CONTENT -->
                
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>
<script>

   
$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );

} );


function orderNum(e)
{
    if(e!='0')
    {
        $('#orderNumber').hide();
    }
    else
    {
        $("#orderNumber").show();
    }
}

</script>
<script>
window.rowCount = 1;
    function addMoreRows(frm, allowbook) {
//$('.add').click(function (event) {

         rowCount++;

        
            //alert(rowCount);
            // 
            var recRow = '<div class="row" style="margin-top:10px" id="rowCount' + rowCount + '"><div class="col-sm-4"><div class="form-group"><input type="text" class="form-control mng" id="i'+ rowCount +'" required="" name="year[]"  value="" placeholder="Enter Year"></div> <p class="sectionerror' + rowCount + '"></div><div class=" col-md-4"><div class="form-group"><select name="type[]" required="" class="form-control" ><?php foreach ($data1 as $row) { ?> <option value="<?php echo $row['categorytype_id']; ?>"<?php if ($result['categorytype_id'] == $row['categorytype_id']) { echo "selected"; }?>><?php echo $row['categorytype_name']; ?></option><?php } ?></select></div></div><div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeRow(' + rowCount + ');">Delete</a></div></div></div>';

            jQuery('#addedRows').append(recRow);
        
        


    }
    
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeRow(removeNum, y) {

         //alert(removeNum);
        
//alert(removeNum);
//removeNum = 0;
//jQuery('#rowCount'+removeNum).remove();
        jQuery('#rowCount' + removeNum).remove();
    }
</script>