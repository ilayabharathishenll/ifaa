<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('dms_grantee', array('grantee_id' => $param1))->row_array();
    $formaction = 'edit';
} else {
    $formaction = 'create';
}
 $userPermission = $this->session->userdata['login']['user_permission'];
?>
<style>
.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('pelogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>-->

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-3" style="height:400px">
                                        <form role="form" action="<?php echo base_url(); ?>adminlogin/exportDms/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" name="backup" id="usersForm1">
                                            
                                                
                                                <!--<div class="row" style="margin-bottom:10px">-->
<!--                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Add CSV<span style="color: red;margin-left: 1px">*</span></label>
                                                        <div class="col-md-8">

                                                           <input name="userfile" type="file"
                                                            class="file-input" style="font-size:11px;" value="" > 
                                                        </div>
                                                    </div>-->
                                                    
                                                    <!--</br>-->   
                                                    <!--</div>-->   
                                            <button type="submit" id="save" class="btn btn-primary start" style="width:120px" 
                                                     onclick="demo()"   value="Validate"   name="submit" > DMS BACKUP</button>    
                                                    
                                        </form> 
                                        </div>
                                        <div class="col-md-3" style="height:400px"><div class="loader" style="display: none"></div></div>
     <!------------------------------ Import Form ----------------------------------->
               <!--<form role="form" action="<?php echo base_url(); ?>adminlogin/importDms1/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" name="backup" id="usersForm1">-->
                                            
                                                
<!--                                                <div class="row" style="margin-bottom:10px">
                                                    <div class="control-label col-md-6"> 
                                                        <label  class="control-label col-md-4">Add CSV<span style="color: red;margin-left: 1px">*</span></label>
                                                       <p class="control-label col-md-4">Browse Computer</p>
                                                        <div class="col-md-8">

                                                           <input name="import" type="file"
                                                            class="file-input" style="font-size:11px;" value="" > 
                                                        </div>
                                                    </div>
                                                    
                                                    </br>   
                                                    </div>   -->
<!--                                            <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" >Import Database</button>    -->
                                             </form>   
                                    
         <!----------------------- End import Form ----------------------------->
                                        <br><br><br><br><br><br>
                                            <div style="text-align: right">
                                                <br><br>
                                           <?php //  if (in_array("add_csv", $userPermission)){ ?>     
<!--                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" >Submit</button>-->
                                           <?php // } ?>
                                            </div>

                                            </div>
                                                               

                                        
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->
                               
                                    <!-- /BASIC -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>
    <script type='text/javascript'>
//        
//        function demo1()
//        {
//       var id=1;
//                $.ajax({
//                    url: "<?php echo base_url(); ?>adminlogin/exportDms",
//                    type: "POST",
//                    data: {'status': id},
//                    success: function (response)
//                    {
//                        $("#years").html(response);
//
//                    }
//
//                });
//   
//        }
//    /* attach a submit handler to the form */
//    $("#usersForm11").submit(function(event) {
//
//      /* stop form from submitting normally */
//      event.preventDefault();
//
//      /* get the action attribute from the <form action=""> element */
//      var $form = $( this ),
//          url = $form.attr( 'action' );
//
//      /* Send the data using post with element id name and name2*/
//      var posting = $.post( url, { name: $('#name').val(), name2: $('#name2').val() } );
//
//      /* Alerts the results */
//      posting.done(function( data ) {
//         $('.loader').show(); 
//        $("#save").attr("disabled", true);
//      });
//    });
</script>
<script>
    
    function demo(){
       $('.loader').show(); 
     // setTimeout($("#save").attr("disabled", true), 3000);
        //$("#save").attr("disabled", true);
    }

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );


//$(function () {   
//    $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd'}).val();
// $("#datepicker1").datepicker({
//     dateFormat: 'yy-mm-dd',
//     minDate: new Date(),
//    
//});
//});


</script>
<script type="text/javascript">
$(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
</script>