
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
<!--                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>                          
                            </div>-->
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                        <div class="col-md-12">
                                <!-- BOX -->
                                <div class="box border green">
                                        <div class="box-title">
                                                <h4><i class="fa fa-table"></i>Dynamic Data Tables</h4>
                                                <div class="tools hidden-xs">
                                                        <a href="#box-config" data-toggle="modal" class="config">
                                                                <i class="fa fa-cog"></i>
                                                        </a>
                                                        <a href="javascript:;" class="reload">
                                                                <i class="fa fa-refresh"></i>
                                                        </a>
                                                        <a href="javascript:;" class="collapse">
                                                                <i class="fa fa-chevron-up"></i>
                                                        </a>
                                                        <a href="javascript:;" class="remove">
                                                                <i class="fa fa-times"></i>
                                                        </a>
                                                </div>
                                        </div>
                                        <div class="box-body">
                                                <table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                                        <thead>
                                                                <tr>
                                                                        <th class="center hidden-xs">S.NO</th>
                                                                        <th class="center hidden-xs">Grantee Name</th>
                                                                        <th class="center hidden-xs">Grantee Email</th>
                                                                        <th class="center hidden-xs">Grantee City</th>
                                                                        <th class="center hidden-xs">Grantee Contact</th>
                                                                        <th class="center hidden-xs">Action</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                $i = 1;
                                                                foreach ($data as $row) {
                                                               // $val = $this->db->get_where('dms_role', array('role_id' => $row['user_role']))->result_array();
                                                            ?>
                                                                <tr class="gradeX">
                                                                        <td class="center hidden-xs"><?php echo $i; ?></td>
                                                                        <td class="center hidden-xs"><?php echo $row['grantee_name']; ?></td>
                                                                        <td class="center hidden-xs"><?php echo $row['grantee_email']; ?></td>
                                                                        <td class="center hidden-xs"><?php echo $row['grantee_city']; ?></td>
                                                                        <td class="center hidden-xs"><?php echo $row['grantee_phone']; ?></td>
                                                                        <td class="center hidden-xs">
                                                                            <a style="color:green" href="<?php echo base_url(); ?>adminlogin/show_add_grantee/<?php echo $row['grantee_id']; ?>/edit" title="Edit Record"><i class="fa fa-pencil-square-o fa-1x" aria-hidden="true"></i></a> /
                                                                            <a style="color:red" href="javascript:;" title="Delete Record" onclick="confirm_modal('<?php echo base_url(); ?>adminlogin/add_grantee/delete/<?php echo $row['grantee_id']; ?>');"><i class="fa fa-trash-o fa-1x" aria-hidden="true"></i></a>
                                                                        </td>
                                                                </tr>
                                                                <?php $i++; } ?>
                                                                
                                                               
                                                        </tbody>
                                                        <tfoot>
                                                                <tr>
                                                                        <th class="center hidden-xs">S.NO</th>
                                                                        <th class="center hidden-xs">User Name</th>
                                                                        <th class="center hidden-xs">User Email</th>
                                                                        <th class="center hidden-xs">User Role</th>
                                                                        <th class="center hidden-xs">Contact</th>
                                                                        <th class="center hidden-xs">Action</th>
                                                                </tr>
                                                        </tfoot>
                                                </table>
                                        </div>
                                </div>
                                <!-- /BOX -->
                        </div>
                </div>
                <!-- /DASHBOARD CONTENT -->
                
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );


//$(function () {   
//    $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd'}).val();
// $("#datepicker1").datepicker({
//     dateFormat: 'yy-mm-dd',
//     minDate: new Date(),
//    
//});
//});


</script>
<script type="text/javascript">
$(function() {
    $('#datepicker1').datepicker( {
        changeMonth: true,
        changeYear: true,
        changeDay: true,
        showButtonPanel: true,
        dateFormat: 'yy-mm-dd',
    });
});
</script>