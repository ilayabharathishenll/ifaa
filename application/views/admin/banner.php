<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css">
<style>
    .fullbox{
display:block !important;
}
    </style>
<!--<form name="form1" action="upload_category" method="post"  enctype="multipart/form-data"/>-->

<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url(); ?>adminlogin/index">Home</a>
                                </li>	
                                <li><?php echo $page_name; ?></li>
                                <li><?php echo $page_title; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>

                            </div>
                            <!--									<div class="description">Blank Page</div>-->
                        </div>
                    </div>
                </div>
                <!--                </form>-->
                <div class="box border primary fullbox">
                    <div class="box-title">
                        <h4><i class="fa fa-table"></i>Banner List</h4>

                    </div>										
                    <div class="box-body" id="my_model">
                        <?php if($this->session->flashdata('flash_message'))
                {
                ?>
                        <div class="alert alert-block alert-success fade in">
                                <a class="close" data-dismiss="alert" href="javascript:;" aria-hidden="true">&times;</a>
                                        <p><h4> Successful!</h4> <?php echo $this->session->flashdata('flash_message'); ?></p>
                        </div>
										
									
			<?php } ?>	
			
			<?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
						<div class="alert alert-block alert-danger fade in">
											<a class="close" data-dismiss="alert" href="javascript:;" aria-hidden="true">&times;</a>
												<p><h4><i class="fa fa-exclamation-circle"></i> Warning</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
										</div>
										
									
			<?php } ?>	
                      
                            <!--</form>-->
                            
                            <table id="example" class="datatable table table-striped table-bordered table-hover" cellspacing="0" width="100%">

                                <thead>
                                    <tr>
                                        <th>Sr. No.</th>
                                        <th>Banner</th>
<!--                                        <th>Status</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    foreach ($banner as $banner1) {
                                       
                                        $i++;
                                        ?>
                                        <tr>    
                                            <td><?php echo $i; ?> </td>
                                            <td><img src="<?php echo base_url();?>uploads/<?php echo $banner1['banner']; ?>" style="height:100px;width: 100px;"></td>
<!--                                             <td><?php echo $banner1['status']; ?> </td>-->
                                            <td>
                                                <span><a href="<?php echo base_url() . 'adminlogin/edit_banner/' . $banner1['id']; ?>"><input type="button" class="btn btn-primary" value="Edit"></a></span>  </td>
                                        </tr>
                                        <?php
                                    }
                                     
                                    ?>
                                </tbody>
                            </table>
                            </form>
                    </div>
                    <div class="modal fade" id="myModal" role="dialog">
                        
                    </div>
                </div>
   <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->


                            <script>
                                    function viewuser(id){
                                       
                                        $.ajax({
                                          url:'<?php echo base_url();?>adminlogin/viewuser',
                                          method:'post',
                                          data:{'id':id},
                                          success:function(data){
                                              console.log(data);
                                              $("#myModal").html(data);
                                          }
                                        });
                                    }
                            </script>
                            