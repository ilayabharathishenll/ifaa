<?php error_reporting(~E_NOTICE); ?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('Adminlogin'); ?>/dashboard">Home</a>
                                </li>

                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle; ?></h3>
                            </div>
                            <?php if ($this->session->flashdata('flash_message')) { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <a class="close" data-dismiss="alert" href="javascript:;" 
                                       aria-hidden="true">X</a>
                                    <h4><i class="fa fa-smile-o"></i> <?php
                                echo
                                $this->session->flashdata('flash_message');
                                ?>  <i class="fa fa-thumbs-up"></i></h4>
                                </div>
                                <? }
                                if($this->session->flashdata('permission_message')){ ?>
                                <div class="alert alert-block alert-danger fade in">
                                    <a class="close" data-dismiss="alert" href="javascript:;" 
                                       aria-hidden="true">X</a>
                                    <h4><i class="fa fa-frown-o"></i> <?php
                                    echo
                                    $this->session->flashdata('permission_message');
                                ?></h4>
                                </div>
                                <? }?>
                            </div>
                        </div>
                    </div>
                    <!-- /PAGE HEADER -->
                    <!-- DASHBOARD CONTENT -->
                    <div class="row">
                        <div class="col-md-12">

                            <div class="box border primary">
                                <div class="box-title">
                                    <h4>All Student fee notification</h4>
                                    <div class="tools">
                                        <a href="javascript:;" class="remove">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>S.No.</th>
                                                    <th>Student</th>
                                                    <th>Class</th>
                                                    <th>Section</th>
                                                    <th>Duration</th>
                                                    <th>Remaining / Over Days</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $balance = "SELECT * FROM `tblstudentfee`  ORDER BY `tblstudentfee`.`fee_id` ASC";
                                                $query = $this->db->query($balance);
                                                $data = $query->result_array();

                                                $newData = [];
                                                foreach ($data as $value) {
                                                    $abc = $value['fee_student'];
                                                    $newData[$abc] = $value;
                                                }

                                                $pagedata['data1'] = $newData;
                                                //echo "<pre>";
                                                //print_r( $pagedata['data1']);
                                                $i = 1;
if(count( $pagedata['data1'])>0){
                                                foreach ($pagedata['data1'] as $key => $row) {
                                                    $duration = $this->db->get_where('tblduration', array('duration_id' => $row['fee_duartion']))->row_array();
                                                    $class = $this->db->get_where('tblclassmaster', array('classmaster_id' => $row['fee_class']))->row_array();
                                                    $section = $this->db->get_where('tblsection', array('section_id' => $row['fee_section']))->row_array();
                                                    $student = $this->db->get_where('tblstudent', array('student_id' => $row['fee_student']))->row_array();
                                                    $duration_endmonth = $duration['end_month'];
                                                    $duration_notificationday = $duration['notification_day'];
                                                    $monthNum = date('Y-m-d');
                                                    $date = date('d');
                                                    $month1 = date('m');
                                                    if ($month1 < 10) {
                                                        $month = trim($month1, '0');
                                                    } else {
                                                        $month = $month1;
                                                    }

                                                    $month_number = date("n", strtotime($duration_endmonth));
                                                    $monthName = date("F", strtotime($monthNum));
                                                    $month_enddate = 30;

                                                    if ($duration_endmonth == $monthName) {
                                                        if ($duration_notificationday <= $date && $date <= $month_enddate) {
                                                            $count = $month_enddate - $date;
                                                            ?>
                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $student['student_name']; ?></td>
                                                                <td><?php echo $class['classmaster_name']; ?></td>
                                                                <td><?php echo $section['section_name']; ?></td>
                                                                <td><?php echo $duration['start_month'] . "-" . $duration['end_month'];
                                            ; ?></td>
                                                                <td><?php echo "Remaining day = " . $count; ?></td>
                                                            </tr>

                <?php
            }
        }

        if ($month_number < $month && $month <= 12) {


            if ($date >= 1) {
                ?>


                                                            <tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $student['student_name']; ?></td>
                                                                <td><?php echo $class['classmaster_name']; ?></td>
                                                                <td><?php echo $section['section_name']; ?></td>
                                                                <td><?php echo $duration['start_month'] . "-" . $duration['end_month'];?></td>
                                                                <td><?php echo "Over day = " . $count; ?></td>
                                                            </tr>



                                                        <?php
                                                        }
                                                    }
                                                    if($month_number==12 && $month==1){
    
    if($date>=1){                      
                  ?><tr>
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $student['student_name']; ?></td>
                                                                <td><?php echo $class['classmaster_name']; ?></td>
                                                                <td><?php echo $section['section_name']; ?></td>
                                                                <td><?php echo $duration['start_month'] . "-" . $duration['end_month'];?></td>
                                                                <td><?php echo "Over day = " . $count; ?></td>
                                                            </tr>


                                                    
                                                    <?php  }}     $i++;
}}
                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <!-- /DASHBOARD CONTENT -->
            </div><!-- /CONTENT-->
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
          
            {
                extend: 'print',
                text: 'Print Details',
                className: 'btn btn-primary start'
            },
            
        ]
    } );
} );</script>

