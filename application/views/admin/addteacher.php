<?php
error_reporting(~E_NOTICE);
if ($param1) {
    $result = $this->db->get_where('tblteacher', array('teacher_id' => $param1))->row_array();
    
    $earning = explode(",",$result['teacher_earning']);
    $earning_amount = explode(",",$result['teacher_earningAmount']);
    $deduction = explode(",",$result['teacher_deduction']);
    $deduction_amount = explode(",",$result['teacher_deductionAmount']);
    $tax = explode(",",$result['teacher_tax']);
    $tax_amount = explode(",",$result['teacher_taxAmount']);

    $formaction = 'edit';
} else {
    $formaction = 'create';
}
?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>
                                    <i class="fa fa-home"></i>
                                    <a href="<?php echo base_url('Adminlogin'); ?>/dashboard">Home</a>
                                </li>
                                <li><?php echo $page; ?></li>
                                <li><?php echo $pagetitle; ?></li>
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo $pagetitle;?></h3>
                            </div>

                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-warning fade in">
                        <a class="close" data-dismiss="alert" href="javascript:;" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                <!-- /PAGE HEADER -->
                <!-- DASHBOARD CONTENT -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BASIC -->
                                <div class="box border primary">
                                    <div class="box-title">
                                        <h4><i class="fa fa-bars"></i><?php echo $pagetitle; ?></h4>
                                        <div class="tools hidden-xs">
                                            <a href="javascript:;" class="remove">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="box-body big">
                                        <form role="form" action="<?php echo base_url(); ?>Adminlogin/addteacher/<?php echo $formaction; ?>" method = "post" enctype="multipart/form-data" id="usersForm1">
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Employee Name<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                         <input type="hidden" class="form-control" 
                                                               name="roleid" value="teacher"> 
                                                        <input type="hidden" class="form-control" 
                                                               name="hidden_id" value="<?php echo $result['teacher_id']; ?>"> 
                                                         <input type="hidden" class="form-control" 
                                                               name="pas" value="<?php echo $result['teacher_password']; ?>">    
                                                        <input type="text" class="form-control" data-validation="length" 
                                                               data-validation-length="min1" 
                                                               data-validation-error-msg="Employee name is required."
                                                               name="username" value="<?php echo $result['teacher_name']; ?>">                                                                                                  
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4"> Date of Birth<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" readonly="" id="datepicker1"  data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Date of birth is required."
                                                                   name="dob" value="<?php echo $result['teacher_dob']; ?>"  onchange="mno();" >
                                                        <p id="error1" style="color:red"></p>
    
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Gender<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-4">
                                                        <input type="radio" checked=""
                                                               name="gender" value="Male" <?php
                                                               if ($result['teacher_gender'] == 'Male') {
                                                                   echo 'checked';
                                                               }
                                                               ?>>  Male</div>
                                                    <div class="col-md-4">
                                                        <input type="radio"
                                                               name="gender" value="Female"  <?php
                                                               if ($result['teacher_gender'] == 'Female') {
                                                                   echo 'checked';
                                                               }
                                                               ?>>  Female
                                                    </div>

                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Email Id<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="email" class="form-control"  
                                                               name="email_id" value="<?php echo $result['teacher_email_id']; ?>" data-validation="email">
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if($formaction=='create'){ ?>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Password<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="password" class="form-control" data-validation="length" 
                                                               data-validation-length="min4" id="pass"
                                                               data-validation-error-msg="Password is required with minimum 4 characters."
                                                               name="password" value="<?php echo $result['teacher_password']; ?>">
                                                        
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Confirm Password<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="password" class="form-control" data-validation="length" 
                                                               data-validation-length="min1" 
                                                             
                                                               data-validation-error-msg=" Confirm Password is required with minimum 4 characters."
                                                               id="conpass"   name="confirm_password" onchange="return chkpass();" >
                                                   <div id="error" style="color:red;"></div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Address<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">         
                                                        <textarea rows="2" cols="6" class="form-control"
                                                                  name="address"><?php echo $result['teacher_address']; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label class="control-label col-md-4">Country<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <select name="country" id="country" class="form-control" onChange="abc123(this.value);" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Country is required.">
                                                            <option value="">select country</option>
                                                            <?php
                                                            foreach ($data1 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['country_id']; ?>" <?php
                                                                if ($result['teacher_country'] == $row['country_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?> ><?php echo $row['country_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">State<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">  
                                                               <?php if ($formaction == 'create') { ?>
                                                        <select name="state" id="state" class="form-control" onChange="abcd(this.value);" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="State is required.">
                                                            <option value="">select state</option>
                                                            <?php
                                                         
                                                            foreach ($data2 as $row) {
                                                            
                                                                ?>
                                                                <option value="<?php echo $row['state_id']; ?>" <?php
                                                                if ($result['teacher_state'] == $row['state_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['state_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>    
                                                               <?php } ?>
                                                         <?php if ($formaction == 'edit') { ?>
                                                            <select name="state" id="state" class="form-control" onChange="abcd(this.value);" data-validation="length" 
                                                               data-validation-length="min1" 
                                                               data-validation-error-msg="State is required.">
                                                                <option value="">select state</option>
                                                                <?php
                                                               $data21=  $this->db->get_where('tblstate', array('state_status' => 'active'))->result_array();
                                                                foreach ($data21 as $row) {
                                                                    ?>
                                                                    <option value="<?php echo $row['state_id']; ?>" <?php
                                                                    if ($result['teacher_state'] == $row['state_id']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row['state_name']; ?></option>
                                                                        <?php } ?>
                                                            </select>   
                                                            <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">City<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                               <?php if ($formaction == 'create') { ?>
                                                        <select name="city" id="city" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="City is required.">
                                                            <option value="">select city</option>
                                                            <?php
                                                            foreach ($data3 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['city_id']; ?>" <?php
                                                                if ($result['teacher_city'] == $row['city_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['city_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>   
                                                               <?php } ?>
                                                        <?php if ($formaction == 'edit') { ?>
                                                            <select name="city" id="city" class="form-control" data-validation="length" 
                                                               data-validation-length="min1" 
                                                               data-validation-error-msg="City is required.">
                                                                <option value="">select city</option>
                                                                <?php
                                                              $data31=  $this->db->get_where('tblcity', array('city_status' => 'active'))->result_array();
                                                                foreach ($data31 as $row) {
                                                                    ?>
                                                                    <option value="<?php echo $row['city_id']; ?>" <?php
                                                                    if ($result['teacher_city'] == $row['city_id']) {
                                                                        echo "selected";
                                                                    }
                                                                    ?>><?php echo $row['city_name']; ?></option>
                                                                        <?php } ?>
                                                            </select>   
                                                              <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Final Degree<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Final Degree is required."
                                                               name="final_degree" value="<?php echo $result['teacher_final_degree']; ?>">
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4"> Passout Year<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8"><input type="text" class="form-control" readonly="" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Passout Year is required."
                                                               id="datepicker4"  name="passout_year" value="<?php echo $result['teacher_final_degree_passout_year']; ?>"  >

                                                        
<!--                                                        <select name="passout_year" class="form-control">
                                                            <option value="">select year</option>
                                                            <?php
                                                            for ($i = 1990; $i <= 2017; $i++) {
                                                                ?>
                                                                <option value="<?php echo $i; ?>"<?php
                                                                if ($result['teacher_final_degree_passout_year'] == $i) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $i; ?></option>

                                                            <?php } ?>
                                                        </select>-->
                                                    </div>
                                                </div>                                              
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Division<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Division is required."
                                                               name="division" value="<?php echo $result['teacher_final_degree_division']; ?>">
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Contact No.<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        
                                                        
                                                        
                                                      <input type="text" class="form-control"  data-validation="number"  onkeypress="javascript:return isNumber(event)" id="mobileNumber"
                                                                              data-validation-error-msg="Contact number is required."    onChange="dilip();"      class="form-control"  name="contact_no" value="<?php echo $result['teacher_contact_no']; ?>"></div>
                                                                                   <div id="mobile" style="color:red;margin-left: 150px"></div>
                                                </div>                                              
                                            </div>
                                            <div class="row" style="margin-bottom:10px"> 
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Joinning Date<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" readonly="" data-validation="length" 
                                                                   data-validation-length="min1" 
                                                                   data-validation-error-msg="Joinning date is required."
                                                               id="datepicker2" name="joinning_date" value="<?php echo $result['teacher_joinning_date']; ?>">
                                                    </div>
                                                </div>
<!--                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">User Type</label>
                                                    <div class="col-md-8">
                                                        <select name="role" class="form-control">
                                                            <option value="">select role</option>
                                                            <?php
                                                            foreach ($data4 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['role_id']; ?> "<?php
                                                                if ($result['teacher_role_id'] == $row['role_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['role_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>-->
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Department<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <select name="department" class="form-control" data-validation="length" 
                                                               data-validation-length="min1" 
                                                               data-validation-error-msg="Department is required.">
                                                            <option value="">select department</option>
                                                            <?php
                                                            foreach ($data5 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['department_id']; ?>" <?php
                                                                if ($result['teacher_department_id'] == $row['department_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['department_name']; ?></option>
                                                                    <?php } ?>
                                                        </select> 
                                                    </div>
                                                </div>
                                            </div>
<!--                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Class</label>
                                                    <div class="col-md-8">
                                                        <select name="class" class="form-control" id="class" onChange="abc(this.value);" >
                                                            <option value="">select class</option>
                                                            <?php
                                                            foreach ($data6 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['classmaster_id']; ?> "<?php
                                                                if ($result['teacher_class_id'] == $row['classmaster_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['classmaster_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>   
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Section</label>
                                                    <div class="col-md-8">
                                                        <select name="section" class="form-control" id="section">
                                                            <option value="">select section</option>
                                                            <?php
                                                            foreach ($data7 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['section_id']; ?>" <?php
                                                                if ($result['teacher_section_id'] == $row['section_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['section_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>   
                                                    </div>
                                                </div>     
                                            </div>-->
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Designation<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <select name="designation" class="form-control" data-validation="length" 
                                                               data-validation-length="min1" 
                                                               data-validation-error-msg="Designation is required.">
                                                            <option value="">select designation</option>
                                                            <?php
                                                            foreach ($data8 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['designation_id']; ?> "<?php
                                                                if ($result['teacher_designation_id'] == $row['designation_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['designation_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>    
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Status</label>
                                                    <div class="col-md-8">
                                                        <select name="status" class="form-control">
                                                          
                                                            <option value="active"<?php
                                                            if ($result['teacher_status'] == 'active') {
                                                                echo 'selected';
                                                            }
                                                            ?>>active</option>
                                                            <option value="inactive"<?php
                                                            if ($result['teacher_status'] == 'inactive') {
                                                                echo 'selected';
                                                            }
                                                            ?>>inactive</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
<!--                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Earning</label>
                                                    <div class="col-md-8">
                                                        <input type="text"  class="form-control" onkeypress="javascript:return isNumber(event)"
                                                               name="earning" value="<?php echo $result['teacher_earning']; ?>">
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Deduction</label>
                                                    <div class="col-md-8">
                                                        <input type="text"  class="form-control" onkeypress="javascript:return isNumber(event)"
                                                               name="deduction" value="<?php echo $result['teacher_deduction']; ?>">
                                                    </div>
                                                </div>
                                            </div>-->
                                            <div class="row" style="margin-bottom:10px">
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Basic Salary<span style="color: red;margin-left: 1px">*</span></label>
                                                    <div class="col-md-8">
                                                        <input  class="form-control" type="text" data-validation="number"
                                                               name="basic_salary" value="<?php echo $result['teacher_basic_salary']; ?>">
                                                    </div>
                                                </div>
                                                <div class="control-label col-md-6"> 
                                                    <label  class="control-label col-md-4">Resume</label>
                                                    <div class="col-md-8">

                                                        <input type="file" class="file-input" 
                                                               name="image" value="<?php echo $result['teacher_jobresume']; ?>" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-bottom:10px">
                                                
                                                <?php if($formaction=='edit'){ ?>
                                                <div class="control-label col-md-6" id="terminate" > 
                                                    <label  class="control-label col-md-4">Termination Date</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" readonly=""
                                                               id="datepicker3" name="termination_date" value="<?php echo $result['teacher_termination_date']; ?>">
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                             
<!--                                        Salary information                        -->
                                                                
                                            <div style="text-align: right">
                                                
                                                 <a href="#demo" class="btn btn-primary start" data-toggle="collapse">Salary Information </a>
                                                
<!--                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>-->
                                            </div>
                                                                </br>
                                            
                                            <div id="demo" class="collapse">
<!--                                                <div id="rowEarn">-->
                                                    <div class="row" style="margin-bottom:10px" id="earndiv">
                                                        
                                                        <?php if ($param1) {
                                                            $i =1;
                                                          foreach($earning as $key => $value)
                                                          {
                                                            ?>
                                                        <div id="rowEarn<?php echo $i; ?>">
                                                        <div class="control-label col-md-6"> 
                                                            <label  class="control-label col-md-4">Earning</label>
                                                            <div class="col-md-8"><?php // echo $earning[$key]; ?>
                                                                <select name="earning[]" class="form-control earn" id="earning<?php echo $i;?>" fd="<?php echo $i;?>" onchange="onchangeEncome(this.value)">
                                                                    <option value="">select Earning</option>
                                                                    <?php
                                                                    foreach ($data9 as $row) {
                                                                        ?>
                                                                        <option value="<?php echo $row['earning_id']; ?> "<?php
                                                                        if ($row['earning_id'].' ' == $earning[$key]) {
                                                                            echo "selected";
                                                                        }
                                                                        ?>><?php echo $row['earning_name']; ?></option>
                                                                            <?php } ?>
                                                                </select> 
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="control-label col-md-3">
                                                            <label  class="control-label col-md-4">Earning Amount</label>
                                                             <div class="col-md-8">
                                                                <input type="text" class="form-control" id="earn<?php echo $i;?>" name="earning_amount[]" value="<?php echo $earning_amount[$key]; ?>">
                                                             </div>
                                                        </div>
                                                        <div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeEarn('<?php echo $i;?>');">Delete</a></div></div>
                                                        </div>
                                                        <?php $i++;
                                                        } } else {?>
                                                        
                                                        <div class="control-label col-md-6"> 
                                                            <label  class="control-label col-md-4">Earning</label>
                                                            <div class="col-md-8">
                                                                <select name="earning[]" class="form-control earn" id="earning1" fd="1" onchange="onchangeEncome(this.value)">
                                                                    <option value="">select Earning</option>
                                                                    <?php
                                                                    foreach ($data9 as $row) {
                                                                        ?>
                                                                        <option value="<?php echo $row['earning_id']; ?> "<?php
                                                                        if ($result['earning_id'] == $row['earning_id']) {
                                                                            echo "selected";
                                                                        }
                                                                        ?>><?php echo $row['earning_name']; ?></option>
                                                                            <?php } ?>
                                                                </select> 
                                                            </div>
                                                        </div>
                                                        

                                                        <div class="control-label col-md-3">
                                                            <label  class="control-label col-md-4">Earning Amount</label>
                                                             <div class="col-md-8">
                                                                <input type="text" class="form-control" id="earn1" name="earning_amount[]" value="<?php echo $result['earning_amoun']; ?>">
                                                             </div>
                                                        </div>
                                                        <?php } ?>

                                                    
                                                </div>
                                                <div class="control-label col-md-2" style=" float: right; margin-top: -50px">
                                                        <div class="col-sm-4"  id="addearn">

                                                            <a  class="button btn btn-primary start" style="float:left" onclick="addMoreEarn(this.form,<?php echo $bookallow; ?>);" />Add More Earning</a>    
                                                        </div>

                                                </div>
                                                
                                                <div id="rowDeduction">
                                                    <div class="row" style="margin-bottom:10px" id="deductdiv">
                                                        <?php if ($param1) {
                                                            $j =1;
                                                          foreach($deduction as $key => $value)
                                                          {
                                                            ?>
                                                        <div id="rowDeduction<?php echo $j; ?>">
                                                            <div class="control-label col-md-6"> 
                                                                <label  class="control-label col-md-4">Deduction</label>
                                                                <div class="col-md-8">
                                                                    <select name="deduction[]" class="form-control desco" id="discoun<?php echo $j;?>" fd="<?php echo $j;?>">
                                                                        <option value="">select Deduction</option>
                                                                        <?php
                                                                        foreach ($data10 as $row) {
                                                                            ?>
                                                                            <option value="<?php echo $row['deduction_id']; ?> "<?php
                                                                            if ($deduction[$key] == $row['deduction_id'].' ') {
                                                                                echo "selected";
                                                                            }
                                                                            ?>><?php echo $row['deduction_name']; ?></option>
                                                                                <?php } ?>
                                                                    </select> 
                                                                </div>
                                                            </div>
                                                            <div class="control-label col-md-3"> 
                                                                <label  class="control-label col-md-4">Deduction Amount</label>
                                                                 <div class="col-md-8">
                                                                    <input type="text" class="form-control"
                                                                       id="deductionA<?php echo $j; ?>" name="deduction_amount[]" value="<?php echo $deduction_amount[$key]; ?>">
                                                                 </div>
                                                            </div>
                                                            <div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeDes('<?php echo $j; ?>');">Delete</a></div></div>
                                                        </div>
                                                        <?php $j++;
                                                        } } else { ?>
                                                        <div id="rowDeduction1">
                                                        <div class="control-label col-md-6"> 
                                                            <label  class="control-label col-md-4">Deduction</label>
                                                            <div class="col-md-8">
                                                                <select name="deduction[]" class="form-control desco" id="discoun1" fd="1">
                                                                    <option value="">select Deduction</option>
                                                                    <?php
                                                                    foreach ($data10 as $row) {
                                                                        ?>
                                                                        <option value="<?php echo $row['deduction_id']; ?> "<?php
                                                                        if ($result['deduction_id'] == $row['deduction_id']) {
                                                                            echo "selected";
                                                                        }
                                                                        ?>><?php echo $row['deduction_name']; ?></option>
                                                                            <?php } ?>
                                                                </select> 
                                                            </div>
                                                        </div>
                                                        <div class="control-label col-md-3"> 
                                                            <label  class="control-label col-md-4">Deduction Amount</label>
                                                             <div class="col-md-8">
                                                                <input type="text" class="form-control"
                                                                   id="deductionA1" name="deduction_amount[]" value="<?php echo $result['deduction_amoun']; ?>">
                                                             </div>
                                                        </div>
                                                            </div>
                                                        <?php } ?>


                                                    </div>
                                                </div>
                                                <div class="control-label col-md-2"style=" float: right; margin-top: -50px">
                                                        <div class="col-sm-12"  id="adddeduction">

                                                            <a  class="button btn btn-primary start" style="float:left" onclick="addMoreDeduction(this.form,<?php echo $bookallow; ?>);" />Add More Deduction</a>    
                                                        </div>

                                                </div>
                                                
                                                
                                                    <div class="row" style="margin-bottom:10px" id="taxdiv">
                                                        <?php if ($param1) {
                                                            $k =1;
                                                          foreach($tax as $key => $value)
                                                          {
                                                            ?>
                                                        <div id="rowTax<?php echo $k; ?>">
                                                            <div class="control-label col-md-6"> 
                                                                <label  class="control-label col-md-4">Tax</label>
                                                                <div class="col-md-8">
                                                                    <select name="tax[]" class="form-control tax" id="tax<?php echo $k; ?>" fd="<?php echo $k; ?>">
                                                                        <option value="">select Tax</option>
                                                                        <?php
                                                                        foreach ($data11 as $row) {
                                                                            ?>
                                                                            <option value="<?php echo $row['tax_id']; ?> "<?php
                                                                            if ($tax[$key] == $row['tax_id'].' ') {
                                                                                echo "selected";
                                                                            }
                                                                            ?>><?php echo $row['tax_name']; ?></option>
                                                                                <?php } ?>
                                                                    </select> 
                                                                </div>
                                                            </div>
                                                            <div class="control-label col-md-3"> 
                                                                <label  class="control-label col-md-4">Tax Amount</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control"
                                                                       id="taxA<?php echo $k; ?>" name="tax_amount[]" value="<?php echo $tax_amount[$key]; ?>">
                                                                 </div>
                                                            </div>
                                                            <div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeTax('<?php echo $k; ?>');">Delete</a></div></div>
                                                        </div> 
                                                        <?php $k++;
                                                        } } else { ?>
                                                        
                                                        <div id="rowTax">
                                                            <div class="control-label col-md-6"> 
                                                                <label  class="control-label col-md-4">Tax</label>
                                                                <div class="col-md-8">
                                                                    <select name="tax[]" class="form-control tax" id="tax1" fd="1">
                                                                        <option value="">select Tax</option>
                                                                        <?php
                                                                        foreach ($data11 as $row) {
                                                                            ?>
                                                                            <option value="<?php echo $row['tax_id']; ?> "<?php
                                                                            if ($tax[$key] == $row['tax_id']) {
                                                                                echo "selected";
                                                                            }
                                                                            ?>><?php echo $row['tax_name']; ?></option>
                                                                                <?php } ?>
                                                                    </select> 
                                                                </div>
                                                            </div>
                                                            <div class="control-label col-md-3"> 
                                                                <label  class="control-label col-md-4">Tax Amount</label>
                                                                <div class="col-md-8">
                                                                    <input type="text" class="form-control"
                                                                       id="taxA1" name="tax_amount[]" value="<?php echo $result['tax_amoun']; ?>">
                                                                 </div>
                                                            </div>
                                                        </div> 
                                                        <?php } ?>

                                                       
                                                </div>
                                                    
                                                <div class="control-label col-md-2" style=" float: right; margin-top: -50px">
                                                        <div class="col-sm-12"  id="addtax">

                                                            <a  class="button btn btn-primary start" onclick="addMoreTax(this.form,<?php echo $bookallow; ?>);" />Add More Tax</a>    
                                                        </div>

                                                </div>
                                                
                                            <div style="text-align: right">
                                                
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        value="Validate"   name="submit" onclick="return chkpass();">Submit</button>
                                            </div>
                                            </div>
                                                               

                                        </form>
                                    </div>
                                </div>
                                <!-- /BASIC -->
                                <!-- BASIC -->
                               
                                    <!-- /BASIC -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /DASHBOARD CONTENT -->
                </div><!-- /CONTENT-->
            </div>
        </div>
    </div>
   
    <script>
//      $( window ).load(function(){
//    //alert("abcd");
//    <?php echo $result[teacher_country]; ?>
////    var c=document.getElementById('country');
////    alert(c);
//    
//    $.ajax({
//                url: "<?php echo base_url(); ?>AdminLogin/regionselect",
//             //url : "http://livesoftwaresolution.net/lms/Adminlogin/regionselect",
//                type: "POST",
//                data: {'id': <?php echo $result[teacher_country]; ?>},
//                success: function (response)
//                {
//                   // alert(response);
//                    $("#state").html(response);
//
//                }
//
//            });
//}); 
//        
        
        
        
        function abc123(id)
        {
            //alert(id);
            $.ajax({
                url: "<?php echo base_url(); ?>Adminlogin/regionselect",
             //url : "http://livesoftwaresolution.net/lms/Adminlogin/regionselect",
                type: "POST",
                data: {'id': id},
                success: function (response)
                {
                    $("#state").html(response);

                }

            });
        }
        function abcd(id)
        {
            //alert(id);
            $.ajax({
             // url : "http://livesoftwaresolution.net/lms/Adminlogin/regionselect1",
              url: "<?php echo base_url(); ?>Adminlogin/regionselect1",
            type: "POST",
            data: {'id': id},
            success: function (response)
            {
                $("#city").html(response);

            }

        });
    }
    $("country").select2({
        placeholder: "Select country"

    });
    $("#state").select2({
        placeholder: "Select state"

    });
    $("#city").select2({
        placeholder: "Select city"

    });

</script>
<script>
//    $(function () {
//        $("#datepicker1").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker2").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker3").datepicker({dateFormat: 'yy-mm-dd',maxDate: new Date()}).val();
//        $("#datepicker4").datepicker({dateFormat: 'yy',maxDate: new Date()}).val();
////
//    });
    $(document).ready(function() {
  
$( "#datepicker1,#datepicker3" ).datepicker({
changeMonth: true,
changeYear: true,
firstDay: 1,
dateFormat: 'yy-mm-dd',
    maxDate: '0'
    
})
$( "#datepicker4" ).datepicker({
changeMonth: true,
changeYear: true,
firstDay: 1,
dateFormat: 'yy',
    maxDate: '0'
    
})
$("#datepicker2" ).datepicker({
changeMonth: true,
changeYear: true,
firstDay: 1,
dateFormat: 'yy-mm-dd',
    maxDate: '0'
    
})

$( "#datepicker1" ).datepicker({ dateFormat: 'yy-mm-dd',maxDate: new Date() }).val();
$( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd',maxDate: new Date() }).val();
$( "#datepicker3" ).datepicker({ dateFormat: 'yy-mm-dd',maxDate: new Date() }).val();
$( "#datepicker4" ).datepicker({ dateFormat: 'yy'}).val();
   //end change function
}); //end re
</script>
<script>

    $.validate({
        modules: 'location, date, security, file',
        onModulesLoaded: function () {
            $('#country').suggestCountry();
        }
    });

    // Restrict presentation length
    $('#presentation').restrictLength($('#pres-max-length'));
    $.validate({
        modules: 'security',
        borderColorOnError: '#FFF',
        addValidClassOnAll: true
    });


</script>
<script>
 
    function calculateAge(birthMonth, birthDay, birthYear) {
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var currentMonth = currentDate.getMonth();
    var currentDay = currentDate.getDate(); 
    var calculatedAge = currentYear - birthYear;

    if (currentMonth < birthMonth - 1) {
        calculatedAge--;
    }
    if (birthMonth - 1 == currentMonth && currentDay < birthDay) {
        calculatedAge--;
    }
   // alert(birthMonth+'/'+birthDay+'/'+birthYear);
   if(calculatedAge>=18)
   {
        //document.getElementById("error1").innerHTML="Age is "+calculatedAge+" .Less than 18 years not allowed.";
         document.getElementById("error1").innerHTML=" ";
   }
   else{
       document.getElementById("error1").innerHTML="Age is "+calculatedAge+" .Age should not be less than 18  years.";
   }
   //alert(calculatedAge);
    return calculatedAge;
}

function mno(){
var currentDate1=new Date(document.getElementById("datepicker1").value);
 var currentYear1 = currentDate1.getFullYear();
    var currentMonth1 = currentDate1.getMonth()+1;
    var currentDay1 = currentDate1.getDate();
var age = calculateAge(currentMonth1, currentDay1, currentYear1);

//alert(age);

}
function isNumber(evt) {
                        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
                        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
                            return false;

                        return true;
                    }
                    function dilip()
                    {

                        var batch_id = document.getElementById('mobileNumber').value;
                        var data = batch_id.toString().length;

                        if (data > 9 && data <11  || data<1)
                        {
    document.getElementById('mobile').innerHTML = '';

                        } else
                        {
                            document.getElementById('mobile').innerHTML='Please Fill only 10 Digit';
                           // alert('Please Fill only 10 Digit');
                           
                        }

                    }
  
    </script>

<script>
    function chkpass(){
    var pass=document.getElementById("pass").value;
    var conpass=document.getElementById("conpass").value;
    if(pass!=conpass)
    {
        document.getElementById("error").innerHTML="password and confirm password not matched.";
    //alert('wrong');
    return false;
    }
    else
    {
        document.getElementById("error").innerHTML=" ";
        return true;
    }
    
    
}
</script>
<script>
            function abc(id)
            {
                $.ajax({
                    url: "<?php echo base_url(); ?>Adminlogin/classselect",
                //url : "http://livesoftwaresolution.net/lms/Adminlogin/classselect",
                    type: "POST",
                    data: {'id': id},
                    success: function (response)
                    {
                       //alert(response);
                        $("#section").html(response);

                    }

                });
            }
            
    $("class").select2({
        placeholder: "Select class"

    });
    $("#section").select2({
        placeholder: "Select section"

    });


</script>
<script>
   
    $('.earn').change(function()
    {
       var id = $(this).val();
        var fd = $(this).attr('fd');
        
        var options =[];
                if(fd!='1'){
                 $(".earn").each(function() {
                     options.push($(this).val());
                     var abc = $(this).val();
                     if(abc==id ){

                     }

                       });

                       var count = options.length;
                      count = count-1;
                       for(  var count1 = 0; count1 < count; count1++ ){
                           var abc11 = options[count1];
                           //alert(abc11);
                           if(id==abc11){
                               //alert("");
                                $('#earning'+fd).val('');
                                exit;
                           }
                       }

                       console.log(options.length);
                       }
       

        $.ajax({
                url: "<?php echo base_url(); ?>Adminlogin/encomeselect",
                type: "POST",
                data: {'id': id},
                success: function (response)
                {
                   //alert(response);
                    $('#earn'+fd).val(response);

                }

            });

    });  


    var rowEarn = 1;
    function addMoreEarn(frm, allowbook) {
//alert("aaa");

        var count = rowEarn++;
        //alert(rowCount);
                                        
            var recRow = '<div id="rowEarn' + rowEarn + '"><div class="control-label col-md-6"><label  class="control-label col-md-4">Earning</label><div class="col-md-8"><select name="earning[]" class="form-control earn" id="earning' + rowEarn + '" fd="'+ rowEarn +'" required="required"> <option value="">select Earning</option><?php  foreach ($data9 as $row) { ?><option value="<?php echo $row['earning_id']; ?> "<?php if ($result['earning_id'] == $row['earning_id']) { echo "selected"; }?>><?php echo $row['earning_name'];  ?></option><?php } ?></select></div></div> <div class="control-label col-md-3"><label  class="control-label col-md-4">Earning Amount</label><div class="col-md-8"><input type="text" class="form-control" id="earn' + rowEarn + '" name="earning_amount[]" value="<?php echo $result['earning_amount']; ?>"></div></div> <div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeEarn(' + rowEarn + ');">Delete</a></div></div></div>';

            jQuery('#earndiv').append(recRow);
            
            $('.earn').change(function()
            {
               var id = $(this).val();
               var fd = $(this).attr('fd');
               
                var options =[];
                if(fd!='1'){
                 $(".earn").each(function() {
                     options.push($(this).val());
                     var abc = $(this).val();
                     if(abc==id ){

                     }

                       });

                       var count = options.length;
                      count = count-1;
                       for(  var count1 = 0; count1 < count; count1++ ){
                           var abc11 = options[count1];
                           //alert(abc11);
                           if(id==abc11){
                               //alert("");
                                $('#earning'+fd).val('');
                                exit;
                           }
                       }

                       console.log(options.length);
                       }

                $.ajax({
                        url: "<?php echo base_url(); ?>Adminlogin/encomeselect",
                        type: "POST",
                        data: {'id': id},
                        success: function (response)
                        {
                          // alert(response);
                            $('#earn'+fd).val(response);

                        }

                    });

            });
    }
    
    function removeEarn(removeNum, y) {
    //alert(removeNum);

        jQuery('#rowEarn' + removeNum).remove();
    }
    
    
    
    $('.desco').change(function()
            {
               var id = $(this).val();
               var fd = $(this).attr('fd');
               //alert(id);
               
               var options =[];
                if(fd!='1'){
                 $(".desco").each(function() {
                     options.push($(this).val());
                     var abc = $(this).val();
                     if(abc==id ){

                     }

                       });

                       var count = options.length;
                      count = count-1;
                       for(  var count1 = 0; count1 < count; count1++ ){
                           var abc11 = options[count1];
                           //alert(abc11);
                           if(id==abc11){
                               //alert("");
                                $('#discoun'+fd).val('');
                                exit;
                           }
                       }

                       console.log(options.length);
                       }

                $.ajax({
                        url: "<?php echo base_url(); ?>Adminlogin/deductionselect",
                        type: "POST",
                        data: {'id': id},
                        success: function (response)
                        {
                           //alert(response);
                            $('#deductionA'+fd).val(response);

                        }

                    });

            });
    
    var rowdescount = 1;
     function addMoreDeduction(frm, allowbook) {
//alert("aaa");

        var count = rowdescount++;
                                       
            var recRow = '<div id="rowDeduction' + rowdescount + '"><div class="control-label col-md-6"><label  class="control-label col-md-4">Deduction</label><div class="col-md-8"><select name="deduction[]" class="form-control desco" id="discoun'+ rowdescount +'" fd="'+ rowdescount +'" required="required"> <option value="">select Deduction</option><?php  foreach ($data10 as $row) { ?><option value="<?php echo $row['deduction_id']; ?> "<?php if ($result['deduction_id'] == $row['deduction_id']) { echo "selected"; }?>><?php echo $row['deduction_name'];  ?></option><?php } ?></select></div></div> <div class="control-label col-md-3"><label  class="control-label col-md-4">deduct Amount</label><div class="col-md-8"><input type="text" class="form-control" id="deductionA'+ rowdescount +'" name="deduction_amount[]" value="<?php echo $result['deduction_amount']; ?>"></div></div> <div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeDes(' + rowdescount + ');">Delete</a></div></div></div>';

            jQuery('#deductdiv').append(recRow);
            
            $('.desco').change(function()
            {
                
               var id = $(this).val();
               var fd = $(this).attr('fd');
               
                var options =[];
                if(fd!='1'){
                 $(".desco").each(function() {
                     options.push($(this).val());
                     var abc = $(this).val();
                     if(abc==id ){

                     }

                       });

                       var count = options.length;
                      count = count-1;
                       for(  var count1 = 0; count1 < count; count1++ ){
                           var abc11 = options[count1];
                           //alert(abc11);
                           if(id==abc11){
                               //alert("");
                                $('#discoun'+fd).val('');
                                exit;
                           }
                       }

                       console.log(options.length);
                       }
//alert(id);
                $.ajax({
                        url: "<?php echo base_url(); ?>Adminlogin/deductionselect",
                        type: "POST",
                        data: {'id': id},
                        success: function (response)
                        {
                           //alert(response);
                            $('#deductionA'+fd).val(response);

                        }

                    });

            });
    }
    function removeDes(removeNum, y) {

        jQuery('#rowDeduction' + removeNum).remove();
    }
    
    
    
    $('.tax').change(function()
            {
               var id = $(this).val();
               var fd = $(this).attr('fd');
               
               var options =[];
                if(fd!='1'){
                 $(".tax").each(function() {
                     options.push($(this).val());
                     var abc = $(this).val();
                     if(abc==id ){

                     }

                       });

                       var count = options.length;
                      count = count-1;
                       for(  var count1 = 0; count1 < count; count1++ ){
                           var abc11 = options[count1];
                           //alert(abc11);
                           if(id==abc11){
                               //alert("");
                                $('#tax'+fd).val('');
                                exit;
                           }
                       }

                       console.log(options.length);
                       }
//alert(id);
                $.ajax({
                        url: "<?php echo base_url(); ?>Adminlogin/taxselect",
                        type: "POST",
                        data: {'id': id},
                        success: function (response)
                        {
                           //alert(response);
                            $('#taxA'+fd).val(response);

                        }

                    });

            });
    
    
    var rowtax = 1;
     function addMoreTax(frm, allowbook) {
//alert("aaa");

        var count = rowtax++;
                                        
            var recRow = '<div id="rowTax' + rowtax + '"><div class="control-label col-md-6"><label  class="control-label col-md-4">Tax</label><div class="col-md-8"><select name="tax[]" class="form-control tax" id="tax'+ rowtax +'" fd="'+ rowtax +'" required="required"> <option value="">select Tax</option><?php  foreach ($data11 as $row) { ?><option value="<?php echo $row['tax_id']; ?> "<?php if ($result['tax_id'] == $row['tax_id']) { echo "selected"; }?>><?php echo $row['tax_name'];  ?></option><?php } ?></select></div></div> <div class="control-label col-md-3"><label  class="control-label col-md-4">Tax Amount</label><div class="col-md-8"><input type="text" class="form-control" id="taxA'+ rowtax +'" name="tax_amount[]" value="<?php echo $result['tax_amount']; ?>"></div></div> <div class="col-sm-3"> <div class="form-group"><a href="javascript:void(0);" style="float:left;margin-left:5px;margin-bottom:0px" class="btn btn-primary" onclick="removeTax(' + rowtax + ');">Delete</a></div></div></div>';

            jQuery('#taxdiv').append(recRow);
            
            $('.tax').change(function()
            {
               var id = $(this).val();
               var fd = $(this).attr('fd');
               
               var options =[];
                if(fd!='1'){
                 $(".tax").each(function() {
                     options.push($(this).val());
                     var abc = $(this).val();
                     if(abc==id ){

                     }

                       });

                       var count = options.length;
                      count = count-1;
                       for(  var count1 = 0; count1 < count; count1++ ){
                           var abc11 = options[count1];
                           //alert(abc11);
                           if(id==abc11){
                               //alert("");
                                $('#tax'+fd).val('');
                                exit;
                           }
                       }

                       console.log(options.length);
                       }

                $.ajax({
                        url: "<?php echo base_url(); ?>Adminlogin/taxselect",
                        type: "POST",
                        data: {'id': id},
                        success: function (response)
                        {
                          // alert(response);
                            $('#taxA'+fd).val(response);

                        }

                    });

            });
    }
    

    var rel = '<?php echo $edit_rel; ?>';

    function removeTax(removeNum, y) {

        jQuery('#rowTax' + removeNum).remove();
    }
    
    
   function deleteRow(data)
   {
       //alert(data);
        if(confirm('Are you sure to delete this Floor?'))
        {
            $.ajax({
                 url: "<?php echo base_url(); ?>Hostel/deleteroom",
                 //url : "http://livesoftwaresolution.net/lms/Adminlogin/bookdelete",
                 type: "POST",
                 data: {'id': data},
                 success: function (response)
                 {
                     if(response=='1'){
                      location.reload();
                     }
                 }
            });
        }
   }
   
   
//    function isNumber(evt) {
//  //  alert();
//        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
//        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
//            return false;
//
//        return true;
//    }
 
// function roomcheck(data,key,id)
// {
//     //alert(key);
//     var options =[];
//     if(key!='01'){
//      $(".mng").each(function() {
//          options.push($(this).val());
//         //alert($(this).id());
//          var abc = $(this).val();
//         // alert(abc);
//         // alert(data);
//          if(abc==data ){
//         
//          }
//          
//            });
//            
//            var count = options.length;
//           count = count-1;
//            for(  var count1 = 0; count1 < count; count1++ ){
//                var abc11 = options[count1];
//                //alert(abc11);
//                if(data==abc11){
//                     $('#i'+key).val('');
//                }
//            }
//            
//            console.log(options.length);
//            }
//            
//          // alert(options);
////            if( $.inArray( $(this).val(), options ))
////              {
////                  alert('abc');
////              }
//     //alert(key);
//     //alert(data);
//     $.ajax({
//            url: "<?php echo base_url(); ?>Hostel/roomcheck",
//            //url : "http://livesoftwaresolution.net/lms/Adminlogin/bookdelete",
//            type: "POST",
//            data: {'datavalue': data},
//            success: function (response)
//            {
//                //alert(response);
//                if(response=='1'){
//                 $(".sectionerror"+key).html('already exist.');
//                 $('#submitbutton').prop('disabled', true);
//                 $("#addmore").hide();
//                 //location.reload();
//                }
//                else{
//                    $(".sectionerror"+key).html('');
//                    $('#submitbutton').prop('disabled', false);
//                    $('#addmore').show();
//                }
//            }
//        });
//        
//     
// }
//   
 
</script>


