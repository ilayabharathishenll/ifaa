<?php

error_reporting(~E_NOTICE);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin extends CI_Controller {

    Public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin_model');
        
        
        
    }

    // Show login page
    public function index() {
        $this->load->view('login');
    }

    /* for admin login  */

    public function login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->admin_model->login($username, $password);
        //print_r($result); die;

        if($result) {
            if ($this->session->userdata['login']['user_role'] == '1')
            {
                $this->session->set_flashdata('flash_message', 'Login Successful');
                redirect('adminlogin/dashboard', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Login Successful');
                redirect('pelogin/dashboard', 'refresh');
            }
//            elseif ($this->session->userdata['login']['user_role'] == '5')
//            {
//                $this->session->set_flashdata('flash_message', 'login Successful');
//                redirect('pelogin/dashboard', 'refresh');
//            }
        } else {
            $this->session->set_flashdata('permission_message', 'Username and Password is invalid');
            // redirect($_SERVER[HTTP_REFFER]);
            redirect('admin', 'refresh');
        }
    }

    /* for admin logout */

    public function logout() {
        $this->session->unset_userdata(); 
        $this->session->sess_destroy();
        $this->session->set_flashdata('flash_message', 'Successfully Logout');
        redirect('admin', 'refresh');
    }
    
//    public function user() {
//        echo "hhhh"; die;
//        //$this->load->view('login');
//    }

}
