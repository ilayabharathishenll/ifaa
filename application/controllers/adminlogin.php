<?php
////Comment:-
// //Author is lazlo software solution private limited
// CREATED DATE:-
// Contact Details for any query:-
// Website:www.lazlosoftwaresolution.com
// eMail:info@lazlosoftwaresolution.com
// Overview about the page:-->
//<!--<--- closed comment----> 
error_reporting(~E_NOTICE);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class adminLogin extends CI_Controller {

    public $data1;

    Public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('admin_model');
        $this->load->model('csv_model');
           set_time_limit(0);
        if ($this->session->userdata['login']['user_role'] == '') {
            $this->session->set_flashdata('flash_message', 'login_again');
            redirect('admin', 'refresh');
        }
    }

// Show admin dashboard page
    public function dashboard($param1 = '') {
        //echo "sss"; die;
        $pagedata['pagetitle']="Dashboard";
        
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/dashboard', $pagedata);
        $this->load->view('admin/footer');
    }
    
//for show  addrole page
    public function show_role($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Role';
        } else {

            $pagedata['pagetitle'] = 'Role detail';
        }
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_role')->result_array();
        //$pagedata['data1'] = $this->db->get_where('tblpermission', array('permission_status' => 'active'))->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/add_role', $pagedata);
        $this->load->view('admin/footer');
    }

//for add role record
    public function add_role($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("dms_role", array('role_name' => $id))->result_array();

        $data = array(
            'role_name' => $this->input->post('name'),
            'role_description' => $this->input->post('description'),
            'role_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'Role  name already exist.');
                redirect('Adminlogin/show_role', 'refresh');
            } else {
                $result = $this->db->insert('dms_role', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('adminlogin/show_role', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('adminlogin/show_role', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('role_id', $id);
            $this->db->update('dms_role', $data);
            $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
            redirect('adminlogin/show_role', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('role_id', $param2);
            $this->db->delete('dms_role');
            $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_role', 'refresh');
        }
    }
    
    //for User Detail
    public function show_user_detail($param1 = '', $param2 = '') {
        

            $pagedata['pagetitle'] = 'User Details';
        
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_user')->result_array();
        //$pagedata['data1'] = $this->db->get_where('dms_role', array('role_status' => 'active'))->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/user_detail', $pagedata);
        $this->load->view('admin/footer');
    }
    
    //for show  addrole page
    public function show_add_user($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit User';
        } else {

            $pagedata['pagetitle'] = 'Add User';
        }
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_user')->result_array();
        $pagedata['data1'] = $this->db->get_where('dms_role', array('role_status' => 'active'))->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/add_user', $pagedata);
        $this->load->view('admin/footer');
    }

    //for add  description
    public function discription($param1 = '', $param2 = '') {
        

         $pagedata['pagetitle'] = 'Add Description';
        
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_document_template')->result_array();
        //$pagedata['data1'] = $this->db->get_where('dms_role', array('role_status' => 'active'))->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/discription', $pagedata);
        $this->load->view('admin/footer');
    }
    
    public function add_discription()
    {
       // print_r($_POST);
        $data = array(
            'Document_temp_id' => $this->input->post('temp'),
            'description' => $this->input->post('description'),
            
        );
         $result = $this->db->insert('dms_discription', $data);
         if (!$result) {
            $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
            redirect('adminlogin/discription', 'refresh');
        } else {
            $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
            redirect('adminlogin/discription', 'refresh');
        }
    }


//for add role page
    public function add_user($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('email');
        $q = $this->db->get_where("dms_user", array('user_email' => $id))->result_array();

        $data = array(
            'user_name' => $this->input->post('name'),
            'user_email' => $this->input->post('email'),
            'user_password' => $this->input->post('password'),
            'user_dob' => $this->input->post('dob'),
            'user_address' => $this->input->post('address'),
            'user_contact' => $this->input->post('contact'),
            'user_role' => $this->input->post('role'),
            'user_status' => $this->input->post('status')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'User Email already exist.');
                redirect('adminlogin/show_add_user', 'refresh');
            } else {
                $result = $this->db->insert('dms_user', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('adminlogin/show_add_user', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('adminlogin/show_add_user', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('user_id', $id);
            $this->db->update('dms_user', $data);
            $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
             echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else if ($param1 == 'delete') {
            $this->db->where('user_id', $param2);
            $this->db->delete('dms_user');
            $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_add_user', 'refresh');
        }
    }


//for show  add category type page
    public function show_category_type($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Programme Type';
        } else {

            $pagedata['pagetitle'] = 'Programme Type';
        }
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_categorytype')->result_array();

        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/add_categorytype', $pagedata);
        $this->load->view('admin/footer');
    }

//for add category type record
    public function add_category_type($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("dms_categorytype", array('categorytype_name' => $id))->result_array();

        $data = array(
            'categorytype_name' => $this->input->post('name'),
            'categorytype_description' => $this->input->post('description'),
            'categorytype_status' => $this->input->post('status')
        );
        //print_r($data); die;
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'Permission  name already exist.');
                redirect('adminlogin/show_category_type', 'refresh');
            } else {
                $result = $this->db->insert('dms_categorytype', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('adminlogin/show_category_type', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('adminlogin/show_category_type', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('categorytype_id', $id);
            $this->db->update('dms_categorytype', $data);
            $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
            redirect('adminlogin/show_category_type', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('categorytype_id', $param2);
            $this->db->delete('dms_categorytype');
            $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_category_type', 'refresh');
        }
    }

//for show  add category type page
    public function show_category($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Programme';
            $pagedata['datas'] = $this->db->get_where('dms_category', array('category_id !=' => $param1))->result_array();
        } else {

            $pagedata['pagetitle'] = 'Create Programme';
            $pagedata['datas'] = $this->db->get('dms_category')->result_array();
        }
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        
         $pagedata['data1'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/add_category', $pagedata);
        $this->load->view('admin/footer');
    }

//for add category type record
    public function add_category($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $year = $this->input->post('year');
        $status = $this->input->post('type');
//        echo "<pre>";
//       print_r($_POST); die;
        $q = $this->db->get_where("dms_category", array('category_name' => $id))->result_array();
        
            $data = array(
                'category_name' => $this->input->post('name'),
                'parent_id' => $this->input->post('parent'),
                //'category_year' => $year[$i]
            );
            
            //print_r($data); die;
            if ($param1 == 'create') {
                if ($q) {
                    $this->session->set_flashdata('permission_message', 'Category  name already exist.');
                    redirect('adminlogin/show_category', 'refresh');
                } else {
                    $result = $this->db->insert('dms_category', $data);
                     $id = $this->db->insert_id();
//                     echo $id; die;
                     $i = 0;
                    foreach($year as $value)
                    {
                        $data2 = array(
                            'category_id' => $id,
                            'status' => $status[$i],
                            'year' => $year[$i]
                        );
                        $result = $this->db->insert('dms_cat_year', $data2);
                        $i++;
                    }
                    if (!$result) {
                       
                        $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                        redirect('adminlogin/show_category', 'refresh');
                    } else {
                        $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                        redirect('adminlogin/show_category', 'refresh');
                    }
                }
            } else if ($param1 == 'edit') {
                $id = $this->input->post('hidden_id');
                $this->db->where('category_id', $id);
                $this->db->update('dms_category', $data);
//                $this->db->where('category_id', $id);
//                $this->db->delete('dms_cat_year');
                
                if($year)
                {
                    $i = 0;
                    foreach($year as $value)
                    {
                        $data2 = array(
                            'category_id' => $id,
                            'status' => $status[$i],
                            'year' => $year[$i]
                        );
                        $result = $this->db->insert('dms_cat_year', $data2);
                        $i++;
                    }
                }
                  
                $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
                echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            }
            else if ($param1 == 'delete') {
                $this->db->where('category_id', $param2);
                $this->db->delete('dms_category');
                $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
                redirect('adminlogin/show_category', 'refresh');
            }
       
    }
    
    //for show  add Documents page
    public function show_documents($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Document';
            $pagedata['datas'] = $this->db->get_where('dms_document_template', array('document_temp_id !=' => $param1))->result_array();
        } else {

            $pagedata['pagetitle'] = 'Create Document';
            $pagedata['datas'] = $this->db->get('dms_document_template')->result_array();
        }
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_document_template')->result_array();
        
        $pagedata['data1'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/add_document', $pagedata);
        $this->load->view('admin/footer');
    }

//for add Document record
    public function add_documents($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("dms_document_template", array('document_temp_name' => $id))->result_array();
        
            $data = array(
                'document_temp_name' => $this->input->post('name'),
                'document_temp_parent' => $this->input->post('parent'),
                'document_order_id' => $this->input->post('document_order_id'),
            );
            
            if ($param1 == 'create') {
                if ($q) {
                    $this->session->set_flashdata('permission_message', 'Document name already exist.');
                    redirect('adminlogin/show_documents', 'refresh');
                } else {
                    $result = $this->db->insert('dms_document_template', $data);
                     $id = $this->db->insert_id();

                    if (!$result) {
                       
                        $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                        redirect('adminlogin/show_documents', 'refresh');
                    } else {
                        $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                        redirect('adminlogin/show_documents', 'refresh');
                    }
                }
            } else if ($param1 == 'edit') {
                $id = $this->input->post('hidden_id');
                $this->db->where('document_temp_id', $id);
                $this->db->update('dms_document_template', $data);

                $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
                echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            }
            else if ($param1 == 'delete') {
                $this->db->where('document_temp_id', $param2);
                $this->db->delete('dms_document_template');
                $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
                redirect('adminlogin/show_documents', 'refresh');
            }
       
    }
    
    public function delete_year($param1 = '', $param2 = '', $param3 = '')
    {
        if($param1 == 'delete') {
                $this->db->where('cat_id', $param2);
                $this->db->delete('dms_cat_year');
                $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
               echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }  
    
//for show  changepassword page
    public function show_changepassword($param1 = '') {
        $pagedata['pagetitle'] = 'Change Password';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_user')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/changepassword', $pagedata);
        $this->load->view('admin/footer');
    }

//for changepassword
    public function changepassword() {
        $oldpassword = $this->input->post('old_password');
        $newpassword = $this->input->post('new_password');
        $confirmpassword = $this->input->post('confirm_password');
        $data = array(
            'user_password' => $this->input->post('new_password')
        );
        $id = $this->session->userdata['login']['user_id'];
        $query = $this->db->get_where('dms_user', array('user_id' => $id))->result_array();

        if (($query[0]['user_password'] == $oldpassword) && $newpassword) {
            $this->db->where('user_id', $id);
            $this->db->update('dms_user', $data);
            $this->session->set_flashdata('flash_message', 'Update  password sucessfully.');
            redirect('adminlogin/show_changepassword', 'refresh');
        } else {
            if (($query[0]['user_password'] != $oldpassword)) {
                $this->session->set_flashdata('permission_message', 'Old password not matched.');
                redirect('adminlogin/show_changepassword', 'refresh');
            }
            $this->session->set_flashdata('permission_message', 'Old password and New password not matched.');
            redirect('adminlogin/show_changepassword', 'refresh');
        }
    }

//for show  profile page
    public function show_profile($param1 = '') {
//$this->session->userdata($userSessiondata);       
        $id = $this->session->userdata['login']['user_id'];
        $pagedata['pagetitle'] = 'Profile';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get_where('dms_user', array('user_id' => $id))->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/profile', $pagedata);
        $this->load->view('admin/footer');
    }

//for update profile
    public function profileupdate() {
        $id = $this->session->userdata['login']['user_id'];

        if ($_FILES['image']['name']) {

            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $file_tmp = $_FILES['image']['tmp_name'];
            $file_type = $_FILES['image']['type'];
            $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
            $expensions = array("gif", "jpg", "jpeg", "png");
            if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
            }
             $data['image'] = $file_name;
//            move_uploaded_file($file_tmp, "uploads/" . $file_name);       
//       move_uploaded_file($file_tmp, "../local_path/" . $file_name); // external directory.
              move_uploaded_file($file_tmp, "local_path/" . $file_name); 
           
        } else {

            $abc = $this->db->get_where("dms_user", array('user_id' => $id))->result_array();

            $file_name = $abc[0]['user_image'];
        }

        $emailid = $this->input->post('user_email');
        $username = $this->input->post('user_name');

        $data = array(
            'user_name' => $this->input->post('username'),
            'user_email' => $this->input->post('emailid'),
            'user_image' => $file_name
        );
        $this->db->where('user_id', $id);
        $this->db->update('dms_user', $data);
        $this->session->set_flashdata('flash_message', 'Profile Updated  sucessfully.');
        redirect('adminlogin/show_profile', 'refresh');
    }
    
     public function yearDropdown() {

       $status = $this->input->post('status');
        $category = $this->input->post('category');
        //echo $status; die;
        $cat = $this->db->get_where("dms_category", array('category_id' => $category))->result_array();
       $catName = $cat[0]['category_name'];
        if ($cat != '0') {
            $this->db->group_by('year');
            $data = $this->db->get_where("dms_cat_year", array('category_id' => $category, 'status' => $status))->result_array();

            echo '<option value="">Select Year</option>';
            $i = '1';
            foreach ($data as $row1) {
                echo '<option value="' . $row1['year'] . '">' . $row1['year'] .' </option>';
                $i++;
            }
        } else {

            echo '<option value="">Select Year</option>';
        }
    }
    
    
    //for show  add category type page
    public function show_grant_main_template($param1 = '', $param2 = '') {


       if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Grant Template';
            $pagedata['result'] = $this->db->get_where('dms_grant', array('id' => $param1))->row_array();
            //$pagedata['result'] = $this->db->get_where("dms_grant", array('id' => $_POST['id']))->row_array();
        } else {

            $pagedata['pagetitle'] = 'Create Grant Template';
            
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        
        }
        $pagedata['page'] = 'Grantee Master list';
        $pagedata['param1'] = $param1;
            //$pagedata['pagetitle'] = 'Grant Main Template';
        
        $this->db->group_by('category_name');
        $pagedata['data1'] = $this->db->get('dms_category')->result_array();
        $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
        $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/grand_main', $pagedata);
        $this->load->view('admin/footer');
    }
    
    //for show  add category type page
    public function view_grant_main_template($param1 = '', $param2 = '') {
       if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'View Grantee Master list';
            $pagedata['result'] = $this->db->get_where('dms_grant', array('id' => $param1))->row_array();
            //$pagedata['result'] = $this->db->get_where("dms_grant", array('id' => $_POST['id']))->row_array();
        } else {

            $pagedata['pagetitle'] = 'View Grantee Master list';
            
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        
        }
        $pagedata['page'] = 'Grantee Master list';
        $pagedata['param1'] = $param1;
            //$pagedata['pagetitle'] = 'Grant Main Template';
        
        $this->db->group_by('category_name');
        $pagedata['data1'] = $this->db->get('dms_category')->result_array();
        $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
        $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/view_grant', $pagedata);
        $this->load->view('admin/footer');
    }
    
    
    public function grantNoValidation()
    {
        //print_r($_POST);
       $result = $this->db->get_where('dms_grant', array('grant_number' => $_POST['grnt']))->row_array();
       if($result)
       {
           echo "yes";
       }
       else
       {
           echo "no";
       }
    }
    
   
    
    
    //for add Grant Main
    public function add_grant_main($param1 = '', $param2 = '', $param3 = '') {
        
        $userId = $this->session->userdata['login']['user_id'];
        $title = $_POST['title'];
        $value = $_POST['value'];
       if($_POST['language']){
        $language = implode(",",$_POST['language']);
       }  if($_POST['state']){
        $state = implode(",",$_POST['state']);
       }  if($_POST['geogrtaphical_area']){
        $geogrtaphical_area = implode(",",$_POST['geogrtaphical_area']);
       }  if($_POST['disciplinary_field']){
        $disciplinary_field = implode(",",$_POST['disciplinary_field']);
       }  if($_POST['grantee_possible_outcome']){
        $grantee_possible_outcome = implode(",",$_POST['grantee_possible_outcome']);
       }  if($_POST['deliverables']){
        $deliverables = implode(",",$_POST['deliverables']);
       }
        // die;
        //$year = explode("-",$_POST['start_date']);
        //echo $year[0];
        
       //die;
        
      $date = date("Y-m-d");
      
    $grantee_id =  $_POST['grantee_id'];   
    if ($param1 == 'create') {   
      if($_POST['grantee_name'] != '')
      {
          
            $id = $this->input->post('grantee_name');
            $q = $this->db->get_where("dms_grantee", array('grantee_name' => $id))->result_array();
            $data = array(
              'grantee_name' => $this->input->post('grantee_name'),
              'grantee_phone' => $this->input->post('phone'),
              'grantee_alternative_no' => $this->input->post('alternative'),
              'grantee_address1' => $this->input->post('address1'),
              'grantee_address2' => $this->input->post('address2'),
              'grantee_city' => $this->input->post('city'),
              'grantee_pin' => $this->input->post('pin'),
              'grantee_country' => $this->input->post('country'),
              'grantee_state' => $state,
              'deliverables' => $deliverables,
              'grantee_geogrtaphical_area' => $geogrtaphical_area,
              'grantee_disciplinary_field' => $disciplinary_field,
              'grantee_language' => $language,
              'grantee_possible_outcome' => $grantee_possible_outcome,
              'grantee_email' => $this->input->post('email'),
              'grantee_website' => $this->input->post('website'),
             
            );
            if ($param1 == 'create') {
                if ($q) {
                    $this->session->set_flashdata('permission_message', 'Grantee Name already exist.');
                    redirect('adminlogin/show_grant_main_template', 'refresh');
                } else {
                    $result = $this->db->insert('dms_grantee', $data);
                    $grantee_id = $this->db->insert_id();

                }
            }
        }
        else
        {
            $grantee_id =  $_POST['grantee_id'];
        }
    }
        
        
       // $id = $this->input->post('email');
        //$q = $this->db->get_where("dms_grant", array('user_email' => $id))->result_array();

        $data = array(
            'archiv_tag' => $this->input->post('archiv_tag'),
            'grant_number' => $this->input->post('grant_no'),
            'category_id' => $this->input->post('category'),
            'categorytype_id' => $this->input->post('cat_status'),
            'category_year' => $_POST['grant_year'],
            'grantee_name' => $grantee_id,
            'deliverables' => $deliverables,
              'grantee_geogrtaphical_area' => $geogrtaphical_area,
              'grantee_disciplinary_field' => $disciplinary_field,
              'grantee_language' => $language,
              'grantee_possible_outcome' => $grantee_possible_outcome,
            'grant_amount' => $this->input->post('grant_amount'),
             'created_by' => $userId,
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'extension' => $this->input->post('extension'),
            'grant_duration' => $this->input->post('grant_duration'),
            'program_executive' => $this->input->post('program_executive'),
            'description_type' => $this->input->post('program_types'),
            'description_sub_type' => $this->input->post('sub_types'),
            'approval_date' => $date,
            
            
            
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'User Email already exist.');
                redirect('adminlogin/show_grant_main_template', 'refresh');
            } else {
                $result = $this->db->insert('dms_grant', $data);
                $last_id = $this->db->insert_id();
                if($last_id)
                {
                     $datas = array(
                               'grant_temp_id' => $last_id,
                               'category_id' => $this->input->post('category'),
                               'status' => $this->input->post('cat_status'),
                               'year' => $_POST['grant_year']
                            );
                    
                    $this->db->insert('dms_cat_year', $datas);
                    
                    $i = 0;
                    if($title){
                    foreach($title as $valuess)
                    {
                        $data = array(
                               'grant_temp_id' => $last_id,
                               'grant_metadeta_title' => $title[$i],
                               'grant_metadeta_value' => $value[$i],
                                'deliverables' => $deliverables,
                            );
                        $result = $this->db->insert('dms_grant_metadeta', $data);
                        $i++;
                    }}
                }
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('adminlogin/show_grant_main_template', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('adminlogin/show_grant_main_template', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
          
            
            $id = $this->input->post('hidden_id');
            $this->db->where('id', $id);
            $this->db->update('dms_grant', $data);
            
            $data_grantee= array(
                'grantee_name' => $this->input->post('grantee_name'),
              'grantee_phone' => $this->input->post('phone'),
              'grantee_alternative_no' => $this->input->post('alternative'),
              'grantee_address1' => $this->input->post('address1'),
              'grantee_address2' => $this->input->post('address2'),
              'grantee_city' => $this->input->post('city'),
              'grantee_pin' => $this->input->post('pin'),
              'grantee_country' => $this->input->post('country'),
              'grantee_state' => $state,
              'deliverables' => $deliverables,
              'grantee_geogrtaphical_area' => $geogrtaphical_area,
              'grantee_disciplinary_field' => $disciplinary_field,
              'grantee_language' => $language,
              'grantee_possible_outcome' => $grantee_possible_outcome,
              'grantee_email' => $this->input->post('email'),
              'grantee_website' => $this->input->post('website')
             
            );
            
            $this->db->where('grantee_id', $_POST['grantee_id']);
            $this->db->update('dms_grantee', $data_grantee);
            
            $datas = array(
                               'grant_temp_id' => $id,
                               'category_id' => $this->input->post('category'),
                               'status' => $this->input->post('cat_status'),
                               'year' => $_POST['grant_year']
                            );
            
            $this->db->where('grant_temp_id', $id);
            $this->db->update('dms_cat_year', $datas);
            
            
            $this->db->where('grant_temp_id', $id);
            $this->db->delete('dms_grant_metadeta');
            
            $i = 0; if($title){
            foreach($title as $valuess)
                    {
                        $data = array(
                               'grant_temp_id' => $id,
                               'grant_metadeta_title' => $title[$i],
                               'grant_metadeta_value' => $value[$i],
                                'deliverables' => $deliverables,
                            );
                        $result = $this->db->insert('dms_grant_metadeta', $data);
                        $i++;
            }}
            
            $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            //redirect('adminlogin/show_add_user', 'refresh');
        } else if ($param1 == 'delete') {
            
            $this->db->where('id', $param2);
            $this->db->delete('dms_grant');
            $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_grant_main_template', 'refresh');
        }
    }
    
    
    //for show  a parent templat
    
    public function show_parent_template($param1 = '', $param2 = '') {
       

            $pagedata['pagetitle'] = 'Parent Template';
        
        $pagedata['page'] = 'Template Demo';
        $pagedata['param1'] = $param1;
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        $this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/parent_template', $pagedata);
        $this->load->view('admin/footer');
    }
    
    //for add Grant user
    public function add_parent_template($param1 = '', $param2 = '', $param3 = '') {
//        echo "<pre>";
//        print_r($_POST);
     if($_POST['keyword']){
            $key = implode(",",$_POST['keyword']);
     }
       // die;
        
      $date = date("Y-m-d");
        
        
       // $id = $this->input->post('email');
        //$q = $this->db->get_where("dms_grant", array('user_email' => $id))->result_array();

        $data = array(
            'grant_id' => $this->input->post('grant_id'),
            'category_id' => $this->input->post('category'),
            'categorytype_id' => $this->input->post('cat_status'),
            'category_year' => $this->input->post('years'),
            'grant_number' => $this->input->post('grant_no'),
            'grantee_name' => $this->input->post('grantee_name'),
            'type_of_document' => $this->input->post('state'),
            'digital_file_name' => $this->input->post('digitalfile_name'),
            'document_id' => $this->input->post('doc_id'),
            'handling_staf' => $this->input->post('handling_staff'),
            'language_original_material' => $this->input->post('language'),
            'medium_original_material' => $this->input->post('medium'),
            'page_number' => $this->input->post('page'),
            'physical_characteristics' => $this->input->post('physical'),
            'comment' => $this->input->post('comments'),
            'material_kept' => $this->input->post('kept'),
            'digital_copy_medium' => $this->input->post('copy'),
            'auther' => $this->input->post('auther'),
            'editor' => $this->input->post('editor'),
            'publisher' => $this->input->post('publisher'),
            'publication_place' => $this->input->post('publication_place'),
            'copyright' => $this->input->post('copyright'),
            'dimensions' => $this->input->post('dimension'),
            'creator_degital_copy' => $this->input->post('creator_digital_copy'),
            'hardware' => $this->input->post('hardware'),
            'software' => $this->input->post('software'),
            'description' => $this->input->post('description'),
            'date' => $this->input->post('date'),
            'keyword' => $key,
            'degital_copy_date' => $this->input->post('degital_copy_date'),
//            'assine' => $this->input->post('disciplinary_field'),
//            'Approval' => $this->input->post('language'),
//            'approved_by' => $this->input->post('grantee_possible_outcome'),
            'approval_date' => $date
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'User Email already exist.');
                redirect('adminlogin/show_parent_template', 'refresh');
            } else {
                $result = $this->db->insert('dms_grant', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('adminlogin/show_parent_template', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('adminlogin/show_parent_template', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {

            $id = $this->input->post('hidden_id');
            $this->db->where('id', $id);
            $this->db->update('dms_grant', $data);
            $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
            echo "<script>window.location=".$_SERVER['HTTP_REFERER']."</script>";
            //redirect('adminlogin/show_add_user', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('dms_grant');
            $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_parent_template', 'refresh');
        }
    }
    
    //for show  a child templat
    
    public function show_child_template($param1 = '', $param2 = '') {
       

            $pagedata['pagetitle'] = 'Child Template';
        
        $pagedata['page'] = 'Template Demo';
        $pagedata['param1'] = $param1;
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        $this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/child_template', $pagedata);
        $this->load->view('admin/footer');
    }
    
    //for show  a Individual templat
    
    public function show_individual_template($param1 = '', $param2 = '') {
       

        $pagedata['pagetitle'] = 'Individual Template';
        
        $pagedata['page'] = 'Template Demo';
        $pagedata['param1'] = $param1;
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        $this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/individual_template', $pagedata);
        $this->load->view('admin/footer');
    }
    
    
    //for User Detail
    public function show_grantee_detail($param1 = '', $param2 = '') {
        

            $pagedata['pagetitle'] = 'Grantee Detail';
        
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_grantee')->result_array();
        //$pagedata['data1'] = $this->db->get_where('dms_role', array('role_status' => 'active'))->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/grantee_detail', $pagedata);
        $this->load->view('admin/footer');
    }
    
    //for show  addrole page
    public function show_add_grantee($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Grantee';
        } else {

            $pagedata['pagetitle'] = 'Add Grantee';
        }
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_grantee')->result_array();
        //$pagedata['data1'] = $this->db->get_where('dms_role', array('role_status' => 'active'))->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/add_grantee', $pagedata);
        $this->load->view('admin/footer');
    }

//for add role record
    public function add_grantee($param1 = '', $param2 = '', $param3 = '') {
        $id = $this->input->post('name');
        $q = $this->db->get_where("dms_grantee", array('grantee_name' => $id))->result_array();

        $data = array(
            'grantee_name' => $this->input->post('name'),
            'grantee_phone' => $this->input->post('phone'),
            'grantee_alternative_no' => $this->input->post('alternative'),
            'grantee_area' => $this->input->post('area'),
            'grantee_address' => $this->input->post('address'),
            'grantee_city' => $this->input->post('city'),
            'grantee_state' => $this->input->post('state'),
            'grantee_pin' => $this->input->post('pin'),
            'grantee_country' => $this->input->post('country'),
            'grantee_geogrtaphical_area' => $this->input->post('geogrtaphical_area'),
            'grantee_disciplinary_field' => $this->input->post('disciplinary_field'),
            'grantee_language' => $this->input->post('language'),
            'grantee_possible_outcome' => $this->input->post('grantee_possible_outcome'),
            'grantee_email' => $this->input->post('email'),
            'grantee_website' => $this->input->post('website')
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'User Email already exist.');
                redirect('adminlogin/show_add_grantee', 'refresh');
            } else {
                $result = $this->db->insert('dms_grantee', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('adminlogin/show_add_grantee', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('adminlogin/show_add_grantee', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('grantee_id', $id);
            $this->db->update('dms_grantee', $data);
            $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            //redirect('adminlogin/show_add_grantee', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('grantee_id', $param2);
            $this->db->delete('dms_grantee');
            $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_add_grantee', 'refresh');
        }
    }
    public function demo()
    {
        $this->load->view('admin/demo');
    }
    
    public function template()
    {
        //echo 'hello';
        if($_POST['id'] == '1')
        {
            $pagedata['pagetitle'] = 'Grant Template';
            $this->load->view('admin/grant_copy_template', $pagedata);
        }
        elseif($_POST['id'] == '2')
        {
            $pagedata['doc'] = $_POST['doc'];
            $pagedata['pagetitle'] = 'Parent Template';
            $this->load->view('admin/parent_copy_template', $pagedata);
        }
        elseif($_POST['id'] == '5')
        {
            $pagedata['doc'] = $_POST['doc'];
            $pagedata['pagetitle'] = 'Parent Template for child';
            $this->load->view('admin/parent_copy_template1', $pagedata);
        }
        elseif($_POST['id'] == '8')
        {
            $pagedata['doc'] = $_POST['doc'];
            $pagedata['pagetitle'] = 'Parent Template for multiple record';
            $this->load->view('admin/parent_copy_template2', $pagedata);
        }
        elseif($_POST['id'] == '3')
        {
            $pagedata['pagetitle'] = 'Child Template';
            $this->load->view('admin/child_copy_template', $pagedata);
        }
        elseif($_POST['id'] == '4')
        {
            $pagedata['pagetitle'] = 'Individual Template';
            $this->load->view('admin/individual_copy_template', $pagedata);
        }
    }
    
    //for show  create template page
    public function show_create_template($param1 = '', $param2 = '') {
       
//$this->admin_model->fetchCategoryTree();
            $pagedata['pagetitle'] = 'Create Template';
        
        $pagedata['page'] = 'Template';
        $pagedata['param1'] = $param1;
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        $pagedata['data6'] = $this->db->get('dms_template_type')->result_array();
        $this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
         $pagedata['data3'] = $this->db->get('dms_document_template')->result_array();
         
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/create_template', $pagedata);
        $this->load->view('admin/footer');
    }
    
    //for show  create template page
    public function edit_create_template($param1 = '', $param2 = '') {
       
//$this->admin_model->fetchCategoryTree();
            $pagedata['pagetitle'] = 'Edit Template';
        
        $pagedata['page'] = 'Template';
        $pagedata['param1'] = $param1;
         $pagedata['data1'] = $this->db->get('dms_template_type')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/edit_assin_template', $pagedata);
        $this->load->view('admin/footer');
    }
    
    // get user by id
    public function getuser()
    {
        $id = $_POST['id'];
        $data = $this->db->get_where("dms_user", array('user_role' => $id))->result_array();
        if($data){
        echo '<option value="">Select User</option>';
            $i = '1';
            foreach ($data as $row1) {
                echo '<option value="' . $row1['user_id'] . '">' . $row1['user_name'] .' </option>';
                $i++;
            }
        } else {

            echo '<option value="">Select User</option>';
        }
    }


    //for Assin template
    public function add_create_template($param1 = '', $param2 = '', $param3 = '') {
      $date = date("Y-m-d");
      $group = $_POST['group'];
      $user = $_POST['user'];
      $steg = $_POST['steg'];
//      echo "<pre>";  
    //  print_r($_POST); die;
        $id = $this->input->post('temp');
        $q = $this->db->get_where("dms_assin_template", array('document_temp_id' => $id))->result_array();

        $data = array(
            'template_type_id' => $this->input->post('temp_type'),
            'document_temp_id' => $this->input->post('temp'),
            'assin_temp_step' => $this->input->post('step'),
            'work_flow' => $this->input->post('workflow'),
            'termination' => $this->input->post('termination'),
            'text_editor' => $this->input->post('text_editor'),
            'description' => $this->input->post('description'),
            'assin_date' => $date
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'Template  already exist.');
                redirect('adminlogin/show_create_template', 'refresh');
            } else {
                $result = $this->db->insert('dms_assin_template', $data);
                 $id = $this->db->insert_id();
                 
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('adminlogin/show_create_template', 'refresh');
                } else {
                if($group)  
                {
                   
                    $i = 0;
                    foreach($group as $val)
                    {
                       $data2 = array(
                           'assin_temp_id' => $id,
                           'role_id' => $group[$i],
                           'user_id' => $user[$i],
                           'step' => $steg[$i],
                           'workflow_status' => $this->input->post('workflow'),
                       );
                       $result = $this->db->insert('dms_workflow', $data2);

                       $i++;
                    }
                }
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('adminlogin/show_create_template', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('assin_temp_id', $id);
            $this->db->update('dms_assin_template', $data);
            
            if($group)  
            {
               
                $this->db->where('assin_temp_id', $id);
                $this->db->delete('dms_workflow');

                $i = 0;
                 foreach($group as $val)
                 {
                    $data2 = array(
                        'assin_temp_id' => $id,
                        'role_id' => $group[$i],
                        'user_id' => $user[$i],
                        'step' => $steg[$i],
                        'workflow_status' => $this->input->post('workflow'),
                    );
                    $result = $this->db->insert('dms_workflow', $data2);
                    
                    $i++;
                 }
            }
            
            $this->session->set_flashdata('flash_message', 'Updated  sucessfully.');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            //redirect('adminlogin/show_add_user', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('dms_assin_template');
            $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_create_template', 'refresh');
        }
    }
    
    
    // show assign template page Detail
    public function detail_create_template($param1 = '', $param2 = '') {
       

            $pagedata['pagetitle'] = 'Create Template Detail';
        
        $pagedata['page'] = 'Template';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_assin_template')->result_array();
//        $this->db->group_by('category_name');
//         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/create_temp_detail', $pagedata);
        $this->load->view('admin/footer');
    }
    
    
    public function show_grant_template_detail($param1 = '', $param2 = '') {
       
        $pagedata['pagetitle'] = 'Grantee Master List';
        $pagedata['page'] = 'Grant Template';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_grant')->result_array();
//        print_r($pagedata['data']); die;
        $this->load->view('admin/header',$pagedata);
        $this->load->view('pe/grant_template_detail', $pagedata);
        $this->load->view('admin/footer');
      }
    
      public function show_grant_template_delete($id)
         {
           $grant_id = $id;         
      $parent_temp_id = $this->db->get_where('dms_parent_temp',array('grant_temp_id' => $grant_id))->result_array(); 
      foreach($parent_temp_id as $row)
      { 
   $this->db->delete('dms_parent_image', array('parent_temp_id' => $row['parent_temp_id']));
   $this->db->delete('dms_parent_metadata', array('parent_temp_id' => $row['parent_temp_id']));
   $this->db->delete('dms_temp_approve', array('parent_temp_id' => $row['parent_temp_id']));
      }
      $this->db->delete('dms_parent_temp', array('parent_temp_id' => $grant_id));
      
//   <!=========================FOR CHILD ====================================>
   $child_temp_id = $this->db->get_where('dms_child_temp',array('grant_temp_id' => $grant_id))->result_array();   
      foreach($child_temp_id as $row)
      {
   $this->db->delete('dms_child_image', array('child_temp_id' => $row['child_temp_id']));
   $this->db->delete('dms_child_metadata', array('child_temp_id' => $row['child_temp_id']));
      }
      $this->db->delete('dms_child_temp', array('grant_temp_id' => $grant_id)); 
//   <!----------------------ENd CHILD ------------------------------------------->     
    
//   <!=========================For Individual Metadata====================================>
  $indi_temp_id = $this->db->get_where('dms_individual_temp',array('grant_temp_id' => $grant_id))->result_array();   
      foreach($indi_temp_id as $row)
      {
      $this->db->delete('dms_individual_metadata', array('individual_temp_id' => $row['individual_temp_id']));
      }
      $this->db->delete('dms_individual_temp', array('grant_temp_id' => $grant_id));
      
//<!----------------------grant_temp_id ------------------------------------------->   
         $this->db->delete('dms_assign_document', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_cat_year', array('grant_temp_id' => $grant_id)); 
         $this->db->delete('dms_grant_metadeta', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_individual_temp', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_metadata', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_skip_temp', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_user_comment', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_grant', array('id' => $grant_id));
         
        $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_grant_template_detail', 'refresh');  
//        echo "delete successfullyly";
   
    }
    public function deleteGrantCheckbox()
    {
        $grant_id1 = $_POST['id'];              
      
 //   <!=========================FOR PARENTs ====================================>       
      foreach($grant_id1 as $grant_id)
    {      
    $parent_temp_id = $this->db->get_where('dms_parent_temp',array('grant_temp_id' => $grant_id))->result_array(); 
    foreach($parent_temp_id as $row)
        {
   $this->db->delete('dms_parent_image', array('parent_temp_id' => $row['parent_temp_id']));
   $this->db->delete('dms_parent_metadata', array('parent_temp_id' => $row['parent_temp_id']));
   $this->db->delete('dms_temp_approve', array('parent_temp_id' => $row['parent_temp_id']));
    $this->db->delete('dms_parent_temp', array('parent_temp_id' => $row['parent_temp_id']));
       }
      $this->db->delete('dms_parent_temp', array('parent_temp_id' => $grant_id));
     
//   <!=========================FOR CHILD ====================================>
   $child_temp_id = $this->db->get_where('dms_child_temp',array('grant_temp_id' => $grant_id))->result_array();   
      foreach($child_temp_id as $row)
      {
   $this->db->delete('dms_child_image', array('child_temp_id' => $row['child_temp_id']));
   $this->db->delete('dms_child_metadata', array('child_temp_id' => $row['child_temp_id']));
    $this->db->delete('dms_child_temp', array('grant_temp_id' => $row['grant_temp_id'])); // may be create problem.
      }
      $this->db->delete('dms_child_temp', array('grant_temp_id' => $grant_id)); 
//   <!----------------------ENd CHILD ------------------------------------------->    
   
//   <!=========================For Individual Metadata====================================>
  $indi_temp_id = $this->db->get_where('dms_individual_temp',array('grant_temp_id' => $grant_id))->result_array();   
      foreach($indi_temp_id as $row)
      {
      $this->db->delete('dms_individual_metadata', array('individual_temp_id' => $row['individual_temp_id']));
      }
      $this->db->delete('dms_individual_temp', array('grant_temp_id' => $row['grant_temp_id']));  
          
    
//<!----------------------grant_temp_id ------------------------------------------->   
         $this->db->delete('dms_assign_document', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_cat_year', array('grant_temp_id' => $grant_id)); 
         $this->db->delete('dms_grant_metadeta', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_individual_temp', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_metadata', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_skip_temp', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_user_comment', array('grant_temp_id' => $grant_id));
         $this->db->delete('dms_grant', array('id' => $grant_id));
          }
          
        $this->session->set_flashdata('flash_message', 'Deleted  sucessfully');
            redirect('adminlogin/show_grant_template_detail', 'refresh');  
//        echo "delete successfullyly";
    }
    
    public function showDmsBackup(){
         $pagedata['pagetitle'] = 'Database Backup'; 
         $pagedata['page'] = 'Backup';
         $this->load->view('admin/header',$pagedata);
         $this->load->view('admin/dmsBackup',$pagedata);
         $this->load->view('admin/footer'); 
    }
     
      public function exportDms()
       {
        // set_time_limit(0);
           date_default_timezone_set('GMT');
           $this->load->helper('file');
          //$con = mysqli_connect("localhost","root","","livenet_dms");
 //$con = mysqli_connect("localhost","livenet_dms","dms123","livenet_dms");
 $con = mysqli_connect($this->db->hostname,$this->db->username,$this->db->password,$this->db->database);

            $tables = array();
            $query = mysqli_query($con, 'SHOW TABLES');
            while($row = mysqli_fetch_row($query)){
                 $tables[] = $row[0];
            } 
//            $result = 'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";';
//            $result .= 'SET time_zone = "+00:00";';

            foreach($tables as $table){
            $query = mysqli_query($con, 'SELECT * FROM `'.$table.'`');
            $num_fields = mysqli_num_fields($query);

            $result .= 'DROP TABLE IF EXISTS '.$table.';';
            $row2 = mysqli_fetch_row(mysqli_query($con, 'SHOW CREATE TABLE `'.$table.'`'));
            $result .= "\n\n".$row2[1].";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
            while($row = mysqli_fetch_row($query)){
               $result .= 'INSERT INTO `'.$table.'` VALUES(';
                 for($j=0; $j<$num_fields; $j++){
                   $row[$j] = addslashes($row[$j]);
                   $row[$j] = str_replace("\n","\\n",$row[$j]);
                if(isset($row[$j])){
                       $result .= '"'.$row[$j].'"' ; 
                    }else{ 
                        $result .= '""';
                    }
                    if($j<($num_fields-1)){ 
                        $result .= ',';
                    }
                }
                $result .= ");\n";
            }
            }
            $result .="\n\n";
            }
            $folder = 'dmsBackup/';
//            $folder = 'database/';
            if (!is_dir($folder))
            mkdir($folder, 0777, true);
            chmod($folder, 0777);

            //$date = date('m-d-Y'); 
            $filename = $folder."dms".$date; 
            $handle = fopen($filename.'.sql','w+');
            fwrite($handle,$result);
            fclose($handle);
//            redirect('showDmsBackup'); 
             $this->zipData();
           // $this->zipData1();
       
           // $this->importDms1();
           // $this->zipData($source, $destination);
        $this->session->set_flashdata('flash_message', 'Database backup store successfully');
            redirect('adminlogin/showDmsBackup', 'refresh');
        }
        
         function zipData_() {
        //set_time_limit(0);
        $rootPath = realpath('D:\xampp\htdocs\dms_demo');
         //$rootPath1 = realpath('application');

// Initialize archive object
        $zip = new ZipArchive();
        $zip->open('dmsBackup/uploads.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
        //$zip->open('dmsBackup/application.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY
        );
        

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
   

// Zip archive will be created only after closing object
        $zip->close();

    }
         function zipData() {
           // set_time_limit(0);
             $rootPath1 = realpath('local_path');

// Initialize archive object
$zip1 = new ZipArchive();
$zip1->open('dmsBackup/local_path.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files1 = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath1),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files1 as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath1) + 1);

        // Add current file to archive
        $zip1->addFile($filePath, $relativePath);
    }
}

// Zip archive will be created only after closing object
$zip1->close();

    }
    function zipData1() {
             set_time_limit(359200);
             $rootPath = realpath('uploads');

// Initialize archive object
$zip = new ZipArchive();
$zip->open('dmsBackup/uploads.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);

        // Add current file to archive
        $zip->addFile($filePath, $relativePath);
    }
}

// Zip archive will be created only after closing object
$zip->close();

    }
    
      public function importDms1()
      {        
           date_default_timezone_set('GMT');
           $this->load->helper('file');
            $filename = 'dms.sql';
           // $con = mysqli_connect("localhost","root","","cd");
            $con = mysqli_connect("localhost","root","","dms_backup");
            if (mysqli_connect_errno())
             {
             echo "Failed to connect to MySQL: " . mysqli_connect_error();
             } 
            $sql = "DROP TABLE `dms_approval_comment`, `dms_assign_document`, `dms_assin_template`, `dms_category`, `dms_categorytype`, `dms_cat_year`, `dms_child_image`, `dms_child_metadata`, `dms_child_temp`, `dms_deliverables`, `dms_disciplinary_field`, `dms_discription`, `dms_document_template`, `dms_grant`, `dms_grantee`, `dms_grant_metadeta`, `dms_individual_metadata`, `dms_individual_temp`, `dms_language`, `dms_metadata`, `dms_outcome`, `dms_parent_image`, `dms_parent_metadata`, `dms_parent_temp`, `dms_role`, `dms_skip_temp`, `dms_state`, `dms_template_type`, `dms_temp_approve`, `dms_user`, `dms_user_comment`, `dms_workflow`";  
              	if ($con->query($sql)) {
//                 echo "successfullyl drop"; die;
		}
            $templine = '';
            //$lines = file('http://localhost/dms/dmsBackup/dms.sql');
            $lines = file('http://erp1.in/dms/dmsBackup/dms.sql');
            
            foreach ($lines as $line)
            {
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;
            $templine .= $line;
            if (substr(trim($line), -1, 1) == ';')
            {
                mysqli_query($con,$templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysqli_error($con) . '<br /><br />');
                $templine = '';
            }
         }
       }
         
       
       public function Assine_permission($param1 = '', $param2 = '') {
      $pagedata['pagetitle'] = 'Assign Permission';
        
        $pagedata['page'] = 'Permission';
        $pagedata['param1'] = $param1;
        $pagedata['data1'] = $this->db->get_where('dms_role', array('role_status' => 'active'))->result_array();
        $pagedata['data'] = $this->db->get('dms_role')->result_array();
        //$pagedata['data1'] = $this->db->get_where('tblpermi
        $this->load->view('admin/header');
        $this->load->view('admin/assine_permission', $pagedata);
        $this->load->view('admin/footer');
       
    }
    
     
    public function permission_table()
    {
        $data = $this->db->get_where('dms_role', array('role_id' => $_POST['id']))->row_array();
        //print_r($data); die;
        $permi = explode(",", $data['role_permission']);
        //print_r($permi); die;
      ?>  
        <table  cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
                                                        <thead>
                                                                <tr>
                                                                        <th class="center hidden-xs">Permission Name</th>
                                                                        <th class="center hidden-xs">View</th>
                                                                        <th class="center hidden-xs">Add</th>
                                                                        <th class="center hidden-xs">Edit</th>
                                                                        <th class="center hidden-xs">Deleted</th>
                                                                </tr>
                                                        </thead>
                                                        <tbody>
                                                                <tr class="gradeX">
<!--                                                                        <td class="center hidden-xs">Master</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_master" <?php if(in_array('view_grantee', $permi)) echo 'checked' ; ?> ></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_master" <?php if(in_array('view_grantee', $permi)) echo 'checked' ; ?> ></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_master" <?php if(in_array('view_grantee', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_master" <?php if(in_array('view_grantee', $permi)) echo 'checked' ; ?>></td>
                                                                        
                                                                </tr>-->
<!--                                                               <tr class="gradeX">
                                                                        <td class="center hidden-xs">Grantee</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_grantee" <?php if(in_array('view_grantee', $permi)) echo 'checked' ; ?> ></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_grantee" <?php if(in_array('view_grantee', $permi)) echo 'checked' ; ?> ></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_grantee" <?php if(in_array('view_grantee', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_grantee" <?php if(in_array('view_grantee', $permi)) echo 'checked' ; ?>></td>
                                                                        
                                                                </tr>-->
                                                                <tr class="gradeX">
                                                                        <td class="center hidden-xs">Grant</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_grant" <?php if(in_array('view_grant', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_grant" <?php if(in_array('add_grant', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_grant" <?php if(in_array('edit_grant', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs">
<!--                                                                            <input class="form-control" type="checkbox" name="permission[]" value="delete_grant" <?php if(in_array('delete_grant', $permi)) echo 'checked' ; ?>>-->
                                                                        </td>
                                                                </tr>
                                                                <tr class="gradeX">
                                                                        <td class="center hidden-xs">Approval</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_approval" <?php if(in_array('view_approval', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_approval" <?php if(in_array('add_approval', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_approval" <?php if(in_array('edit_approval', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_approval" <?php if(in_array('delete_approval', $permi)) echo 'checked' ; ?>></td>
                                                                        
                                                                </tr>
                                                                <tr class="gradeX">
                                                                        <td class="center hidden-xs">Comment</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_comment" <?php if(in_array('view_comment', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_comment" <?php if(in_array('add_comment', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_comment" <?php if(in_array('edit_comment', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_comment" <?php if(in_array('delete_comment', $permi)) echo 'checked' ; ?>></td>
                                                                       
                                                                </tr>
                                                                <tr class="gradeX">
                                                                        <td class="center hidden-xs">Add Csv</td>
                                                                        <td class="center hidden-xs"><input class="form-control"  type="checkbox" name="permission[]" value="view_csv" <?php if(in_array('view_csv', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control"  type="checkbox" name="permission[]" value="add_csv" <?php if(in_array('add_csv', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control"  type="checkbox" name="permission[]" value="edit_csv" <?php if(in_array('edit_csv', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control"  type="checkbox" name="permission[]" value="delete_csv" <?php if(in_array('delete_csv', $permi)) echo 'checked' ; ?>></td>
                                                                       
                                                                </tr>
<!--                                                                
                                                         <tr class="gradeX">
                                                                        <td class="center hidden-xs">Programme Type</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_programme" <?php if(in_array('view_programme', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_programme" <?php if(in_array('add_programme', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_programme" <?php if(in_array('edit_programme', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_Programme" <?php if(in_array('delete_programme', $permi)) echo 'checked' ; ?>></td>
                                                                        
                                                          </tr>   -->
<!--                                                            
<!--                                                             <tr class="gradeX">
                                                                        <td class="center hidden-xs">Create Programme and Initiative</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_initiative" <?php if(in_array('view_initiative', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_initiative" <?php if(in_array('add_initiative', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_initiative" <?php if(in_array('edit_initiative', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_initiative" <?php if(in_array('delete_initiative', $permi)) echo 'checked' ; ?>></td>
                                                                        
                                                             </tr>       -->
<!--                                                           <tr class="gradeX">
                                                                        <td class="center hidden-xs">Document</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_document" <?php if(in_array('view_document', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_document" <?php if(in_array('add_document', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_document" <?php if(in_array('edit_document', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_document" <?php if(in_array('delete_document', $permi)) echo 'checked' ; ?>></td>
                                                                        
                                                             </tr>-->
                                                            
<!--                                                               <tr class="gradeX">
                                                                        <td class="center hidden-xs">User</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_user" <?php if(in_array('view_user', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_user" <?php if(in_array('add_user', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_user" <?php if(in_array('edit_user', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_user" <?php if(in_array('delete_user', $permi)) echo 'checked' ; ?>></td>
                                                                        
                                                             </tr>    -->
<!--                                                     <tr class="gradeX">
                                                                        <td class="center hidden-xs">Description</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_description" <?php // if(in_array('view_user', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_description" <?php if(in_array('add_user', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_description" <?php // if(in_array('edit_user', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_description" <?php // if(in_array('delete_user', $permi)) echo 'checked' ; ?>></td>  
                                                                </tr>                      -->
                                                            
                                                                
                                                                
                                                                
                                                                
                                                                <!--
                                                                <tr class="gradeX">
                                                                        <td class="center hidden-xs">Category</td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="view_category" <?php if(in_array('view_category', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="add_category" <?php if(in_array('add_category', $permi)) echo 'checked' ; ?>></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="edit_category" <?php if(in_array('edit_category', $permi)) echo 'checked' ; ?> ></td>
                                                                        <td class="center hidden-xs"><input class="form-control" type="checkbox" name="permission[]" value="delete_category" <?php if(in_array('delete_category', $permi)) echo 'checked' ; ?>></td>
                                                                        
                                                                </tr>-->
                                                                
                                                                
                                                               
                                                        </tbody>
                                                        <tfoot>
<!--                                                                <tr>
                                                                        <th class="center hidden-xs">Permission Name</th>
                                                                        <th class="center hidden-xs">View</th>
                                                                        <th class="center hidden-xs">Add</th>
                                                                        <th class="center hidden-xs">Edit</th>
                                                                        <th class="center hidden-xs">Deleted</th>
                                                                </tr>-->
                                                        </tfoot>
                                                </table>
                                                <div style="text-align: right">
                                                <button type="submit" class="btn btn-primary start" style="width:120px" 
                                                        name="submit">Submit</button>
                                            </div>
                <?php
    }
    
    public function save_permission()
    {
//        print_r($_POST);
        if($_POST['permission']){
        $permission = implode(',', $_POST['permission']);}
//        die;
        $this->db->where('role_id', $_POST['role']);
        $this->db->update('dms_role', array('role_permission' => $permission));
        $this->session->set_flashdata('flash_message', 'Permission Updated  sucessfully.');
         echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
    }
    
    //tree selection
    
    

    public function show_documents_order($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Document';
            $pagedata['datas'] = $this->db->get_where('dms_document_template', array('document_temp_id !=' => $param1))->result_array();
        } else {

            $pagedata['pagetitle'] = 'Add Document';
            $pagedata['datas'] = $this->db->get('dms_document_template')->result_array();
        }
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $this->db->order_by('document_order_id');
        $pagedata['data'] = $this->db->get_where('dms_document_template', array('document_temp_parent' => '0'))->result_array();
        
         $pagedata['data1'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('admin/header',$pagedata);
        $this->load->view('admin/document_order', $pagedata);
        $this->load->view('admin/footer');
    }
    
    function get_records() {
            //echo $_POST['data'];die;
            //if(isset($_POST['token']))
//{
            $data = json_decode($_POST['data']);
            $counter = 1;
            $a1 = $this->db->get_where('dms_document_template')->result_array();
            foreach ($a1 as $key => $row) {
                $b1[$row['document_temp_id']] = $data[$key];
            }
            foreach ($data as $key => $val) {
                $this->db->set('document_order_id', $counter);
                $this->db->where('document_temp_id', $val);
                $this->db->update('dms_document_template');
                //save_record($val,$counter);
                $counter++;
            }
            $document = $this->db->get_where("dms_document_template", array('document_temp_id' => $_POST['doc']))->row_array();
            echo $document['document_order_id'];
            //echo  $this->db->last_query();
//}
           // redirect('adminlogin/show_documents_order', 'refresh');
        }


        public function banner_list() {
            $data['page_name'] = 'CMS PAGES';
            $data['page_title'] = 'Banner';
            $data['banner'] = $this->db->get('banner')->result_array();
            $this->load->view('admin/header');
            $this->load->view('admin/banner', $data);
            $this->load->view('admin/footer');
        }

        public function edit_banner($para1 = "") {
            $data['page_name'] = 'CMS PAGES';
            $data['page_title'] = 'Edit Banner';
            $data['banner'] = $this->db->get_where('banner', array('id' => $para1))->result_array();
            $this->load->view('admin/header');
            $this->load->view('admin/edit_banner', $data);
            $this->load->view('admin/footer');
        }

        public function editbanner() {
            $id = $this->input->post('id');
            if ($_FILES["banner"]["name"]) {
                $random = substr(number_format(time() * mt_rand(), 0, '', ''), 0, 4);
                $image = $random . $_FILES['banner']['name'];
                if ($image) {
                    $output_dir = "uploads/";
                    $fileName = $image;
                    move_uploaded_file($_FILES["banner"]["tmp_name"], $output_dir . $fileName);
                    $data['banner'] = $fileName;
                }
            }
            $data['status'] = 'active';
            $this->db->where('id', $id);
            $this->db->update('banner', $data);
            $this->session->set_flashdata('flash_message', ' Banner Updated Successfuly');
            redirect("adminlogin/banner_list/", 'refresh');
        }
    public function delete_workflow(){
         $id = $this->input->post('id');
         $this->db->where('workflow_id', $id);
            $this->db->delete('dms_workflow');
    }

}
