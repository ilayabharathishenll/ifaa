<?php

error_reporting(~E_NOTICE);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class backup extends CI_Controller {

    Public function __construct() {
        parent::__construct();
   $this->load->helper('url');
        $this->load->database();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('admin_model');
        $this->load->model('csv_model');
           set_time_limit(0);
    }

    public function index() {
        
    }

    //database backup
    public function exportDms() {
       set_time_limit(0);
        date_default_timezone_set('GMT');
        $this->load->helper('file');
       // $con = mysqli_connect("localhost", "root", "", "livenet_dms");
       $con = mysqli_connect("localhost","livenet_dms","dms123","livenet_dms");
        $tables = array();
        $query = mysqli_query($con, 'SHOW TABLES');
        while ($row = mysqli_fetch_row($query)) {
            $tables[] = $row[0];
        }
//            $result = 'SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";';
//            $result .= 'SET time_zone = "+00:00";';

        foreach ($tables as $table) {
            $query = mysqli_query($con, 'SELECT * FROM `' . $table . '`');
            $num_fields = mysqli_num_fields($query);

            $result .= 'DROP TABLE IF EXISTS ' . $table . ';';
            $row2 = mysqli_fetch_row(mysqli_query($con, 'SHOW CREATE TABLE `' . $table . '`'));
            $result .= "\n\n" . $row2[1] . ";\n\n";

            for ($i = 0; $i < $num_fields; $i++) {
                while ($row = mysqli_fetch_row($query)) {
                    $result .= 'INSERT INTO `' . $table . '` VALUES(';
                    for ($j = 0; $j < $num_fields; $j++) {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = str_replace("\n", "\\n", $row[$j]);
                        if (isset($row[$j])) {
                            $result .= '"' . $row[$j] . '"';
                        } else {
                            $result .= '""';
                        }
                        if ($j < ($num_fields - 1)) {
                            $result .= ',';
                        }
                    }
                    $result .= ");\n";
                }
            }
            $result .="\n\n";
        }
        $folder = 'dmsBackup/';
//            $folder = 'database/';
        if (!is_dir($folder))
            mkdir($folder, 0777, true);
        chmod($folder, 0777);

        //$date = date('m-d-Y');
        $filename = $folder . "dms" . $date;
        $handle = fopen($filename . '.sql', 'w+');
        fwrite($handle, $result);
        fclose($handle);
        $this->zipData();
        //$this->zipData1();
       
    }
//folder backup
    function zipData() {
        //set_time_limit(0);
        $rootPath1 = realpath('local_path');
        
// Initialize archive object
        $zip1 = new ZipArchive();
        $zip1->open('dmsBackup/local_path.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
       
// Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files1 = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($rootPath1), RecursiveIteratorIterator::LEAVES_ONLY
        );
         
        foreach ($files1 as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath1) + 1);

                // Add current file to archive
                $zip1->addFile($filePath, $relativePath);
            }
        }
       
// Zip archive will be created only after closing object
        $zip1->close();

    }
    function zipData1() {
             //set_time_limit(0);
             $rootPath = realpath('uploads');

// Initialize archive object
$zip = new ZipArchive();
$zip->open('dmsBackup/uploads.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

// Create recursive directory iterator
/** @var SplFileInfo[] $files */
$files = new RecursiveIteratorIterator(
    new RecursiveDirectoryIterator($rootPath),
    RecursiveIteratorIterator::LEAVES_ONLY
);

foreach ($files as $name => $file)
{
    // Skip directories (they would be added automatically)
    if (!$file->isDir())
    {
        // Get real and relative path for current file
        $filePath = $file->getRealPath();
        $relativePath = substr($filePath, strlen($rootPath) + 1);

        // Add current file to archive
        $zip->addFile($filePath, $relativePath);
    }
}

// Zip archive will be created only after closing object
$zip->close();

    }
}
