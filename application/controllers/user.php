<?php

error_reporting(~E_NOTICE);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user extends CI_Controller {

    Public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('admin_model');
    }
 // Show login page
    public function index() {
        $pagedata['pagetitle'] = 'India Foundation for the Arts';
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/index', $pagedata);
        $this->load->view('user/footer');
    }
    public function advance_search() {
        $pagedata['pagetitle'] = 'India Foundation for the Arts';
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/advance_search', $pagedata);
        $this->load->view('user/footer');
    }
     public function multy_search($parameter)
    {
        $pagedata['pagetitle'] = $parameter;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/multysearch',$pagedata);
        $this->load->view('user/footer',$pagedata);
    }
    public function grant_list($parm = '') {
        // echo "hhhh"; die;
        //print_r($_POST); die;
        if($parm == 'popular')
        {
           //echo "hhhh"; die; 
           $this->db->order_by('popular_search', 'desc');
           $pagedata['data'] = $this->db->get('dms_grant')->result_array();
        }
        elseif($_POST){
//            echo "<pre>";
//            print_r($_POST); die;
            if($_POST['search'])
            {
                //echo "hh"; die;
                $catId = $this->db->get_where('dms_category', array('category_name' => $_POST['search']))->row_array();
                  $value=$_POST['search'];
                 $this->db->select('*');
                 $this->db->where('metadata_title','keywords');
              $this->db->where("FIND_IN_SET('$value',metadata_value) !=", 0);
              $result1=$this->db->get('dms_metadata')->result_array();
                
                //$result1 = $this->db->get_where('dms_metadata', array('metadata_title' => 'keywords','metadata_value' => $_POST['key']))->result_array();

             
                if(count($catId)>0){
                $result = $this->db->get_where('dms_grant', array('category_id' => $catId['category_id']))->result_array();
                $pagedata['data'] = $result;
                }else if(count($result1)>0){
                   foreach($result1 as $row1){
                  
              
                $result = $this->db->get_where('dms_grant', array('id' => $row1['grant_temp_id']))->row_array();
                
                $data_arr[]= $result;
                
              }  
               $pagedata['data'] = $data_arr;
              //echo "<pre>";  print_r($data_arr);die;
                }
//echo "<pre>";  print_r($result);die;
                //$result = $this->admin_model->grant_serch($_POST['category'], $_POST['year'],$_POST['key'], $_POST['status'], $_POST['language'],$_POST['region'], $_POST['art'],$_POST['grantee_name']);
               
//                if($result)
//                {
//                    $pagedata['data'] = $result;
//                }
//                else
//                {
//                    
//                }
            }
            else if($_POST['key'])
            {
                $value=$_POST['key'];
                 
                   $this->db->like('parent_description',$value);
                $this->db->escape_like_str($query);
                $res = $this->db->get('dms_parent_temp')->result_array();
                //echo $this->db->last_query();
                //echo "<pre>"; print_r($res);die;
                 foreach($res as $row1){
                  
              
                $result1 = $this->db->get_where('dms_grant', array('id' => $row1['grant_temp_id']))->row_array();
                
                $data_arr1[]= $result1;
              }
                     
                //----------------------------------------------------------------------------------
                 
                 
                 $this->db->select('*');
                 $this->db->where('metadata_title','Keywords');
              $this->db->where("FIND_IN_SET('$value',metadata_value) !=", 0);
              $result1=$this->db->get('dms_metadata')->result_array();
                //echo $this->db->last_query();die;
                //$result1 = $this->db->get_where('dms_metadata', array('metadata_title' => 'keywords','metadata_value' => $_POST['key']))->result_array();

              foreach($result1 as $row1){
                  
              
                $result = $this->db->get_where('dms_grant', array('id' => $row1['grant_temp_id']))->row_array();
                
                $data_arr[]= $result;
              }
                            //echo "<pre>"; print_r($data_arr);die;
//echo $this->db->last_query();
                //die;
                if(!empty($data_arr) && !empty($data_arr1))
                {
                   // echo $this->db->last_query();
                    $pagedata['data'] = array_merge($data_arr,$data_arr1);
                }
                else if(!empty($data_arr) && empty($data_arr1))
                {
                   // echo $this->db->last_query();
                    $pagedata['data'] =$data_arr;
                }
                 else if(empty($data_arr) && !empty($data_arr1))
                {
                   // echo $this->db->last_query();
                    $pagedata['data'] =$data_arr1;
                }
                else
                {
                    
                }
            }
            else
            {
                $result = $this->admin_model->grant_serch($_POST['category'], $_POST['year'],$_POST['key'], $_POST['status'], $_POST['language'],$_POST['region'], $_POST['art'],$_POST['grantee_name']);
                if($result)
                {
                    $pagedata['data'] = $result;
                }
                else
                {
                    //$pagedata['data'] = "No Result Gound";
                }
            }
            
        }
        else
        {
//            $pagedata['data'] = $this->db->get('dms_grant')->result_array();
        
        }
        $pagedata['pagetitle'] = 'Grant List';
        $pagedata['page'] = 'Searched Result';
        $pagedata['param1'] = $param1;
       // $this->db->select('*')->from('dms_grant')->where("column LIKE '%$keyword%'")->get();
        $this->db->group_by('category_year');
        $pagedata['year'] = $this->db->get('dms_grant')->result_array(); 
        //$pagedata['data'] = $this->db->get('dms_grant')->result_array(); 
        $pagedata['category'] = $this->db->get('dms_category')->result_array();
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/grant_list', $pagedata);
        $this->load->view('user/footer');
    }
     public function grant_list1($parm = '') {
        // echo "hhhh"; die;
        //print_r($_POST); die;
//        if($parm == 'popular')
//        {
//           //echo "hhhh"; die; 
//           $this->db->order_by('popular_search', 'desc');
//           $pagedata['data'] = $this->db->get('dms_grant')->result_array();
//        }
        if($_POST){
//            echo "<pre>";
//            print_r($_POST); die;
            if($_POST['search'])
            {
                
               $value=$_POST['search'];
                //  $deliverables_start = $this->db->get_where('dms_grant', array('start_date' => $value))->result_array();
 //echo "gf";
               //echo "<pre>";
//               $this->db->like('start_date',$value);
//                $this->db->escape_like_str($query);
//                $deliverables_start = $this->db->get('dms_grant', array('start_date' => $value))->result_array();
              //  print_r($deliverables_start);
                $deliverables_end = $this->db->get_where('dms_grant', array('category_year' => $value))->result_array();
  // print_r($deliverables_end);
         // echo   $this->db->last_query();die; 
                 $this->db->like('deliverables_name',$value);
                $this->db->escape_like_str($query);
                $deliverables = $this->db->get('dms_deliverables')->row_array();
                 $this->db->select('*');
                 //$this->db->where('metadata_title','keywords');
              $this->db->where("FIND_IN_SET('$deliverables[deliverables_id]',deliverables) !=", 0);
              $deliverables1=$this->db->get('dms_grant')->result_array();
              //  echo $this->db->last_query();die;
                
               $this->db->like('outcome_name',$value);
                $this->db->escape_like_str($query);
                $dms_outcome = $this->db->get('dms_outcome')->row_array();
                 $this->db->select('*');
         
              $this->db->where("FIND_IN_SET('$dms_outcome[outcome_id]',grantee_possible_outcome) !=", 0);
              $dms_outcome1=$this->db->get('dms_grant')->result_array();
                    
               $this->db->like('language_name',$value);
                $this->db->escape_like_str($query);
                $language = $this->db->get('dms_language')->row_array();
                 $this->db->select('*');
         
              $this->db->where("FIND_IN_SET('$language[language_id]',grantee_language) !=", 0);
              $language1=$this->db->get('dms_grant')->result_array();
              
              $this->db->like('field_name',$value);
                $this->db->escape_like_str($query);
                $dms_disciplinary_field = $this->db->get('dms_disciplinary_field')->row_array();
                 $this->db->select('*');
         
              $this->db->where("FIND_IN_SET('$dms_disciplinary_field[field_id]',grantee_disciplinary_field) !=", 0);
              $dms_disciplinary_field1=$this->db->get('dms_grant')->result_array();
              
            // echo $this->db->last_query();die;
              $this->db->like('grantee_name',$value);
                $this->db->escape_like_str($query);
                $dms_grantee = $this->db->get('dms_grantee')->row_array();
                 $this->db->select('*');
         
              $this->db->where("FIND_IN_SET('$dms_grantee[grantee_id]',grantee_name) !=", 0);
              $dms_grantee1=$this->db->get('dms_grant')->result_array();
              
               $this->db->like('state_name',$value);
                $this->db->escape_like_str($query);
                $graphical = $this->db->get('dms_state')->row_array();
                 $this->db->select('*');
         
              $this->db->where("FIND_IN_SET('$graphical[state_id]',grantee_geogrtaphical_area) !=", 0);
              $graphical1=$this->db->get('dms_grant')->result_array();
              
                 $this->db->like('grant_number',$value);
                $this->db->escape_like_str($query);
                $deliverables_start = $this->db->get('dms_grant')->result_array();
              
              $this->db->like('archiv_tag',$value);
                $this->db->escape_like_str($query);
                $tag = $this->db->get('dms_grant')->result_array();
              
                    $this->db->like('parent_description',$value);
                $this->db->escape_like_str($query);
                $res = $this->db->get('dms_parent_temp')->result_array();
                //echo $this->db->last_query();
                //echo "<pre>"; print_r($res);die;
                if($res){
                 foreach($res as $row1){
                  
              
                $result1 = $this->db->get_where('dms_grant', array('id' => $row1['grant_temp_id']))->row_array();
                
                $data_arr1[]= $result1;
              }
             $d10= $data_arr1;
                }
                   // echo "<pre>"; print_r($data_arr1);die; 
                  $catId = $this->db->get_where('dms_category', array('category_name' => $_POST['search']))->row_array();
                
                 $this->db->select('*');
                 //$this->db->where('metadata_title','keywords');
              $this->db->where("FIND_IN_SET('$value',metadata_value) !=", 0);
              $result1=$this->db->get('dms_metadata')->result_array();
                
                //$result1 = $this->db->get_where('dms_metadata', array('metadata_title' => 'keywords','metadata_value' => $_POST['key']))->result_array();

                if(count($catId)>0){
                $result = $this->db->get_where('dms_grant', array('category_id' => $catId['category_id']))->result_array();
                //$pagedata['data'] = $result;
                 $d11= $result;
                }
                if(count($tag)>0)
                {
                  $d12 = $tag;
                   // echo $this->db->last_query();
                   // $pagedata['data'] = $deliverables_end;
                }
                
                 if(count($deliverables_start)>0)
                {
               $d1 = $deliverables_start;
                   // echo $this->db->last_query();
                    //$pagedata['data'] = $deliverables_start;
                }
                if(count($deliverables_end)>0)
                {
                  $d2 = $deliverables_end;
                   // echo $this->db->last_query();
                   // $pagedata['data'] = $deliverables_end;
                }
                 if(count($deliverables1)>0)
                {
                 $d3 = $deliverables1;
                   // echo $this->db->last_query();
                   // $pagedata['data'] = $deliverables1;
                }
                  if(count($language1)>0)
                {
                  $d4 = $language1;
                   // echo $this->db->last_query();
                   // $pagedata['data'] = $language1;
                }
                   if(count($dms_outcome1)>0)
                {
                 $d5= $dms_outcome1;
                   // echo $this->db->last_query();
                   // $pagedata['data'] = $dms_outcome1;
                }
                   if(count($dms_disciplinary_field1)>0)
                {
                  $d6 = $dms_disciplinary_field1;
                  //echo "erfkej";die;
                   // echo $this->db->last_query();
                   // $pagedata['data'] = $dms_disciplinary_field1;
                }
                   if(count($graphical1)>0)
                {
                   $d7 = $graphical1;
                   // echo $this->db->last_query();
                    //$pagedata['data'] = $graphical1;
                }
                  if(count($grantee_name1)>0)
                {
                   $d8 = $grantee_name1;
                   // echo $this->db->last_query();
                  //  $pagedata['data'] = $grantee_name1;
                }
                 if(count($result1)>0 || count($res)>0){
                     
                   foreach($result1 as $row1){
                  
       
                $result = $this->db->get_where('dms_grant', array('id' => $row1['grant_temp_id']))->row_array();
                
                $data_arr[]= $result;
                
              }  
              $d9 = $data_arr;
                 }
                 $array1= array();
           
               if(!empty($d1) )
                {
               foreach($d1 as $a1)
                 {
                 array_push($array1,$a1); 
                 }
                   //$array1= array_push($array1,$d1);
                     //$pagedata['data'] =$array1;
                }
               if(!empty($d2)) {
               //echo "<pre>";
              
                 foreach($d2 as $a1)
                 {
                 array_push($array1,$a1); 
                 }
                 //print_r($array1); 
                // print_r($array11);
                //$pagedata['data'] =$array1;  die;
                }
            if(!empty($d3))
                {
                foreach($d3 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
                   //$array1= array_push($array1,$d3);
                     //$pagedata['data'] =$array1;
                }
               if(!empty($d4))
                {
                foreach($d4 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
//                   $array1= array_push($array1,$d4);
//                     $pagedata['data'] =$array1;
                }
              if(!empty($d5))
                {
                foreach($d5 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
//                   $array1= array_push($array1,$d5);
//                     $pagedata['data'] =$array1;
                }
                 if(!empty($d6))
                {
                foreach($d6 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
                   //  echo "<pre>";print_r($array1);die;
//                   $array1= array_push($array1,$d6);
//                     $pagedata['data'] =$array1;
                }
                 if(!empty($d7))
                {
                foreach($d7 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
//                   $array1= array_push($array1,$d7);
//                     $pagedata['data'] =$array1;
                }
                 if(!empty($d8))
                {
                foreach($d8 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
//                   $array1= array_push($array1,$d8);
//                     $pagedata['data'] =$array1;
                }
               if(!empty($d9))
                {
                foreach($d9 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
//                   $array1= array_push($array1,$d9);
//                     $pagedata['data'] =$array1;
                }
                 if(!empty($d10))
                {
                     foreach($d10 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
               
//                   $array1= array_push($array1,$d10);
//                     $pagedata['data'] =$array1;
                }
                 if(!empty($d11))
                {
                foreach($d11 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
//                   $array1= array_push($array1,$d11);
//                   $pagedata['data'] =$array1;
                }
               if(!empty($d12))
                {
                foreach($d12 as $a1)
                 {
                     array_push($array1,$a1); 
                 }
//                   $array1= array_push($array1,$d11);
//                   $pagedata['data'] =$array1;
                }
              
              
//                  if(!empty($d1) || !empty($d2) || !empty($d3)|| !empty($d4)|| !empty($d5)|| !empty($d6)|| !empty($d7)|| !empty($d8)|| !empty($d9)|| !empty($d10)|| !empty($d11))
//                {
//                   // echo $this->db->last_query();
//                    $pagedata['data'] = array_filter($d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$d10,$d11);
//                }
//                else if(!empty($d1) && empty($data_arr1))
//                {
//                   // echo $this->db->last_query();
//                    $pagedata['data'] =$data_arr;
//                }
//                 else if(empty($data_arr) && !empty($data_arr1))
//                {
//                   // echo $this->db->last_query();
//                    $pagedata['data'] =$data_arr1;
//                }
//               
//                else
//                {
//                    
//                }
              // $pagedata['data'] = $data_arr;
              //echo "<pre>";  print_r($data_arr);die;
               // }
//echo "<pre>";  print_r($result);die;
                //$result = $this->admin_model->grant_serch($_POST['category'], $_POST['year'],$_POST['key'], $_POST['status'], $_POST['language'],$_POST['region'], $_POST['art'],$_POST['grantee_name']);
               
//                if($result)
//                {
//                    $pagedata['data'] = $result;
//                }
//                else
//                {
//                    
//                }
            }
           
            
        }
    
        $pagedata['data'] =$array1;
        $pagedata['pagetitle'] = 'Grant List';
        $pagedata['page'] = 'Searched Result';
        $pagedata['param1'] = $param1;
       // $this->db->select('*')->from('dms_grant')->where("column LIKE '%$keyword%'")->get();
        $this->db->group_by('category_year');
        $pagedata['year'] = $this->db->get('dms_grant')->result_array(); 
        //$pagedata['data'] = $this->db->get('dms_grant')->result_array(); 
        $pagedata['category'] = $this->db->get('dms_category')->result_array();
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/grant_list', $pagedata);
        $this->load->view('user/footer');
    }
    public function document($param) {
        // echo "hhhh"; die;
        //$this->load->view('login');
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/document', $pagedata);
        $this->load->view('user/footer');
    }
    public function image($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/image_page', $pagedata);
        $this->load->view('user/footer');
    }
    public function video($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/popup_video', $pagedata);
        $this->load->view('user/footer');
    }
	
	 public function about_us() {
      
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/aboutus', $pagedata);
        $this->load->view('user/footer');
    }
		 public function terms_conditions() {
      
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/termsconditions', $pagedata);
        $this->load->view('user/footer');
    }
	
	 public function privacy_policy() {
      
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/privacypolicy', $pagedata);
        $this->load->view('user/footer');
    }
	
	public function contact_us() {
        //$pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/contactus', $pagedata);
        $this->load->view('user/footer');
    }
	public function audio($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/Audio_page', $pagedata);
        $this->load->view('user/footer');
    }
    public function weblink($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/weblink_page', $pagedata);
        $this->load->view('user/footer');
    }
       public function weblinks($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/weblink_page1', $pagedata);
        $this->load->view('user/footer');
    }
       public function weblinks_($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/weblink_page2', $pagedata);
        $this->load->view('user/footer');
    }
    public function blogs($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/blogs', $pagedata);
        $this->load->view('user/footer');
    }
	
	   
    public function programs($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/programs', $pagedata);
        $this->load->view('user/footer');
    }
    public function sourcecode($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/sourcecode', $pagedata);
        $this->load->view('user/footer');
    }
    public function single_grant($param = '',$param1 = '') {
        //echo $param; die;
        
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['document'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'20'))->row_array(); 
        $pagedata['img'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'17'))->row_array();
        $pagedata['audio'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'18'))->row_array();
        $pagedata['video'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'19'))->row_array();
        $pagedata['img1'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'35'))->row_array();
        $pagedata['audio1'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'36'))->row_array();
        $pagedata['video1'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'37'))->row_array();
        $pagedata['document1'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'34'))->row_array();
        $pagedata['img2'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'52'))->row_array();
        $pagedata['audio2'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'54'))->row_array();
        $pagedata['video2'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'53'))->row_array();
        $pagedata['document2'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'51'))->row_array();
        $pagedata['document3'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'50'))->row_array();
//        $pagedata['weblink1'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'41'))->row_array();
//        $pagedata['weblink2'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'55'))->row_array();
//        $pagedata['weblink'] = $this->db->get_where('dms_individual_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'21'))->row_array();
        $pagedata['Sourcecode'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'38'))->row_array();
        $pagedata['Programs'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'39'))->row_array();
        $pagedata['Blogs'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'40'))->row_array();
        $pagedata['weblink1'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'41'))->row_array();
        $pagedata['weblink2'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'55'))->row_array();
        $pagedata['weblink'] = $this->db->get_where('dms_child_temp', array('grant_temp_id' =>$param, 'document_temp_id' =>'21'))->row_array();
       // echo $this->db->last_query();
      // print_r($pagedata['weblink']);die;
        $popu = $data['popular_search'] + '1';
        $datass = array('popular_search' => $popu);
        $this->db->where('id', $param);
        $this->db->update('dms_grant', $datass);
        
        //$data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/single_grant', $pagedata);
        $this->load->view('user/footer');
    }

    /* for admin login  */

    public function login() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->admin_model->login($username, $password);
        //print_r($result); die;

        if($result) {
            if ($this->session->userdata['login']['user_role'] == '1')
            {
                $this->session->set_flashdata('flash_message', 'login Successfull');
                redirect('adminlogin/dashboard', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'login Successfull');
                redirect('pelogin/dashboard', 'refresh');
            }

        } else {
            $this->session->set_flashdata('permission_message', 'username and Password is invalid');
            // redirect($_SERVER[HTTP_REFFER]);
            redirect('admin', 'refresh');
        }
    }
      public function metadata123()
    {
        //print_r($_POST); die;
        $meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'General Information'))->result_array();
        $meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Author, Publisher, Access'))->result_array();
        $meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Physical Characteristics'))->result_array();
        $meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Digitisation Details'))->result_array();
        $meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Audio-Visual Material Details'))->result_array();
        $meta6 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Others'))->result_array();
        //echo "<pre>"; print_r($meta2);die;
        ?>

        <h5>Metadata</h5>
        <?php if($meta1) { ?>
            <h4 style="margin: 20px 0;"><strong>General Information</strong></h4>

            <?php foreach($meta1 as $value) {if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
            
            <?php } }}  //echo "<pre>";print_r($meta5); 
        if($meta2) { ?>

        <h4  style="margin: 20px 0;"><strong>Author, Publisher, Access</strong></h4>
	<?php foreach($meta2 as $value) { if(!empty($value['metadata_value']) ){ ?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php } }} if($meta3) { ?>
        <h4  style="margin: 20px 0;"><strong>Physical Characteristics</strong></h4>
	<?php foreach($meta3 as $value) { if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
        
        <?php } }} if($meta4) {  ?>
        <h4  style="margin: 20px 0;"><strong>Digitisation Details</strong></h4>
	<?php foreach($meta4 as $value) { if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
 <?php } }} if($meta5) { ?>
        <h4  style="margin: 20px 0;"><strong>Audio-Visual Material Details</strong></h4>
	<?php foreach($meta5 as $value) { if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
           
 <?php } }} 
    }

    public function metadata()
    {
        //print_r($_POST); die;
        $meta1 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'General Information'))->result_array();
        $meta2 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Author, Publisher, Access'))->result_array();
        $meta3 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Physical Characteristics'))->result_array();
        $meta4 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Digitisation Details'))->result_array();
        $meta5 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Audio-Visual Material Details'))->result_array();
        $meta6 = $this->db->get_where('dms_metadata', array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc'], 'metadata_individual_number' => $_POST['nmbr'], 'detail' => 'Others'))->result_array();
        //echo "<pre>"; print_r($meta2);die;
        ?>
  <h5>Metadata</h5>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            General Information
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                    <?php if($meta1) { ?>

            <?php foreach($meta1 as $value) {if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
            
            <?php } }}?>
                                  </div>
                                </div>
                              </div>
     <?php if($meta2) {$i2=1; ?>

            <?php foreach($meta2 as $value) {if($value['metadata_value']!='' && $i2==1){?>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                     Author, Publisher, Access
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                                   <?php if($meta2) { ?>

            <?php foreach($meta2 as $value) {if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
            
            <?php } }}?>
                                  </div>
                                </div>
                              </div> <?php $i2++;} }}?>
    <?php if($meta3) {$i3=1; ?>

            <?php foreach($meta3 as $value) {if($value['metadata_value']!='' && $i3==1){?>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                      Physical Characteristics
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                                   <?php if($meta3) { ?>

            <?php foreach($meta3 as $value) {if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
            
            <?php } }}?>

                                  </div>
                                </div>
                            </div> <?php $i3++;} }}?>
   <?php if($meta4) { $i4=1;?>

            <?php foreach($meta4 as $value) {if($value['metadata_value']!='' && $i4==1){?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                      Digitisation Details
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                                   <?php if($meta4) { ?>

            <?php foreach($meta4 as $value) {if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
            
            <?php } }}?>
                                    </div>
                                </div>
                            </div> 
<?php $i4++; } }}?>     <?php if($meta5) {$i5=1; ?>

            <?php foreach($meta5 as $value) {if($value['metadata_value']!=''&& $i5==1 ){?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapseFour">
                                      Audio-Visual Material Details 
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                             <?php if($meta5) { ?>

            <?php foreach($meta5 as $value) {if($value['metadata_value']!='' ){?>
            <p><span class="inner_heading"><?php echo $value['metadata_title'] ?></span> : <?php echo $value['metadata_value'] ?></p>
            
            <?php } }}?>
                                    </div>
                                </div>
                            </div>
                    <?php $i5++;} }}?> 
                        </div>
<?php
    }

    /* for admin logout */

    public function logout() {
        $this->session->unset_userdata(); 
        $this->session->sess_destroy();
        $this->session->set_flashdata('flash_message', 'Successfully Logout');
        redirect('admin', 'refresh');
    }
    
 public function documents($param) {
        // echo "hhhh"; die;
        //$this->load->view('login');
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/document1', $pagedata);
        $this->load->view('user/footer');
    }
    public function images($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/image_page1', $pagedata);
        $this->load->view('user/footer');
    }
    public function videos($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/popup_video1', $pagedata);
        $this->load->view('user/footer');
    }
    public function audios($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/Audio_page1', $pagedata);
        $this->load->view('user/footer');
    }
     public function documents_($param) {
        // echo "hhhh"; die;
        //$this->load->view('login');
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/document2', $pagedata);
        $this->load->view('user/footer');
    }
     public function documents_communication($param) {
        // echo "hhhh"; die;
        //$this->load->view('login');
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/document3', $pagedata);
        $this->load->view('user/footer');
    }
    public function images_($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/image_page2', $pagedata);
        $this->load->view('user/footer');
    }
    public function videos_($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/popup_video2', $pagedata);
        $this->load->view('user/footer');
    }
    public function audios_($param) {
        $data = $this->db->get_where('dms_grant', array('id' =>$param))->row_array();
        $pagedata['data'] = $data;
        $this->load->view('user/header',$pagedata);
        $this->load->view('user/Audio_page2', $pagedata);
        $this->load->view('user/footer');
    }

}
