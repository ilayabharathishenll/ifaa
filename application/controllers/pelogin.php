<?php

error_reporting(~E_NOTICE);
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class pelogin extends CI_Controller {

    public $data1;

    Public function __construct() {
        parent::__construct();
        //ini_set('upload_max_filesize', '1024M'); 
        //ini_set('max_execution_time', 30000); 
        $this->load->helper('url');
        $this->load->database();
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->model('admin_model');
        $this->load->model('csv_model');
        
        if ($this->session->userdata['login']['user_role'] == '') {
            $this->session->set_flashdata('flash_message', 'login_again');
            redirect('admin', 'refresh');
        }
    }

// Show admin dashboard page
    public function dashboard($param1 = '') {
        //echo "sss"; die;
        $pagedata['pagetitle']="Dashboard";
        
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/dashboard', $pagedata);
        $this->load->view('pe/footer');
    }

//for show  changepassword page
    public function show_changepassword($param1 = '') {
        $pagedata['pagetitle'] = 'Change Password';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_user')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/changepassword', $pagedata);
        $this->load->view('pe/footer');
    }

//for changepassword
    public function changepassword() {
        $oldpassword = $this->input->post('old_password');
        $newpassword = $this->input->post('new_password');
        $confirmpassword = $this->input->post('confirm_password');
        $data = array(
            'user_password' => $this->input->post('new_password')
        );
        $id = $this->session->userdata['login']['user_id'];
        $query = $this->db->get_where('dms_user', array('user_id' => $id))->result_array();

        if (($query[0]['user_password'] == $oldpassword) && $newpassword) {
            $this->db->where('user_id', $id);
            $this->db->update('dms_user', $data);
            $this->session->set_flashdata('flash_message', 'Update  password successfully.');
            redirect('pelogin/show_changepassword', 'refresh');
        } else {
            if (($query[0]['user_password'] != $oldpassword)) {
                $this->session->set_flashdata('permission_message', 'Old password not matched.');
                redirect('pelogin/show_changepassword', 'refresh');
            }
            $this->session->set_flashdata('permission_message', 'Old password and New password not matched.');
            redirect('pelogin/show_changepassword', 'refresh');
        }
    }

//for show  profile page
    public function show_profile($param1 = '') {
//$this->session->userdata($userSessiondata);       
        $id = $this->session->userdata['login']['user_id'];
        $pagedata['pagetitle'] = 'Profile';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get_where('dms_user', array('user_id' => $id))->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/profile', $pagedata);
        $this->load->view('pe/footer');
    }

//for update profile
    public function profileupdate() {
        $id = $this->session->userdata['login']['user_id'];

        if ($_FILES['image']['name'])
          {
            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['image']['name'];
            $file_size = $_FILES['image']['size'];
            $file_tmp = $_FILES['image']['tmp_name'];
            $file_type = $_FILES['image']['type'];
            $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
            $expensions = array("gif", "jpg", "jpeg", "png", "tiff");
            if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
            } 
//            move_uploaded_file($file_tmp, "uploads/" . $file_name);
//              move_uploaded_file($file_tmp, "../local_path/" . $file_name); 
            $data['image'] = $file_name;
            
            //            move_uploaded_file($file_tmp, "uploads/" . $file_name);       
//       move_uploaded_file($file_tmp, "../local_path/" . $file_name); // external directory.
              move_uploaded_file($file_tmp, "local_path/" . $file_name); 
        } else {

            $abc = $this->db->get_where("dms_user", array('user_id' => $id))->result_array();

            $file_name = $abc[0]['user_image'];
        }

        $emailid = $this->input->post('user_email');
        $username = $this->input->post('user_name');

        $data = array(
            'user_name' => $this->input->post('username'),
            'user_email' => $this->input->post('emailid'),
            'user_image' => $file_name
        );
        $this->db->where('user_id', $id);
        $this->db->update('dms_user', $data);
        $this->session->set_flashdata('flash_message', 'Profile Updated  successfully.');
        redirect('pelogin/show_profile', 'refresh');
    }
    
     public function yearDropdown() {

       $status = $this->input->post('status');
        $category = $this->input->post('category');
        //echo $status; die;
        $cat = $this->db->get_where("dms_category", array('category_id' => $category))->result_array();
       $catName = $cat[0]['category_name'];
        if ($cat != '0') {
            $this->db->group_by('year');
            $data = $this->db->get_where("dms_cat_year", array('category_id' => $category, 'status' => $status))->result_array();

            echo '<option value="">Select Year</option>';
            $i = '1';
            foreach ($data as $row1) {
                echo '<option value="' . $row1['year'] . '">' . $row1['year'] .' </option>';
                $i++;
            }
        } else {

            echo '<option value="">Select Year</option>';
        }
    }
    
    //for show  Gramt Template Detail page
    public function show_grant_template_detail($param1 = '', $param2 = '') {
       

            $pagedata['pagetitle'] = 'Grant Template Detail';
        
        $pagedata['page'] = 'Template';
        $pagedata['param1'] = $param1;
       $pagedata['data'] = $this->db->get('dms_grant')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/grant_template_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
   
    //for show  add category type page
    public function show_grant_main_template($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Grant';
            $pagedata['result'] = $this->db->get_where('dms_grant', array('id' => $param1))->row_array();
            //$pagedata['result'] = $this->db->get_where("dms_grant", array('id' => $_POST['id']))->row_array();
        } else {

            $pagedata['pagetitle'] = 'Add Grant';
            
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        
        }
        $pagedata['page'] = 'Grant';
        $pagedata['param1'] = $param1;
            //$pagedata['pagetitle'] = 'Grant Main Template';
        
        $this->db->group_by('category_name');
        $pagedata['data1'] = $this->db->get('dms_category')->result_array();
        $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
        $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/grant_main_templale', $pagedata);
        $this->load->view('pe/footer');
    }
    
   
    
    
    //for add Grant Main
    public function add_grant_main($param1 = '', $param2 = '', $param3 = '') {
//        echo "<pre>";
//        echo $param1;
//        print_r($_POST); die;
       $liid = $_POST['liid']; 
       $userId = $this->session->userdata['login']['user_id'];
        $title = $_POST['title'];
        $value = $_POST['value'];
        if($_POST['language']){
        $language = implode(",",$_POST['language']);}
         if($_POST['state']){
         $state = implode(",",$_POST['state']);}
          if($_POST['geogrtaphical_area']){
          $geogrtaphical_area = implode(",",$_POST['geogrtaphical_area']);}
           if($_POST['disciplinary_field']){
           $disciplinary_field = implode(",",$_POST['disciplinary_field']);}
            if($_POST['grantee_possible_outcome']){
            $grantee_possible_outcome = implode(",",$_POST['grantee_possible_outcome']);}
             if($_POST['deliverables']){
             $deliverables = implode(",",$_POST['deliverables']);}
        
        //$key = implode(",",$_POST['keyword']);
        $year = explode("-",$_POST['start_date']);
        
       // die;
       
      $date = date("Y-m-d");
    $grantee_id =  $_POST['grantee_id'];   
    if ($param1 == 'create') {    
       if($_POST['grantee_name'] != '')
      {
          
            $id = $this->input->post('grantee_name');
            $q = $this->db->get_where("dms_grantee", array('grantee_name' => $id))->result_array();
            $data4 = array(
              'grantee_name' => $this->input->post('grantee_name'),
              'grantee_phone' => $this->input->post('phone'),
              'grantee_alternative_no' => $this->input->post('alternative'),
              'grantee_address1' => $this->input->post('address1'),
              'grantee_address2' => $this->input->post('address2'),
              'grantee_city' => $this->input->post('city'),
              'grantee_pin' => $this->input->post('pin'),
              'grantee_country' => $this->input->post('country'),
              'grantee_state' => $state,
               'deliverables' => $deliverables,
              'grantee_geogrtaphical_area' => $geogrtaphical_area,
              'grantee_disciplinary_field' => $disciplinary_field,
              'grantee_language' => $language,
              'grantee_possible_outcome' => $grantee_possible_outcome,
              'grantee_email' => $this->input->post('email'),
              'grantee_website' => $this->input->post('website')
            );
            if ($param1 == 'create') {
                if ($q) {
                    $this->session->set_flashdata('permission_message', 'Grantee Name already exist.');
                    redirect('adminlogin/show_grant_main_template', 'refresh');
                } else {
                    $result = $this->db->insert('dms_grantee', $data4);
                    $grantee_id = $this->db->insert_id();

                }
            }
        }
        else
        {
            $grantee_id =  $_POST['grantee_id'];
        }
    }
        
       // $id = $this->input->post('email');
        //$q = $this->db->get_where("dms_grant", array('user_email' => $id))->result_array();
      //$q = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();

        $data = array(
            'archiv_tag' => $this->input->post('archiv_tag'),
            'grant_number' => $this->input->post('grant_no'),
            'category_id' => $this->input->post('category'),
            'categorytype_id' => $this->input->post('cat_status'),
            'category_year' => $_POST['grant_year'],
            'grantee_name' => $grantee_id,
            'deliverables' => $deliverables,
              'grantee_geogrtaphical_area' => $geogrtaphical_area,
              'grantee_disciplinary_field' => $disciplinary_field,
              'grantee_language' => $language,
              'grantee_possible_outcome' => $grantee_possible_outcome,
            'grant_amount' => $this->input->post('grant_amount'),
             'created_by' => $userId,
            'start_date' => $this->input->post('start_date'),
            'end_date' => $this->input->post('end_date'),
            'extension' => $this->input->post('extension'),
            'grant_duration' => $this->input->post('grant_duration'),
            'program_executive' => $this->input->post('program_executive'),
            'description_type' => $this->input->post('program_types'),
            'description_sub_type' => $this->input->post('sub_types'),
            'approval_date' => $date,
            
            
            
        );
        //print_r($data); die;
        if($_POST['hidden_id'] != '')
        {
            $id = $this->input->post('hidden_id');
            $this->db->where('id', $id);
            $this->db->update('dms_grant', $data);
            
            $data_grantee= array(
              'grantee_name' => $this->input->post('grantee_name'),
              'grantee_phone' => $this->input->post('phone'),
              'grantee_alternative_no' => $this->input->post('alternative'),
              'grantee_address1' => $this->input->post('address1'),
              'grantee_address2' => $this->input->post('address2'),
              'grantee_city' => $this->input->post('city'),
              'grantee_pin' => $this->input->post('pin'),
              'grantee_country' => $this->input->post('country'),
              'grantee_state' => $state,
              'deliverables' => $deliverables,
              'grantee_geogrtaphical_area' => $geogrtaphical_area,
              'grantee_disciplinary_field' => $disciplinary_field,
              'grantee_language' => $language,
              'grantee_possible_outcome' => $grantee_possible_outcome,
              'grantee_email' => $this->input->post('email'),
              'grantee_website' => $this->input->post('website')
            );
            
            $this->db->where('grantee_id', $_POST['grantee_id']);
            $this->db->update('dms_grantee', $data_grantee);
            
            
            $datas = array(
                               'grant_temp_id' => $id,
                               'category_id' => $this->input->post('category'),
                               'status' => $this->input->post('cat_status'),
                               'year' => $this->input->post('grant_year')
                            );
            
            $this->db->where('grant_temp_id', $id);
            $this->db->update('dms_cat_year', $datas);
            
            
            $this->db->where('grant_temp_id', $id);
            $this->db->delete('dms_grant_metadeta');
            
            $i = 0;
            if($title){
            foreach($title as $valuess)
                    {
                        $data = array(
                               'grant_temp_id' => $id,
                               'grant_metadeta_title' => $title[$i],
                               'grant_metadeta_value' => $value[$i],
                                'deliverables' => $deliverables,
                            );
                        $result = $this->db->insert('dms_grant_metadeta', $data);
                        $i++;
                    }
            
            }
            $grant = $_POST['hidden_id'];
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
          redirect('pelogin/show_template_manage/'.$grant.'/'.$liid, 'refresh');
        }
        
        
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'User Email already exist.');
                redirect('pelogin/show_grant_main_template', 'refresh');
            } else {
                $result = $this->db->insert('dms_grant', $data);
                $last_id = $this->db->insert_id();
                if($last_id)
                {
                    $datas = array(
                               'grant_temp_id' => $last_id,
                               'category_id' => $this->input->post('category'),
                               'status' => $this->input->post('cat_status'),
                               'year' => $this->input->post('grant_year')
                            );
                    
                    $this->db->insert('dms_cat_year', $datas);
                    
                    $i = 0;
                    if($title){
                    foreach($title as $valuess)
                    {
                        $data = array(
                               'grant_temp_id' => $last_id,
                               'grant_metadeta_title' => $title[$i],
                               'grant_metadeta_value' => $value[$i],
                                'deliverables' => $deliverables,
                            );
                        $result = $this->db->insert('dms_grant_metadeta', $data);
                        $i++;
                    }}
                }
                
                
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('pelogin/show_grant_main_template', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('pelogin/show_template_manage', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('id', $id);
            $this->db->update('dms_grant', $data);
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
             //redirect('pelogin/show_grant_main_template', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('dms_grant');
            $this->session->set_flashdata('flash_message', 'Deleted  successfully');
            redirect('pelogin/show_grant_main_template', 'refresh');
        }
    }
    
    
    //for show  a parent templat
    
    public function show_parent_template($param1 = '', $param2 = '') {
      $pagedata['pagetitle'] = 'Parent Template';
      $pagedata['page'] = 'Template';
      $pagedata['param1'] = $param1;
      $this->db->group_by('category_name');
      $pagedata['data1'] = $this->db->get('dms_category')->result_array();
      $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
      $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
      $this->load->view('pe/header',$pagedata);
      $this->load->view('pe/parent_template', $pagedata);
      $this->load->view('pe/footer');
    }
    
    //for add paren Template
    public function add_parent_template($param1 = '', $param2 = '', $param3 = '') {
        //echo "<pre>";
        //print_r($_FILES['detail_image']['name']); 
//        $stt = $this->admin_model->add_parent_img('a', $_FILES['scanned_image'], $_FILES['detail_image']);
//        die;
        ini_set('upload_max_filesize', '1024M'); 
        $grant_id = $this->input->post('grant_id');
        $liid = $this->input->post('liid');
        $pid = $this->input->post('pid');
        $aid = $this->input->post('aid');
        $doc_id = $this->input->post('doc_id');
        
        
        $title = $_POST['title'];
        $type = $_POST['type'];
        $detail = $_POST['detail'];
        $metaValue = $_POST['value'];
        $userId = $this->session->userdata['login']['user_id'];
          if($_POST['keyword']){
        $key = implode(",",$_POST['keyword']);
          }
        $q = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();

        
      $date = date("Y-m-d");
      
      $doc = $_POST['document_id'];
      $staf = $_POST['handling_staff'];
      $auther = $_POST['auther'];
      $copyright = $_POST['copyright'];

        
        
       // $id = $this->input->post('email');
        //$q = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();

        $data = array(
            'grant_temp_id' => $this->input->post('grant_id'),
            'document_temp_id' => $this->input->post('doc_id'),
            'parent_description' => $this->input->post('description'),
            'parent_keyword' => $key,
            'parent_comment' => $this->input->post('comment'),
            'parent_scann_copy' => $copy_img,
            'parent_additional_detail' => $detai_img,
            'created_by' => $userId
           
            //'approval_date' => $date
        );
        //print_r($data); die;
        if($q)
        {
           //print_r($_FILES['detail_image']); die;
            $this->db->where('parent_temp_id', $q['parent_temp_id']);
            $this->db->update('dms_parent_temp', $data);
            
            if($_FILES['scanned_image'] OR $_FILES['detail_image'])
            {
                $this->admin_model->add_parent_img($q['parent_temp_id'], $_FILES['scanned_image'], $_FILES['detail_image']);
            }
             //die;
            
            $this->db->where('template_id', $q['parent_temp_id']);
            $this->db->where('grant_temp_id', $q['grant_temp_id']);
            $this->db->where('document_temp_id', $q['document_temp_id']);
            $this->db->delete('dms_metadata');
            
            
            $i = 0;
            if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' => $q['parent_temp_id'],
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i]

                    //'approval_date' => $date
                );
                
                $result = $this->db->insert('dms_metadata', $data1);
                $i++;
            }}
            
            
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id.'/2', 'refresh');
        }
        else
        {
            $result = $this->db->insert('dms_parent_temp', $data);
            $id = $this->db->insert_id();
            
            //for add scan and Detail image
            $this->admin_model->add_parent_img($id, $_FILES['scanned_image'], $_FILES['detail_image']);
            
            //$stt = $this->admin_model->add_parent_img($id, $_FILES['detail_image'], 'detail');
            
            $i = 0;
            if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' => $id,
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i]

                    //'approval_date' => $date
                );
                
                $result = $this->db->insert('dms_metadata', $data1);
                $i++;
            }
            }
            $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id.'/2', 'refresh');
        }
        
    }
    public function delete_parent_image($param1 = '', $param2 = '', $param3 = '') {
       // echo "hhh"; die;
        $this->db->where('image_id', $_POST['id']);
        $this->db->delete('dms_parent_image');
    }
    
    public function delete_child_image($param1 = '', $param2 = '', $param3 = '') {
       // echo "hhh"; die;
        $this->db->where('image_id', $_POST['id']);
        $this->db->delete('dms_child_image');
    }
    
    //for update paren Template files
    public function update_parent_template($param1 = '', $param2 = '', $param3 = '') {
        $grant_id = $this->input->post('grant_id');
        $liid = $this->input->post('liid');
        $pid = $this->input->post('pid');
        $aid = $this->input->post('aid');
        $doc_id = $this->input->post('doc_id');
        
        $userId = $this->session->userdata['login']['user_id'];
        
        $q = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();

        if($q)
        {
           
//            $this->db->where('parent_temp_id', $q['parent_temp_id']);
//            $this->db->update('dms_parent_temp', $data);
             $result = $this->admin_model->add_parent_img($q['parent_temp_id'], $_FILES['scanned_image'], $_FILES['detail_image']);
            
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id, 'refresh');
        }
        
        
    }
    
    //for add multiple parent Template
    public function add_multiple_parent_template($param1 = '', $param2 = '', $param3 = '') {
//        echo "<pre>";
//        print_r($_POST); die;
        //ini_set('upload_max_filesize', '1024M'); 
        $grant_id = $this->input->post('grant_id');
        $liid = $this->input->post('liid');
        $pid = $this->input->post('pid');
        $aid = $this->input->post('aid');
        $doc_id = $this->input->post('doc_id');
        
        
        $title = $_POST['title'];
        $type = $_POST['type'];
        $detail = $_POST['detail'];
        $metaValue = $_POST['value'];
        $userId = $this->session->userdata['login']['user_id'];
        if($_POST['keyword']){
        $key = implode(",",$_POST['keyword']);
        }
        $q = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();
        

        
      $date = date("Y-m-d");
      
      $doc = $_POST['document_id'];
      $staf = $_POST['handling_staff'];
      $auther = $_POST['auther'];
      $copyright = $_POST['copyright'];

        
        
       // $id = $this->input->post('email');
        //$q = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();

        $data = array(
            'grant_temp_id' => $this->input->post('grant_id'),
            'document_temp_id' => $this->input->post('doc_id'),
            //'parent_description' => $this->input->post('description'),
            'parent_keyword' => $key,
            'parent_comment' => $this->input->post('comment'),
            'parent_scann_copy' => $copy_img,
            'parent_additional_detail' => $detai_img,
            'created_by' => $userId
           
            //'approval_date' => $date
        );
        //print_r($data); die;
        if($q)
        {
           //print_r($_FILES['detail_image']); die;
            $this->db->where('parent_temp_id', $q['parent_temp_id']);
            $this->db->update('dms_parent_temp', $data);
            
            if($_FILES['scanned_image'] OR $_FILES['detail_image'])
            {
                $this->admin_model->add_parent_img($q['parent_temp_id'], $_FILES['scanned_image'], $_FILES['detail_image'], $this->input->post('metadata_individual_number'));
            }
             //die;
            
            $this->db->where('template_id', $q['parent_temp_id']);
            $this->db->where('grant_temp_id', $q['grant_temp_id']);
            $this->db->where('document_temp_id', $q['document_temp_id']);
            $this->db->where('metadata_individual_number', $this->input->post('metadata_individual_number'));
            $this->db->delete('dms_metadata');
            
            
            $i = 0;if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' => $q['parent_temp_id'],
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i],
                    'metadata_individual_number' => $this->input->post('metadata_individual_number')

                    //'approval_date' => $date
                );
                
                $result = $this->db->insert('dms_metadata', $data1);
                $i++;
            }}
            
            
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id, 'refresh');
        }
        else
        {
            $result = $this->db->insert('dms_parent_temp', $data);
            $id = $this->db->insert_id();
            
            //for add scan and Detail image
            $this->admin_model->add_parent_img($id, $_FILES['scanned_image'], $_FILES['detail_image'], $this->input->post('metadata_individual_number'));
            
            //$stt = $this->admin_model->add_parent_img($id, $_FILES['detail_image'], 'detail');
            
            $i = 0;if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' => $id,
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i],
                    'metadata_individual_number' => $this->input->post('metadata_individual_number')

                    //'approval_date' => $date
                );
                
                $result = $this->db->insert('dms_metadata', $data1);
                $i++;
            }
            }
            $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id, 'refresh');
        }
        
    }
    
    //for add multiple child Template
    public function add_multiple_child_template($param1 = '', $param2 = '', $param3 = '') {
        //echo "<pre>";
        //print_r($_FILES['detail_image']['name']); 
//        $stt = $this->admin_model->add_parent_img('a', $_FILES['scanned_image'], $_FILES['detail_image']);
//        die;
        
        $grant_id = $this->input->post('grant_id');
        $liid = $this->input->post('liid');
        $pid = $this->input->post('pid');
        $aid = $this->input->post('aid');
        $doc_id = $this->input->post('doc_id');
        
        
        $title = $_POST['title'];
        $type = $_POST['type'];
        $detail = $_POST['detail'];
        $metaValue = $_POST['value'];
        $userId = $this->session->userdata['login']['user_id'];
          if($_POST['keyword']){
    $key = implode(",",$_POST['keyword']);}
        
        $q = $this->db->get_where("dms_child_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();
        

        
      $date = date("Y-m-d");
      
      $doc = $_POST['document_id'];
      $staf = $_POST['handling_staff'];
      $auther = $_POST['auther'];
      $copyright = $_POST['copyright'];

        
        
       // $id = $this->input->post('email');
        //$q = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();

        $data = array(
            'grant_temp_id' => $this->input->post('grant_id'),
            'document_temp_id' => $this->input->post('doc_id'),
            //'parent_description' => $this->input->post('description'),
            'child_keyword' => $key,
            'child_comment' => $this->input->post('comment'),
            'child_scann_copy' => $copy_img,
            'child_additional_detail' => $detai_img,
            'created_by' => $userId
           
            //'approval_date' => $date
        );
        //print_r($data); die;
        if($q)
        {
           //print_r($_FILES['detail_image']); die;
            $this->db->where('child_temp_id', $q['child_temp_id']);
            $this->db->update('dms_child_temp', $data);
            
            if($_FILES['scanned_image'] OR $_FILES['detail_image'])
            {
                $this->admin_model->add_child_img($q['child_temp_id'], $_FILES['scanned_image'], $_FILES['detail_image'], $this->input->post('metadata_individual_number'));
            }
             //die;
            
            $this->db->where('template_id', $q['child_temp_id']);
            $this->db->where('grant_temp_id', $q['grant_temp_id']);
            $this->db->where('document_temp_id', $q['document_temp_id']);
            $this->db->where('metadata_individual_number', $this->input->post('metadata_individual_number'));
            $this->db->delete('dms_metadata');
            
            
            $i = 0;if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' => $q['child_temp_id'],
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i],
                    'metadata_individual_number' => $this->input->post('metadata_individual_number')

                    //'approval_date' => $date
                );
                
                $result = $this->db->insert('dms_metadata', $data1);
                $i++;
            }
            }
            
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id.'/2', 'refresh');
        }
        else
        {
            $result = $this->db->insert('dms_child_temp', $data);
            $id = $this->db->insert_id();
            
            //for add scan and Detail image
            $this->admin_model->add_child_img($id, $_FILES['scanned_image'], $_FILES['detail_image'], $this->input->post('metadata_individual_number'));
            
            //$stt = $this->admin_model->add_parent_img($id, $_FILES['detail_image'], 'detail');
            
            $i = 0;if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' => $id,
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i],
                    'metadata_individual_number' => $this->input->post('metadata_individual_number')

                    //'approval_date' => $date
                );
                
                $result = $this->db->insert('dms_metadata', $data1);
                $i++;
            }
            }
            $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id.'/2', 'refresh');
        }
        
    }
    
    //for show  a child templat
    
    public function show_child_template($param1 = '', $param2 = '') {
       

            $pagedata['pagetitle'] = 'Child Template';
        
        $pagedata['page'] = 'Template';
        $pagedata['param1'] = $param1;
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        $this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/child_template', $pagedata);
        $this->load->view('pe/footer');
    }
    
    //for add Child Template
    public function add_child_template($param1 = '', $param2 = '', $param3 = '') {
    //echo "<pre>";
       // print_r($_POST);
       //die;
        $grant_id = $this->input->post('grant_id');
        $liid = $this->input->post('liid');
        $pid = $this->input->post('pid');
        $aid = $this->input->post('aid');
        $doc_id = $this->input->post('doc_id');
        
        $title = $_POST['title'];
        $type = $_POST['type'];
        $detail = $_POST['detail'];
        $metaValue = $_POST['value'];
        //print_r($title);die;
        $userId = $this->session->userdata['login']['user_id'];
        if($_POST['keyword']){
        $key = implode(",",$_POST['keyword']);}
        if($doc_id == '38' OR $doc_id == '39' OR $doc_id == '40' OR $doc_id == '41' OR $doc_id == '21' OR $doc_id == '55')
        {
            $q = $this->db->get_where("dms_child_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id'), 'metadata_individual_number' => $this->input->post('metadata_individual_number') ))->row_array();
        }
        else{
            $q = $this->db->get_where("dms_child_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();
        }

        // $q = $this->db->get_where("dms_child_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();
       
      $date = date("Y-m-d");
        
        
      $doc = $_POST['document_id'];
      $staf = $_POST['handling_staff'];
      $auther = $_POST['auther'];
      $copyright = $_POST['copyright'];

        
        
       // $id = $this->input->post('email');
        

        $data = array(
            'grant_temp_id' => $this->input->post('grant_id'),
            'document_temp_id' => $this->input->post('doc_id'),
            'child_description' => $this->input->post('description'),
            'blogs' => $this->input->post('Weblink'),
            'metadata_individual_number' => $this->input->post('metadata_individual_number'),
            'child_keyword' => $key,
            'child_comment' => $this->input->post('comment'),
            'child_scann_copy' => $copy_img,
            'child_additional_detail' => $detai_img,
            'created_by' => $userId
                
                
                
           
            //'approval_date' => $date
        );
      
        
        if($q)
        {//echo "dlktfd";die;
            $this->db->where('child_temp_id', $q['child_temp_id']);
            $this->db->update('dms_child_temp', $data);
            if($_FILES['scanned_image'] OR $_FILES['detail_image'])
            {
                $this->admin_model->add_child_img($q['child_temp_id'], $_FILES['scanned_image'], $_FILES['detail_image']);
            }
            
           $this->db->where('template_id', $q['child_temp_id']);
            $this->db->where('grant_temp_id', $q['grant_temp_id']);
            $this->db->where('document_temp_id', $q['document_temp_id']);
             $this->db->where('metadata_individual_number', $q['metadata_individual_number']);
            $this->db->delete('dms_metadata');
            
            
            $i = 0;if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' => $q['child_temp_id'],
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_individual_number' => $this->input->post('metadata_individual_number'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i],

                    //'approval_date' => $date
                );
               // print_r($data1);
                $result = $this->db->insert('dms_metadata', $data1);
               // echo "<br>".$this->db->last_query();
                $i++;
            }}
           // die;
          // echo "<pre>";
           // print_r($data1);
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            //echo $this->db->last_query();die;
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
             redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id.'/2', 'refresh');
        }
        else
        {
            $result = $this->db->insert('dms_child_temp', $data);
            $id = $this->db->insert_id();
            if($_FILES['scanned_image'] OR $_FILES['detail_image'])
            {
                $this->admin_model->add_child_img($id, $_FILES['scanned_image'], $_FILES['detail_image']);
            }
            
            
            $i = 0;if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' => $id,
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_individual_number' => $this->input->post('metadata_individual_number'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i],

                    //'approval_date' => $date
                );
                //print_r($data1);die;
                $result = $this->db->insert('dms_metadata', $data1);
                $i++;
            }}
            
            $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
             redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id.'/2', 'refresh');
        }
    }
    
    
    public function update_child_template($param1 = '', $param2 = '', $param3 = '') {
        $grant_id = $this->input->post('grant_id');
        $liid = $this->input->post('liid');
        $pid = $this->input->post('pid');
        $aid = $this->input->post('aid');
        $doc_id = $this->input->post('doc_id');
        
        $userId = $this->session->userdata['login']['user_id'];
        
        $q = $this->db->get_where("dms_child_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ))->row_array();

        if($q)
        {
           
//            $this->db->where('parent_temp_id', $q['parent_temp_id']);
//            $this->db->update('dms_parent_temp', $data);
             $result = $this->admin_model->add_child_img($q['parent_temp_id'], $_FILES['scanned_image'], $_FILES['detail_image']);
            
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id, 'refresh');
        }
        
        
    }
    
    
    //for show  a Individual templat
    
    public function show_individual_template($param1 = '', $param2 = '') {
       

            $pagedata['pagetitle'] = 'Individual Template';
        
        $pagedata['page'] = 'Template';
        $pagedata['param1'] = $param1;
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        $this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/individual_template', $pagedata);
        $this->load->view('pe/footer');
    }
    
    //for add Child Template
    public function add_individual_template($param1 = '', $param2 = '', $param3 = '') {
       // echo "<pre>";
       // print_r($_POST); die;
        
        
        $grant_id = $this->input->post('grant_id');
        $liid = $this->input->post('liid');
        $pid = $this->input->post('pid');
        $aid = $this->input->post('aid');
        $doc_id = $this->input->post('doc_id');
        
        
        $title = $_POST['title'];
        $type = $_POST['type'];
        $detail = $_POST['detail'];
        $metaValue = $_POST['value'];
       
        $userId = $this->session->userdata['login']['user_id'];
        if($_POST['keyword']){
        $key = implode(",",$_POST['keyword']);
        }
        $qnum = $this->db->get_where("dms_individual_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id') ));
        $qnum_count = $qnum->num_rows;
        
        $q = $this->db->get_where("dms_individual_temp", array('grant_temp_id' => $this->input->post('grant_id'), 'document_temp_id' => $this->input->post('doc_id'), 'metadata_individual_number' => $this->input->post('metadata_individual_number') ))->row_array();
        
        if ($_FILES['detail_image']['name']) {

            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['detail_image']['name'];
            $file_size = $_FILES['detail_image']['size'];
            $file_tmp = $_FILES['detail_image']['tmp_name'];
            $file_type = $_FILES['detail_image']['type'];
            $file_ext = strtolower(end(explode('.', $_FILES['detail_image']['name'])));
            $expensions = array("gif", "jpg", "jpeg", "png", "mp4", "mp3", "pdf", "pdf", "tiff", "swf", "psd", "bmp", "jpc", "aiff", "wbmp", "xbm");
            if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
            }
            //echo $file_name;die;
            move_uploaded_file($file_tmp, "local_path/" . $file_name);
            $file_img = $file_name;
        } else {
            $file_img = $q['individual_file'];
//
//            $abc = $this->db->get_where("dms_user", array('user_id' => $id))->result_array();
//
//            $file_name = $abc[0]['user_image'];
        }
        
//        echo $file_name;
//        die;
        
      $date = date("Y-m-d");
        
        
      $doc = $_POST['document_id'];
      $staf = $_POST['handling_staff'];
      $auther = $_POST['auther'];
      $copyright = $_POST['copyright'];

        
        
       // $id = $this->input->post('email');
        

        $data = array(
            'grant_temp_id' => $this->input->post('grant_id'),
            'document_temp_id' => $this->input->post('doc_id'),
            'metadata_individual_number' => $this->input->post('metadata_individual_number'),
            //'child_description' => $this->input->post('description'),
            'individual_keyword' => $key,
            'Individual_file' => $file_img,
            'created_by' => $userId
           
            //'approval_date' => $date
        );
        
        if($q)
        {
            $this->db->where('individual_temp_id', $q['individual_temp_id']);
            $this->db->update('dms_individual_temp', $data);
            
           $this->db->where('template_id', $q['individual_temp_id']);
            $this->db->where('grant_temp_id', $q['grant_temp_id']);
            $this->db->where('document_temp_id', $q['document_temp_id']);
            $this->db->where('metadata_individual_number', $q['metadata_individual_number']);
            $this->db->delete('dms_metadata');
            
            
            $i = 0;
            //echo "<pre>"; print_r($metaValue);
            if($title){
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' =>  $q['individual_temp_id'],
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_individual_number' => $this->input->post('metadata_individual_number'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i],

                    //'approval_date' => $date
                );
               // echo "<pre>";
               // print_r($data1);
                $result = $this->db->insert('dms_metadata', $data1);
                //echo $this->db->last_query();
                $i++;
                
            }}
            
           // die;
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
             redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id, 'refresh');
        }
        else
        {
            $result = $this->db->insert('dms_individual_temp', $data);
            $id = $this->db->insert_id();
            
            
            $i = 0;
            foreach($title as $val)
            {
                $data1 = array(
                    'template_id' =>  $id,
                    'grant_temp_id' => $this->input->post('grant_id'),
                    'document_temp_id' => $this->input->post('doc_id'),
                    'metadata_individual_number' => $this->input->post('metadata_individual_number'),
                    'metadata_value' => $metaValue[$i],
                    'metadata_title' => $title[$i],
                    'metadata_type' => $type[$i],
                    'detail' => $detail[$i],

                    //'approval_date' => $date
                );
                
                $result = $this->db->insert('dms_metadata', $data1);
                $i++;
            }
            
            $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
            //echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
             redirect('pelogin/show_template_manage/'.$grant_id.'/'.$liid.'/'.$pid.'/'.$aid.'/'.$doc_id, 'refresh');
        }
    }
    
    
    
    //for User Detail
    public function show_grantee_detail($param1 = '', $param2 = '') {
        

            $pagedata['pagetitle'] = 'Grantee Master list';
        
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_grantee')->result_array();
        //$pagedata['data1'] = $this->db->get_where('dms_role', array('role_status' => 'active'))->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/grantee_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    //for show  addrole page
    public function show_add_grantee($param1 = '', $param2 = '') {
        if ($param2 == 'edit') {

            $pagedata['pagetitle'] = 'Edit Grantee Masterl ist';
        } else {

            $pagedata['pagetitle'] = 'Add Grantee Master list';
        }
        $pagedata['page'] = 'Master';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_grantee')->result_array();
        //$pagedata['data1'] = $this->db->get_where('dms_role', array('role_status' => 'active'))->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/add_grantee', $pagedata);
        $this->load->view('pe/footer');
    }

//for add role record
    public function add_grantee($param1 = '', $param2 = '', $param3 = '') {
        $userId = $this->session->userdata['login']['user_id'];
        $id = $this->input->post('name');
        $q = $this->db->get_where("dms_grantee", array('grantee_name' => $id))->result_array();

        $data = array(
            'grantee_name' => $this->input->post('name'),
            'grantee_phone' => $this->input->post('phone'),
            'grantee_alternative_no' => $this->input->post('alternative'),
            'grantee_area' => $this->input->post('area'),
            'grantee_address' => $this->input->post('address'),
            'grantee_city' => $this->input->post('city'),
            'grantee_state' => $this->input->post('state'),
            'grantee_pin' => $this->input->post('pin'),
            'grantee_country' => $this->input->post('country'),
            'grantee_geogrtaphical_area' => $this->input->post('geogrtaphical_area'),
            'grantee_disciplinary_field' => $this->input->post('disciplinary_field'),
            'grantee_language' => $this->input->post('language'),
            'grantee_possible_outcome' => $this->input->post('grantee_possible_outcome'),
            'grantee_email' => $this->input->post('email'),
            'grantee_website' => $this->input->post('website'),
            'created_by' => $userId
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'User Email already exist.');
                redirect('pelogin/show_add_grantee', 'refresh');
            } else {
                $result = $this->db->insert('dms_grantee', $data);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('pelogin/show_add_grantee', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('pelogin/show_add_grantee', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('grantee_id', $id);
            $this->db->update('dms_grantee', $data);
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            //redirect('adminlogin/show_add_grantee', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('grantee_id', $param2);
            $this->db->delete('dms_grantee');
            $this->session->set_flashdata('flash_message', 'Deleted  successfully');
            redirect('pelogin/show_add_grantee', 'refresh');
        }
    }
    public function demo()
    {
        $this->load->view('admin/demo');
    }
    
    public function template()
    {
        $pagedata['liid'] = $_POST['liid'];
//        print_r($pagedata['liid']); die;     
        if($_POST['temp'] == '1')
        {
            $this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         print_r($pagedata['data1']); die;
         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
         $pagedata['result'] = $this->db->get_where("dms_grant", array('id' => $_POST['id']))->row_array();
         $pagedata['pagetitle'] = 'Grant Template';
            $this->load->view('pe/grant_template', $pagedata);

        }
    }
    
    public function boldHeader()  
    {
        $grant_id = $_POST['id'];         
     $parent = $this->db->get_where('dms_parent_temp', array('grant_temp_id' => $grant_id))->result_array();
         foreach($parent as $parent1)
         {
           $docum[] =  $parent1['document_temp_id'];
         }          
     $child = $this->db->get_where('dms_child_temp', array('grant_temp_id' => $grant_id))->result_array();
          foreach($child as $child1)
         {
           $docum[] =  $child1['document_temp_id'];
         }          
    $individual = $this->db->get_where('dms_individual_temp', array('grant_temp_id' => $grant_id))->result_array();
    foreach($individual as $individual1)
         {
           $docum[] =  $individual1['document_temp_id'];
         }            
         
        $this->db->order_by('document_order_id');
        $doc = $this->db->get_where('dms_document_template', array('document_temp_parent' =>'0',))->result_array();
        ?>
<style>    
.bold123
{
    font-size: 22px !important;
}

</style>
<ul>
                                        <?php 
                                        $i = '1';
                                        foreach($doc as $value){   
                                             ?>
                                        <li class="dropdown-toggle " id="dropdownMenus<?php echo $i; ?>" onclick="test34('<?php echo $i; ?>')" data-toggle="dropdowns" aria-haspopup="true" aria-expanded="true"> 
                               <?php  if($docum) {?>             
                       <a href="javascript:void(0);"  id="doca<?php echo $i; ?>" onclick="documents('<?php echo $value['document_temp_id']; ?>', 'doca<?php echo $i; ?>', 'advancecontents<?php echo $i; ?>')" class="<?php  if(in_array($value['document_temp_id'],$docum)){ ?>bold123<?php } ?>" ><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $value['document_temp_name'];?></a><!--for Parents -->
                               <?php } else { ?>   
                       <a href="javascript:void(0);" id="doca<?php echo $i; ?>" onclick="documents('<?php echo $value['document_temp_id']; ?>', 'doca<?php echo $i; ?>', 'advancecontents<?php echo $i; ?>')"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $value['document_temp_name'];?> </a>
                                        <?php }?>
                                              <ul class="advancecontents" id="advancecontents<?php echo $i; ?>">
                                                <?php 
//                                                $this->db->group_by('status');
                                                $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $value['document_temp_id']))->result_array();
                                                  if($sta) {
                                                      $j = '1';
                                                      foreach($sta as $val)
                                                      {                          
                                                ?>
                                                                                       
                                               <li id="dropdownseconds<?php echo $i.$j; ?>">   
                                         <?php  if($docum) {?>           
                          <a href="javascript:void(0);" class="seca11" id="seca<?php echo $i.$j; ?>" onclick="documents('<?php echo $val['document_temp_id']; ?>', 'seca<?php echo $i.$j; ?>', 'advancecontents<?php echo $i; ?>')"  <?php  if(in_array($value['document_temp_id'],$docum)){ ?>style="font-size:17px;"<?php } ?> ><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $val['document_temp_name']; ?> </a>   <!--for child -->
                                                 <?php } else { ?>   
                       <a href="javascript:void(0);" class="seca11" id="seca<?php echo $i.$j; ?>" onclick="documents('<?php echo $val['document_temp_id']; ?>', 'seca<?php echo $i.$j; ?>', 'advancecontents<?php echo $i; ?>')"> <i class="fa fa-caret-right" aria-hidden="true"></i>  <?php echo $val['document_temp_name']; ?></a>
                                        <?php }?>
                                         
                                               <ul>
                                                    <?php
                                                      $sta = $this->db->get_where('dms_document_template', array('document_temp_parent' => $val['document_temp_id']))->result_array();
                                                      if($sta) {
                                                          $k =1;
                                                      foreach($sta as $val)
                                                      {
                                                        ?>
                                                    
                                                    <li><i class="fa fa-caret-right" aria-hidden="true"></i> 
                                               <?php  if($docum) {?>         
                                                        <a href="javascript:void(0);" class="thia11" id="thia<?php echo $i.$j.$k; ?>" onclick="documents('<?php echo $val['document_temp_id']; ?>', 'thia<?php echo $i.$j.$k; ?>', 'advancecontents<?php echo $i; ?>')" <?php  if(in_array($value['document_temp_id'],$docum)){ ?>style="font-size:17px;"<?php } ?>><?php echo $val['document_temp_name']; ?></a>                 
                                             <?php } else { ?> 
                    <a href="javascript:void(0);" class="thia11" id="thia<?php echo $i.$j.$k; ?>" onclick="documents('<?php echo $val['document_temp_id']; ?>', 'thia<?php echo $i.$j.$k; ?>', 'advancecontents<?php echo $i; ?>')"><?php echo $val['document_temp_name']; ?></a>
                                        <?php }?>
                                                    </li>
                                                       <?php $k++; } } ?>                                                    
                                                </ul>                                                
                                                </li>
                                        <?php $j++; } }?>
                                            </ul>                                        
                                        </li>
                                        <?php $i++; } ?>                                        
                                    </ul>         
<!----------------------JS File ------------------------->
<script>
$(".catsaa").click(function() {
        abc = 'off'; 
        $(".actives").removeClass('actives');
        $('#'+this.id).addClass('actives');
        var currentli = $(this).attr('gg');
        $(".advancecontent").hide();
        $('#'+currentli).show(); 
});
$(".seca11").click(function() {
        abcs = 'on'; 
        $(".active").removeClass('active');
        $('#'+this.id).addClass('active');
        return false
});
$(".thia11").click(function() {
        abcs = 'on'; 
        $(".active").removeClass('active');
        $('#'+this.id).addClass('active');
        return false
          //alert('asd');
});
</script>              
<!-------------------------End Js File ------------------------->




 <?php }
    public function all_template()
    {
         
       // print_r($_POST); die;
        $documentstep = $this->db->get_where("dms_document_template", array('document_temp_id' => $_POST['doc']))->row_array();
        $order_id = $documentstep['document_order_id'];
        $grant = $this->db->get_where("dms_grant", array('id' => $_POST['id']))->row_array();
//        print_r($grant); die;
        $temp = $this->db->get_where("dms_assin_template", array('document_temp_id' => $_POST['doc']))->row_array();
        if($grant)
        {
            $pagedata['liid'] = $_POST['liid'];  //document templett li id
            $pagedata['aid'] = $_POST['aid'];  //category ancher tab id
            $pagedata['pid'] = $_POST['pid'];  //category parent li id
            $pagedata['result'] = $grant;
            $pagedata['doc'] = $_POST['doc'];
            $pagedata['grant'] = $_POST['id'];
            $pagedata['description'] = $temp['description'];
            $pagedata['text_editor'] = $temp['text_editor'];
            $doc = $_POST['doc'];
            if($_POST['doc'] == '1' OR $_POST['doc'] == '8' OR $_POST['doc'] == '12' OR $_POST['doc'] == '22' OR $_POST['doc'] == '24' OR $_POST['doc'] == '46' OR $_POST['doc'] == '26' OR $_POST['doc'] == '27' OR $_POST['doc'] == '29' OR $_POST['doc'] == '42' OR $_POST['doc'] == '43' OR $_POST['doc'] == '44')
            {
                //echo "hh"; die;
                $description = $this->db->get_where("dms_discription", array('Document_temp_id' => $doc))->row_array();
                $pagedata['description'] = $description['description'];
                //echo $description['description']; die;
            }
            if($_POST['doc'] == '9')
            {
                $types = $grant['description_type'];
                $sub_type = $grant['description_sub_type'];
                $description = $this->db->get_where("dms_discription", array('Document_temp_id' => $doc, 'template_type' => $types, 'document_term' => $sub_type))->row_array();
                $pagedata['description'] = $description['description'];
            }
            
            if($temp['template_type_id']=='2' OR $temp['template_type_id']=='5'  OR $temp['template_type_id']=='8')
            {
                
                $doc = $_POST['doc'];
                $parent = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $_POST['id'], 'document_temp_id' => $_POST['doc']))->row_array();
                $parent_img = $this->db->get_where("dms_parent_image", array('parent_temp_id' => $parent['parent_temp_id']))->result_array();
                //print_r($parent_img); die;
                // for procide button
                $pagedata['skip'] = $this->admin_model->skip_temp($_POST['id'], $_POST['doc']);
                
                $stt = $this->admin_model->parent_Status($_POST['id'], $_POST['doc'],$temp['template_type_id']);
                $pagedata['stat'] = $stt;
                //die;
                //die;
                //print_r($parent_img); die;
                $pagedata['parent'] =$parent;
                $pagedata['parent_img'] = $parent_img;
                //echo $order_id;die;
                if($order_id > '2')
                {
                     //echo $temp['template_type_id']; die;
                    
                    $order_id= $order_id - '1';
                    $vali = $this->db->get_where("dms_document_template", array('document_order_id' => $order_id))->row_array();
                    
                    $skip = $this->admin_model->skip_temp($_POST['id'], $vali['document_temp_id']);
                    $pagedata['skip'] = $skip;
                    if($skip == 'false')
                    {
                        $valid = $this->db->get_where("dms_parent_temp", array('grant_temp_id' => $_POST['id'],'document_temp_id' => $vali['document_temp_id'], 'parent_approval' => 'Approved'))->row_array();
                        if($valid)
                        {

                            if($parent)
                            {
                                $pagedata['metadeta'] = $this->db->get_where("dms_metadata", array('template_id' => $parent['parent_temp_id'], 'document_temp_id' => $_POST['doc'], 'grant_temp_id' => $_POST['id']))->result_array();
                            }
                            $pagedata['pagetitle'] = 'Parent Template';
                            if($temp['template_type_id']=='2')
                            {
                                $this->load->view('pe/parent_template', $pagedata);
                                                     ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php

                            }
                            if($temp['template_type_id']=='8')
                            {
                                $this->load->view('pe/multiple_parent_template', $pagedata);
                                                     ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                            }
                            if($temp['template_type_id']=='5')
                            {
                                $this->load->view('pe/parent_for_child', $pagedata);
                                                     ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                            }
                        }
                        else
                        {
                            echo 'no';
                        }
                    }
                    else
                    {//echo $order_id;die;
                        //echo $temp['template_type_id']; die;
                        $pagedata['pagetitle'] = 'Parent Template';
                        if($temp['template_type_id']=='2')
                        {
                            $this->load->view('pe/parent_template', $pagedata);
                                                 ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                        }
                        if($temp['template_type_id']=='8')
                        {
                            $this->load->view('pe/multiple_parent_template', $pagedata);
                                                 ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                        }
                        if($temp['template_type_id']=='5')
                        {
                            $this->load->view('pe/parent_for_child', $pagedata);
                                                 ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                        }
                    }
                }
                else
                {
                    
                    if($parent)
                    {
                        
                        $pagedata['metadeta'] = $this->db->get_where("dms_metadata", array('template_id' => $parent['parent_temp_id'], 'document_temp_id' => $_POST['doc'], 'grant_temp_id' => $_POST['id']))->result_array();
                    
                         
                    }
                    //print_r($_POST); die;
                    $pagedata['pagetitle'] = 'Parent Template';
                    if($temp['template_type_id']=='2')
                    {
                        $this->load->view('pe/parent_template', $pagedata);
                                             ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                    }
                    if($temp['template_type_id']=='8')
                    {
                        $this->load->view('pe/multiple_parent_template', $pagedata);
                                             ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                    }
                    if($temp['template_type_id']=='5')
                    {
                        $this->load->view('pe/parent_for_child', $pagedata);
                                             ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                    }
                }
            }
            elseif($temp['template_type_id']=='3' OR $temp['template_type_id']=='6' OR $temp['template_type_id']=='7')
            { $pagedata['p1'] = $_POST['id'];$pagedata['p2'] = $_POST['doc'];
                $child = $this->db->get_where("dms_child_temp", array('grant_temp_id' => $_POST['id'], 'document_temp_id' => $_POST['doc']))->row_array();
              //echo "<pre>";  print_r($child);die;
                if($child)
                { 
                    $child_img = $this->db->get_where("dms_child_image", array('child_temp_id' => $child['child_temp_id']))->result_array();
                    if($child_img){
                        $pagedata['child_img'] = $child_img;
                    }
                    $pagedata['child'] = $child;
                    
                    $pagedata['param2'] = 'edit';
                    $qnum =  $this->db->get_where("dms_child_temp", array('grant_temp_id' => $_POST['id'], 'document_temp_id' => $_POST['doc']));
                    $pagedata['qnum_count'] = $qnum->num_rows;
                    if($_POST['doc'] == '38' OR $doc == '39' OR $doc == '40' OR $doc == '41' OR $doc == '21' OR $doc == '55')
                    {
                     // $pagedata['metadeta'] = $this->db->get_where("dms_metadata", array('template_id' => $child['child_temp_id'], 'document_temp_id' => $_POST['doc'], 'grant_temp_id' => $_POST['id']))->result_array();
                      
                    }
                    else{
                    $pagedata['metadeta'] = $this->db->get_where("dms_metadata", array('template_id' => $child['child_temp_id'], 'document_temp_id' => $_POST['doc'], 'grant_temp_id' => $_POST['id']))->result_array();
                    //$pagedata['metadeta'] = $this->db->get_where("dms_child_metadata", array('child_temp_id' => $child['child_temp_id'], 'document_temp_id' => $_POST['doc']))->result_array();
                    }
                }
                //echo 'hh';die;
                $pagedata['pagetitle'] = 'Child Template';
                if($temp['template_type_id']=='3')
                {
                    $this->load->view('pe/child_template', $pagedata);
                     ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                }
                if($temp['template_type_id']=='7')
                {
                    $this->load->view('pe/multiple_child_template', $pagedata);
                     ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                }
                if($temp['template_type_id']=='6')
                { 
                    $this->load->view('pe/child_for_individual', $pagedata);
                     ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
                }
            }
            elseif($temp['template_type_id']=='4')
            {
                //echo "hhh"; die;
                $individual = $this->db->get_where("dms_individual_temp", array('grant_temp_id' => $_POST['id'], 'document_temp_id' => $_POST['doc']))->result_array();
                if($individual)
                {
                    $pagedata['individual'] =$individual;
                    $pagedata['param2'] == 'edit';
                    
                    $qnum = $this->db->get_where("dms_individual_temp", array('grant_temp_id' => $_POST['id'], 'document_temp_id' => $_POST['doc'] ));
                    $pagedata['qnum_count'] = $qnum->num_rows;
                    
                    //$pagedata['metadeta'] = $this->db->get_where("dms_metadata", array('template_id' => $individual['individual_temp_id'], 'document_temp_id' => $_POST['doc'], 'grant_temp_id' => $_POST['id']))->result_array();
                    
                    //$pagedata['metadeta'] = $this->db->get_where("dms_individual_metadata", array('individual_temp_id' => $individual['individual_temp_id'], 'document_temp_id' => $_POST['doc']))->result_array();
                }
                $pagedata['pagetitle'] = 'Individual Template';
                $this->load->view('pe/individual_template', $pagedata);
                ?>
<script>
                                    abss();
//        function aa(){
//            alert('dd');
//        }

</script>
<?php
            }
        }
            
    }
    
    
    
    //for show  create template page
    public function show_create_template($param1 = '', $param2 = '') {
       
//$this->admin_model->fetchCategoryTree();
            $pagedata['pagetitle'] = 'Create Grant';
        
        $pagedata['page'] = 'Grant';
        $pagedata['param1'] = $param1;
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        $this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
         $pagedata['data3'] = $this->db->get('dms_document_template')->result_array();
         
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/create_template', $pagedata);
        $this->load->view('pe/footer');
    }
    
    //for show  create template page
    public function edit_create_template($param1 = '', $param2 = '') {
       
//$this->admin_model->fetchCategoryTree();
            $pagedata['pagetitle'] = 'Edit Grant';
        
        $pagedata['page'] = 'Grant';
        $pagedata['param1'] = $param1;
//        $pagedata['data'] = $this->db->get('dms_category')->result_array();
        //$this->db->group_by('category_name');
         $pagedata['data1'] = $this->db->get('dms_template_type')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_document_template')->result_array();
         
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/edit_assin_template', $pagedata);
        $this->load->view('pe/footer');
    }
    
    // get user by id
    public function getuser()
    {
        $id = $_POST['id'];
        $data = $this->db->get_where("dms_user", array('user_role' => $id))->result_array();
        if($data){
        echo '<option value="">Select User</option>';
            $i = '1';
            foreach ($data as $row1) {
                echo '<option value="' . $row1['user_id'] . '">' . $row1['user_name'] .' </option>';  $i++;
            }
        } else {

            echo '<option value="">Select User</option>';
        }
    }


    //for Assin template
    public function add_create_template($param1 = '', $param2 = '', $param3 = '') {
      $date = date("Y-m-d");
      $group = $_POST['group'];
      $user = $_POST['user'];
//      echo "<pre>";  
//      print_r($_POST); die;
        $id = $this->input->post('temp');
        $q = $this->db->get_where("dms_assin_template", array('document_temp_id' => $id))->result_array();

        $data = array(
            'template_type_id' => $this->input->post('temp_type'),
            'document_temp_id' => $this->input->post('temp'),
            'assin_temp_step' => $this->input->post('step'),
            'assin_date' => $date
        );
        if ($param1 == 'create') {
            if ($q) {
                $this->session->set_flashdata('permission_message', 'Template  already exist.');
                redirect('pelogin/show_create_template', 'refresh');
            } else {
                $result = $this->db->insert('dms_assin_template', $data);
                 $id = $this->db->insert_id();
                 
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('pelogin/show_create_template', 'refresh');
                } else {
                if($group)  
                {
                    $i = 0;
                    foreach($group as $val)
                    {
                       $data2 = array(
                           'assin_temp_id' => $id,
                           'role_id' => $group[$i],
                           'user_id' => $user[$i]
                       );
                       $result = $this->db->insert('dms_workflow', $data2);

                       $i++;
                    }
                }
                    $this->session->set_flashdata('flash_message', 'Record inserted  successfully.');
                    redirect('pelogin/show_create_template', 'refresh');
                }
            }
        } else if ($param1 == 'edit') {
            $id = $this->input->post('hidden_id');
            $this->db->where('assin_temp_id', $id);
            $this->db->update('dms_assin_template', $data);
            
            if($group)  
            {
                $this->db->where('assin_temp_id', $id);
                $this->db->delete('dms_workflow');

                $i = 0;
                 foreach($group as $val)
                 {
                    $data2 = array(
                        'assin_temp_id' => $id,
                        'role_id' => $group[$i],
                        'user_id' => $user[$i]
                    );
                    $result = $this->db->insert('dms_workflow', $data2);
                    
                    $i++;
                }
            }
            
            $this->session->set_flashdata('flash_message', 'Updated  successfully.');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            //redirect('adminlogin/show_add_user', 'refresh');
        } else if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('dms_assin_template');
            $this->session->set_flashdata('flash_message', 'Deleted  successfully');
            redirect('pelogin/show_create_template', 'refresh');
        }
    }
    
    
    // show assign template page Detail
    public function detail_create_template($param1 = '', $param2 = '') {
       

            $pagedata['pagetitle'] = 'Create Template Detail';
        
        $pagedata['page'] = 'Template';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get('dms_assin_template')->result_array();
//        $this->db->group_by('category_name');
//         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/create_temp_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    // show assign template data page Detail
    public function show_template_manage($param1 = '', $param2 = '', $param3 = '', $param4 = '', $param5 = '',$param6 = '') {
        
      $pagedata['pagetitle'] = 'Grant Management';
        
        $pagedata['page']   =  'Grant';
        $pagedata['param1'] = $param1;
        $pagedata['param2'] = $param2;
        $pagedata['param3'] = $param3;
        $pagedata['param4'] = $param4;
        $pagedata['param5'] = $param5;
        $pagedata['param6'] = $param6;
        $pagedata['cat'] = $this->db->get('dms_category')->result_array();

        $this->db->order_by('document_order_id');
        $pagedata['doc'] = $this->db->get_where('dms_document_template', array('document_temp_parent' =>'0',))->result_array();
//        $this->db->group_by('category_name');
//         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/create_temp_managemrnt', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function change_temp_status()
    {
        $userId = $this->session->userdata['login']['user_id'];
        //echo "<pre>";
        //print_r($_POST);
        //$parent = $this->db->get_where("dms_parent_temp", array('parent_temp_id' => $_POST['id'],'parent_status' => "Completed"))->row_array();
        
        //$data = array('parent_status' => "Completed");
        $this->db->where('parent_temp_id', $_POST['id']);
        $this->db->update('dms_parent_temp', array('parent_status' => "Completed"));
        
        $parent = $this->db->get_where("dms_parent_temp", array('parent_temp_id' => $_POST['id']))->row_array();
        $parent['parent_temp_id'];
        $parent['grant_temp_id'];
        $parent['document_temp_id'];
        
        if($parent['document_temp_id'] == '16')
        {
            
            $this->db->where('grant_temp_id',  $parent['grant_temp_id']);
            $this->db->update('dms_assign_document', array('approval' => "Completed"));
        }
        
        $q = $this->db->get_where("dms_temp_approve", array('parent_temp_id' => $_POST['id']))->row_array();
        
        $assinTemp = $this->db->get_where("dms_assin_template", array('document_temp_id' => $parent['document_temp_id']))->row_array();
         
        $workflow = $this->db->get_where("dms_workflow", array('assin_temp_id' => $assinTemp['assin_temp_id']))->result_array();
        //print_r($workflow);
        
        if($q)
        {
            $this->db->where('parent_temp_id', $q['parent_temp_id']);
            $this->db->where('temp_approve_status', 'Rejected');
            $this->db->update('dms_temp_approve', array('temp_approve_status' => "Pending"));
        }
        else
        {
            // for Dms tApproval template
            $stt = $this->admin_model->statusApproval_Status($workflow, $parent,$userId);

        }
    }
    
    
    
    public function completestatus()
    {
        //print_r($_POST);
        $this->db->where('id', $_POST['id']);
        $this->db->update('dms_grant', array('categorytype_id' => '1'));

        $this->db->where('grant_temp_id', $_POST['id']);
        $this->db->update('dms_cat_year', array('status' => '1'));
    }
    
    
    public function show_template_approval($param1 = '', $param2 = '')
    {
        
        $pagedata['pagetitle'] = 'Grant Approval Detail';
        
        $pagedata['page'] = 'Approval';
        $pagedata['param1'] = $param1;
        
        $userId = $this->session->userdata['login']['user_id'];
        $pagedata['data'] = $this->db->get_where("dms_temp_approve", array('user_id' => $userId, 'temp_approve_status' => 'Pending'))->result_array();

        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/approval_temp_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function show_template_approvalcomplete($param1 = '', $param2 = '')
    {
        
        $pagedata['pagetitle'] = 'Grant Approval Detail';
        
        $pagedata['page'] = 'Approval';
        $pagedata['param1'] = $param1;
        
        $userId = $this->session->userdata['login']['user_id'];
        $pagedata['data'] = $this->db->get_where("dms_temp_approve", array('user_id' => $userId, 'temp_approve_status' => 'Approved'))->result_array();

        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/approval_completed_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function approval_template($param1 = '', $param2 = '', $param3 = '')
    {
        //echo $param2; die;
        $pagedata['pagetitle'] = 'Approval Grant';
        
        $pagedata['page'] = 'Approval';
        $pagedata['param1'] = $param1;
         $pagedata['param2'] = $param2;
         $pagedata['param3'] = $param3;
        
        $userId = $this->session->userdata['login']['user_id'];
        $data = $this->db->get_where("dms_temp_approve", array('temp_approve_id' => $param1))->row_array();
        
        
        
        $pagedata['data'] = $data;

        $this->db->order_by('document_order_id');
        $pagedata['doc'] = $this->db->get_where('dms_document_template', array('document_temp_id' =>$data['document_temp_id']))->result_array();
        $pagedata['grant'] = $this->db->get_where('dms_grant', array('id' => $data['grant_temp_id']))->row_array();
        //$grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $grant['grantee_name']))->row_array();
        
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/approval_template', $pagedata);
        $this->load->view('pe/footer');
    }
       public function approval_templates($param1 = '', $param2 = '', $param3 = '')
    {
        //echo $param2; die;
        $pagedata['pagetitle'] = 'Approval Grant';
        
        $pagedata['page'] = 'Approval';
        $pagedata['param1'] = $param1;
         $pagedata['param2'] = $param2;
         $pagedata['param3'] = $param3;
        
        $userId = $this->session->userdata['login']['user_id'];
        $data = $this->db->get_where("dms_temp_approve", array('temp_approve_id' => $param1))->row_array();
        
        
        
        $pagedata['data'] = $data;

        $this->db->order_by('document_order_id');
        $pagedata['doc'] = $this->db->get_where('dms_document_template', array('document_temp_id' =>$data['document_temp_id']))->result_array();
        $pagedata['grant'] = $this->db->get_where('dms_grant', array('id' => $data['grant_temp_id']))->row_array();
        //$grantee = $this->db->get_where('dms_grantee', array('grantee_id' => $grant['grantee_name']))->row_array();
        
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/approval_templates', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function approval_statusChange($param1 = '', $param2 = '')
    {
         $userId = $this->session->userdata['login']['user_id'];
//         echo "<pre>";
//         print_r($_POST); die;
         $data = array('temp_approve_status' => $_POST['status'],
                      'temp_approve_comment' => $_POST['description'],
             
             );
         $this->db->where('temp_approve_id', $_POST['id']);
         $this->db->update('dms_temp_approve', $data);
         
          $q = $this->db->get_where("dms_temp_approve", array('temp_approve_id' => $_POST['id']))->row_array();
          $par = $q['parent_temp_id'];
          $docc = $q['document_temp_id'];
          $grnt = $q['grant_temp_id'];
          $step = $q['temp_approve_step'] + '1';
         
        $this->db->where('parent_temp_id', $q['parent_temp_id']);
        $this->db->update('dms_parent_temp', array('parent_status' => $_POST['status']));
        
        
            $steps = $this->db->get_where("dms_temp_approve", array('parent_temp_id' => $parent['parent_temp_id'], 'grant_temp_id' => $parent['grant_temp_id'], 'document_temp_id' => $parent['document_temp_id'], 'temp_approve_step' => $step))->row_array();
            if($steps)
            {
                $stt = $this->admin_model->Approval_mail($steps['user_id'], $q);
            }
            
        if($_POST['status'] == 'Rejected')
        {
            $this->db->where('temp_approve_status', 'Approved');
            $this->db->where('parent_temp_id', $par );
            $this->db->update('dms_temp_approve', array('temp_approve_status' => $_POST['status']));
            
            $this->db->where('parent_temp_id', $q['parent_temp_id']);
            $this->db->update('dms_parent_temp', array('terminate_status' => 'False'));
        }
        
        //$result = $this->db->get_where("dms_temp_approve", array('parent_temp_id' => $q['parent_temp_id'],'document_temp_id' => $q['document_temp_id'], 'temp_approve_status' => 'pending'))->row_array();
        $sqls = "SELECT * FROM `dms_temp_approve` WHERE `parent_temp_id` = $par AND `document_temp_id` = $docc AND `temp_approve_status` = 'Pending' OR `temp_approve_status` = 'Rejected'";
            $querys = $this->db->query($sqls)->result_array() ;
            if($querys)
            {
                $this->db->where('parent_temp_id', $q['parent_temp_id']);
                $this->db->update('dms_parent_temp', array('parent_approval' => 'Pending'));
            }
            else
            {
                $this->db->where('parent_temp_id', $q['parent_temp_id']);
                $this->db->update('dms_parent_temp', array('parent_approval' => 'Approved'));
                
                $qr = $this->db->get_where("dms_parent_temp", array('parent_temp_id' => $q['parent_temp_id'], 'terminate_status' => 'True'))->row_array();
                if($qr)
                {
                    $this->db->where('id', $qr['grant_temp_id']);
                    $this->db->update('dms_grant', array('categorytype_id' => '1'));
                }
            }
            
            $data1 = array('temp_approve_id' => $_POST['id'],
                   'approvel_by' => $q['user_id'],
                   'comments' => $_POST['description'],
                   'approval_status' => $_POST['status']
                   
                   );
                   //print_r($data1);

                $result = $this->db->insert('dms_approval_comment', $data1);
                
                $order = $this->db->get_where("dms_document_template", array('document_order_id' => '23'))->row_array();
                $temp_id = $order['document_temp_id'];
                
                $final = "SELECT * FROM `dms_temp_approve` WHERE `parent_temp_id` = $par AND `document_temp_id` = $temp_id  AND `temp_approve_status` = 'Approved'";
                $querys = $this->db->query($final)->result_array() ;
                if($querys)
                {
                    $this->db->where('id', $grnt);
                    $this->db->update('dms_grant', array('categorytype_id' => '1'));
                    
                    $this->db->where('grant_temp_id', $grnt);
                    $this->db->update('dms_cat_year', array('status' => '1'));
                }
         
          $this->session->set_flashdata('flash_message', $_POST['status'].' successfully');
          redirect('pelogin/show_template_approval', 'refresh');
    }
    
    
     public function reject_temp_detail($param1 = '', $param2 = '') {
       //echo "gggggg"; die;

        $pagedata['pagetitle'] = 'Reject Grant';
        $pagedata['docstatus'] = 'Rejected';
        $pagedata['reasion'] = 'Rejected Reason';    
        $pagedata['page'] = 'Approval';
        $pagedata['param1'] = $param1;
        $this->db->where('approval_status', 'Rejected');
        $this->db->group_by('temp_approve_id');
        $pagedata['data'] = $this->db->get('dms_approval_comment')->result_array();
//        $this->db->group_by('category_name');
//         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/reject_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function allCommentbyapprove() {
        $userId = $this->session->userdata['login']['user_id'];
        $data = $this->db->get_where('dms_approval_comment', array('temp_approve_id'=> $_POST['id']))->result_array();
        //print_r($data);
        foreach($data as $value)
        {
            if($value['approvel_by'] != $userId){
        ?>
                                    <div class=" col-md-12">                
                        <label class="control-label col-md-4">Comment : </label>
                        <div class="col-md-8">

                                       <?php echo $value['comments']; ?>                                                                       
                        </div>
                    </div>
                        </br>
            
        <?php }
        else{
            ?>
                        <div class=" col-md-12">
                        <label class="control-label col-md-4">Reply : </label>
                        <div class="col-md-8">

                                       <?php echo $value['comments']; ?>                                                                       
                        </div>
                        </div>
                        </br>
      <?php
        }
        }
    }
    
    public function approval_reaply() {
       // print_r($_POST);
        $RoleId = $this->session->userdata['login']['user_role'];
        $data1 = array('temp_approve_id' => $_POST['approval_id'],
                   'approvel_by' =>  $_POST['userid'],
                   'comments' => $_POST['comment'],
                   'approval_status' => $_POST['status'],
                   'type' => 'reply'
                   
                   );
        $result = $this->db->insert('dms_approval_comment', $data1);
        $last_id = $this->db->insert_id();
        
        $this->db->where('approvel_comment_id', $_POST['comment_id']);
        $this->db->update('dms_approval_comment', array('notification' => $last_id));
        
        if($RoleId == '6' OR $RoleId == '7')
        {
            $this->db->where('temp_approve_id', $_POST['approval_id']);
            $this->db->update('dms_temp_approve', array('notification' => NULL));
        }
        else
        {
            $this->db->where('temp_approve_id', $_POST['approval_id']);
            $this->db->update('dms_temp_approve', array('notification' => $last_id));
        }
        
         $this->session->set_flashdata('flash_message', 'Comment Add  successfully');
         echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
    }
    public function comment_temp_detail($param) {
        $pagedata['pagetitle'] = 'comment';
        
        $pagedata['reasion'] = 'Comment Reason';    
        $pagedata['page'] = 'Approval / Reject';
        $pagedata['param1'] = $param1;
        $pagedata['docstatus'] = 'Rejected';
        $pagedata['data'] = $this->db->get_where('dms_approval_comment', array('approval_status'=> 'Rejected'))->result_array();
//        $this->db->group_by('category_name');
//         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/reply_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function pending_temp_detail($param1 = '', $param2 = '') {
       
         $data1=array(
                     'notification'=>'seen'
                 );
             $this->db->where('temp_approve_status', 'Pending');
            $this->db->update('dms_temp_approve', $data1);
        
        $userId = $this->session->userdata['login']['user_id'];
            $pagedata['pagetitle'] = 'Pending Grant';
        
        $pagedata['reasion'] = 'Pending Reason';    
        $pagedata['page'] = 'Approval';
        $pagedata['param1'] = $param1;
        $pagedata['docstatus'] = 'Pending';
        $pagedata['data'] = $this->db->get_where('dms_temp_approve', array('temp_approve_status'=> 'Pending', 'approval_sender' => $userId ))->result_array();
//        $this->db->group_by('category_name');
//         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/pending_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function approved_temp_detail($param1 = '', $param2 = '') {
                 $data=array(
                     'notification'=>'seen'
                 );
             $this->db->where('temp_approve_status', 'Approved');
            $this->db->update('dms_temp_approve', $data);
        
        
        
        $pagedata['pagetitle'] = 'Approved Grant';
        
         $pagedata['reasion'] = 'Approved Reason'; 
        $pagedata['page'] = 'Approval';
        $pagedata['docstatus'] = 'Approved';
        $pagedata['param1'] = $param1;
         $this->db->where('approval_status', 'Approved');
        $this->db->group_by('temp_approve_id');
        $pagedata['data'] = $this->db->get('dms_approval_comment')->result_array();
       // $pagedata['data'] = $this->db->get_where('dms_approval_comment', array('approval_status'=> 'Approved'))->result_array();
//        $this->db->group_by('category_name');
//         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/reject_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    
    public function user_comment()
    {
        $userId = $this->session->userdata['login']['user_id'];
        //print_r($_POST);
        $data = array('grant_temp_id' => $_POST['grant'],
                   'document_temp_id' => $_POST['document'],
                   'user_comment' => $_POST['comment'],
                    'created_by' => $userId
                   
        );
         $result = $this->db->insert('dms_user_comment', $data);
         $this->session->set_flashdata('flash_message', 'Comment Add  successfully');
         echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
    }


    public function View_user_comment($param1 = '', $param2 = '') {
       

        $pagedata['pagetitle'] = 'Comments Details';
        
        
        $pagedata['page'] = 'comments';
        $pagedata['param1'] = $param1;
        $pagedata['data'] = $this->db->get_where('dms_user_comment')->result_array();
       
//        $this->db->group_by('category_name');
//         $pagedata['data1'] = $this->db->get('dms_category')->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/user_comment_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function assignForArc()
    {
        $userId = $this->session->userdata['login']['user_id'];
        //print_r($_POST); die;
        $user = $this->db->get_where('dms_user', array('user_id'=> $_POST['user_id']))->row_array();
        $email = $user['user_email'];
        $result = $this->db->get_where('dms_assign_document', array('grant_temp_id'=> $_POST['grant'], 'document_temp_id' => $_POST['doc']))->result_array();
        $data = array('grant_temp_id' => $_POST['grant_id'],
                   'document_temp_id' => $_POST['doc_id'],
                   'approval' => 'Pending',
                    'assign_by' => $userId,
                    'user_id' => $_POST['user_id']
                   
        );
        if($result)
        {
            //echo "no";
            $this->session->set_flashdata('permission_message', 'already exist.');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
        else
        {
            $result = $this->db->insert('dms_assign_document', $data);
            
            //$email = 'sunny@livesoftwaresolution.com';
            $this->load->library('email', $config);  // mail to lss
            $this->email->from('dms@erp1.in', 'DMS');
            // $this->email->to($email);
            $this->email->to($email);
             $body = 'You have registered successfully.
                    UserId:' . $email . '
                    password:' . $pass . '    
                Please click the following link to activate your account';
                   // <a href="http://www.crm.mauzoo.com/client/confirm/' . $insert . '">Confirm</a>';
            $this->email->subject('Email Confirmation');
            $this->email->message($body);
            $path = './user/images/';
            // $this->email->attach($path.$img);
            $email_success = $this->email->send();
            //echo "yes";
            $this->session->set_flashdata('flash_message', 'Assign  successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }
    
    // show assign template data page Detail
    public function show_assign_document_temp($param1 = '', $param2 = '') {
       
        $userId = $this->session->userdata['login']['user_id'];

            $pagedata['pagetitle'] = 'Grant Management';
        
        $pagedata['page'] = 'Template';
        $pagedata['param1'] = $param1;
        
        $this->db->group_by('grant_temp_id');
        $pagedata['data_cat'] = $this->db->get_where('dms_assign_document', array('approval' =>'Pending', 'user_id' => $userId))->result_array();
        
        //$pagedata['cat'] = $this->db->get('dms_category')->result_array();
        
        $this->db->group_by('document_temp_id');
        $pagedata['data1'] = $this->db->get_where('dms_assign_document', array('approval' =>'Pending', 'user_id' => $userId))->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/assign_doc_temp', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function selectCategory()
    {
        //$this->db->group_by('document_temp_id');
        $data1 = $this->db->get_where('dms_assign_document', array('grant_temp_id' =>$_POST['grant'], 'approval' =>'Pending'))->result_array();
        ?>

            <ul>
                <?php foreach($data1 as $value){

                    $doc = $this->db->get_where('dms_document_template', array('document_temp_id' => $value['document_temp_id']))->row_array();
//                                            
//                                            if($assin) { ?>
                <li> <i class="fa fa-caret-right" aria-hidden="true"></i>
                    <a href="#" onclick="documents('4','<?php echo $doc['document_temp_id']; ?>')"><?php  echo $doc['document_temp_name'];?></a>


                </li>
                <?php } ?>

            </ul>

<?php
        
    }
    
    public function grantee_grant_detail($param1 = '', $param2 = '')
    {
        //echo $param1.$param2; die;
        $pagedata['pagetitle'] = 'Grant Detail';
        
        $pagedata['page'] = 'Grantee';
        $pagedata['param1'] = $param1;
        
       // $this->db->group_by('grant_temp_id');
        //$pagedata['data_cat'] = $this->db->get_where('dms_assign_document', array('approval' =>'Pending'))->result_array();
        
        //$pagedata['cat'] = $this->db->get('dms_category')->result_array();
        
        //$this->db->group_by('document_temp_id');
        $pagedata['data'] = $this->db->get_where('dms_grant', array('grantee_name' =>$param1, 'categorytype_id' =>$param2))->result_array();
//         $pagedata['data2'] = $this->db->get('dms_grantee')->result_array();
//         $pagedata['data3'] = $this->db->get('dms_categorytype')->result_array();
        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/grantee_grant_detail', $pagedata);
        $this->load->view('pe/footer');
    }
    
    public function add_state()
    {
        //print_r($_POST); die;
        $da = $this->db->get_where("dms_state", array('state_name' => $_POST['name'] ))->result_array();
        if($da){
            
        }
        else
        {
            $data = array('state_name' => $_POST['name']);
            $result = $this->db->insert('dms_state', $data);
            $last_id = $this->db->insert_id();
            
        }
        
        return $stt = $this->admin_model->dropdown_state();
    }
    
    public function add_deliverables()
    {
        //print_r($_POST); die;
        $da = $this->db->get_where("dms_deliverables", array('deliverables_name' => $_POST['name'] ))->result_array();
        if($da){
            
        }
        else
        {
            $data = array('deliverables_name' => $_POST['name']);
            $result = $this->db->insert('dms_deliverables', $data);
            $last_id = $this->db->insert_id();
            
        }
        
        return $stt = $this->admin_model->dropdown_deliverables();
    }
    
    
    public function add_language()
    {
        //print_r($_POST); die;
        $da = $this->db->get_where("dms_language", array('language_name' => $_POST['name'] ))->result_array();
        if($da){
            
        }
        else
        {
            $data = array('language_name' => $_POST['name']);
            $result = $this->db->insert('dms_language', $data);
            $last_id = $this->db->insert_id();
           
        }
        
        return $stt = $this->admin_model->dropdown_language();
    }
    
    public function add_outcome()
    {
        //print_r($_POST); die;
        $da = $this->db->get_where("dms_outcome", array('outcome_name' => $_POST['name'] ))->result_array();
        if($da){
            
        }
        else
        {
            $data = array('outcome_name' => $_POST['name']);
            $result = $this->db->insert('dms_outcome', $data);
            $last_id = $this->db->insert_id();
            
        }
        
        return $stt = $this->admin_model->dropdown_outcome();
    }
    
    
    public function adddisiplinary_field()
    {
        //print_r($_POST); die;
        $da = $this->db->get_where("dms_disciplinary_field", array('field_name' => $_POST['name'] ))->result_array();
        if($da){
            
        }
        else
        {
            $data = array('field_name' => $_POST['name']);
            $result = $this->db->insert('dms_disciplinary_field', $data);
            //$last_id = $this->db->insert_id();
            
        }
        return $stt = $this->admin_model->dropdown_disiplinary();
    }
    
    
    
    public function csvfile($param1 = '', $param2 = '') {
       
        $userId = $this->session->userdata['login']['user_id'];

        $this->load->view('pe/header',$pagedata);
        $this->load->view('pe/addcsv', $pagedata);
        $this->load->view('pe/footer');
    }
    
    
    public function uploadData()
    {
         $this->load->library('Excel');
          //$inputfilename = $_FILES['userfile']['name'];
      //$sheet = $objPHPExcel->getActiveSheet();
         // move_uploaded_file($file_tmp, "uploads/" . $file_name);
        $config['upload_path'] = './local_path/'; 
        $config['allowed_types'] = 'xlsx|csv|xls';



        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $config['upload_path'] . $_FILES["userfile"]['name'])) 
        {


        }



   $inputfilename = FCPATH.'local_path/'.$_FILES['userfile']['name'];
   $exceldata = array();
      try
   {

       $inputfiletype = PHPExcel_IOFactory::identify($inputfilename);

       $objReader = PHPExcel_IOFactory::createReader($inputfiletype);
       $objPHPExcel = $objReader->load($inputfilename);

   }
   catch(Exception $e)
   {
       die('Error loading file "'.pathinfo($inputfilename,PATHINFO_BASENAME).'": '.$e->getMessage());
   }

   //  Get worksheet dimensions
   $sheet = $objPHPExcel->getSheet(0); 
   $highestRow = $sheet->getHighestRow(); 
   $highestColumn = $sheet->getHighestColumn();

   //  Loop through each row of the worksheet in turn
   for ($row = 1; $row <= $highestRow; $row++)
   { 
       if($row!=1){
       //  Read a row of data into an array
       $csv_line = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
       print_r($csv_line); die;

       //  Insert row data array into your database of choice here
            if($csv_line[0][0])
            {

                    $admin_record=$this->session->userdata('front_admin');
                   $data = array();
                   $data['ecom_product_name']=$csv_line[0][0];
                   $data['ecom_product_part_n']=$csv_line[0][1];
                   $data['ecom_product_manufacturer']=$csv_line[0][2];
                   $data['ecom_product_cat_id']=$csv_line[0][3];
                        $data['ecom_product_sub_cat_id']=$csv_line[0][4];
                   $data['ecom_product_description']=$csv_line[0][5];

                   $data['ecom_product_availability']= date( "Y-m-d", strtotime($csv_line[0][6]) );
                   $data['ecom_product_sale_price']=$csv_line[0][7];
                   $data['ecom_product_quantity']=$csv_line[0][8];
                   $data['ecom_product_min_time']=$csv_line[0][9];
                   $data['ecom_product_max_time']=$csv_line[0][10];
                   $data['ecom_product_compatible_machines']=$csv_line[0][11];
                   $data['ecom_product_original_purchased_date']= date( "Y-m-d", strtotime($csv_line[0][12]) );
                   $data['ecom_product_condition']=$csv_line[0][13];
                   $data['ecom_product_storage_conditions']=$csv_line[0][14];
                   $data['ecom_product_additional_information']=$csv_line[0][15];
                   $data['ecom_product_operational_hours']=$csv_line[0][16];

                           $manu= $this->db->get_where('ecom_manufacture_brand',array('ecom_manufacture_brand_name'=>$data['ecom_product_manufacturer']))->row_array();
                           if($manu['ecom_manufacture_brand_id'])
                           {
                               $data['ecom_product_manufacturer']=$manu['ecom_manufacture_brand_id'];
                           }
                           else
                           {
                                  $main['ecom_manufacture_brand_name']=$data['ecom_product_manufacturer'];
                                    $main['ecom_manufacture_brand_addedby']= $admin_record['id'];

                                    $this->db->insert('ecom_manufacture_brand',$main);
                                    $data['ecom_product_manufacturer'] =$this->db->insert_id();   

                           }


                           $cat=$this->db->get_where('ecom_cat',array('ecom_cat_name'=>$data['ecom_product_cat_id']))->row_array();
                           if($cat['ecom_cat_id'])
                           {
                              $data['ecom_product_cat_id']=$cat['ecom_cat_id'];
                           }
                           else
                           {
                                   $cat1['ecom_cat_name']=$data['ecom_product_cat_id'];
                                    $cat1['ecom_cat_param_id']=0;
                                    $cat1['ecom_cat_status']='active';
                                    $this->db->insert('ecom_cat',$cat1);
                                    $data['ecom_product_cat_id'] =$this->db->insert_id();      
                           }
                             $catsub=$this->db->get_where('ecom_cat',array('ecom_cat_name'=>$data['ecom_product_sub_cat_id'],'ecom_cat_param_id'=>$data['ecom_product_cat_id']))->row_array();
                              if($catsub['ecom_cat_id'])
                           {
                              $data['ecom_product_sub_cat_id']=$catsub['ecom_cat_id'];
                           }
                           else
                           {
                                   $cat11['ecom_cat_name']=$data['ecom_product_sub_cat_id'];
                                    $cat11['ecom_cat_param_id']=$data['ecom_product_cat_id'];
                                    $cat11['ecom_cat_status']='active';
                                    $this->db->insert('ecom_cat',$cat11);
                                    $data['ecom_product_sub_cat_id'] =$this->db->insert_id();      
                           }
                           $data['ecom_product_status']='test';
                           $data['ecom_product_vendor_id']=$admin_record['id'];
                           $data['ecom_product_created_date']= date( "Y-m-d") ;
                           $data['lot_number']=substr(number_format(time() * rand(),0,'',''),0,'8');



                $this->db->insert('ecom_product', $data);
                $this->sendmail_part_n($data['ecom_product_part_n']);

               }

            }
        } 
        unlink($inputfilename);
  
    }
    
    public function get_Grantee_name()
    {
        //print_r($_POST);
        $record = $this->db->get_where('dms_grantee',array('grantee_name'=>$_POST['name']))->row_array();
        if($record)
        {
            echo "Grantee name all ready exist in file. plese select in drop down";
        }
        else
        {
            echo "good";
        }
    }
    
    public function skip_temp()
    {
       // print_r($_POST);die;
        $data = array('grant_temp_id' => $_POST['grnt'], 'document_temp_id' => $_POST['doc']);
       $result = $this->db->insert('dms_skip_temp', $data);
      //  echo $this->db->last_query();
    }
    
    
public function delete_individual_record($param1=''){
      $id = $this->input->post('id');
    $this->db->where('individual_temp_id',$id);
     $this->db->delete('dms_individual_temp');
            $this->session->set_flashdata('flash_message', 'Deleted  successfully');
            //redirect('pelogin/show_role', 'refresh');
}
public function delete_child_record($param1=''){
      $id = $this->input->post('id');
    $this->db->where('child_temp_id',$id);
     $this->db->delete('dms_child_temp');
            $this->session->set_flashdata('flash_message', 'Deleted  successfully');
            //redirect('pelogin/show_role', 'refresh');
}
public function delete_child_record_multi($param1=''){
     $id = $this->input->post('id');
     $id1 = $this->input->post('id1');

$where = array('metadata_individual_number' => $id1, 'template_id' => $id);

 $this->db->where($where);
$this->db->delete('dms_metadata');
   echo $this->db->last_query();   
    
   
}
public function terminate_temp_status(){
  $categorytype_id = $this->input->post('cat_status');
   $parent_id = $this->input->post('parent_id');
    $doc_id = $this->input->post('doc_status');
  $data=array(
       "categorytype_id" => '3' 
  );
  
  $this->db->where('id',$categorytype_id);
$this->db->update('dms_grant',$data);
$data1=array(
       "terminate_status" => '1' 
  );
 $this->db->where('parent_temp_id',$parent_id);
$this->db->update('dms_parent_temp',$data1);

 $this->db->where('grant_temp_id',$categorytype_id);
$this->db->update('dms_cat_year', array('status' => '3'));
}
}
