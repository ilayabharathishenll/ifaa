DROP TABLE IF EXISTS dms_approval_comment;

CREATE TABLE `dms_approval_comment` (
  `approvel_comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `approvel_by` bigint(20) NOT NULL,
  `temp_approve_id` bigint(20) NOT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `approval_status` varchar(250) DEFAULT NULL,
  `notification` varchar(250) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`approvel_comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `dms_approval_comment` VALUES("1","0","0","","","trtrt","");
INSERT INTO `dms_approval_comment` VALUES("2","0","0","","ddd","","ddd");
INSERT INTO `dms_approval_comment` VALUES("3","0","0","dd","dd","dddd","dddddd");
INSERT INTO `dms_approval_comment` VALUES("4","0","0","ffgfsd","adfsad","afadf","adfads");
INSERT INTO `dms_approval_comment` VALUES("5","0","0","wert","eee","ee","eeee");
INSERT INTO `dms_approval_comment` VALUES("6","0","0","ggg","gghfhgdd","hdfghdddhddfg","ghddddgddfdtrytryr");
INSERT INTO `dms_approval_comment` VALUES("7","0","0","ali1","ali2","ali3","ali4");


DROP TABLE IF EXISTS dms_assign_document;

CREATE TABLE `dms_assign_document` (
  `assign_document_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grant_temp_id` bigint(20) NOT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  `assign_by` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `approval` varchar(250) NOT NULL,
  `notification` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`assign_document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS dms_assin_template;

CREATE TABLE `dms_assin_template` (
  `assin_temp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `template_type_id` bigint(20) DEFAULT NULL,
  `document_temp_id` bigint(20) DEFAULT NULL,
  `assin_date` date DEFAULT NULL,
  `assin_temp_step` varchar(250) DEFAULT NULL,
  `work_flow` varchar(250) DEFAULT NULL,
  `termination` varchar(250) DEFAULT NULL,
  `description` text,
  `text_editor` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`assin_temp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

INSERT INTO `dms_assin_template` VALUES("1","2","9","2018-04-14","1","enable","enable","<p>0</p>\n","enable");
INSERT INTO `dms_assin_template` VALUES("2","3","2","2018-04-14","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("3","3","4","2018-04-14","0","0","enable","0","disable");
INSERT INTO `dms_assin_template` VALUES("4","3","5","2017-11-13","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("5","3","6","2017-11-13","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("6","3","7","2017-11-13","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("7","3","48","2017-11-13","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("8","2","46","2018-04-14","0","enable","disable","<p>0</p>\n","disable");
INSERT INTO `dms_assin_template` VALUES("9","2","33","2018-04-14","0","enable","enable","<p>0</p>\n","enable");
INSERT INTO `dms_assin_template` VALUES("10","4","17","2017-11-21","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("11","4","18","2017-11-21","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("12","4","19","2017-11-21","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("13","4","20","2017-11-21","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("14","3","38","2017-11-22","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("15","4","35","2017-11-25","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("16","3","39","2018-04-14","0","0","disable","0","disable");
INSERT INTO `dms_assin_template` VALUES("17","4","34","2017-11-29","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("18","4","36","2017-11-29","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("19","4","37","2017-11-29","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("20","3","3","2018-04-14","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("21","3","21","2017-12-13","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("22","3","41","2017-12-13","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("23","6","16","2018-03-12","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("24","3","6","2018-04-14","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("25","8","11","2018-03-16","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("26","2","9","2018-04-21","0","enable","enable","<p>0</p>\n","enable");
INSERT INTO `dms_assin_template` VALUES("27","2","10","2018-03-16","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("28","3","4","2018-04-14","0","0","disable","0","disable");
INSERT INTO `dms_assin_template` VALUES("29","5","1","2018-04-18","0","0","enable","0","enable");
INSERT INTO `dms_assin_template` VALUES("30","5","13","2018-04-18","0","0","enable","0","enable");


DROP TABLE IF EXISTS dms_cat_year;

CREATE TABLE `dms_cat_year` (
  `cat_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) NOT NULL,
  `grant_temp_id` bigint(20) DEFAULT NULL,
  `status` varchar(250) NOT NULL,
  `year` varchar(250) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

INSERT INTO `dms_cat_year` VALUES("35","2","34","2","2005");
INSERT INTO `dms_cat_year` VALUES("36","1","35","1","2016");
INSERT INTO `dms_cat_year` VALUES("39","9","38","2","2017");
INSERT INTO `dms_cat_year` VALUES("40","2","39","1","2018");


DROP TABLE IF EXISTS dms_category;

CREATE TABLE `dms_category` (
  `category_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL,
  `category_name` varchar(100) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO `dms_category` VALUES("1","0","Arts research");
INSERT INTO `dms_category` VALUES("2","0","Arts practice");
INSERT INTO `dms_category` VALUES("3","0","Arts education");
INSERT INTO `dms_category` VALUES("5","0","Archival & Museum fellowships");
INSERT INTO `dms_category` VALUES("6","0","FAP");
INSERT INTO `dms_category` VALUES("7","0","Arts research & documentation");
INSERT INTO `dms_category` VALUES("8","7","Bengali language initiative");
INSERT INTO `dms_category` VALUES("9","1","assdfsdf");


DROP TABLE IF EXISTS dms_categorytype;

CREATE TABLE `dms_categorytype` (
  `categorytype_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `categorytype_name` varchar(250) NOT NULL,
  `categorytype_description` varchar(250) DEFAULT NULL,
  `categorytype_status` varchar(250) NOT NULL,
  PRIMARY KEY (`categorytype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `dms_categorytype` VALUES("1","Completed","decsccsc","active");
INSERT INTO `dms_categorytype` VALUES("2","Ongoing","onnnnn","active");
INSERT INTO `dms_categorytype` VALUES("3","Terminated","stop work flow","active");


DROP TABLE IF EXISTS dms_child_image;

CREATE TABLE `dms_child_image` (
  `image_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `child_temp_id` bigint(11) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `image_type` varchar(250) NOT NULL,
  `metadata_individual_number` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `dms_child_image` VALUES("2","2","35264dms2.jpg","scan","");
INSERT INTO `dms_child_image` VALUES("3","3","25087dms4.jpg","scan","");
INSERT INTO `dms_child_image` VALUES("4","2","21614dms3.jpg","scan","");


DROP TABLE IF EXISTS dms_child_metadata;

CREATE TABLE `dms_child_metadata` (
  `child_metadata_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `child_temp_id` bigint(20) NOT NULL,
  `child_document` varchar(250) DEFAULT NULL,
  `child_handling_staff` varchar(250) DEFAULT NULL,
  `child_creator` varchar(250) DEFAULT NULL,
  `child_copy_right` varchar(250) DEFAULT NULL,
  `child_metadata_status` varchar(2501) DEFAULT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  PRIMARY KEY (`child_metadata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS dms_child_temp;

CREATE TABLE `dms_child_temp` (
  `child_temp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grant_temp_id` bigint(20) NOT NULL,
  `child_description` longtext,
  `child_keyword` varchar(250) DEFAULT NULL,
  `child_scann_copy` varchar(250) DEFAULT NULL,
  `child_additional_detail` varchar(250) DEFAULT NULL,
  `blogs` text,
  `child_comment` varchar(250) DEFAULT NULL,
  `child_status` varchar(250) DEFAULT NULL,
  `child_approval` varchar(250) DEFAULT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `metadata_individual_number` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`child_temp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

INSERT INTO `dms_child_temp` VALUES("2","34","<p>0dfs</p>\n","","","","0","0","","","2","3","0");
INSERT INTO `dms_child_temp` VALUES("3","35","<p>hello lss2</p>\n","","","","0","0","","","2","3","0");
INSERT INTO `dms_child_temp` VALUES("4","38","<p>hhihiihihihihhiihi</p>\n","","","","0","0","","","2","3","0");


DROP TABLE IF EXISTS dms_deliverables;

CREATE TABLE `dms_deliverables` (
  `deliverables_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deliverables_name` varchar(250) NOT NULL,
  PRIMARY KEY (`deliverables_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

INSERT INTO `dms_deliverables` VALUES("1","deliverable1");
INSERT INTO `dms_deliverables` VALUES("2","deli2");
INSERT INTO `dms_deliverables` VALUES("3","ddd");
INSERT INTO `dms_deliverables` VALUES("4","ddddd");
INSERT INTO `dms_deliverables` VALUES("5","sasas");
INSERT INTO `dms_deliverables` VALUES("6","nhjk");
INSERT INTO `dms_deliverables` VALUES("7","rrrrrr");
INSERT INTO `dms_deliverables` VALUES("8","posible1");
INSERT INTO `dms_deliverables` VALUES("9","simple");
INSERT INTO `dms_deliverables` VALUES("10","testing");
INSERT INTO `dms_deliverables` VALUES("11","kyabat h");
INSERT INTO `dms_deliverables` VALUES("12","kyuuuu");
INSERT INTO `dms_deliverables` VALUES("13","ali123");
INSERT INTO `dms_deliverables` VALUES("14","excecuse");


DROP TABLE IF EXISTS dms_disciplinary_field;

CREATE TABLE `dms_disciplinary_field` (
  `field_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(250) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO `dms_disciplinary_field` VALUES("1","Animation","");
INSERT INTO `dms_disciplinary_field` VALUES("2","Ceramics","");
INSERT INTO `dms_disciplinary_field` VALUES("3","asd","");
INSERT INTO `dms_disciplinary_field` VALUES("4","disck","");
INSERT INTO `dms_disciplinary_field` VALUES("5","diddid","");
INSERT INTO `dms_disciplinary_field` VALUES("6","nanana","");
INSERT INTO `dms_disciplinary_field` VALUES("7","hahaaha","");
INSERT INTO `dms_disciplinary_field` VALUES("8","gooddddd","");
INSERT INTO `dms_disciplinary_field` VALUES("9","testing","");
INSERT INTO `dms_disciplinary_field` VALUES("10","3470dfow","");


DROP TABLE IF EXISTS dms_discription;

CREATE TABLE `dms_discription` (
  `description_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Document_temp_id` bigint(20) DEFAULT NULL,
  `description` text,
  `template_type` varchar(250) DEFAULT NULL,
  `document_term` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`description_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO `dms_discription` VALUES("1","1","<p>&nbsp;</p>\n\n<h6>Month Date, Year</h6>\n\n<p>&nbsp;</p>\n\n<p>Name:</p>\n\n<p>Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>Email:</p>\n\n<p>Phone: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Subject: Fellowship Number- <u>XXXX-X-X-XXX</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p>Dear______________,</p>\n\n<p>&nbsp;</p>\n\n<p>I am delighted to inform you that the India Foundation for the Arts (IFA) has approved a Fellowship of Rs ___________/- (Rupees _______________ only) with you as the &lsquo;Fellow&rsquo; for the Fellowship Number mentioned in the Subject line of this letter. This fellowship is in collaboration with ____________________________.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<td>\n			<div>\n			<p>Precis</p>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>This Fellowship is being made in response to your proposal submitted under IFA&rsquo;s __________________ Programme and your letter of request dated <u>Month Date, Year</u>. Fellowship funds will be available over a period of _________&nbsp;&nbsp; __________ beginning from <u>Month Date, Year</u> and ending on <u>Month Date, Year</u>.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>This letter outlines the terms and conditions of accepting this Fellowship from IFA. Please read all the terms and conditions carefully before signing. </strong></p>\n\n<p>&nbsp;</p>\n\n<p>After we receive the following, IFA will release the first installment of the fellowship:</p>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>A signed copy of this letter</li>\n	<li>A payment request from the Fellow for the first installment of the project</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3>FELLOWSHIP TERMS AND CONDITIONS</h3>\n\n<h3>&nbsp;</h3>\n\n<p><strong><em>A. Financial</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Fellowship funds must be used specifically for the designated purpose(s) in fulfillment of the objectives outlined in the project proposal.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The total commitment of IFA is limited to the amount approved in this letter. Any expenses incurred on the project beyond the approved limit will be your personal responsibility.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Fellowship funds will be released in three periodic installments of 50%, 40% and 10% respectively. Subsequent installments will only be released after the satisfactory submission of requisite reports on time as mentioned in <em>Section B</em> on <em>Reporting</em> in this letter. The final installment of 10% will be released after three weeks of submission of satisfactory final reports on time and depositing of expected deliverables as outlined on Page 1 of this letter, along with the duly signed undertaking as per the format in <em>Annexure I: Undertaking</em>.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3>4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; The Fellow cannot seek funding from any other sources for this project and for the same purposes as mentioned in the proposal submitted to IFA.</h3>\n\n<p>&nbsp;</p>\n\n<p><strong><em>B. Reporting</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Fellow needs to submit two signed narrative reports to IFA &ndash; the Interim Report halfway during the fellowship and the Final Report within 45 days after the end of the fellowship period.&nbsp;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Interim Narrative Report should describe the progress of the fellowship activities towards achieving the goals and objectives of the project as mentioned in the proposal, reflections on the process and challenges and hurdles faced during the reporting period.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>If there are any deliverables that the Fellow wishes to deposit with IFA at this point, they must send those in accordance with <em>Annexure II: Archival Requirements.</em></li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Only if the Interim Narrative Report is satisfactory, the request for second installment will be considered. From the time we receive a request for the second installment, it will take IFA a minimum of two weeks to release the installment.&nbsp;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Final Narrative Report should be comprehensive in describing and assessing all the activities undertaken during the entire fellowship period. It should be a self-evaluative document, covering shortcomings, accomplishments and innovations, responses to specific constraints or problems, and lessons learnt.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Fellow is required to send IFA soft copies as well as signed hard copies in duplicate of all reports.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In case of failure to comply with the above mentioned reporting conditions, the Fellow will be required to return to IFA all the funds disbursed with interest earned if any. Failure to comply with fellowship reporting conditions and undertakings will make the Fellow ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3><em>C. Monitoring and Evaluation</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA and representative of _______________ will monitor the progress of the project as needed through site visits and examination of relevant records by its officers or consultants. IFA may also engage external evaluators to evaluate fellowship accomplishments from time to time.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>D. Extension</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA does not encourage the Fellow to seek extensions of the term of their fellowship. However, in rare circumstances beyond their control, the Fellow may request&nbsp;a one-time extension of half of the term or six months, whichever is lesser. &nbsp;</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The request will be given consideration only if it is made&nbsp;two months prior to the end of the fellowship period.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>E. Copyright</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Copyright on materials resulting from the fellowship remains with the authors of such materials unless expressly transferred in writing to IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>F. Licence Terms</em></strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<ol>\n	<li>IFA and the _______________ will have the licence to use the process materials and outcomes deposited with IFA as deliverables of the fellowship for specific purposes that will include but not be exclusive of broadcasting, displaying, publicising, reproducing and archiving in a publicly accessible platform for non-commercial purposes.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>G. Deliverables to IFA</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All deliverables as mentioned on Page 1 of this letter will be deposited with IFA at the time of submission of the Final Reports as per <em>Annexure II: Archival Requirements. </em></li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>Failure to deposit this material will result in forfeiture of the 10% of the fellowship amount and will make the Fellow ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p style=\"margin-left:21.3pt\">&nbsp;</p>\n\n<ol>\n	<li>After the end of the fellowship period, IFA may request the Fellow to present their project at mutually agreed upon spaces (a maximum of three) that IFA puts together so that the wider public gets to know the work of IFA&rsquo;s Fellows. While all costs will be taken care of by IFA, there will be no honorarium for the Fellows.</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<p>&nbsp;</p>\n\n<h3><em>H. Acknowledgements</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All activities, process materials and final outcomes of the fellowship supported by IFA, including but not exclusive to exhibitions, screenings, stage productions, films, videos, audio tapes, publications, archives, websites etc, in private or public domain must carry acknowledgment of the support of IFA and _______________ as indicated by them in writing. This would include a line framed by, and the logos of IFA and _______________ as by the respective institutions.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The acknowledgement of IFA and _______________ should continue every time it is showcased, presented, published or exhibited if its making has been partly or fully supported by IFA, within and beyond the Fellowship term; and even after it is acquired through commercial transactions by other organisations or individuals. &nbsp;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In some cases, IFA may also require acknowledgement of specific donor contributions, if any, that have enabled IFA to make this fellowship.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>I. Permissions</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Fellow shall be solely responsible for any permission for use of material or documentation of any kind, required to be obtained from government/ agencies/local communities/ other artists and whosoever it may concern, in furtherance of project work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>This permission shall include and extend to IFA&rsquo;s Licence Terms as indicated in <em>Section F</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Fellow shall indemnify IFA and _______________ for any breach of contract between the Fellow and the abovementioned bodies in the event of any dispute.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>J. Termination of Fellowship</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to terminate the fellowship at any time during the fellowship period, suspend all payments and recover the funds with interest earned if any, in any of the following situations:</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If there is delinquency in reporting to IFA in the manner and time or reports are not satisfactory, as mentioned in the <em>Section B</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If the progress of the fellowship has been stalled due to reasons within the Fellow&rsquo;s control.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>If IFA concludes that it is impossible to implement fellowship activities for reasons beyond the Fellow&rsquo;s control in spite of their best intensions, IFA reserves the right to terminate the fellowship and take a considered decision on what amount of funds should be refunded.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>If IFA becomes aware that the Fellowship activities are unlikely to make further or satisfactory progress for any other reasons, IFA reserves the right to terminate the Fellowship and take a considered decision on what amount of Fellowship funds should be refunded.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>K. Indemnity Clause</em></h3>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>IFA and _______________ shall not be responsible for any loss or prejudice to yourself or any third person in consequence of any act or omission of or by the IFA and _______________, their Trustees, Executive Directors, Directors, or staff in connection with the fellowship being hereby made to the Fellow including any premature termination of the fellowship and the Fellow agrees to indemnify and hold harmless each of the IFA and _______________, their Trustees, Executive Directors, Directors, and staff in respect of any claim for such loss or prejudice.</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>If this letter with the Fellowship Terms and Conditions correctly set forth your understanding of the terms of this fellowship, please indicate your agreement by countersigning the attached copy of this letter and return it to IFA with a request for the first installment for your project.</p>\n\n<p>&nbsp;</p>\n\n<p>On behalf of IFA, I extend my best wishes for the success of this important endeavour.</p>\n\n<p>&nbsp;</p>\n\n<p>Warm regards,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>________________________________________</strong></p>\n\n<p><strong>Executive Director</strong></p>\n\n<p><strong>India Foundation for the Arts</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Accepted and agreed</p>\n\n<p>&nbsp;</p>\n\n<p>Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Signature: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><em>Annexure I: Undertaking</em></p>\n\n<p><em>Annexure II: Archival Requirements</em></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("2","8","<p>Grant Number:<strong> 2016-G-0-015</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Grantee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Name, City/Town/District/Village, State</p>\n\n<p>&nbsp;</p>\n\n<p>Grant amount&nbsp; : Rs ____________</p>\n\n<p>&nbsp;</p>\n\n<p>Grant period&nbsp;&nbsp;&nbsp; : __ year and ___ months</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>Start date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Month Date, Year<br />\n&nbsp;</p>\n\n<p>End date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Month Date, Year</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td>\n			<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n				<tbody>\n					<tr>\n						<td>\n						<div>\n						<p>Narrative (No word limit)</p>\n						</div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p><strong>Pr&eacute;cis</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>____</strong><strong><u>Name of Programme____</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p><strong>Grant Description</strong></p>\n\n<p>&nbsp;</p>\n\n<p><img alt=\"Text Box: Narrative (No word limit)\" src=\"file:///C:/Users/LSS-26~1/AppData/Local/Temp/msohtmlclip1/01/clip_image001.gif\" style=\"height:877px; width:598px\" /></p>\n\n<p>&nbsp;</p>\n\n<p><strong>Budget</strong></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"width:475px\">\n			<p>(number of rows can vary)</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("3","9","<h6>&nbsp;</h6>\n\n<h6>&nbsp;</h6>\n\n<h6>&nbsp;</h6>\n\n<h6>Month Date, Year</h6>\n\n<p>&nbsp;</p>\n\n<p>Name:</p>\n\n<p>Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>Email:</p>\n\n<p>Phone: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Subject: Grant Number- <u>XXXX-X-X-XXX</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p>Dear______________,</p>\n\n<p>&nbsp;</p>\n\n<p>I am delighted to inform you that the India Foundation for the Arts (IFA) has approved a grant of Rs ___________/- (Rupees _______________ only) with you as the &lsquo;Grantee&rsquo; for the Grant Number mentioned in the Subject line of this letter.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<td>\n			<div>\n			<p>Precis</p>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>This grant is being made in response to your proposal submitted under IFA&rsquo;s _______ Programme and your letter of request dated <u>Month Date, Year</u>. Grant funds will be available over a period of _________&nbsp;&nbsp; __________ beginning from <u>Month Date, Year</u> and ending on <u>Month Date, Year</u>.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Approved Budget</strong></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"width:475px\">\n			<p>(number of rows can vary)</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>Total: Rs ________ &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>(Amount in words: ________ &nbsp;only)</p>\n\n<p>&nbsp;</p>\n\n<p><strong>This letter outlines the terms and conditions of accepting this grant from IFA. Please read all the terms and conditions carefully before signing. </strong></p>\n\n<p>&nbsp;</p>\n\n<p>After we receive the following, IFA will release the first installment of the grant:</p>\n\n<ul>\n	<li>A signed copy of this letter</li>\n	<li>A payment request from the Grantee for the first installment of the project</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3>GRANT TERMS AND CONDITIONS</h3>\n\n<h3>&nbsp;</h3>\n\n<p><strong><em>A. Financial</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Grant funds must be used specifically for the designated purpose(s) in fulfillment of the objectives outlined in the project proposal.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The total commitment of IFA is limited to the amount approved in this letter. Any expenses incurred on the project beyond the approved limit will be the Grantee&rsquo;s personal responsibility.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The grant funds must be used specifically in accordance with the attached budget categories. In case you anticipate a change in the provisions made in the approved budget categories by 20 per cent or Rs. 50,000/-, whichever is less, a prior written approval must be sought from IFA at least one month before the end of the grant term.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>A grant modification will also need to be approved if it is found necessary to introduce a new budget category (or categories) for legitimate grant-related expenditure.&nbsp; The Grantee is requested to seek an approval for a grant modification request before using funds to cover costs that cannot be accounted for under approved budget categories.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Any funds committed for the purposes of the grant but not expended within the term of the grant, will be returned to IFA with interest earned if any.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li><u>Grant funds will be released in three periodic installments of 50%, 40% and 10% respectively</u>. Subsequent installments will only be released after the satisfactory submission of requisite reports on time as mentioned in <em>Section B</em> on <em>Reporting</em> in this letter. The final installment of 10% will be released after three weeks of submission of satisfactory final reports on time and depositing of expected deliverables as outlined on Page 1 of this letter, along with the duly signed undertaking as per the format in <em>Annexure I: Undertaking</em>.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to call for copies of vouchers and receipts pertaining to the grant for verification any time during the term of the grant and until all reports and deliverables are submitted.</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Grantee has sought, received or will seek funding from any other sources for this project and for the same purposes as mentioned in the proposal submitted to IFA, the Grantee needs a written approval from IFA to do so. Co-funders will not be entitled to any rights or license terms that impinge on the license terms of IFA as mentioned in <em>Section G</em> of this letter.</h3>\n\n<p>&nbsp;</p>\n\n<p><strong><em>B. Reporting</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee needs to submit two sets of signed reports to IFA &ndash; the Interim Reports at the end of the first installment of the grant and the Final Reports within 45 days after the end of the grant period. Each set of reports will consist of a Narrative Report and a Financial Report.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Interim Narrative Report should describe the progress of the grant activities towards achieving the goals and objectives of the project as mentioned in the proposal, reflections on the process and challenges and hurdles faced during the reporting period.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Interim Financial Report should be as per the format in <em>Annexure II: Financial Report. </em>This report should reflect the actual expenditure under the various approved budget categories and does not need to be audited.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>If there are any deliverables that the Grantee wishes to deposit with IFA at this point, they must send those in accordance with <em>Annexure III: Archival Requirements.</em></li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>Only if the Interim Narrative and Financial Reports are satisfactory, the request for second installment will be considered. From the time we receive a request for the second installment, it will take IFA a minimum of two weeks to release the grant installment.&nbsp;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Final Narrative Report should be comprehensive in describing and assessing all the activities undertaken during the entire grant period. It should be a self-evaluative document, covering shortcomings, accomplishments and innovations, responses to specific constraints or problems, and lessons learnt.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Final Financial Report should be as per the format in <em>Annexure II: Financial Report</em>.&nbsp; It should have all records of expenditures like bills, vouchers and documents attached and should be certified by a practicing Chartered Accountant confirming that the funds have been utilised for the purpose for which the grant was made.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee is required to send IFA soft copies as well as signed hard copies in duplicate of all reports.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>It is essential for the Grantee to keep copies of all reports and supporting financial and accounting documents that have been submitted to IFA for at least four years after the end of the grant term. IFA reserves its right to conduct or cause to be conducted an audit of your books of accounts in respect of the grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In case of failure to comply with the above mentioned reporting conditions, the Grantee will be required to return to IFA all the funds disbursed with interest earned if any. Failure to comply with grant reporting conditions and undertakings will make the Grantee ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>C. Monitoring and Evaluation</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA will monitor grant progress as needed through site visits and examination of relevant records by its officers or consultants. IFA may also engage external evaluators to evaluate grant accomplishments from time to time.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>D. Extension</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA does not encourage the Grantee to seek extensions of the term of their grant. However, in rare circumstances beyond their control, the Grantee may request&nbsp;a one-time extension of half of the grant term or six months, whichever is lesser.&nbsp;</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The request will be given consideration only if it is made&nbsp;two months prior to the end of the grant period.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>E. Equipment</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All equipment purchased using grant funds will be treated as the property of IFA and, at the time of submitting the Final Reports, it will be returned to IFA in good working condition.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In case the capital equipment is lost, the grantee shall reimburse the cost of replacing it, and in case it is damaged, the grantee shall reimburse the cost of repairing it.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3><em>F. Copyright</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Copyright on materials resulting from the grant remains with the authors of such materials unless expressly transferred in writing to IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>G. Licence Terms</em></strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<ol>\n	<li>IFA will have the licence to use the process materials and outcomes deposited with IFA as deliverables of the grant for specific purposes that will include but not be exclusive of broadcasting, displaying, publicising, reproducing and archiving in a publicly accessible platform for non-commercial purposes.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>H. Deliverables to IFA</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All deliverables as mentioned on Page 1 of this letter will be deposited with IFA at the time of submission of the Final Reports as per <em>Annexure III: Archival Requirements. </em></li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>Failure to deposit this material will result in forfeiture of the 10% of the grant amount and will make the Grantee ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>After the end of the grant period, IFA may request the Grantee to present their project at mutually agreed upon Grant Showcases (a maximum of three) that IFA puts together so that the wider public gets to know the work of IFA&rsquo;s Grantees. While all costs will be taken care of by IFA, there will be no honorarium for the Grantee.</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3><em>I. Acknowledgements</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All activities, process materials and final outcomes of the grant supported by IFA, including but not exclusive to exhibitions, screenings, stage productions, films, videos, audio tapes, publications, archives, websites etc, in private or public domain must carry acknowledgment of IFA&rsquo;s support as indicated by IFA in writing. This would include a line framed by, and the IFA logo provided by IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The acknowledgement of IFA should continue every time it is showcased, presented, published or exhibited if its making has been partly or fully supported by IFA, within and beyond the grant term; and even after it is acquired through commercial transactions by other organisations or individuals.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In some cases, IFA may also require acknowledgement of specific donor contributions, if any, that have enabled IFA to make this grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>J. Permissions</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee shall be solely responsible for any permission for use of material or documentation of any kind, required to be obtained from government/ agencies/local communities/ other artists and whosoever it may concern, in furtherance of project work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>This permission shall include and extend to IFA&rsquo;s Licence Terms as indicated in <em>Section G</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee shall indemnify IFA for any breach of contract between the Grantee and the abovementioned bodies in the event of any dispute.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>K. Public Access of Grant Materials</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA encourages the Grantees to deposit the various process documents and outcomes from the grant to archives, libraries and bodies that facilitate public access to such work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>L. Termination of Grant</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to terminate a grant at any time during the grant period, suspend all grant payments and recover the grant funds with interest earned if any, in any of the following situations:</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If IFA comes to the conclusion that grant funds are - not being used for the purposes intended; or diverted / misapplied to the prejudice of the fulfillment of the objectives as stated in the proposal; or not fully and properly accounted for;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If there is delinquency in reporting to IFA in the manner and time or reports are not satisfactory, as mentioned in the <em>Section B</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If the progress of the grant has been stalled due to reasons within the grantee&rsquo;s control.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>If IFA concludes that it is impossible to implement grant activities for reasons beyond the Grantee&rsquo;s control in spite of their best intentions, IFA reserves the right to terminate the grant and take a considered decision on what amount of grant funds should be refunded.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>If IFA becomes aware that the grant activities are unlikely to make further or satisfactory progress for any other reasons, IFA reserves the right to terminate the grant and take a considered decision on what amount of grant funds should be refunded.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>M. Indemnity Clause</em></h3>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>IFA shall not be responsible for any loss or prejudice to yourself or any third person in consequence of any act or omission of or by the IFA, its Trustees, Executive Director or staff in connection with the grant being hereby made to the Grantee including any premature termination of the grant and the Grantee agrees to indemnify and hold harmless each of the IFA, its Trustees, Executive Director and staff in respect of any claim for such loss or prejudice.</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>If this letter with the Grant Terms and Conditions correctly set forth your understanding of the terms of this grant, please indicate your agreement by countersigning the attached copy of this letter and return it to IFA with a request for the first installment for your project.</p>\n\n<p>&nbsp;</p>\n\n<p>On behalf of IFA, I extend my best wishes for the success of this important endeavour.</p>\n\n<p>&nbsp;</p>\n\n<p>Warm regards,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>_______________________________ </strong></p>\n\n<p><strong>Executive Director</strong></p>\n\n<p><strong>India Foundation for the Arts</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Accepted and agreed</p>\n\n<p>&nbsp;</p>\n\n<p>Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Signature: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><em>Annexure I: Undertaking</em></p>\n\n<p><em>Annexure II: (Interim/Final) Financial Report</em></p>\n\n<p><em>Annexure III: Archive Requirements</em></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n","1","short");
INSERT INTO `dms_discription` VALUES("4","9","<h6>&nbsp;</h6>\n\n<h6>&nbsp;</h6>\n\n<p>&nbsp;</p>\n\n<h6>Month Date, Year</h6>\n\n<p>&nbsp;</p>\n\n<p>Name:</p>\n\n<p>Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>Email:</p>\n\n<p>Phone: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Subject: Grant Number- <u>XXXX-X-X-XXX</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p>Dear______________,</p>\n\n<p>&nbsp;</p>\n\n<p>I am delighted to inform you that the India Foundation for the Arts (IFA) has approved a grant of Rs ___________/- (Rupees _______________ only) with you as the &lsquo;Grantee&rsquo; for the Grant Number mentioned in the Subject line of this letter.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<td>\n			<div>\n			<p>Precis</p>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>This grant is being made in response to your proposal submitted under IFA&rsquo;s _______ Programme and your letter of request dated <u>Month Date, Year</u>. Grant funds will be available over a period of _________&nbsp;&nbsp; __________ beginning from <u>Month Date, Year</u> and ending on <u>Month Date, Year</u>.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Approved Budget</strong></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"width:475px\">\n			<p>(number of rows can vary)</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>Total: Rs ________ &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>(Amount in words: ________ &nbsp;only)</p>\n\n<p>&nbsp;</p>\n\n<p><strong>This letter outlines the terms and conditions of accepting this grant from IFA. Please read all the terms and conditions carefully before signing. </strong></p>\n\n<p>&nbsp;</p>\n\n<p>After we receive the following, IFA will release the first installment of the grant:</p>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>A signed copy of this letter</li>\n	<li>A payment request from the Grantee for the first installment of the project</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3>GRANT TERMS AND CONDITIONS</h3>\n\n<h3>&nbsp;</h3>\n\n<p><strong><em>A. Financial</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Grant funds must be used specifically for the designated purpose(s) in fulfillment of the objectives outlined in the project proposal.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The total commitment of IFA is limited to the amount approved in this letter. Any expenses incurred on the project beyond the approved limit will be your personal responsibility.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The grant funds must be used specifically in accordance with the attached budget categories. In case you anticipate a change in the provisions made in the approved budget categories by 20 per cent or Rs. 50,000/-, whichever is less, a prior written approval must be sought from IFA at least one month before the end of the grant term.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>A grant modification will also need to be approved if it is found necessary to introduce a new budget category (or categories) for legitimate grant-related expenditure.&nbsp; The Grantee is requested to seek an approval for a grant modification request before using funds to cover costs that cannot be accounted for under approved budget categories.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Any funds committed for the purposes of the grant but not expended within the term of the grant, will be returned to IFA with interest earned if any.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li><u>Grant funds will be released in two periodic installments of 90% and 10% respectively</u>. The final installment of 10% will be released after three weeks of submission of satisfactory final reports on time and depositing of expected deliverables as outlined on Page 1 of this letter, along with the duly signed undertaking as per the format in <em>Annexure I: Undertaking</em>.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to call for copies of vouchers and receipts pertaining to the grant for verification any time during the term of the grant and until all reports and deliverables are submitted.</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Grantee has sought, received or will seek funding from any other sources for this project and for the same purposes as mentioned in the proposal submitted to IFA, the Grantee needs a written approval from IFA to do so. Co-funders will not be entitled to any rights or license terms that impinge on the license terms of IFA as mentioned in <em>Section G</em> of this letter.</h3>\n\n<p>&nbsp;</p>\n\n<p><strong><em>B. Reporting</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee needs to submit one set of signed reports to IFA &ndash; the Final Reports at the end of the first installment of the grant within 45 days after the end of the grant period. The set will consist of a Narrative Report and a Financial Report.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Final Narrative Report should be comprehensive in describing and assessing all the activities undertaken during the entire grant period. It should be a self-evaluative document, covering shortcomings, accomplishments and innovations, responses to specific constraints or problems, and lessons learnt.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Final Financial Report should be as per the format in <em>Annexure II: Financial Report. </em>It should have all records of expenditures like bills, vouchers and documents attached and should be certified by a practicing Chartered Accountant confirming that the funds have been utilised for the purpose for which the grant was made.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee is required to send IFA soft copies as well as signed hard copies in duplicate of all reports.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>It is essential for the Grantee to keep copies of all reports and supporting financial and accounting documents that have been submitted to IFA for at least four years after the end of the grant term. IFA reserves its right to conduct or cause to be conducted an audit of your books of accounts in respect of the grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In case of failure to comply with the above mentioned reporting conditions, the Grantee will be required to return to IFA all the funds disbursed with interest earned if any. Failure to comply with grant reporting conditions and undertakings will make the Grantee ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>C. Monitoring and Evaluation</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA will monitor grant progress as needed through site visits and examination of relevant records by its officers or consultants. IFA may also engage external evaluators to evaluate grant accomplishments from time to time.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>D. Extension</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA does not encourage the Grantee to seek extensions of the term of their grant. However, in rare circumstances beyond their control, the Grantee may request&nbsp;a one-time extension of half of the grant term or 6 months, whichever is lesser. &nbsp;</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The request will be given consideration only if it is made&nbsp;one month prior to the end of the grant period.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>E. Equipment</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All equipment purchased using grant funds will be treated as the property of IFA and, at the time of submitting the Final Reports, it will be returned to IFA in good working condition.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In case the capital equipment is lost, the grantee shall reimburse the cost of replacing it, and in case it is damaged, the grantee shall reimburse the cost of repairing it.</li>\n</ol>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<h3><em>F. Copyright</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Copyright on materials resulting from the grant remains with the authors of such materials unless expressly transferred in writing to IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>G. Licence Terms</em></strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<ol>\n	<li>IFA will have the licence to use the process materials and outcomes deposited with IFA as deliverables of the grant for specific purposes that will include but not be exclusive of broadcasting, displaying, publicising, reproducing and archiving in a publicly accessible platform for non-commercial purposes.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>H. Deliverables to IFA</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All deliverables as mentioned on Page 1 of this letter will be deposited with IFA at the time of submission of the Final Reports as per <em>Annexure III: Archival Requirements. </em></li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>Failure to deposit this material will result in forfeiture of the 10% of the grant amount and will make the Grantee ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>After the end of the grant period, IFA may request the Grantee to present their project at mutually agreed upon Grant Showcases (a maximum of three) that IFA puts together so that the wider public gets to know the work of IFA&rsquo;s Grantees. While all costs will be taken care of by IFA, there will be no honorarium for the Grantee.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>I. Acknowledgements</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All activities, process materials and final outcomes of the grant supported by IFA, including but not exclusive to exhibitions, screenings, stage productions, films, videos, audio tapes, publications, archives, websites etc, in private or public domain must carry acknowledgment of IFA&rsquo;s support as indicated by IFA in writing. This would include a line framed by, and the IFA logo provided by IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The acknowledgement of IFA should continue every time it is showcased, presented, published or exhibited if its making has been partly or fully supported by IFA, within and beyond the grant term; and even after it is acquired through commercial transactions by other organisations or individuals.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In some cases, IFA may also require acknowledgement of specific donor contributions, if any, that have enabled IFA to make this grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>J. Permissions</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee shall be solely responsible for any permission for use of material or documentation of any kind, required to be obtained from government/ agencies/local communities/ other artists and whosoever it may concern, in furtherance of project work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>This permission shall include and extend to IFA&rsquo;s Licence Terms as indicated in <em>Section G</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee shall indemnify IFA for any breach of contract between the Grantee and the abovementioned bodies in the event of any dispute.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>K. Public Access of Grant Materials</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA encourages the Grantees to deposit the various process documents and outcomes from the grant to archives, libraries and bodies that facilitate public access to such work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>L. Termination of Grant</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to terminate a grant at any time during the grant period, suspend all grant payments and recover the grant funds with interest earned if any, in any of the following situations:</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If IFA comes to the conclusion that grant funds are - not being used for the purposes intended; or diverted / misapplied to the prejudice of the fulfillment of the objectives as stated in the proposal; or not fully and properly accounted for;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If there is delinquency in reporting to IFA in the manner and time or reports are not satisfactory, as mentioned in the <em>Section B</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If the progress of the grant has been stalled due to reasons within the grantee&rsquo;s control.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>If IFA concludes that it is impossible to implement grant activities for reasons beyond the Grantee&rsquo;s control inspite of their best intensions, IFA reserves the right to terminate the grant and take a considered decision on what amount of grant funds should be refunded.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>If IFA becomes aware that the grant activities are unlikely to make further or satisfactory progress for any other reasons, IFA reserves the right to terminate the grant and take a considered decision on what amount of grant funds should be refunded.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>M. Indemnity Clause</em></h3>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>IFA shall not be responsible for any loss or prejudice to yourself or any third person in consequence of any act or omission of or by the IFA, its Trustees, Executive Director or staff in connection with the grant being hereby made to the Grantee including any premature termination of the grant and the Grantee agrees to indemnify and hold harmless each of the IFA, its Trustees, Executive Director and staff in respect of any claim for such loss or prejudice.</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>If this letter with the Grant Terms and Conditions correctly set forth your understanding of the terms of this grant, please indicate your agreement by countersigning the attached copy of this letter and return it to IFA with a request for the first installment for your project.</p>\n\n<p>&nbsp;</p>\n\n<p>On behalf of IFA, I extend my best wishes for the success of this important endeavour.</p>\n\n<p>&nbsp;</p>\n\n<p>Warm regards,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>________________________________</strong></p>\n\n<p><strong>&shy;&shy;&shy;&shy;Executive Director</strong></p>\n\n<p><strong>India Foundation for the Arts</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Accepted and agreed</p>\n\n<p>&nbsp;</p>\n\n<p>Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Signature: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p><em>Annexure I: Undertaking</em></p>\n\n<p><em>Annexure II: Final Financial Report</em></p>\n\n<p><em>Annexure III: Archival Requirements </em></p>\n\n<p>&nbsp;</p>\n\n<p><strong><u>Annexure I</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p>Date: _____________</p>\n\n<p><strong><u>Undertaking</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p>I hereby confirm that I have submitted the final narrative report, certified financial report as per the format, and deposited all the deliverables as specified in the Grant (XXXX-G-X-XXX) Notification Letter.</p>\n\n<p>&nbsp;</p>\n\n<p>I hereby request India Foundation for the Arts to release the balance 10% of the Grant Amount.</p>\n\n<p>&nbsp;</p>\n\n<p><strong><em>Grantee Signature</em></strong></p>\n\n<p>&nbsp;</p>\n\n<p><strong>Annexure II: (</strong><strong>Final) Financial Report<br />\n(Sample)</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>(Interim financial report submitted by one of our grantees in the case of a one year grant of Rs 5,00,000/- made to him. Please make changes to suit your proposed budget items, dates and details specific to your project)</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<strong><u>Insert grant number)</u>&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grant Number</p>\n\n<p>&nbsp;</p>\n\n<p>Grantee: <strong>(<u>Insert your name</u>)</strong></p>\n\n<p>Financial statement for the year ending <strong>(<u>insert date</u>)</strong>:</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (in rupees)&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"width:367px\">\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>Description<br />\n			<strong>(Change the heads according to your Budget </strong></p>\n\n			<p><strong>approved in the GNL)</strong></p>\n			</td>\n			<td style=\"width:102px\">\n			<p>&nbsp;</p>\n\n			<p>Approved</p>\n\n			<p>Budget</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>Amount expended from <strong>(<u>insert date</u>)</strong> to <strong>(<u>insert date</u>)</strong></p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:367px\">\n			<p>Honorarium</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>2,00,000</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>1,00,000</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:367px\">\n			<p>Travel</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>1,30,000</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>40,000</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:367px\">\n			<p>Administration, communications and supplies</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>70,000</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>42,000</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:367px\">\n			<p>Equipment rentals</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>50,000</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>30,000</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:367px\">\n			<p>Workshops</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>50,000</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>27,000</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:367px\">\n			<p>&nbsp;</p>\n\n			<p><strong>Total</strong></p>\n			</td>\n			<td style=\"width:102px\">\n			<p>&nbsp;</p>\n\n			<p>5,00,000</p>\n			</td>\n			<td style=\"width:102px\">\n			<p>&nbsp;</p>\n\n			<p>2,39,000</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Approved grant amount:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rs 5,00,000</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Approved grant term: (grant term) beginning <strong>(<u>insert date</u>)</strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total funds released by IFA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rs&nbsp; 2,50,000</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Amount expended&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rs &nbsp;<u>2,39,000</u></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Balance with the grantee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Rs&nbsp;&nbsp;&nbsp;&nbsp; 11,000</u></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Balance due from IFA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rs&nbsp;&nbsp;2,50,000</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;<strong><em><u>Grantee Signature</u></em></strong></p>\n\n<p><strong><em><u>(Auditor&rsquo;s Seal and Signature for Final Financial report)</u></em></strong></p>\n\n<p>&nbsp;</p>\n\n<p><strong>Annexure III: Archival Requirements</strong><br />\n&nbsp;</p>\n\n<p><strong><u>The IFA Archive: Quality Restrictions for the Deliverables Submission</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p><strong><u>Types of Final Deliverables</u></strong></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"width:37px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:444px\">\n			<p><strong>Deliverables</strong></p>\n			</td>\n			<td style=\"width:157px\">\n			<p><strong>Type</strong></p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>1</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Manuscripts</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Text/ Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>2</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Essays</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Text/ Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>3</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Monographs</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Text/ Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>4</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Published books</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Text/ Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>5</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Seminar agenda</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Text/ Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>6</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Presented papers</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Text/ Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>7</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Web publication</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Soft copy/ PDF/ (Needs date last viewed and the full URL mentioned)</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>8</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>E-books</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Searchable PDF (HQ)</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>9</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Graphic novels</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image/ PDF (HQ)</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>10</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Catalogues</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Text/ image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>11</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>A/V recordings of the process/production/</p>\n\n			<p>A/V essay, Seminar recordings</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Audio/ video</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>12</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Still images of production showcase, debates, oratory presentation, poetry readings, folk art performance</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>13</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Media reports, articles about performance</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>14</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>A/V recording of the final performance (in case of play/ performing arts practice</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Audio/Video</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>15</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Publicity material</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>16</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Still photographs</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>17</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Rehearsal notes</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>18</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Final film</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>DVD-video + mp4</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>19</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Low-res rush footage</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Mp4</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>20</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Film production stills</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>21</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Film publicity material</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image/Audio/Video</p>\n\n			<p>(soft copies)</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>22</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>DVD covers</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Image</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>23</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Link and access to website, blogs</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Needs date last viewed and the full URL (and userid-password, if applicable)</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>24</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Film and video documentation</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Audio/Video</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>25</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Audio recordings</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Audio</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>26</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Source codes, fonts, offline version of websites</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Contact personally for specifications</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:37px\">\n			<p>27</p>\n			</td>\n			<td style=\"width:444px\">\n			<p>Textual reports (internal/ external)</p>\n			</td>\n			<td style=\"width:157px\">\n			<p>Text</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong><u>Format and Quality Requirement:</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p><u>Text</u>: May be submitted as document files (DOC/ DOCX etc), and a scanned copy/ PDF. You can convert a doc file to PDF from MS Word, using the &lsquo;Save As&rsquo; option. If you want to scan the document, please consult the specifications suggested here for images, and maintain that.</p>\n\n<p>&nbsp;</p>\n\n<p><u>Images</u>: If you are scanning, it should be in 24 bit RGB, uncompressed Tiff and compressed JPG (one copy each). For printed documents and good quality colour photographs, scan at 300 DPI; for manuscripts and damaged documents, use 600 DPI. Photographs should be submitted in RAW format along with Tiff and JPG. Please keep the embedded EXIFF data with the photographs.</p>\n\n<p>&nbsp;</p>\n\n<p><u>Audio</u>: 16 bit 96 KHz recordings in uncompressed WAV and MP3.</p>\n\n<p>&nbsp;</p>\n\n<p><u>Video</u>: Main submission: DVD-Video format with uncompressed MPEG-2 audio or Dolby digital audio (this will be 4.7GB=1 hr). Also submit a low quality mp4. For low-res rush, please submit only the compressed mp4 files.</p>\n\n<p>&nbsp;</p>\n\n<p><u>Web Reference:</u> Try to save the page as HTML and PDF. You will get an option to do this from &lsquo;Print&rsquo;. Change &lsquo;Destination&rsquo; to &lsquo;Save as PDF&rsquo; in the print window. Please save the full URL of the page and the date you are saving the file in a separate notepad file, and keep this within the same folder.</p>\n\n<p>&nbsp;</p>\n\n<p><u>Source codes, offline versions, fonts, ebooks</u>: Please consult your Programme Executive regarding the format before submission.</p>\n\n<p>&nbsp;</p>\n\n<p><strong><u>Information related to the submission:</u></strong></p>\n\n<p>For all the scans, photographs and digital recordings (a/v), please mention</p>\n\n<ol>\n	<li>a caption explaining the context of the item</li>\n	<li>name of the photographer(s)/ recordist(s)/ technician(s),</li>\n	<li>date of scanning/ photographing/ recording</li>\n	<li>place where it was recorded/ digitised,</li>\n	<li>hardwares and softwares used in the process.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p>You must also include</p>\n\n<ol>\n	<li>copyright information (for both digital copy and the original work)</li>\n	<li>name of the artist concerned (whose work you are recording/ digitising)</li>\n	<li>date and place of the creation of the original work</li>\n	<li>other relevant information such as editorial and publication details.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p>We request you to give clear instructions about any restriction (if there is any) on public sharing/ future reproduction or use. Please do not forget to give a short caption for the recording/ photograph/ scan you are sending, explaining the context and the importance of the item, so that we understand the significance of it. The table and the specifications mentioned above are for the digital copies. Please submit hard copies of your documents and deliverables along with the digital copies. We strongly suggest you send us both. Contact your Programme Executive or the Archivist if you need help.&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n","1","long");
INSERT INTO `dms_discription` VALUES("5","9","<h6>Month Date, Year</h6>\n\n<p>&nbsp;</p>\n\n<p>Name:</p>\n\n<p>Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>Email:</p>\n\n<p>Phone: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Subject: Grant Number- <u>XXXX-X-X-XXX</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p>Dear______________,</p>\n\n<p>&nbsp;</p>\n\n<p>I am delighted to inform you that the India Foundation for the Arts (IFA) has approved a grant of Rs ___________/- (Rupees _______________ only) with you as the &lsquo;Grantee&rsquo; for the Grant Number mentioned in the Subject line of this letter.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<td>\n			<div>\n			<p>Precis</p>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>This grant is being made in response to your proposal submitted under IFA&rsquo;s _______ Programme and your letter of request dated <u>Month Date, Year</u>. Grant funds will be available over a period of _________&nbsp;&nbsp; __________ beginning from <u>Month Date, Year</u> and ending on <u>Month Date, Year</u>.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Approved Budget</strong></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"width:475px\">\n			<p>(number of rows can vary)</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>Total: Rs ________ &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>(Amount in words: ________ &nbsp;only)</p>\n\n<p>&nbsp;</p>\n\n<p><strong>This letter outlines the terms and conditions of accepting this grant from IFA. Please read all the terms and conditions carefully before signing. </strong></p>\n\n<p>&nbsp;</p>\n\n<p>After we receive the following, IFA will release the first installment of the grant:</p>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>A copy of this letter signed by the duly authorised signatory with the seal of the organisation</li>\n	<li>A payment request from the organisation for the first installment of the project</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3>GRANT TERMS AND CONDITIONS</h3>\n\n<h3>&nbsp;</h3>\n\n<p><strong><em>A. Financial</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Grant funds must be used specifically for the designated purpose(s) in fulfillment of the objectives outlined in the project proposal. Grant funds must be physically segregated in a separate bank account established and maintained exclusively for charitable/not-for-profit purposes.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The total commitment of IFA is limited to the amount approved in this letter. Any expenses incurred on the project beyond the approved limit will be the organisation&rsquo;s responsibility.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The grant funds must be used specifically in accordance with the attached budget categories. In case you anticipate a change in the provisions made in the approved budget categories by 20 per cent or Rs. 50,000/-, whichever is less, a prior written approval must be sought from IFA at least one month before the end of the grant term.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>A grant modification will also need to be approved if it is found necessary to introduce a new budget category (or categories) for legitimate grant-related expenditure.&nbsp; The Grantee is requested to seek an approval for a grant modification request before using funds to cover costs that cannot be accounted for under approved budget categories.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Any funds committed for the purposes of the grant but not expended within the term of the grant, will be returned to IFA with interest earned if any.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Grant funds will be released in three periodic installments of 50%, 40% and 10% respectively. Subsequent installments will only be released after the satisfactory submission of requisite reports duly signed by the head of the organisation on time as mentioned in <em>Section B</em> on <em>Reporting </em>in this letter. The final installment of 10% will be released after three weeks of submission of satisfactory final reports on time and depositing of expected deliverables as outlined on Page 1 of this letter, along with the duly signed undertaking as per the format in <em>Annexure I: Undertaking</em>.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to call for copies of vouchers and receipts pertaining to the grant for verification, any time during the term of the grant, until all reports and deliverables are submitted and also to conduct or cause to be conducted an audit of the books of the Grantee&rsquo;s accounts.</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Grantee has sought, received or will seek funding from any other sources for this project and for the same purposes as mentioned in the proposal submitted to IFA, the Grantee needs a written approval from IFA to do so. Co-funders will not be entitled to any rights or license terms that impinge on the license terms of IFA as mentioned in <em>Section G</em> of this letter.</h3>\n\n<p>&nbsp;</p>\n\n<p><strong><em>B. Reporting</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee needs to submit two sets of reports to IFA duly signed by the head of the organisation &ndash; the Interim Reports at the end of the first installment of the grant and the Final Reports within 45 days after the end of the grant period. Each set of reports will consist of a Narrative Report and a Financial Report.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Interim Narrative Report should describe the progress of the grant activities towards achieving the goals and objectives of the project as mentioned in the proposal, reflections on the process and challenges and hurdles faced during the reporting period.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Interim Financial Report should be as per the format in <em>Annexure II: Financial Report. </em>This report should reflect the actual expenditure under the various approved budget categories and does not need to be audited.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>If there are any deliverables that the Grantee wishes to deposit with IFA at this point, they must send those in accordance with <em>Annexure III: Archival Requirements.</em></li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>Only if the Interim Narrative and Financial Reports are satisfactory, the request for second installment will be considered. From the time we receive a request for the second installment, it will take IFA a minimum of two weeks to release the grant installment.&nbsp;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Final Narrative Report should be comprehensive in describing and assessing all the activities undertaken during the entire grant period. It should be a self-evaluative document, covering shortcomings, accomplishments and innovations, responses to specific constraints or problems, and lessons learnt.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Final Financial Report should be as per the format in <em>Annexure II: Financial Report</em>.&nbsp; It should have all records of expenditures like bills, vouchers and documents attached and should be certified by a practicing Chartered Accountant confirming that the funds have been utilised for the purpose for which the grant was made.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee is required to send IFA soft copies as well as signed hard copies in duplicate of all reports.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>It is essential for the Grantee to keep copies of all reports and supporting financial and accounting documents that have been submitted to IFA for at least four years after the end of the grant term. IFA reserves its right to conduct or cause to be conducted an audit of your books of accounts in respect of the grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In case of failure to comply with the abovementioned reporting conditions, the Grantee will be required to return to IFA all the funds disbursed with interest earned if any. Failure to comply with grant reporting conditions and undertakings will make the Grantee ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In addition, the audited financial accounts (Income &amp; Expenditure account with Balance sheet) of the Grantee should be submitted by September 30<sup>th</sup> each year for the previous year, for the period of the grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>C. Monitoring and Evaluation</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA will monitor grant progress as needed through site visits and examination of relevant records by its officers or consultants. IFA may also engage external evaluators to evaluate grant accomplishments from time to time.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>D. Extension</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA does not encourage the Grantee to seek extensions of the term of their grant. However, in rare circumstances beyond their control, the Grantee may request&nbsp;a one-time extension of half of the grant term or six months, whichever is lesser.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The request will be given consideration only if it is made&nbsp;two months prior to the end of the grant period.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>E. Equipment</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All equipment purchased using grant funds will be treated as the property of IFA and, at the time of submitting the Final Reports, it will be returned to IFA in good working condition.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>In case the capital equipment is lost, the grantee shall reimburse the cost of replacing it, and in case it is damaged, the grantee shall reimburse the cost of repairing it.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3><em>F. Copyright</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Copyright on materials resulting from the grant remains with the authors of such materials unless expressly transferred in writing to IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>G. Licence Terms</em></strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<ol>\n	<li>IFA will have the licence to use the process materials and outcomes deposited with IFA as deliverables of the grant for specific purposes that will include but not be exclusive of broadcasting, displaying, publicising, reproducing and archiving in a publicly accessible platform for non-commercial purposes.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>H. Deliverables to IFA</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All deliverables as mentioned on Page 1 of this letter will be deposited with IFA at the time of submission of the Final Reports as per <em>Annexure III: Archival Requirements.</em></li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>Failure to deposit this material will result in forfeiture of the 10% of the grant amount and will make the Grantee ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>After the end of the grant period, IFA may request the _________________________________________ to present the project at mutually agreed upon Grant Showcases (a maximum of three) that IFA puts together so that the wider public gets to know the work of IFA&rsquo;s Grantees. While all costs will be taken care of by IFA, there will be no honorarium for the _________________________ or the Grantee.</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3><em>I. Acknowledgements</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All activities, process materials and final outcomes of the grant supported by IFA, including but not exclusive to exhibitions, screenings, stage productions, films, videos, audio tapes, publications, archives, websites etc, in private or public domain must carry acknowledgment of IFA&rsquo;s support as indicated by IFA in writing. This would include a line framed by, and the IFA logo provided by IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The acknowledgement of IFA should continue every time it is showcased, presented, published or exhibited if its making has been partly or fully supported by IFA, within and beyond the grant term; and even after it is acquired through commercial transactions by other organisations or individuals.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In some cases, IFA may also require acknowledgement of specific donor contributions, if any, that have enabled IFA to make this grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong><em>J. Permissions</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee shall be solely responsible for any permission for use of material or documentation of any kind, required to be obtained from government/ agencies/local communities/ other artists and whosoever it may concern, in furtherance of project work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>This permission shall include and extend to IFA&rsquo;s Licence Terms as indicated in <em>Section G</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee shall indemnify IFA for any breach of contract between the Grantee and the abovementioned bodies in the event of any dispute.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>K. Public Access of Grant Materials</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA encourages the Grantees to deposit the various process documents and outcomes from the grant to archives, libraries and bodies that facilitate public access to such work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>L. Termination of Grant</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to terminate a grant at any time during the grant period, suspend all grant payments and recover the grant funds with interest earned if any, in any of the following situations:</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If IFA comes to the conclusion that grant funds are - not being used for the purposes intended; or diverted / misapplied to the prejudice of the fulfillment of the objectives as stated in the proposal; or not fully and properly accounted for;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If there is delinquency in reporting to IFA in the manner and time or reports are not satisfactory, as mentioned in the <em>Section B</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If the progress of the grant has been stalled due to reasons within the grantee&rsquo;s control.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>If IFA concludes that it is impossible to implement grant activities for reasons beyond the Grantee&rsquo;s control inspite of their best intensions, IFA reserves the right to terminate the grant and take a considered decision on what amount of grant funds should be refunded.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>If IFA becomes aware that the grant activities are unlikely to make further or satisfactory progress for any other reasons, IFA reserves the right to terminate the grant and take a considered decision on what amount of grant funds should be refunded.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<h3><em>M. Indemnity Clause</em></h3>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>IFA shall not be responsible for any loss or prejudice to yourself or any third person in consequence of any act or omission of or by the IFA, its Trustees, Executive Director or staff in connection with the grant being hereby made to the Grantee including any premature termination of the grant and the Grantee agrees to indemnify and hold harmless each of the IFA, its Trustees, Executive Director and staff in respect of any claim for such loss or prejudice.</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>If this letter with the Grant Terms and Conditions correctly set forth your understanding of the terms of this grant, please indicate your agreement by countersigning the attached copy of this letter and return it to IFA with a request for the first installment for your project.</p>\n\n<p>&nbsp;</p>\n\n<p>On behalf of IFA, I extend my best wishes for the success of this important endeavour.</p>\n\n<p>&nbsp;</p>\n\n<p>Warm regards,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>____________________________</strong></p>\n\n<p><strong>Executive Director</strong></p>\n\n<p><strong>India Foundation for the Arts</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Accepted and agreed</p>\n\n<p>&nbsp;</p>\n\n<p>Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Signature: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Name of the Organisation: __________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><em>Annexure I: Undertaking</em></p>\n\n<p><em>Annexure II: (Interim/Final) Financial Report</em></p>\n\n<p><em>Annexure III: Archival Requirements.</em></p>\n\n<p>&nbsp;</p>\n","2","short");
INSERT INTO `dms_discription` VALUES("6","9","<p>&nbsp;</p>\n\n<h6>Month Date, Year</h6>\n\n<p>&nbsp;</p>\n\n<p>Name:</p>\n\n<p>Address:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>Email:</p>\n\n<p>Phone: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Subject: Grant Number- <u>XXXX-X-X-XXX</u></strong></p>\n\n<p>&nbsp;</p>\n\n<p>Dear______________,</p>\n\n<p>&nbsp;</p>\n\n<p>I am delighted to inform you that the India Foundation for the Arts (IFA) has approved a grant of Rs ___________/- (Rupees _______________ only) with you as the &lsquo;Grantee&rsquo; for the Grant Number mentioned in the Subject line of this letter.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<td>\n			<div>\n			<p>Precis</p>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>This grant is being made in response to your proposal submitted under IFA&rsquo;s _______ Programme and your letter of request dated <u>Month Date, Year</u>. Grant funds will be available over a period of _________&nbsp;&nbsp; __________ beginning from <u>Month Date, Year</u> and ending on <u>Month Date, Year</u>.</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Approved Budget</strong></p>\n\n<p>&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"width:475px\">\n			<p>(number of rows can vary)</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"width:475px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"width:115px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>Total: Rs ________ &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>(Amount in words: ________ &nbsp;only)</p>\n\n<p>&nbsp;</p>\n\n<p><strong>This letter outlines the terms and conditions of accepting this grant from IFA. Please read all the terms and conditions carefully before signing. </strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>After we receive the following, IFA will release the first installment of the grant:</p>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>A copy of this letter signed by the duly authorised signatory with the seal of the organisation</li>\n	<li>A payment request from the organisation for the first installment of the project</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<h3>GRANT TERMS AND CONDITIONS</h3>\n\n<h3>&nbsp;</h3>\n\n<p><strong><em>A. Financial</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Grant funds must be used specifically for the designated purpose(s) in fulfillment of the objectives outlined in the project proposal. Grant funds must be physically segregated in a separate bank account established and maintained exclusively for charitable/not-for-profit purposes.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The total commitment of IFA is limited to the amount approved in this letter. Any expenses incurred on the project beyond the approved limit will be the organisation&rsquo;s responsibility.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The grant funds must be used specifically in accordance with the attached budget categories. In case you anticipate a change in the provisions made in the approved budget categories by 20 per cent or Rs. 50,000/-, whichever is less, a prior written approval must be sought from IFA at least one month before the end of the grant term.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>A grant modification will also need to be approved if it is found necessary to introduce a new budget category (or categories) for legitimate grant-related expenditure.&nbsp; The Grantee is requested to seek an approval for a grant modification request before using funds to cover costs that cannot be accounted for under approved budget categories.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Any funds committed for the purposes of the grant but not expended within the term of the grant, will be returned to IFA with interest earned if any.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Grant funds will be released in two periodic installments of 90% and 10% respectively. The final installment of 10% will be released after three weeks of submission of satisfactory final reports on time and depositing of expected deliverables as outlined on Page 1 of this letter, along with the duly signed undertaking as per the format in <em>Annexure I: Undertaking</em>.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to call for copies of vouchers and receipts pertaining to the grant for verification, any time during the term of the grant, until all reports and deliverables are submitted, and also to conduct or cause to be conducted an audit of the books of the Grantee&rsquo;s accounts.</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3>8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; If the Grantee has sought, received or will seek funding from any other sources for this project and for the same purposes as mentioned in the proposal submitted to IFA, the Grantee needs a written approval from IFA to do so. Co-funders will not be entitled to any rights or license terms that impinge on the license terms of IFA as mentioned in <em>Section G</em> of this letter.</h3>\n\n<p>&nbsp;</p>\n\n<p><strong><em>B. Reporting</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee needs to submit one set of reports to IFA duly signed by the head of the organisation &ndash; the Final Reports at the end of the first installment of the grant within 45 days after the end of the grant period. The set will consist of a Narrative Report and a Financial Report.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The Final Narrative Report should be comprehensive in describing and assessing all the activities undertaken during the entire grant period. It should be a self-evaluative document, covering shortcomings, accomplishments and innovations, responses to specific constraints or problems, and lessons learnt.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Final Financial Report should be as per the format in <em>Annexure II: Financial Report</em>.&nbsp; It should have all records of expenditures like bills, vouchers and documents attached and should be certified by a practicing Chartered Accountant confirming that the funds have been utilised for the purpose for which the grant was made.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee is required to send IFA soft copies as well as signed hard copies in duplicate of all reports.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>It is essential for the Grantee to keep copies of all reports and supporting financial and accounting documents that have been submitted to IFA for at least four years after the end of the grant term. IFA reserves its right to conduct or cause to be conducted an audit of your books of accounts in respect of the grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In case of failure to comply with the abovementioned reporting conditions, the Grantee will be required to return to IFA all the funds disbursed with interest earned if any. Failure to comply with grant reporting conditions and undertakings will make the Grantee ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>In addition, the audited financial accounts (Income &amp; Expenditure account with Balance sheet) of the Grantee should be submitted by September 30<sup>th</sup> each year for the previous year, for the period of the grant.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<h3><em>C. Monitoring and Evaluation</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA will monitor grant progress as needed through site visits and examination of relevant records by its officers or consultants. IFA may also engage external evaluators to evaluate grant accomplishments from time to time.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>D. Extension</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA does not encourage the Grantee to seek extensions of the term of their grant. However, in rare circumstances beyond their control, the Grantee may request&nbsp;a one-time extension of half of the grant term or six months, whichever is lesser.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>The request will be given consideration only if it is made&nbsp;two months prior to the end of the grant period.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>E. Equipment</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All equipment purchased using grant funds will be treated as the property of IFA and, at the time of submitting the Final Reports, it will be returned to IFA in good working condition.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>In case the capital equipment is lost, the grantee shall reimburse the cost of replacing it, and in case it is damaged, the grantee shall reimburse the cost of repairing it.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3><em>F. Copyright</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>Copyright on materials resulting from the grant remains with the authors of such materials unless expressly transferred in writing to IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>G. Licence Terms</em></strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<ol>\n	<li>IFA will have the licence to use the process materials and outcomes deposited with IFA as deliverables of the grant for specific purposes that will include but not be exclusive of broadcasting, displaying, publicising, reproducing and archiving in a publicly accessible platform for non-commercial purposes.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>H. Deliverables to IFA</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All deliverables as mentioned on Page 1 of this letter will be deposited with IFA at the time of submission of the Final Reports as per <em>Annexure III: Archival Requirements. </em></li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>Failure to deposit this material will result in forfeiture of the 10% of the grant amount and will make the Grantee ineligible for applying to IFA in the future.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>After the end of the grant period, IFA may request the _______________________________________ to present the project at mutually agreed upon Grant Showcases (a maximum of three) that IFA puts together so that the wider public gets to know the work of IFA&rsquo;s Grantees. While all costs will be taken care of by IFA, there will be no honorarium for the _____________________________________ or the Grantee.</li>\n</ol>\n\n<h3>&nbsp;</h3>\n\n<h3><em>I. Acknowledgements</em></h3>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>All activities, process materials and final outcomes of the grant supported by IFA, including but not exclusive to exhibitions, screenings, stage productions, films, videos, audio tapes, publications, archives, websites etc, in private or public domain must carry acknowledgment of IFA&rsquo;s support as indicated by IFA in writing. This would include a line framed by, and the IFA logo provided by IFA.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The acknowledgement of IFA should continue every time it is showcased, presented, published or exhibited if its making has been partly or fully supported by IFA, within and beyond the grant term; and even after it is acquired through commercial transactions by other organisations or individuals.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>In some cases, IFA may also require acknowledgement of specific donor contributions, if any, that have enabled IFA to make this grant.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>J. Permissions</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee shall be solely responsible for any permission for use of material or documentation of any kind, required to be obtained from government/ agencies/local communities/ other artists and whosoever it may concern, in furtherance of project work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>This permission shall include and extend to IFA&rsquo;s Licence Terms as indicated in <em>Section G</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>The Grantee shall indemnify IFA for any breach of contract between the Grantee and the abovementioned bodies in the event of any dispute.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p><strong><em>K. Public Access of Grant Materials</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA encourages the Grantees to deposit the various process documents and outcomes from the grant to archives, libraries and bodies that facilitate public access to such work.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong><em>L. Termination of Grant</em></strong></p>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>IFA reserves the right to terminate a grant at any time during the grant period, suspend all grant payments and recover the grant funds with interest earned if any, in any of the following situations:</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If IFA comes to the conclusion that grant funds are - not being used for the purposes intended; or diverted / misapplied to the prejudice of the fulfillment of the objectives as stated in the proposal; or not fully and properly accounted for;</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If there is delinquency in reporting to IFA in the manner and time or reports are not satisfactory, as mentioned in the <em>Section B</em> of this letter.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol style=\"list-style-type:lower-alpha\">\n	<li>If the progress of the grant has been stalled due to reasons within the grantee&rsquo;s control.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<ol>\n	<li>If IFA concludes that it is impossible to implement grant activities for reasons beyond the Grantee&rsquo;s control inspite of their best intensions, IFA reserves the right to terminate the grant and take a considered decision on what amount of grant funds should be refunded.</li>\n</ol>\n\n<p style=\"margin-left:.25in\">&nbsp;</p>\n\n<ol>\n	<li>If IFA becomes aware that the grant activities are unlikely to make further or satisfactory progress for any other reasons, IFA reserves the right to terminate the grant and take a considered decision on what amount of grant funds should be refunded.</li>\n</ol>\n\n<p>&nbsp;</p>\n\n<h3><em>M. Indemnity Clause</em></h3>\n\n<p>&nbsp;</p>\n\n<ul>\n	<li>IFA shall not be responsible for any loss or prejudice to yourself or any third person in consequence of any act or omission of or by the IFA, its Trustees, Executive Director or staff in connection with the grant being hereby made to the Grantee including any premature termination of the grant and the Grantee agrees to indemnify and hold harmless each of the IFA, its Trustees, Executive Director and staff in respect of any claim for such loss or prejudice.</li>\n</ul>\n\n<p>&nbsp;</p>\n\n<p>If this letter with the Grant Terms and Conditions correctly set forth your understanding of the terms of this grant, please indicate your agreement by countersigning the attached copy of this letter and return it to IFA with a request for the first installment for your project.</p>\n\n<p>&nbsp;</p>\n\n<p>On behalf of IFA, I extend my best wishes for the success of this important endeavour.</p>\n\n<p>&nbsp;</p>\n\n<p>Warm regards,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>__________________________________</strong></p>\n\n<p><strong>Executive Director</strong></p>\n\n<p><strong>India Foundation for the Arts</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Accepted and agreed</p>\n\n<p>&nbsp;</p>\n\n<p>Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Signature: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Name of the Organisation: __________________________</p>\n\n<p>&nbsp;</p>\n\n<p>Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ____________________________________</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><em>Annexure I: Undertaking</em></p>\n\n<p><em>Annexure II: Final Financial Report</em></p>\n\n<p><em>Annexure III: Archival Requirements</em></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n","2","long");
INSERT INTO `dms_discription` VALUES("7","46","<p>Grant Number:<strong> 2016-G-0-015</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Grantee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Name, City/Town/District/Village, State</p>\n\n<p>&nbsp;</p>\n\n<p>Grant amount&nbsp; : Rs ____________</p>\n\n<p>&nbsp;</p>\n\n<p>Grant period&nbsp;&nbsp;&nbsp; : __ year and ___ months</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>Start date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Month Date, Year<br />\n&nbsp;</p>\n\n<p>End date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Month Date, Year</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td>\n			<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n				<tbody>\n					<tr>\n						<td>\n						<div>\n						<p>Narrative</p>\n						</div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p><strong>Pr&eacute;cis</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>___</strong><strong><u>Name of Programme____</u></strong></p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("8","12","<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Date: _________</p>\n\n<p><u>Name of the Grantee </u></p>\n\n<p><u>Address</u>_________</p>\n\n<p>____________</p>\n\n<p>____________</p>\n\n<p>____________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ________________</p>\n\n<p>Email and Mobile: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grant Number</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Dear Mr/Ms ____________________,</p>\n\n<p>This has reference to your letter of request dated _____________ addressed to our Executive Director.</p>\n\n<p>In response to it, I am enclosing a cheque bearing number _______________ dated ____________ for Rs. ________________ (Rupees _______________________________only) towards release of the ______instalment of your above referred grant/fellowship.</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kindly acknowledge receipt of the cheque.</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; With best wishes,</p>\n\n<p>&nbsp;</p>\n\n<p>Sincerely,</p>\n\n<p>&nbsp;</p>\n\n<p>___<u>Name of Deputy Manager</u>_____</p>\n\n<p>Deputy Manager &ndash; Management Services</p>\n\n<p>India Foundation for the Arts</p>\n\n<p>Encl: As above<a name=\"_GoBack\"></a></p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("9","22","<p><strong>INDIA FOUNDATION FOR THE ARTS</strong></p>\n\n<p><strong>BANGALORE</strong></p>\n\n<p>&nbsp;</p>\n\n<p><u>Memorandum</u></p>\n\n<p>&nbsp;</p>\n\n<p>To&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : The Trustees&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<a name=\"_GoBack\"></a>Month Date, Year</p>\n\n<p>&nbsp;</p>\n\n<p>Via&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <u>___Name of Executive Director_______</u></p>\n\n<p><strong>[Name of Programme]</strong></p>\n\n<p>From&nbsp;&nbsp;&nbsp; : __<u>Name of Programme Executive</u>_______</p>\n\n<p>&nbsp;</p>\n\n<p>Subject&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <u>&nbsp;&nbsp;Name of Grantee (Grant Number<strong>)</strong>: Grant Monitoring</u></p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<td>\n			<div>\n			<p>Narrative (No word limit)</p>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<td>\n			<div>\n			<p>Narrative Continued&hellip;</p>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n	<tbody>\n		<tr>\n			<td>\n			<div>\n			<p>Narrative part from Precis</p>\n			</div>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("10","24","<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Date: _________</p>\n\n<p><u>Name of the Grantee </u></p>\n\n<p><u>Address</u>_________</p>\n\n<p>____________</p>\n\n<p>____________</p>\n\n<p>____________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ________________</p>\n\n<p>Email and Mobile: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grant Number</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Dear Mr/Ms ____________________,</p>\n\n<p>This has reference to your letter of request dated _____________ addressed to our Executive Director.</p>\n\n<p>In response to it, I am enclosing a cheque bearing number _______________ dated ____________ for Rs. ________________ (Rupees _______________________________only) towards release of the ______instalment of your above referred grant/fellowship.</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kindly acknowledge receipt of the cheque.</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; With best wishes,</p>\n\n<p>&nbsp;</p>\n\n<p>Sincerely,</p>\n\n<p>&nbsp;</p>\n\n<p>___<u>Name of Deputy Manager</u>_____</p>\n\n<p>Deputy Manager &ndash; Management Services</p>\n\n<p>India Foundation for the Arts</p>\n\n<p>Encl: As above<a name=\"_GoBack\"></a></p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("11","26","<p style=\"margin-left:3.0in\">Date: ____________</p>\n\n<p>&nbsp;</p>\n\n<p>To</p>\n\n<p>&nbsp;</p>\n\n<p>____<u>Grantee Name</u>______</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; _______________</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grant Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>Email:</p>\n\n<p>Mobile:</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Dear _____________,</p>\n\n<p>&nbsp;</p>\n\n<p>This has reference to your letter dated ____________ requesting an extension of the term of your grant.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; As per your request, the term of your grant has been extended till _________ to enable you to complete grant-related activities.</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Under no circumstances will further extension be granted.</p>\n\n<p>&nbsp;</p>\n\n<p>Please note that in view of this extension, the final narrative and the financial reports will now fall due immediately after the completion of the term of the grant.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; With regards,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sincerely,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; __<u>Name of ED</u>________</p>\n\n<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Executive Director</strong></p>\n\n<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; India Foundation for the Arts</strong></p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("12","27","<p>Date: ________</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>To</p>\n\n<p>&nbsp;</p>\n\n<p>____<u>Grantee Name</u>______</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; __________________</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grant Number</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Dear _________,</p>\n\n<p>&nbsp;</p>\n\n<p>This has reference to your letter dated ___________ requesting that the budget pertaining to the above referred grant be modified.</p>\n\n<p>&nbsp;</p>\n\n<p>Your request for the modification of the budget for the project has been approved and the revised budget stands as follows:</p>\n\n<p>&nbsp;</p>\n\n<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"height:36px; width:304px\">\n			<p>Budget Category</p>\n			</td>\n			<td style=\"height:36px; width:163px\">\n			<p>Original Budget</p>\n			</td>\n			<td style=\"height:36px; width:177px\">\n			<p>Modified Budget</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:24px; width:304px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:24px; width:163px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:24px; width:177px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:16px; width:304px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:16px; width:163px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:16px; width:177px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:16px; width:304px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:16px; width:163px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:16px; width:177px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:20px; width:304px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:20px; width:163px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:20px; width:177px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:23px; width:304px\">\n			<p>Total</p>\n			</td>\n			<td style=\"height:23px; width:163px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:23px; width:177px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; With warm regards,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sincerely,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>___<u>Name of the ED</u>_____</strong></p>\n\n<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Executive Director</strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong>India Foundation for the Arts</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("13","29","<p><strong><u>End of Project Reminder</u></strong></p>\n\n<h6>Date: _________</h6>\n\n<p>&nbsp;</p>\n\n<p>To</p>\n\n<p>&nbsp;</p>\n\n<p>Grantee</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <strong><u>XXXX-G-X-XXX</u></strong></p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grant Number</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Dear _______,</p>\n\n<p>&nbsp;</p>\n\n<p>You are aware that the term of your abovereferred grant comes to an end on _________.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I hope that your grant-related work is progressing well, and that you will be able to send in your narrative and financial reports within two months of the close of the term of this grant.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; In case you need to extend the term of this grant, please send us a letter of request before the term of your grant ends. The letter of request should indicate the new proposed closing date for the grant, and provide reasons why you require this extension.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please note that we will not be able to consider requests for extension after the end of the term of this grant. You will then not be able to utilise grant funds beyond&nbsp;&nbsp;__________ and the unutilised balance would have to be returned to IFA.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sincerely,</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; XXXXXXXXXX</strong></p>\n\n<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Manager &ndash; Management Services</strong></p>\n\n<p><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; India Foundation for the Arts</strong></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("14","42","<p><strong>INDIA FOUNDATION FOR THE ARTS</strong></p>\n\n<p><strong>BANGALORE</strong></p>\n\n<p><strong>Project Completion Memo</strong></p>\n\n<p style=\"margin-left:5.0in\">Date:&nbsp;&nbsp;</p>\n\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"height:22px; width:187px\">\n			<p>Grantee Name:</p>\n			</td>\n			<td style=\"height:22px; width:451px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:22px; width:187px\">\n			<p>Grant Number:</p>\n			</td>\n			<td style=\"height:22px; width:451px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:22px; width:187px\">\n			<p>Programme Name:</p>\n			</td>\n			<td style=\"height:22px; width:451px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:22px; width:187px\">\n			<p>Programme Executive(s):</p>\n			</td>\n			<td style=\"height:22px; width:451px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:22px; width:187px\">\n			<p>Grant Started On:</p>\n			</td>\n			<td style=\"height:22px; width:451px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:22px; width:187px\">\n			<p>Grant Closed On:</p>\n			</td>\n			<td style=\"height:22px; width:451px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td style=\"height:22px; width:187px\">\n			<p>Extension (if any, please mention no. of months):</p>\n			</td>\n			<td style=\"height:22px; width:451px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan=\"2\" style=\"height:22px; width:638px\">\n			<p>Narrative part of Pr&eacute;cis:</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p><strong>This is to confirm that IFA has received the following items as mentioned in the GNL:</strong></p>\n\n<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td style=\"height:15px; width:222px\">\n			<p>Final Narrative Report:</p>\n			</td>\n			<td style=\"height:15px; width:96px\">\n			<p>&nbsp;</p>\n			</td>\n			<td style=\"height:15px; width:215px\">\n			<p>Final Financial Report:</p>\n			</td>\n			<td style=\"height:15px; width:103px\">\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan=\"4\" style=\"height:81px; width:636px\">\n			<p>Deliverables (please mention individual items):</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n		<tr>\n			<td colspan=\"4\" style=\"height:81px; width:636px\">\n			<p>Outcomes (please mention if expected on a later date):</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n\n			<p>&nbsp;</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p style=\"margin-left:-9.0pt\"><img src=\"file:///C:/Users/LSS-26~1/AppData/Local/Temp/msohtmlclip1/01/clip_image001.png\" style=\"height:83px; width:638px\" /><br />\n&nbsp;<strong>Programme Officer&rsquo;s Comment and Clarification (if any):</strong></p>\n\n<p><strong>Deliverables and outcomes are submitted to the IFA Archive on: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></p>\n\n<p><strong>Final Installment Cheque Details and Payment Date:</strong></p>\n\n<p>&nbsp;</p>\n\n<p>---------------------------&nbsp; --------------------------- &nbsp;&nbsp;&nbsp;&nbsp; -----------------------------&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -----------------------------</p>\n\n<p>Programme Officer(s)&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Archivist&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MSD&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;Executive Director</p>\n","","");
INSERT INTO `dms_discription` VALUES("15","43","<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Date: _________</p>\n\n<p><u>Name of the Grantee </u></p>\n\n<p><u>Address</u>_________</p>\n\n<p>____________</p>\n\n<p>____________</p>\n\n<p>____________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ________________</p>\n\n<p>Email and Mobile: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grant Number</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>Dear Mr/Ms ____________________,</p>\n\n<p>This has reference to your undertaking dated _____________ (as per annexure 1) confirming submission of the final narrative report, certified financial report, and deposit of deliverables as specified in the Grant Notification Letter.</p>\n\n<p>In view of the above, we now enclose a cheque bearing number _______________ dated ____________ for Rs. ________________ (Rupees _______________________________only) towards release of the balance amount of ____________instalment (10%) in respect<a name=\"_GoBack\"></a> of your above referred grant/fellowship.</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kindly acknowledge receipt of the cheque.</p>\n\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; With best wishes,</p>\n\n<p>&nbsp;</p>\n\n<p>Sincerely,</p>\n\n<p>&nbsp;</p>\n\n<p>___<u>Name of Deputy Manager</u>_____</p>\n\n<p>Deputy Manager &ndash; Management Services</p>\n\n<p>India Foundation for the Arts</p>\n\n<p>Encl: As above</p>\n\n<p>&nbsp;</p>\n","","");
INSERT INTO `dms_discription` VALUES("16","44","<p>Grant Number: <strong>2013-0-010</strong></p>\n\n<p>&nbsp;</p>\n\n<p><strong>&nbsp; ___ Grantee Name____&nbsp;&nbsp; </strong></p>\n\n<p>&nbsp;</p>\n\n<p>Final Evaluation</p>\n\n<p>&nbsp;</p>\n\n<p><img alt=\"Text Box: Narrative part of Precis\n\n\" src=\"file:///C:/Users/LSS-26~1/AppData/Local/Temp/msohtmlclip1/01/clip_image001.gif\" style=\"float:left; height:313px; width:291px\" /><img alt=\"Text Box: Narrative (No word limit)\" src=\"file:///C:/Users/LSS-26~1/AppData/Local/Temp/msohtmlclip1/01/clip_image002.gif\" style=\"float:left; height:350px; width:325px\" /></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<table cellpadding=\"0\" cellspacing=\"0\">\n	<tbody>\n		<tr>\n			<td>\n			<table cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%\">\n				<tbody>\n					<tr>\n						<td>\n						<div>\n						<p>Narrative Continued&hellip;</p>\n						</div>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><strong>Name of Programme Executive</strong></p>\n\n<p><strong>Month, Year</strong></p>\n","","");


DROP TABLE IF EXISTS dms_document_template;

CREATE TABLE `dms_document_template` (
  `document_temp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_temp_name` varchar(250) NOT NULL,
  `document_temp_parent` bigint(20) DEFAULT NULL,
  `document_order_id` bigint(20) DEFAULT NULL,
  `short_code` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`document_temp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

INSERT INTO `dms_document_template` VALUES("1","Final proposal","0","1","fprp");
INSERT INTO `dms_document_template` VALUES("2","Proposal ","1","0","");
INSERT INTO `dms_document_template` VALUES("3","Budget","1","4","bg**");
INSERT INTO `dms_document_template` VALUES("4","Bio Data","1","","bd");
INSERT INTO `dms_document_template` VALUES("5","Supporting materials","1","","spmt");
INSERT INTO `dms_document_template` VALUES("6","Draft","1","","dft");
INSERT INTO `dms_document_template` VALUES("7","Translation of Proposals","1","","fptr");
INSERT INTO `dms_document_template` VALUES("9","GNL","0","3","gncl ");
INSERT INTO `dms_document_template` VALUES("10","Signed GNL ","0","4","sgnl");
INSERT INTO `dms_document_template` VALUES("11","Request to release the first instalment from grantee","0","5","freq");
INSERT INTO `dms_document_template` VALUES("13","Interim Provision to upload more than 1 report","0","6","");
INSERT INTO `dms_document_template` VALUES("14","Narrative","13","","");
INSERT INTO `dms_document_template` VALUES("15","Financial","13","","");
INSERT INTO `dms_document_template` VALUES("16","Process Document","13","","");
INSERT INTO `dms_document_template` VALUES("17","Images","16","","");
INSERT INTO `dms_document_template` VALUES("18","Audio","16","","");
INSERT INTO `dms_document_template` VALUES("19","Video","16","","");
INSERT INTO `dms_document_template` VALUES("20","Documents","16","","");
INSERT INTO `dms_document_template` VALUES("21","Websites","16","","");
INSERT INTO `dms_document_template` VALUES("22","Monitoring Memo","0","7","");
INSERT INTO `dms_document_template` VALUES("23","Request to release second instalment ֠Grantee","0","8","");
INSERT INTO `dms_document_template` VALUES("24","Second instalment release letter","0","9","");
INSERT INTO `dms_document_template` VALUES("25","Application for extension ֠Grantee","0","10","");
INSERT INTO `dms_document_template` VALUES("26","Extension Application for Budget modification","0","11","");
INSERT INTO `dms_document_template` VALUES("27","Modified Budget","0","12","");
INSERT INTO `dms_document_template` VALUES("28","Approval for budget modification","0","13","");
INSERT INTO `dms_document_template` VALUES("29","End of term notification","0","14","");
INSERT INTO `dms_document_template` VALUES("30","Final report ","0","15","");
INSERT INTO `dms_document_template` VALUES("31","Narrative","30","","");
INSERT INTO `dms_document_template` VALUES("32","Financial","30","","");
INSERT INTO `dms_document_template` VALUES("33","Deliverables","0","16","d***");
INSERT INTO `dms_document_template` VALUES("34","Document","33","","");
INSERT INTO `dms_document_template` VALUES("35","Images","33","","");
INSERT INTO `dms_document_template` VALUES("36","Audio","33","","");
INSERT INTO `dms_document_template` VALUES("37","Video","33","","");
INSERT INTO `dms_document_template` VALUES("38","Source code","33","","");
INSERT INTO `dms_document_template` VALUES("39","Programs","33","","");
INSERT INTO `dms_document_template` VALUES("40","Blogs","33","","");
INSERT INTO `dms_document_template` VALUES("41","Websites","33","","");
INSERT INTO `dms_document_template` VALUES("42","Project Completion Memo","0","17","");
INSERT INTO `dms_document_template` VALUES("43","Final instalment release letter","0","18","");
INSERT INTO `dms_document_template` VALUES("44","Final Evaluation Report","0","20","");
INSERT INTO `dms_document_template` VALUES("45","External Evaluation Report ","0","19","");
INSERT INTO `dms_document_template` VALUES("46","Precis","0","2","");
INSERT INTO `dms_document_template` VALUES("47","Communication","0","21","");
INSERT INTO `dms_document_template` VALUES("48","Evaluation report","1","","");
INSERT INTO `dms_document_template` VALUES("49","PR Material","0","22","");
INSERT INTO `dms_document_template` VALUES("50","don\'t be indomitable","1","0","");


DROP TABLE IF EXISTS dms_grant;

CREATE TABLE `dms_grant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `archiv_tag` varchar(250) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `categorytype_id` bigint(20) DEFAULT NULL,
  `category_year` varchar(250) NOT NULL,
  `grant_number` varchar(20) DEFAULT NULL,
  `grantee_name` varchar(250) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `extension` date DEFAULT NULL,
  `grant_duration` varchar(250) DEFAULT NULL,
  `program_executive` bigint(20) DEFAULT NULL,
  `grant_amount` varchar(250) DEFAULT NULL,
  `grantee_language` varchar(250) DEFAULT NULL,
  `grantee_geogrtaphical_area` varchar(250) DEFAULT NULL,
  `deliverables` varchar(250) DEFAULT NULL,
  `grantee_disciplinary_field` varchar(250) DEFAULT NULL,
  `comment` text,
  `grantee_possible_outcome` varchar(250) DEFAULT NULL,
  `digital_copy_medium` varchar(250) DEFAULT NULL,
  `auther` varchar(250) DEFAULT NULL,
  `editor` varchar(250) DEFAULT NULL,
  `publisher` varchar(250) DEFAULT NULL,
  `publication_place` varchar(250) DEFAULT NULL,
  `copyright` varchar(250) DEFAULT NULL,
  `dimensions` varchar(250) DEFAULT NULL,
  `creator_degital_copy` varchar(250) DEFAULT NULL,
  `hardware` varchar(250) DEFAULT NULL,
  `software` int(11) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `keyword` varchar(250) DEFAULT NULL,
  `degital_copy_date` date DEFAULT NULL,
  `assine` varchar(250) DEFAULT NULL,
  `Approval` varchar(250) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `approval_date` varchar(250) DEFAULT NULL,
  `popular_search` varchar(250) NOT NULL,
  `description_type` varchar(250) DEFAULT NULL,
  `description_sub_type` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

INSERT INTO `dms_grant` VALUES("34","lss01","2","2","2005","01","39","2018-04-09","2018-04-25","2018-04-25","45-","3","45k","3","2","4","4","","3","","","","","","","","","","","","","","","","","3","2018-04-23","","3","long");
INSERT INTO `dms_grant` VALUES("35","lss2","1","1","2016","0202","40","2018-04-24","2018-04-27","2019-03-27","565","3","t54654","2","2","2","3","","1","","","","","","","","","","","","","","","","","3","2018-04-26","","2","short");
INSERT INTO `dms_grant` VALUES("38","23","9","2","2017","012","45","2018-04-27","2018-04-28","2018-04-30","4","3","fdsfsad","3","1","2","1","","5","","","","","","","","","","","","","","","","","1","2018-04-26","","3","long");
INSERT INTO `dms_grant` VALUES("39","lss8","2","1","2018","0110","46","2018-04-11","2018-04-25","2018-04-08","1232","3","123","3","3","1","2","","2","","","","","","","","","","","","","","","","","1","2018-04-26","","2","long");


DROP TABLE IF EXISTS dms_grant_metadeta;

CREATE TABLE `dms_grant_metadeta` (
  `grant_metadeta_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grant_temp_id` bigint(20) NOT NULL,
  `grant_metadeta_title` varchar(250) NOT NULL,
  `grant_metadeta_value` varchar(250) NOT NULL,
  `deliverables` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`grant_metadeta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS dms_grantee;

CREATE TABLE `dms_grantee` (
  `grantee_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grantee_name` varchar(250) DEFAULT NULL,
  `grantee_phone` bigint(20) DEFAULT NULL,
  `grantee_alternative_no` varchar(250) DEFAULT NULL,
  `grantee_address1` varchar(250) DEFAULT NULL,
  `grantee_address2` varchar(250) DEFAULT NULL,
  `grantee_city` varchar(250) DEFAULT NULL,
  `grantee_state` varchar(250) DEFAULT NULL,
  `deliverables` varchar(250) DEFAULT NULL,
  `grantee_pin` bigint(20) DEFAULT NULL,
  `grantee_country` varchar(250) DEFAULT NULL,
  `grantee_geogrtaphical_area` varchar(250) DEFAULT NULL,
  `grantee_disciplinary_field` varchar(250) DEFAULT NULL,
  `grantee_language` varchar(250) DEFAULT NULL,
  `grantee_possible_outcome` varchar(250) DEFAULT NULL,
  `grantee_email` varchar(250) DEFAULT NULL,
  `grantee_website` varchar(250) DEFAULT NULL,
  `grantee_status` varchar(250) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`grantee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

INSERT INTO `dms_grantee` VALUES("1","sssssss","0","0","","","","","","0","0","","","","","","","","");
INSERT INTO `dms_grantee` VALUES("2","ggggggggggg","0","0","","","","0","","0","0","0","0","","0","","","","");
INSERT INTO `dms_grantee` VALUES("3","sksks","0","0","","","","2,4","1,3","226016","0","2,4","1,4","2,3","2,4","","","","");
INSERT INTO `dms_grantee` VALUES("4","rose","81153223222","0","deoria","gkp","deoria","","13","274404","0","","1","3","1","deoria@gmail.com","w3school","","");
INSERT INTO `dms_grantee` VALUES("5","excecuse","2613304545","0","lko1","lko2","lucknow","","14","261330","0","5","7","3","1","excecuse@gmail.com","w3school","","");
INSERT INTO `dms_grantee` VALUES("6","twincle","0","0","","","","","1","0","0","","2","","","","","","");
INSERT INTO `dms_grantee` VALUES("7","mark","274405","0","cicago","cicago","washington","3","15","274404","0","","10","2","6","3470@gmail.com","3470@gmail.com","","");
INSERT INTO `dms_grantee` VALUES("16","acd","546798","0","uk","uk2","uk","2","2","274404","0","1","2","2","1","sdfghgf","wefrgh","","");
INSERT INTO `dms_grantee` VALUES("17","jkl","0","0","kanupur","kanupur","kanupur","","1","0","0","3","2","","3","kanupur","kanupur","","");
INSERT INTO `dms_grantee` VALUES("18","pk2","0","0","kanupur","kanupur","sdfer","","1","0","0","","2","","","kanupur","kanupur","","");
INSERT INTO `dms_grantee` VALUES("19","dk","3456","0","r","kanupur","kanupur","1","","456","0","","","","","kanupur","erth","","");
INSERT INTO `dms_grantee` VALUES("20","t","3443","0","f","f","ff","1","4","0","0","2","3","2","3","fdsafab@gmail.com","fsadfsdafsa","","");
INSERT INTO `dms_grantee` VALUES("21","joya","89876543","0","rr","rr","gkp","3","2","444444","0","2","1","3","2","fdasfasd@gmail.com","fdsfsa","","");
INSERT INTO `dms_grantee` VALUES("22","name","9876543","0","hr","hr1","23243","2","2","274404","0","2","2","2","2","gmail.com","gmail.com","","");
INSERT INTO `dms_grantee` VALUES("32","g8","45788776","0","ghk","ghty","rkjhgsfdg","1","1","456789876543","0","1","2","2","1","gsdfg@gmail.com","fsadfsa","","");
INSERT INTO `dms_grantee` VALUES("33","fasdsa","45654","0","dsfdsa","hjfh","dfsa","1","2","5678908765","0","1","2","1","1","gfsdfgsd@gmail.com","fasdfsa","","");
INSERT INTO `dms_grantee` VALUES("34","grantee1","86575324","0","gee","eee","deoria","1","2","47675","0","3","3","2","2","g@gmail.com","sgfwasd","","");
INSERT INTO `dms_grantee` VALUES("35","ar4","345678908765","0","dfdsfsda","dfsdfsdafds","f56gfbrg","1","2","261330","0","2","3","1","1","fad@gmail.com","fasdfsad","","");
INSERT INTO `dms_grantee` VALUES("36","fasdfsa","8115603279","0","rer","rt5","werwe","1","4","34567","0","4","2","3","1","admin@gmail.com","fdasfa","","");
INSERT INTO `dms_grantee` VALUES("37","dfsadfsa","345465768","0","dfad","dfadf","43sdffaewf","1","2","345","0","2","2","2","2","dsfsad@gmail.com","dfasd","","");
INSERT INTO `dms_grantee` VALUES("38","fasdfsdadfsdafdsfsdf","345678986543","0","fasdfa","fdsafdsafsadfdsa","dfadsfsafasfa","1","1","0","0","2","2","2","2","ad@gmail.com","fsadfsad","","");
INSERT INTO `dms_grantee` VALUES("39","lss1","8115603279","0","gkp","gkp1","gkp","2","4","274404","0","2","4","3","3","gkp@gmail.com","lss.com","","");
INSERT INTO `dms_grantee` VALUES("40","lss2","81156032789","0","gkp2","gkp2.com","gkp2","","2","274404","0","2","3","2","1","hii@gmail.com","dfasdfsa","","");
INSERT INTO `dms_grantee` VALUES("41","lss3","8115603279999","0","gkp1","gkp2","gkp456","2","5","274404","0","3","3","3","3","lss3@gmail.com","lss3.com","","");
INSERT INTO `dms_grantee` VALUES("42","lss4","8115603279","0","deos","deos","deoria","2","2","274404","0","1","2","3","3","lss4@gmail.com","dfdfsda","","");
INSERT INTO `dms_grantee` VALUES("43","lss5","3647869701","0","delhi","delhi2","gkp56","34","5","2274404","0","2","2","2","2","fdfaasfsad@gmail.com","w3school","","");
INSERT INTO `dms_grantee` VALUES("44","lss6","4386854234","0","rt","ret","gkp","3","1","0","0","3","1","2","2","f@gmail.com","fad","","");
INSERT INTO `dms_grantee` VALUES("45","lss7","5675978545","0","fd","fdsfs","hfasdf","2","2","274404","0","1","1","3","5","dfasdfa@gmail.com","fasdfsa","","");
INSERT INTO `dms_grantee` VALUES("46","lss8","34567897","0","terte","etryrtyer","dfsdasfsa","1","1","274404","0","3","2","3","2","dfsa","dfasdfsa","","");


DROP TABLE IF EXISTS dms_individual_metadata;

CREATE TABLE `dms_individual_metadata` (
  `individual_metadata_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `individual_temp_id` bigint(20) NOT NULL,
  `individual_document` varchar(250) DEFAULT NULL,
  `individual_handling_staff` varchar(250) DEFAULT NULL,
  `individual_creator` varchar(250) DEFAULT NULL,
  `individual_copy_right` varchar(250) DEFAULT NULL,
  `individual_metadata_status` varchar(250) DEFAULT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  PRIMARY KEY (`individual_metadata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS dms_individual_temp;

CREATE TABLE `dms_individual_temp` (
  `individual_temp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grant_temp_id` bigint(20) NOT NULL,
  `individual_file` varchar(250) DEFAULT NULL,
  `individual_keyword` varchar(250) DEFAULT NULL,
  `individual_status` varchar(250) DEFAULT NULL,
  `individual_approval` varchar(250) DEFAULT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `metadata_individual_number` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`individual_temp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS dms_language;

CREATE TABLE `dms_language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(250) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

INSERT INTO `dms_language` VALUES("1","Assamese (Axomiya)");
INSERT INTO `dms_language` VALUES("2","english");
INSERT INTO `dms_language` VALUES("3","hindi");
INSERT INTO `dms_language` VALUES("4","asd");
INSERT INTO `dms_language` VALUES("5","vvv");
INSERT INTO `dms_language` VALUES("6","a");
INSERT INTO `dms_language` VALUES("7","ssssss");
INSERT INTO `dms_language` VALUES("8","kkkkk");
INSERT INTO `dms_language` VALUES("9","my test");
INSERT INTO `dms_language` VALUES("10","aaaaaaaaa");
INSERT INTO `dms_language` VALUES("11","bbb");
INSERT INTO `dms_language` VALUES("12","cccc");
INSERT INTO `dms_language` VALUES("13","aaaaaaaa");
INSERT INTO `dms_language` VALUES("14","aaaaa");
INSERT INTO `dms_language` VALUES("15","aaaa");
INSERT INTO `dms_language` VALUES("16","ggggg");
INSERT INTO `dms_language` VALUES("17","hhhhhhhhhh");
INSERT INTO `dms_language` VALUES("18","kkkkkkk");
INSERT INTO `dms_language` VALUES("19","sorrry");
INSERT INTO `dms_language` VALUES("20","good");
INSERT INTO `dms_language` VALUES("21","onse again");
INSERT INTO `dms_language` VALUES("22","no idea");
INSERT INTO `dms_language` VALUES("23","bnvbn");
INSERT INTO `dms_language` VALUES("24","baldau");
INSERT INTO `dms_language` VALUES("25","sunny");
INSERT INTO `dms_language` VALUES("26","dilip");
INSERT INTO `dms_language` VALUES("27","qwerty");
INSERT INTO `dms_language` VALUES("28","sunny baba");
INSERT INTO `dms_language` VALUES("29","chacha");
INSERT INTO `dms_language` VALUES("30","aaa");
INSERT INTO `dms_language` VALUES("31","nnnnn");
INSERT INTO `dms_language` VALUES("32","geo11");
INSERT INTO `dms_language` VALUES("33","laannnnnnnnnn");
INSERT INTO `dms_language` VALUES("34","testing");


DROP TABLE IF EXISTS dms_metadata;

CREATE TABLE `dms_metadata` (
  `metadata_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grant_temp_id` bigint(20) DEFAULT NULL,
  `template_id` bigint(20) DEFAULT NULL,
  `document_temp_id` bigint(20) DEFAULT NULL,
  `metadata_title` varchar(250) DEFAULT NULL,
  `metadata_value` varchar(250) DEFAULT NULL,
  `template_type` varchar(250) DEFAULT NULL,
  `detail` varchar(250) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `metadata_type` varchar(250) DEFAULT NULL,
  `metadata_individual_number` varchar(250) DEFAULT NULL,
  `stage` int(11) DEFAULT NULL,
  PRIMARY KEY (`metadata_id`)
) ENGINE=InnoDB AUTO_INCREMENT=521 DEFAULT CHARSET=latin1;

INSERT INTO `dms_metadata` VALUES("53","34","13","46","Document ID","0123","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("54","34","13","46","Type of Document","Precis","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("55","34","13","46","Digital file name","lss1file","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("56","34","13","46","Date","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("57","34","13","46","Handling Staff","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("58","34","13","46","Language of the Original Material","1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("59","34","13","46","Comments","lss1 lss1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("60","34","13","46","keywords","lss1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("61","34","13","46","Creator/ Author","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("62","34","13","46","Creator/Editor","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("63","34","13","46","Publisher","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("64","34","13","46","Place of Publication","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("65","34","13","46","Copyright","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("66","34","13","46","Access","Public","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("67","34","13","46","Reference to other documents – URL","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("68","34","13","46","Dimensions (in cm)","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("69","34","13","46","Physical Characteristics","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("70","34","13","46","Medium of Original Material","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("71","34","13","46","Extent/No. of Pages","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("72","34","13","46","Place where the original Material is kept","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("73","34","13","46","Creator of the Digital Copy","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("74","34","13","46","Date of digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("75","34","13","46","Comments on digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("76","34","13","46","Hardware used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("77","34","13","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("78","34","13","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("79","35","3","2","Document ID","","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("80","35","3","2","Type of Document","Proposal ","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("81","35","3","2","Digital file name","lss2 file","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("82","35","3","2","Date","2018-04-25","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("83","35","3","2","Handling Staff","fsd","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("84","35","3","2","Language of the Original Material","1","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("85","35","3","2","Comments","hello lss2","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("86","35","3","2","keywords","","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("87","35","3","2","Creator/ Author","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("88","35","3","2","Creator/Editor","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("89","35","3","2","Publisher","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("90","35","3","2","Place of Publication","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("91","35","3","2","Copyright","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("92","35","3","2","Access","Public","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("93","35","3","2","Reference to other documents URL","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("94","35","3","2","Dimensions (in cm)","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("95","35","3","2","Physical Characteristics","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("96","35","3","2","Medium of Original Material","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("97","35","3","2","Extent/No. of Pages","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("98","35","3","2","Place where the original Material is kept","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("99","35","3","2","Creator of the Digital Copy","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("100","35","3","2","Date of digitisation","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("101","35","3","2","Comments on digitisation","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("102","35","3","2","Hardware used","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("103","35","3","2","Software used","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("104","35","3","2","Digital container","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("105","35","15","46","Document ID","012","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("106","35","15","46","Type of Document","Precis","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("107","35","15","46","Digital file name","lss2 parents file","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("108","35","15","46","Date","2018-04-03","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("109","35","15","46","Handling Staff","sdfds","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("110","35","15","46","Language of the Original Material","1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("111","35","15","46","Comments","fdsfsad","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("112","35","15","46","keywords","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("113","35","15","46","Creator/ Author","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("114","35","15","46","Creator/Editor","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("115","35","15","46","Publisher","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("116","35","15","46","Place of Publication","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("117","35","15","46","Copyright","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("118","35","15","46","Access","Public","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("119","35","15","46","Reference to other documents – URL","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("120","35","15","46","Dimensions (in cm)","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("121","35","15","46","Physical Characteristics","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("122","35","15","46","Medium of Original Material","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("123","35","15","46","Extent/No. of Pages","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("124","35","15","46","Place where the original Material is kept","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("125","35","15","46","Creator of the Digital Copy","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("126","35","15","46","Date of digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("127","35","15","46","Comments on digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("128","35","15","46","Hardware used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("129","35","15","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("130","35","15","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("291","38","17","46","Hardware used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("292","38","17","46","Digital file name","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("293","38","17","46","Handling Staff","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("294","38","17","46","Digital file name","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("295","38","17","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("296","38","17","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("297","38","17","46","Document ID","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("298","38","17","46","Digital file name","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("299","38","17","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("300","38","17","46","Date","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("301","38","17","46","Language of the Original Material","1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("302","38","17","46","Date","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("303","38","17","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("304","38","17","46","Type of Document","Precis","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("305","38","17","46","Date","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("306","38","17","46","Handling Staff","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("307","38","17","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("308","38","17","46","Handling Staff","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("309","38","17","46","Comments","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("310","38","17","46","Digital file name","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("311","38","17","46","Language of the Original Material","1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("312","38","17","46","Handling Staff","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("313","38","17","46","Language of the Original Material","1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("314","38","17","46","keywords","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("315","38","17","46","Date","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("316","38","17","46","Language of the Original Material","1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("317","38","17","46","Comments","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("318","38","17","46","Creator/ Author","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("319","38","17","46","Comments","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("320","38","17","46","Handling Staff","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("321","38","17","46","Creator/Editor","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("322","38","17","46","Comments","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("323","38","17","46","keywords","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("324","38","17","46","keywords","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("325","38","17","46","Language of the Original Material","1","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("326","38","17","46","keywords","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("327","38","17","46","Publisher","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("328","38","17","46","Creator/ Author","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("329","38","17","46","Comments","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("330","38","17","46","Creator/ Author","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("331","38","17","46","Creator/ Author","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("332","38","17","46","Place of Publication","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("333","38","17","46","Creator/Editor","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("334","38","17","46","Creator/Editor","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("335","38","17","46","Creator/Editor","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("336","38","17","46","keywords","","","General Information","","define","","");
INSERT INTO `dms_metadata` VALUES("337","38","17","46","Publisher","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("338","38","17","46","Copyright","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("339","38","17","46","Publisher","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("340","38","17","46","Publisher","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("341","38","17","46","Creator/ Author","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("342","38","17","46","Access","Public","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("343","38","17","46","Place of Publication","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("344","38","17","46","Place of Publication","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("345","38","17","46","Reference to other documents – URL","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("346","38","17","46","Place of Publication","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("347","38","17","46","Creator/Editor","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("348","38","17","46","Copyright","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("349","38","17","46","Copyright","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("350","38","17","46","Publisher","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("351","38","17","46","Copyright","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("352","38","17","46","Dimensions (in cm)","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("353","38","17","46","Access","Public","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("354","38","17","46","Access","Public","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("355","38","17","46","Physical Characteristics","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("356","38","17","46","Access","Public","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("357","38","17","46","Place of Publication","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("358","38","17","46","Reference to other documents – URL","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("359","38","17","46","Reference to other documents – URL","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("360","38","17","46","Medium of Original Material","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("361","38","17","46","Copyright","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("362","38","17","46","Reference to other documents – URL","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("363","38","17","46","Dimensions (in cm)","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("364","38","17","46","Dimensions (in cm)","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("365","38","17","46","Extent/No. of Pages","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("366","38","17","46","Dimensions (in cm)","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("367","38","17","46","Access","Public","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("368","38","17","46","Physical Characteristics","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("369","38","17","46","Physical Characteristics","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("370","38","17","46","Place where the original Material is kept","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("371","38","17","46","Reference to other documents – URL","","","Author, Publisher, Access","","define","","");
INSERT INTO `dms_metadata` VALUES("372","38","17","46","Physical Characteristics","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("373","38","17","46","Medium of Original Material","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("374","38","17","46","Medium of Original Material","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("375","38","17","46","Dimensions (in cm)","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("376","38","17","46","Medium of Original Material","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("377","38","17","46","Creator of the Digital Copy","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("378","38","17","46","Physical Characteristics","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("379","38","17","46","Extent/No. of Pages","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("380","38","17","46","Extent/No. of Pages","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("381","38","17","46","Medium of Original Material","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("382","38","17","46","Extent/No. of Pages","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("383","38","17","46","Date of digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("384","38","17","46","Place where the original Material is kept","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("385","38","17","46","Place where the original Material is kept","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("386","38","17","46","Extent/No. of Pages","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("387","38","17","46","Place where the original Material is kept","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("388","38","17","46","Comments on digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("389","38","17","46","Creator of the Digital Copy","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("390","38","17","46","Creator of the Digital Copy","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("391","38","17","46","Creator of the Digital Copy","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("392","38","17","46","Place where the original Material is kept","","","Physical Characteristics","","define","","");
INSERT INTO `dms_metadata` VALUES("393","38","17","46","Hardware used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("394","38","17","46","Date of digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("395","38","17","46","Creator of the Digital Copy","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("396","38","17","46","Date of digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("397","38","17","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("398","38","17","46","Date of digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("399","38","17","46","Date of digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("400","38","17","46","Comments on digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("401","38","17","46","Comments on digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("402","38","17","46","Comments on digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("403","38","17","46","Comments on digitisation","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("404","38","17","46","Hardware used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("405","38","17","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("406","38","17","46","Hardware used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("407","38","17","46","Hardware used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("408","38","17","46","Hardware used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("409","38","17","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("410","38","17","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("411","38","17","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("412","38","17","46","Software used","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("413","38","17","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("414","38","17","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("415","38","17","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("416","38","17","46","Digital container","","","Digitisation Details","","define","","");
INSERT INTO `dms_metadata` VALUES("438","38","4","2","Date of digitisation","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("439","38","4","2","Document ID","12","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("440","38","4","2","Comments on digitisation","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("441","38","4","2","Type of Document","Proposal ","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("442","38","4","2","Hardware used","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("443","38","4","2","Software used","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("444","38","4","2","Digital file name","frt","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("445","38","4","2","Date","2018-04-09","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("446","38","4","2","Digital container","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("447","38","4","2","Handling Staff","fdffd","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("448","38","4","2","Language of the Original Material","1","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("449","38","4","2","Comments","fadfsa","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("450","38","4","2","keywords","fsdfas","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("451","38","4","2","Creator/ Author","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("452","38","4","2","Creator/Editor","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("453","38","4","2","Publisher","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("454","38","4","2","Place of Publication","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("455","38","4","2","Copyright","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("456","38","4","2","Access","Public","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("457","38","4","2","Reference to other documents URL","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("458","38","4","2","Dimensions (in cm)","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("459","38","4","2","Physical Characteristics","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("460","38","4","2","Medium of Original Material","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("461","38","4","2","Extent/No. of Pages","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("462","38","4","2","Place where the original Material is kept","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("463","38","4","2","Creator of the Digital Copy","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("464","38","4","2","Date of digitisation","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("465","38","4","2","Comments on digitisation","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("466","38","4","2","Hardware used","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("467","38","4","2","Software used","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("468","38","4","2","Digital container","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("495","34","2","2","Document ID","lss12","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("496","34","2","2","Type of Document","Proposal ","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("497","34","2","2","Digital file name","lss123","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("498","34","2","2","Date","2018-04-10","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("499","34","2","2","Handling Staff","rtr","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("500","34","2","2","Language of the Original Material","1","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("501","34","2","2","Comments","ftyu","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("502","34","2","2","keywords","678","","General Information","","define","0","");
INSERT INTO `dms_metadata` VALUES("503","34","2","2","Creator/ Author","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("504","34","2","2","Creator/Editor","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("505","34","2","2","Publisher","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("506","34","2","2","Place of Publication","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("507","34","2","2","Copyright","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("508","34","2","2","Access","Public","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("509","34","2","2","Reference to other documents URL","","","Author, Publisher, Access","","define","0","");
INSERT INTO `dms_metadata` VALUES("510","34","2","2","Dimensions (in cm)","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("511","34","2","2","Physical Characteristics","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("512","34","2","2","Medium of Original Material","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("513","34","2","2","Extent/No. of Pages","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("514","34","2","2","Place where the original Material is kept","","","Physical Characteristics","","define","0","");
INSERT INTO `dms_metadata` VALUES("515","34","2","2","Creator of the Digital Copy","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("516","34","2","2","Date of digitisation","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("517","34","2","2","Comments on digitisation","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("518","34","2","2","Hardware used","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("519","34","2","2","Software used","","","Digitisation Details","","define","0","");
INSERT INTO `dms_metadata` VALUES("520","34","2","2","Digital container","","","Digitisation Details","","define","0","");


DROP TABLE IF EXISTS dms_outcome;

CREATE TABLE `dms_outcome` (
  `outcome_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `outcome_name` varchar(250) NOT NULL,
  PRIMARY KEY (`outcome_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `dms_outcome` VALUES("1","Film");
INSERT INTO `dms_outcome` VALUES("2","Book");
INSERT INTO `dms_outcome` VALUES("3","Performance");
INSERT INTO `dms_outcome` VALUES("4","opopo");
INSERT INTO `dms_outcome` VALUES("5","out");
INSERT INTO `dms_outcome` VALUES("6","possssss");
INSERT INTO `dms_outcome` VALUES("7","testing");


DROP TABLE IF EXISTS dms_parent_image;

CREATE TABLE `dms_parent_image` (
  `image_id` bigint(11) NOT NULL AUTO_INCREMENT,
  `parent_temp_id` bigint(11) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `image_type` varchar(250) NOT NULL,
  `metadata_individual_number` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO `dms_parent_image` VALUES("21","7","33065281170.jpg","scan","1");
INSERT INTO `dms_parent_image` VALUES("22","7","33268412-kids-birthday-wishes.jpg","scan","1");
INSERT INTO `dms_parent_image` VALUES("23","7","78112eau3.png","detail","1");
INSERT INTO `dms_parent_image` VALUES("24","7","29833hapi birthday.gif","detail","1");
INSERT INTO `dms_parent_image` VALUES("25","13","49263dms3.jpg","scan","");
INSERT INTO `dms_parent_image` VALUES("26","15","26925m1.jpg","scan","");


DROP TABLE IF EXISTS dms_parent_metadata;

CREATE TABLE `dms_parent_metadata` (
  `parent_metadata_id` bigint(250) NOT NULL AUTO_INCREMENT,
  `parent_temp_id` bigint(250) NOT NULL,
  `parent_document` varchar(250) DEFAULT NULL,
  `parent_handling_staff` varchar(250) DEFAULT NULL,
  `parent_creator` varchar(250) DEFAULT NULL,
  `parent_copy_right` varchar(250) DEFAULT NULL,
  `parent_metadata_status` varchar(250) DEFAULT NULL,
  `document_temp_id` bigint(11) NOT NULL,
  PRIMARY KEY (`parent_metadata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS dms_parent_temp;

CREATE TABLE `dms_parent_temp` (
  `parent_temp_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grant_temp_id` bigint(20) NOT NULL,
  `parent_description` longtext,
  `parent_keyword` varchar(250) DEFAULT NULL,
  `parent_scann_copy` varchar(250) DEFAULT NULL,
  `parent_additional_detail` varchar(250) DEFAULT NULL,
  `parent_comment` varchar(250) DEFAULT NULL,
  `parent_status` varchar(250) DEFAULT NULL,
  `parent_approval` varchar(250) DEFAULT NULL,
  `terminate_status` varchar(250) DEFAULT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`parent_temp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

INSERT INTO `dms_parent_temp` VALUES("12","34","0","","","","0","","","","1","3");
INSERT INTO `dms_parent_temp` VALUES("13","34","0","","","","0","","","","46","3");
INSERT INTO `dms_parent_temp` VALUES("14","35","0","","","","0","","","","1","3");
INSERT INTO `dms_parent_temp` VALUES("15","35","0","","","","0","","","","46","3");
INSERT INTO `dms_parent_temp` VALUES("16","38","0","","","","0","","","","1","3");
INSERT INTO `dms_parent_temp` VALUES("17","38","0","","","","0","","","","46","3");
INSERT INTO `dms_parent_temp` VALUES("18","39","0","","","","0","","","","1","3");


DROP TABLE IF EXISTS dms_role;

CREATE TABLE `dms_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(250) NOT NULL,
  `role_description` varchar(250) DEFAULT NULL,
  `role_status` varchar(250) NOT NULL,
  `role_permission` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

INSERT INTO `dms_role` VALUES("1","Super admin ","Permission to manage the entire DMS","active","view_grant,add_grant,edit_grant,view_approval,add_approval,edit_approval,view_comment,add_comment,edit_comment,view_programme,add_programme,edit_programme,view_role,add_role,edit_role,view_initiative,add_initiative,edit_initiative,view_document,add_d");
INSERT INTO `dms_role` VALUES("2","Public","permission to view only public content","active","view_grant,add_grant,edit_grant,view_approval,add_approval,edit_approval,view_comment,add_comment,edit_comment,view_programme,add_programme,edit_programme,view_role,add_role,edit_role,view_initiative,add_initiative,edit_initiative,view_document,add_d");
INSERT INTO `dms_role` VALUES("3","Admin","Permission to manage the entire DMS, manageable by Super admin","active","view_grant,view_comment,add_csv");
INSERT INTO `dms_role` VALUES("4","Archivist","Permission to create the archive and can able to edit his own archive alone","active","dashboard,grant,view_grant,edit_grant,add_comment");
INSERT INTO `dms_role` VALUES("5","PE","Permission to create Grant ","active","view_grant,add_grant,edit_grant,view_approval,add_approval,edit_approval,view_comment,add_comment,edit_comment,view_csv,add_csv,edit_csv");
INSERT INTO `dms_role` VALUES("6","ED","Permission to edit, comment approve & disapprove the Grant ","active","view_approval,add_approval,edit_approval,view_comment,add_comment");
INSERT INTO `dms_role` VALUES("7","MS","Permission to edit, comment approve & disapprove the Grant","active","view_approval,add_approval,edit_approval,view_comment,add_comment");
INSERT INTO `dms_role` VALUES("8","RMO","Permission to view ","active","dashboard");
INSERT INTO `dms_role` VALUES("9","Communications","Permission To View","active","dashboard");
INSERT INTO `dms_role` VALUES("10","Proof Checker ","Edit rights for all grants","active","dashboard");
INSERT INTO `dms_role` VALUES("11","Other","","active","dashboard");


DROP TABLE IF EXISTS dms_skip_temp;

CREATE TABLE `dms_skip_temp` (
  `skip_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grant_temp_id` bigint(20) NOT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  `skip_status` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`skip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

INSERT INTO `dms_skip_temp` VALUES("18","35","1","");


DROP TABLE IF EXISTS dms_state;

CREATE TABLE `dms_state` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(250) NOT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

INSERT INTO `dms_state` VALUES("1","Andaman and Nicobar Islands");
INSERT INTO `dms_state` VALUES("2","Andhra Pradesh");
INSERT INTO `dms_state` VALUES("3","Arunachal Pradesh");
INSERT INTO `dms_state` VALUES("4","Assam");
INSERT INTO `dms_state` VALUES("5","Bihar");
INSERT INTO `dms_state` VALUES("6","Chandigarh");
INSERT INTO `dms_state` VALUES("7","Chhattisgarh");
INSERT INTO `dms_state` VALUES("8","Dadra and Nagar Haveli");
INSERT INTO `dms_state` VALUES("9","Daman and Diu");
INSERT INTO `dms_state` VALUES("10","Delhi");
INSERT INTO `dms_state` VALUES("11","Goa");
INSERT INTO `dms_state` VALUES("12","Gujarat");
INSERT INTO `dms_state` VALUES("13","Haryana");
INSERT INTO `dms_state` VALUES("14","Himachal Pradesh");
INSERT INTO `dms_state` VALUES("15","Jammu and Kashmir");
INSERT INTO `dms_state` VALUES("16","Jharkhand");
INSERT INTO `dms_state` VALUES("17","Karnataka");
INSERT INTO `dms_state` VALUES("18","Kerala");
INSERT INTO `dms_state` VALUES("19","Lakshadweep");
INSERT INTO `dms_state` VALUES("20","Madhya Pradesh");
INSERT INTO `dms_state` VALUES("21","Maharashtra");
INSERT INTO `dms_state` VALUES("22","Manipur");
INSERT INTO `dms_state` VALUES("23","Meghalaya");
INSERT INTO `dms_state` VALUES("24","Mizoram");
INSERT INTO `dms_state` VALUES("25","Nagaland");
INSERT INTO `dms_state` VALUES("26","Odisha");
INSERT INTO `dms_state` VALUES("27","Puducherry");
INSERT INTO `dms_state` VALUES("28","Punjab");
INSERT INTO `dms_state` VALUES("29","Rajasthan");
INSERT INTO `dms_state` VALUES("30","Sikkim");
INSERT INTO `dms_state` VALUES("31","Tamil Nadu");
INSERT INTO `dms_state` VALUES("32","Telangana");
INSERT INTO `dms_state` VALUES("33","Tripura");
INSERT INTO `dms_state` VALUES("34","Uttar Pradesh");
INSERT INTO `dms_state` VALUES("35","Uttarakhand");
INSERT INTO `dms_state` VALUES("36","West Bengal");
INSERT INTO `dms_state` VALUES("37","siple");
INSERT INTO `dms_state` VALUES("38","noooooooooooo");
INSERT INTO `dms_state` VALUES("39","testing");


DROP TABLE IF EXISTS dms_temp_approve;

CREATE TABLE `dms_temp_approve` (
  `temp_approve_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_temp_id` bigint(20) NOT NULL,
  `grant_temp_id` bigint(20) NOT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `temp_approve_status` varchar(250) NOT NULL,
  `temp_approve_comment` varchar(250) DEFAULT NULL,
  `temp_approve_step` bigint(20) NOT NULL,
  `approval_sender` bigint(20) DEFAULT NULL,
  `notification` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`temp_approve_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS dms_template_type;

CREATE TABLE `dms_template_type` (
  `template_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_type_name` varchar(250) NOT NULL,
  `template_type_states` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`template_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `dms_template_type` VALUES("1","Grant Template","");
INSERT INTO `dms_template_type` VALUES("2","Parent Template","");
INSERT INTO `dms_template_type` VALUES("3","Child  Template","");
INSERT INTO `dms_template_type` VALUES("4","Individual Template","");
INSERT INTO `dms_template_type` VALUES("5","Parent Template for Child","");
INSERT INTO `dms_template_type` VALUES("6","child template for individual","");
INSERT INTO `dms_template_type` VALUES("7","Child Template for multiple record","");
INSERT INTO `dms_template_type` VALUES("8","Parent Template for multiple record","");


DROP TABLE IF EXISTS dms_user;

CREATE TABLE `dms_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_password` varchar(250) NOT NULL,
  `user_dob` date DEFAULT NULL,
  `user_contact` bigint(20) DEFAULT NULL,
  `user_address` varchar(250) DEFAULT NULL,
  `user_type` varchar(250) DEFAULT NULL,
  `user_role` varchar(250) DEFAULT NULL,
  `user_status` varchar(250) DEFAULT NULL,
  `user_image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO `dms_user` VALUES("1","Admin","demo","demo123","2017-08-01","52211111","lko","admin","1","active","");
INSERT INTO `dms_user` VALUES("2","USER1","user1@gmail.com","123","2007-01-01","1234561235","lko","","6","active","");
INSERT INTO `dms_user` VALUES("3","pe","pe@gmail.com","123","2017-08-01","1234567890","lko","","5","active","17510dms2.jpg");
INSERT INTO `dms_user` VALUES("4","user2","ms@gmail.com","123","2017-08-01","1234567890","lko","","7","active","");
INSERT INTO `dms_user` VALUES("5","Archivist","ar@gmail.com","123","2007-01-01","1111111111","123456","","4","active","");


DROP TABLE IF EXISTS dms_user_comment;

CREATE TABLE `dms_user_comment` (
  `user_comment_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `grant_temp_id` bigint(20) NOT NULL,
  `document_temp_id` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `user_comment` varchar(250) NOT NULL,
  PRIMARY KEY (`user_comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS dms_workflow;

CREATE TABLE `dms_workflow` (
  `workflow_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assin_temp_id` bigint(20) NOT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `workflow_status` varchar(250) DEFAULT NULL,
  `step` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`workflow_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

INSERT INTO `dms_workflow` VALUES("25","9","6","2","","1");
INSERT INTO `dms_workflow` VALUES("26","9","7","4","","2");
INSERT INTO `dms_workflow` VALUES("61","1","6","2","","1");
INSERT INTO `dms_workflow` VALUES("62","1","7","4","","2");
INSERT INTO `dms_workflow` VALUES("67","8","6","2","","1");
INSERT INTO `dms_workflow` VALUES("68","8","7","4","","2");


